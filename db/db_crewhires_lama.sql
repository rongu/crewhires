-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Bulan Mei 2021 pada 16.06
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crewhires`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agency_info`
--

CREATE TABLE `agency_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `companyname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_agency` enum('AGENCY','SHIP OWNER') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic_phone_code` int(4) DEFAULT NULL,
  `fb` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tg` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portofolio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siup` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siuppak` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agency_info`
--

INSERT INTO `agency_info` (`id`, `users_id`, `companyname`, `type_agency`, `address`, `city`, `country`, `pic`, `pic_phone`, `pic_phone_code`, `fb`, `ig`, `tg`, `tw`, `wa`, `portofolio`, `document`, `siup`, `siuppak`, `created_at`, `updated_at`) VALUES
(1, 39, 'PT laut Biru', 'AGENCY', 'Jl Sempurna jaya', 'townroy', 'Mauritania', 'Hura', '0852111111', 506, 'a', 'b', 'c', 'd', 'e', 'port_39_1619669373.jpg', 'doc_39_1619669373.jpg', 'siup_39_1619669373.png', 'siuppak_39_1619669373.jpg', '2021-04-29 04:09:33', '2021-04-29 04:09:33'),
(2, 41, 'PT Dermaga Dunia', 'AGENCY', 'Jl pahlawan dunia', 'Ferita', 'Cyprus', 'Uyahhh', '06585521255', 355, NULL, NULL, NULL, NULL, NULL, 'port_41_1619669746.png', 'doc_41_1619669746.jpg', 'siup_41_1619669746.jpg', 'siuppak_41_1619669746.png', '2021-04-29 04:15:46', '2021-04-29 04:15:46'),
(3, 42, 'PT ARISNAM', 'SHIP OWNER', 'JL juanda Raya', 'Besta', 'Gambia', 'Jeku', '02521252225', 213, NULL, NULL, NULL, NULL, NULL, 'port_42_1619793202.png', 'doc_42_1619793202.jpg', NULL, NULL, '2021-04-30 14:33:22', '2021-04-30 14:33:22'),
(4, 44, 'PT kejora Timur', 'AGENCY', 'JL bangka', 'Jakarta', 'Indonesia', 'Iwan Banaran', '085211111', 213, NULL, NULL, NULL, NULL, NULL, 'port_44_1619880354.jpg', 'doc_44_1619880354.jpg', 'siup_44_1619880354.jpg', NULL, '2021-05-01 14:45:54', '2021-05-01 14:45:54'),
(5, 45, 'SHIPOWNER PESDRAD', 'SHIP OWNER', 'Jl bandung raya nomor 5566', 'bandung', 'Ghana', 'Jiuki Vardks', '085545554', 253, NULL, NULL, NULL, NULL, NULL, 'port_45_1619880839.jpg', 'doc_45_1619880839.jpg', 'siup_45_1619880839.jpg', NULL, '2021-05-01 14:53:59', '2021-05-01 14:53:59'),
(6, 46, 'SHIPOWNER GERIA', 'SHIP OWNER', 'Jl Canjur Raya Nomor 6569', 'Cimahai', 'Ghana', 'Keow nue', '0855545555', 57, NULL, NULL, NULL, NULL, NULL, 'port_46_1619882023.jpg', 'doc_46_1619882023.jpg', 'siup_46_1619882023.jpg', NULL, '2021-05-01 15:13:43', '2021-05-01 15:13:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agency_invite`
--

CREATE TABLE `agency_invite` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `crew_id` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `status_invitation` enum('ADD','APPROVE','DELETE','REJECT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADD',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agency_invite`
--

INSERT INTO `agency_invite` (`id`, `users_id`, `crew_id`, `date_add`, `status_invitation`, `created_at`, `updated_at`) VALUES
(1, 41, 37, '2021-04-29', 'APPROVE', '2021-04-29 04:16:02', '2021-04-29 04:16:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agency_jobs`
--

CREATE TABLE `agency_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `engine` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirement` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_open` date DEFAULT NULL,
  `date_close` date DEFAULT NULL,
  `status` enum('open','close') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agency_jobs`
--

INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(1, 39, 'STAFF TEKNIK & PRODUKSI', 'Bn', 'Kxxx', '<div class=\"FYwKg _2MJ7O_0 _36UVG_0\" data-automation=\"jobDetailsHeader\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><div class=\"FYwKg _2Bz3E _1aq_D_0 _1lyEa BDgXc_0 _3ucB1_0 _28vkp_0\" style=\"margin: 0px 0px 0px -12px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-direction: row;\"><div class=\"FYwKg _3VCZm _1uk_1 _3Ve9Z _2li9R\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 530.656px; min-width: 0px; flex: 0 0 66.6667%;\"><div class=\"FYwKg _1GAuD zoxBO_0 JNERa_0 hLznC_0 _34Q9p_0 _2Bz3E rNAgI _1tfFt\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 302.328px; justify-content: flex-start;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _2Cp5K _3ftyQ _1lyEa\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative; display: flex; flex-direction: column;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _17IyL_0 sDUog_0 _1yTEl_0\" style=\"margin: 0px; padding: 0px 20px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"detailsTitle\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r UJoTY_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h1 class=\"FYwKg C6ZIU_0 _3nVJR_0 _642YY_0 _2DNlq_0 _2k6I7_0\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">STAFF TEKNIK &amp; PRODUKSI</h1></div><div class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _2CELK_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">PT Unichem Candi Indonesia</span></div></div></div></div></div></div></div></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _2Cp5K _3ftyQ _1lyEa _1xsO__0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative; display: flex; flex-direction: row; align-items: center;\"><div class=\"FYwKg _3VCZm _3qNSL_0 bMBHP_0 sDUog_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 32px 20px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 518.656px;\"><div class=\"FYwKg d7v3r _3122U_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Gresik</span></div></div><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">IDR&nbsp;4.300.000 - IDR&nbsp;4.500.000</span></div><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Ditayangkan pada 27-Apr-21</span></div></div></div></div></div></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3Ve9Z f3ti9\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 265.328px; min-width: 0px; flex: 0 0 33.3333%;\"><div class=\"FYwKg _1GAuD zoxBO_0 JNERa_0 hLznC_0 _34Q9p_0 _2Bz3E rNAgI _1tfFt\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 302.328px; justify-content: flex-start;\"></div></div></div></div><div class=\"FYwKg _2MJ7O_0 _3gJU3_0 _1yPon_0 _36Yi4_0 _1WtCj_0 FLByR_0 _2QIfI_0\" style=\"margin: 0px; padding: 48px 24px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">S1 TEKNIK INDUSTRI / TEKNIK MESIN / TEKNIK ELEKTRO</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">FRESH GRADUATED / PENGALAMAN LEBIH DIUTAMAKAN</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">MAHIR MS. OFFICE, TERUTAMA MS. EXCEL, MS. WORD, MS. POWER POIN</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">DIUTAMAKAN PAHAM ISO 9001 MANAJEMEN MUTU, FSSC, 5R DAN K3</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">DOMISILI GRESIK DAN SEKITARNYA LEBIH DIUTAMAKAN</li></ul></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tidak terspesifikasi</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><a href=\"https://www.jobstreet.co.id/id/job-search/engineering-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Teknik\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Teknik</a>,&nbsp;<a href=\"https://www.jobstreet.co.id/id/job-search/electrical-engineering-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Teknik Elektro\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Teknik Elektro</a></span></div></div></div></div></div></div></div></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3VCZm _36Yi4_0 _1WtCj_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 48px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px;\"><div class=\"FYwKg _2Cp5K\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative;\"><div class=\"FYwKg _3VCZm _3ra6Y _1vY2t_0 TfmPI_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px; position: absolute; height: 1px; background: rgb(216, 220, 226);\"></div></div></div><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Tentang Perusahaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: 700;\">P.T. Unichem Candi Industri</span>&nbsp;is a leader in producing special minerals and chemicals for oil &amp; gas industry, mining and other related services to various industries.</div><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px; text-align: justify;\">Our mission is to continually provide these and other services by promoting a reputation for excellence and value while fully anticipating, then recognizing the needs and expectations of our customers.</div><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px; text-align: justify;\">Our market focus is directed towards providing unequaled service to our customers, co-workers and investors through recognition of the processes that are of vital importance to them and the goals, which much be established and achieved to continually progress toward total reliability and satisfaction.</div></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan Perusahaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Ukuran Perusahaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">51 - 200 pekerja</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Waktu Proses Lamaran</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">29 hari</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Industri</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Produk Konsumen/Barang konsumen yang bergerak cepat</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tunjangan dan Lain-lain</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Asuransi kesehatan,&nbsp;Parkir,&nbsp;Formil (contoh: Kemeja + Dasi),&nbsp;Monday - Saturday</span></div></div></div></div></div></div></div></div></div></div></div></div></div>', '2021-04-01', '2021-04-30', 'open', '2021-04-29 04:09:33', '2021-04-29 04:09:33');
INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(2, 41, 'Warehouse Supervisor', 'Kl', 'uyss', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\"><span style=\"font-weight: 700;\">Why Join Us?</span><span style=\"font-weight: 700;\">&nbsp;</span>Shipper is on a mission to invent a new standard for online shipping in Indonesia. We have assembled the largest asset-light delivery network to make shipping simple, reliable, and cost competitive. With over 17,000 islands, 2,500 logistics providers, and a fast growing e-commerce market, Indonesia is one of the most challenging markets to build a great online shipping experience and we are primed to be a part of the solution. We are privileged to have some of the best global investors including Lightspeed, Y Combinator, Sequoia Capital India, DST Global Partners. We are looking for passionate and outstanding individuals to join our team.<br><span style=\"font-weight: 700;\">What You Will Do</span><br><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Maintaining inbound and outbound process in the warehouse</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Organizing, coordinating, and supervising all subordinate task to fit with the company\'s SOP</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Managing all resource for warehouse &amp; transportation activity including people, vehicle, and equipment</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Improving activity in the warehouse, include for forecasting</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Making reports and analysis of warehouse performance</li></ul><br><span style=\"font-weight: 700;\">What Are the Requirements</span><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Diploma degree of any major</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Having experience in Logistic/Supply Chain/Warehouse at least 2 year</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Understanding inventory management and method</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Clear understanding of logistic &amp; warehouse</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Having excellent skill (s) in communication, leadership, analytical thinking &amp; problem solving</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Candidate who can join as soon as possible will be prioritized</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Willing to be placed in any warehouse location</li></ul></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Supervisor/Koordinator</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Sertifikat Professional, D3 (Diploma), D4 (Diploma), Sarjana (S1)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">2 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Pelayanan</span></font>,&nbsp;<font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Logistik/Rantai Pasokan</span></font></span></div></div></div></div></div></div></div></div></div>', '2021-01-01', '2021-06-30', 'open', '2021-04-29 04:15:47', '2021-04-29 04:15:47'),
(3, 41, 'Customer Service', 'Znnnm', 'Klllsk', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"job-details-job-highlights\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Keuntungan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul class=\"FYwKg _302h6 d7v3r UJoTY_0\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; list-style: none;\"><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Lingkungan kerja yang baik. Prospek untuk maju dan berkembang</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">mendapatkan training dan pengetahuan</span></div></div></li></ul></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\">Persyaratan :<ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Kandidat minimal lulusan SLTA/sederajat</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kesehatan yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kemampuan komunikasi verbal</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki passion di bidang customer service</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kemampuan problem solving yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Berpenampilan rapi</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Bisa berbahasa Inggris minimal pasif</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki pengalaman sebagai customer service minimal 1 tahun (fresh graduate silahkan boleh melamar)</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Ramah dan senang melayani customer</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Maksimal berusia 24 tahun, memiliki berat dan tinggi yang ideal</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Memiliki attitude yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Dapat mengoperasikan komputer.</li></ul></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tidak terspesifikasi</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">1 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><a href=\"https://www.jobstreet.co.id/id/job-search/services-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Pelayanan\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Pelayanan</a>,&nbsp;<a href=\"https://www.jobstreet.co.id/id/job-search/customer-service-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Layanan Pelanggan\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Layanan Pelanggan</a></span></div></div></div></div></div></div></div></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3VCZm _36Yi4_0 _1WtCj_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 48px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px;\"><div class=\"FYwKg _2Cp5K\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative;\"></div></div></div>', '2021-04-01', '2021-06-05', 'open', '2021-04-29 04:15:47', '2021-04-29 04:15:47');
INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(4, 42, 'Export Customer Service & Documentation Semarang Office', 'KLOA', 'JKLKI', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"job-details-job-highlights\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Keuntungan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul class=\"FYwKg _302h6 d7v3r UJoTY_0\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; list-style: none;\"><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Top 10 NVOCC from Asia to USA</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Reliable Partner to all Carriers</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Convenience Working Place Atmosphere</span></div></div></li></ul></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><p style=\"padding-top: 12px; padding-bottom: 12px; margin-bottom: 0px;\">Job Description:</p><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Export administration and customer service</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Follow SOP in performance of all operation processes</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Co-ordinate with overseas offices, partner agents and customers for timely shipment</li></ul><p style=\"padding-top: 12px; padding-bottom: 12px; margin-bottom: 0px;\">Job requirement:</p><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Candidate must possess at least Bachelor\'s Degree in Business Studies/Administration/Management, Logistic/Transportation or equivalent.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Required language(s):&nbsp;English, Bahasa Indonesia</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Must have at least 2&nbsp;Year(s) of working experience in international freight forwarding is required for this position.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Required Skill(s): Microsoft Office, Forwarding Application</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Preferably Staff (non-management &amp; non-supervisor) specialized in Customer Service or equivalent.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Having a good communication skill and capable in English communication</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Good computer skill</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Able to work under pressure and deadline</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Responsible and can work proactively&nbsp;and can work in team</li></ul></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Sarjana (S1)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">2 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Pelayanan</span></font>,&nbsp;<font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Logistik/Rantai Pasokan</span></font></span></div></div></div></div></div></div></div></div></div>', '2021-04-01', '2021-08-01', 'open', '2021-04-30 14:33:22', '2021-04-30 14:33:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `coc`
--

CREATE TABLE `coc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','deck department','engine department','ratings deck','ratings engine','cook engine') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `coc`
--

INSERT INTO `coc` (`id`, `name`, `types`, `created_at`, `updated_at`) VALUES
(1, 'None', 'none', '2021-02-28 06:37:59', '2021-02-28 06:37:59'),
(2, 'COC  Deck Officer Class I', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(3, 'COC  Deck Officer Class II', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(4, 'COC  Deck Officer Class III', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(5, 'COC  Deck Officer Class IV', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(6, 'COC  Deck Officer Class V', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(7, 'COC Engineer Officer Class I', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(8, 'COC Engineer Officer Class II', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(9, 'COC Engineer Officer Class III', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(10, 'COC Engineer Officer Class IV', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(11, 'COC Engineer Officer Class V', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(12, 'Ratings As Able Deck Seaferer', 'ratings deck', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(13, 'Ratings As Able Engine Seaferer', 'ratings engine', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(14, 'Ship\'s Cook MLC 2006', 'cook engine', '2021-02-27 23:25:05', '2021-02-27 23:25:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cop`
--

CREATE TABLE `cop` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','general','deck department','engine department') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cop`
--

INSERT INTO `cop` (`id`, `name`, `codes`, `types`, `created_at`, `updated_at`) VALUES
(1, 'None', '', 'none', '2021-02-28 05:51:19', '2021-02-28 05:51:26'),
(2, 'Basic Safety Training', 'BST', 'general', '2021-02-28 05:51:24', '2021-02-28 05:51:29'),
(3, 'Proficiency Survival Craft and Rescue Boat', 'SCRB', 'general', '2021-02-28 05:51:36', '2021-02-28 05:51:36'),
(4, 'Advanced Fire Fighting', 'AFF', 'general', '2021-02-28 05:52:05', '2021-02-28 05:52:05'),
(5, 'Medical First Aid', 'MFA', 'general', '2021-02-28 05:52:30', '2021-02-28 05:52:30'),
(6, 'Medical Care Onboard Ship', 'MC', 'general', '2021-02-28 05:52:58', '2021-02-28 05:52:58'),
(7, 'Security Awareness Training', 'SAT', 'general', '2021-02-28 05:53:22', '2021-02-28 05:53:22'),
(8, 'Seaferer With Designated Security Duties', 'SDSD', 'general', '2021-02-28 05:53:54', '2021-02-28 05:53:54'),
(9, 'Ship Security Officer', 'SSO', 'general', '2021-02-28 05:54:21', '2021-02-28 05:54:21'),
(10, 'Bridge Resource Management', 'BRM', 'deck department', '2021-02-28 05:54:55', '2021-02-28 05:54:55'),
(11, 'ARPA Simulator', '', 'deck department', '2021-02-28 05:55:21', '2021-02-28 05:55:21'),
(12, 'RADAR Simulator', '', 'deck department', '2021-02-28 05:55:42', '2021-02-28 05:55:42'),
(13, 'Electronic Chart Display', 'ECDIS', 'deck department', '2021-02-28 05:56:08', '2021-02-28 05:56:08'),
(14, 'Global Maritime Distress Safety System', 'GMDSS', 'deck department', '2021-02-28 05:56:55', '2021-02-28 05:56:55'),
(15, 'General Operator Certificate', 'GOC Book', 'deck department', '2021-02-28 05:57:20', '2021-02-28 05:57:20'),
(16, 'Basic Training for Liquified Gas Tanker Cargo Operations', 'BLGT', 'general', '2021-02-28 05:58:07', '2021-02-28 05:58:07'),
(17, 'Advanced Training for Liquified Gas Tanker Cargo Operations', 'ALGT', 'general', '2021-02-28 05:59:17', '2021-02-28 05:59:17'),
(18, 'Basic Training for Oil and Chemical Tanker Cargo Operations', 'BOCT', 'general', '2021-02-28 06:00:16', '2021-02-28 06:00:16'),
(19, 'DP Basic', '', 'deck department', '2021-02-28 06:01:07', '2021-02-28 06:01:07'),
(20, 'DP Advanced', '', 'deck department', '2021-02-28 06:01:27', '2021-02-28 06:01:27'),
(21, 'Full DP', '', 'deck department', '2021-02-28 06:01:50', '2021-02-28 06:01:50'),
(22, 'DP Maintenance', '', 'engine department', '2021-02-28 06:02:05', '2021-02-28 06:02:05'),
(23, 'Engine Resources Management', 'ERM', 'engine department', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(24, 'Marine High Voltage', 'MHV', 'deck department', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(25, 'Crowd Management', 'CM', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(26, 'Food Handling', '', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(27, 'Food Safety', '', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(28, 'Helicopter Under Escape Training', 'HUET', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(29, 'Basic Offshore Safety Induction & Emergency Training', 'BOSIET', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39'),
(30, 'International Safety Management Code', 'ISM Code', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `country`
--

CREATE TABLE `country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `country`
--

INSERT INTO `country` (`id`, `name`, `codes`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'AF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(2, 'Albania', 'AL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(3, 'Algeria', 'DZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(4, 'American Samoa', 'AS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(5, 'Andorra', 'AD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(6, 'Angola', 'AO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(7, 'Anguilla', 'AI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(8, 'Argentina', 'AR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(9, 'Armenia', 'AM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(10, 'Aruba', 'AW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(11, 'Australia', 'AU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(12, 'Austria', 'AT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(13, 'Azerbaijan', 'AZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(14, 'Bahamas', 'BS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(15, 'Bahrain', 'BH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(16, 'Bangladesh', 'BD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(17, 'Barbados', 'BB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(18, 'Belarus', 'BY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(19, 'Belgium', 'BE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(20, 'Belize', 'BZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(21, 'Benin', 'BJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(22, 'Bermuda', 'BM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(23, 'Bhutan', 'BT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(24, 'Bolivia', 'BO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(25, 'Bosnia and Herzegowina', 'BA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(26, 'Botswana', 'BW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(27, 'Bouvet Island', 'BV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(28, 'Brazil', 'BR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(29, 'British Indian Ocean Territory', 'IO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(30, 'Brunei Darussalam', 'BN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(31, 'Bulgaria', 'BG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(32, 'Burkina Faso', 'BF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(33, 'Burundi', 'BI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(34, 'Cambodia', 'KH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(35, 'Cameroon', 'CM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(36, 'Canada', 'CA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(37, 'Cape Verde', 'CV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(38, 'Cayman Islands', 'KY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(39, 'Central African Republic', 'CF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(40, 'Chad', 'TD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(41, 'Chile', 'CL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(42, 'China', 'CN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(43, 'Christmas Island', 'CX', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(44, 'Cocos (Keeling) Islands', 'CC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(45, 'Colombia', 'CO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(46, 'Comoros', 'KM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(47, 'Congo', 'CG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(48, 'Congo, the Democratic Republic of the', 'CD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(49, 'Cook Islands', 'CK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(50, 'Costa Rica', 'CR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(51, 'Cote d\'Ivoire', 'CI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(52, 'Croatia', 'HR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(53, 'Cuba', 'CU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(54, 'Cyprus', 'CY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(55, 'Czech Republic', 'CZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(56, 'Denmark', 'DK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(57, 'Djibouti', 'DJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(58, 'Dominica', 'DM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(59, 'Dominican Republic', 'DO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(60, 'Ecuador', 'EC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(61, 'Egypt', 'EG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(62, 'El Salvador', 'SV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(63, 'Equatorial Guinea', 'GQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(64, 'Eritrea', 'ER', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(65, 'Estonia', 'EE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(66, 'Ethiopia', 'ET', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(67, 'Falkland Islands (Malvinas)', 'FK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(68, 'Faroe Islands', 'FO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(69, 'Fiji', 'FJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(70, 'Finland', 'FI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(71, 'France', 'FR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(72, 'French Guiana', 'GF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(73, 'French Polynesia', 'PF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(74, 'French Southern Territories', 'TF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(75, 'Gabon', 'GA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(76, 'Gambia', 'GM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(77, 'Georgia', 'GE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(78, 'Germany', 'DE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(79, 'Ghana', 'GH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(80, 'Gibraltar', 'GI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(81, 'Greece', 'GR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(82, 'Greenland', 'GL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(83, 'Grenada', 'GD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(84, 'Guadeloupe', 'GP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(85, 'Guam', 'GU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(86, 'Guatemala', 'GT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(87, 'Guinea', 'GN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(88, 'Guinea-Bissau', 'GW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(89, 'Guyana', 'GY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(90, 'Haiti', 'HT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(91, 'Heard and Mc Donald Islands', 'HM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(92, 'Holy See (Vatican City State)', 'VA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(93, 'Honduras', 'HN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(94, 'Hong Kong', 'HK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(95, 'Hungary', 'HU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(96, 'Iceland', 'IS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(97, 'India', 'IN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(98, 'Indonesia', 'ID', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(99, 'Iran (Islamic Republic of)', 'IR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(100, 'Iraq', 'IQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(101, 'Ireland', 'IE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(102, 'Israel', 'IL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(103, 'Italy', 'IT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(104, 'Jamaica', 'JM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(105, 'Japan', 'JP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(106, 'Jordan', 'JO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(107, 'Kazakhstan', 'KZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(108, 'Kenya', 'KE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(109, 'Kiribati', 'KI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(110, 'Korea, Democratic People\'s Republic of', 'KP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(111, 'Korea, Republic of', 'KR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(112, 'Kuwait', 'KW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(113, 'Kyrgyzstan', 'KG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(114, 'Lao People\'s Democratic Republic', 'LA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(115, 'Latvia', 'LV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(116, 'Lebanon', 'LB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(117, 'Lesotho', 'LS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(118, 'Liberia', 'LR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(119, 'Libyan Arab Jamahiriya', 'LY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(120, 'Liechtenstein', 'LI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(121, 'Lithuania', 'LT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(122, 'Luxembourg', 'LU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(123, 'Macau', 'MO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(124, 'Macedonia, The Former Yugoslav Republic of', 'MK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(125, 'Madagascar', 'MG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(126, 'Malawi', 'MW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(127, 'Malaysia', 'MY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(128, 'Maldives', 'MV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(129, 'Mali', 'ML', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(130, 'Malta', 'MT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(131, 'Marshall Islands', 'MH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(132, 'Martinique', 'MQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(133, 'Mauritania', 'MR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(134, 'Mauritius', 'MU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(135, 'Mayotte', 'YT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(136, 'Mexico', 'MX', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(137, 'Micronesia, Federated States of', 'FM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(138, 'Moldova, Republic of', 'MD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(139, 'Monaco', 'MC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(140, 'Mongolia', 'MN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(141, 'Montserrat', 'MS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(142, 'Morocco', 'MA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(143, 'Mozambique', 'MZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(144, 'Myanmar', 'MM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(145, 'Namibia', 'NA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(146, 'Nauru', 'NR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(147, 'Nepal', 'NP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(148, 'Netherlands', 'NL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(149, 'Netherlands Antilles', 'AN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(150, 'New Caledonia', 'NC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(151, 'New Zealand', 'NZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(152, 'Nicaragua', 'NI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(153, 'Niger', 'NE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(154, 'Nigeria', 'NG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(155, 'Niue', 'NU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(156, 'Norfolk Island', 'NF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(157, 'Northern Mariana Islands', 'MP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(158, 'Norway', 'NO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(159, 'Oman', 'OM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(160, 'Pakistan', 'PK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(161, 'Palau', 'PW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(162, 'Panama', 'PA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(163, 'Papua New Guinea', 'PG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(164, 'Paraguay', 'PY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(165, 'Peru', 'PE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(166, 'Philippines', 'PH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(167, 'Pitcairn', 'PN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(168, 'Poland', 'PL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(169, 'Portugal', 'PT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(170, 'Puerto Rico', 'PR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(171, 'Qatar', 'QA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(172, 'Reunion', 'RE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(173, 'Romania', 'RO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(174, 'Russian Federation', 'RU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(175, 'Rwanda', 'RW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(176, 'Saint Kitts and Nevis', 'KN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(177, 'Saint LUCIA', 'LC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(178, 'Saint Vincent and the Grenadines', 'VC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(179, 'Samoa', 'WS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(180, 'San Marino', 'SM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(181, 'Sao Tome and Principe', 'ST', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(182, 'Saudi Arabia', 'SA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(183, 'Senegal', 'SN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(184, 'Seychelles', 'SC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(185, 'Sierra Leone', 'SL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(186, 'Singapore', 'SG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(187, 'Slovakia (Slovak Republic)', 'SK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(188, 'Slovenia', 'SI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(189, 'Solomon Islands', 'SB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(190, 'Somalia', 'SO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(191, 'South Africa', 'ZA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(192, 'South Georgia and the South Sandwich Islands', 'GS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(193, 'Spain', 'ES', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(194, 'Sri Lanka', 'LK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(195, 'St. Helena', 'SH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(196, 'St. Pierre and Miquelon', 'PM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(197, 'Sudan', 'SD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(198, 'Suriname', 'SR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(199, 'Svalbard and Jan Mayen Islands', 'SJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(200, 'Swaziland', 'SZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(201, 'Sweden', 'SE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(202, 'Switzerland', 'CH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(203, 'Syrian Arab Republic', 'SY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(204, 'Taiwan, Province of China', 'TW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(205, 'Tajikistan', 'TJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(206, 'Tanzania, United Republic of', 'TZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(207, 'Thailand', 'TH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(208, 'Togo', 'TG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(209, 'Tokelau', 'TK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(210, 'Tonga', 'TO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(211, 'Trinidad and Tobago', 'TT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(212, 'Tunisia', 'TN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(213, 'Turkey', 'TR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(214, 'Turkmenistan', 'TM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(215, 'Turks and Caicos Islands', 'TC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(216, 'Tuvalu', 'TV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(217, 'Uganda', 'UG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(218, 'Ukraine', 'UA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(219, 'United Arab Emirates', 'AE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(220, 'United Kingdom', 'GB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(221, 'United States', 'US', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(222, 'United States Minor Outlying Islands', 'UM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(223, 'Uruguay', 'UY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(224, 'Uzbekistan', 'UZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(225, 'Vanuatu', 'VU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(226, 'Venezuela', 'VE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(227, 'Viet Nam', 'VN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(228, 'Virgin Islands (British)', 'VG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(229, 'Virgin Islands (U.S.)', 'VI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(230, 'Wallis and Futuna Islands', 'WF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(231, 'Western Sahara', 'EH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(232, 'Yemen', 'YE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(233, 'Zambia', 'ZM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(234, 'Zimbabwe', 'ZW', '2021-03-02 09:59:28', '2021-03-02 09:59:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crew_apply`
--

CREATE TABLE `crew_apply` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `agency_jobs_id` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `apply_status` enum('ADD','APPROVE','REJECT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADD',
  `apply_by` enum('USER','ADMIN') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `crew_apply`
--

INSERT INTO `crew_apply` (`id`, `users_id`, `agency_jobs_id`, `date_add`, `apply_status`, `apply_by`, `created_at`, `updated_at`) VALUES
(4, 38, 3, '2021-04-29', 'APPROVE', 'ADMIN', '2021-04-29 11:22:12', '2021-04-29 11:22:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crew_doc`
--

CREATE TABLE `crew_doc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cer_doc_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `types` enum('exp','cop','coc','flg','stc') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doi` date DEFAULT NULL,
  `ed` date DEFAULT NULL,
  `rank` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_vessel` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_from` date DEFAULT NULL,
  `p_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `crew_doc`
--

INSERT INTO `crew_doc` (`id`, `users_id`, `name`, `cer_doc_number`, `types`, `place`, `doi`, `ed`, `rank`, `t_vessel`, `company`, `p_from`, `p_to`, `created_at`, `updated_at`) VALUES
(1, 37, 'xxxxxx', NULL, 'exp', NULL, NULL, NULL, 'xxx', 'xxx', 'xxxx', '2020-01-01', '2022-03-01', '2021-04-29 04:01:34', '2021-04-29 04:01:34'),
(2, 37, 'Proficiency Survival Craft and Rescue Boat', 'ssssssss', 'cop', 'sss', '2018-02-01', '2024-10-01', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(3, 37, 'COC  Deck Officer Class I', 'cccccc', 'coc', 'ccc', '2021-01-01', '2021-10-02', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(4, 37, 'zzz', 'zzzz', 'flg', 'zzz', '2021-04-01', '2021-07-09', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(5, 37, 'qqq', 'qqq', 'stc', 'qqq', '2021-03-01', '2022-01-01', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crew_info`
--

CREATE TABLE `crew_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `fullname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_crew` enum('MARINE','CRUISE') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `place_birth` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital` enum('Single','Married','Divorced','Separated','Widowed') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('m','f') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seaferer` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_activity` enum('STANDBY','AVAILABLE','ON BOARD') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tg` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portofolio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `crew_info`
--

INSERT INTO `crew_info` (`id`, `users_id`, `fullname`, `type_crew`, `birth`, `place_birth`, `marital`, `religion`, `gender`, `address`, `city`, `country`, `nationality`, `seaferer`, `status_activity`, `fb`, `ig`, `tg`, `tw`, `wa`, `portofolio`, `created_at`, `updated_at`) VALUES
(1, 37, 'Wawan', 'MARINE', '1990-02-28', 'Cbbr', 'Single', 'Islam', 'm', 'Jln stok land no 30', 'Ziak', 'Scotland', 'Scotland', NULL, 'STANDBY', NULL, NULL, NULL, NULL, NULL, '37_1619668894.png', '2021-04-29 04:01:34', '2021-04-29 04:01:34'),
(2, 38, 'Marni', 'MARINE', '1999-02-10', 'Ciza', 'Single', 'Islam', 'f', 'Jl vrank kx', 'Cvzz', 'Germany', 'Germany', NULL, 'STANDBY', 'fb1', 'ig1', 'tg1', 'tw1', 'wa1', '38_1619669104.png', '2021-04-29 04:05:04', '2021-04-29 04:05:04'),
(3, 43, 'Marsha Ivarzya', 'CRUISE', '1992-06-18', 'Bogor', 'Single', 'Islam', 'f', 'Jln Bangka Raya No. 55 Jakarta', 'Jakarta', 'Fiji Islands', 'Ghana', NULL, 'STANDBY', NULL, NULL, NULL, NULL, NULL, '43_1619877391.jpg', '2021-05-01 13:56:31', '2021-05-01 14:07:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `home_slide`
--

CREATE TABLE `home_slide` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `home_slide`
--

INSERT INTO `home_slide` (`id`, `title`, `images`, `created_at`, `updated_at`) VALUES
(1, 'Gambar Satu', 'gambar1.jpg', '2021-03-28 04:42:15', '2021-03-28 04:42:15'),
(2, 'Gambar Duas', '1_1617000604.jpg', '2021-03-28 04:42:15', '2021-03-29 06:50:04'),
(3, 'Gambar Tiga', 'gambar3.jpg', '2021-03-28 04:42:15', '2021-03-28 04:42:15'),
(4, 'Gambar Empat', 'gambar4.jpg', '2021-03-28 04:42:15', '2021-03-28 04:42:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laravel_logger_activity`
--

CREATE TABLE `laravel_logger_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `route` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ipAddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userAgent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `methodType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `laravel_logger_activity`
--

INSERT INTO `laravel_logger_activity` (`id`, `description`, `details`, `userType`, `userId`, `route`, `ipAddress`, `userAgent`, `locale`, `referer`, `methodType`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/home', 'POST', '2021-05-20 07:36:13', '2021-05-20 07:36:13', NULL),
(2, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'POST', '2021-05-20 07:37:06', '2021-05-20 07:37:06', NULL),
(3, 'Service', '', 'Registered', 39, 'http://localhost/crewhires/service/bunker', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 07:45:29', '2021-05-20 07:45:29', NULL),
(4, 'Service', 'certificaterevalidation', 'Registered', 39, 'http://localhost/crewhires/service/certificaterevalidation', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 07:47:39', '2021-05-20 07:47:39', NULL),
(5, 'Logged In', NULL, 'Registered', 38, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'POST', '2021-05-20 11:16:14', '2021-05-20 11:16:14', NULL),
(6, 'Service', '[SVC162150937415] Request Certificate Revalidation', 'Registered', 38, 'http://localhost/crewhires/service/certificaterevalidation', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'GET', '2021-05-20 11:16:14', '2021-05-20 11:16:14', NULL),
(7, 'Service', '[SVC162150937995] Request Training Center', 'Registered', 38, 'http://localhost/crewhires/service/trainingcenter', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:19', '2021-05-20 11:16:19', NULL),
(8, 'Service', '[SVC162150938266] Request Seaman\'s Book', 'Registered', 38, 'http://localhost/crewhires/service/seamanbook', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:22', '2021-05-20 11:16:22', NULL),
(9, 'Service', '[SVC162150938585] Request Visa', 'Registered', 38, 'http://localhost/crewhires/service/visa', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:25', '2021-05-20 11:16:25', NULL),
(10, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7', 'http://localhost/crewhires/login', 'POST', '2021-05-21 02:14:23', '2021-05-21 02:14:23', NULL),
(11, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7', 'http://localhost/crewhires/login', 'POST', '2021-05-21 13:13:28', '2021-05-21 13:13:28', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `membership`
--

CREATE TABLE `membership` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code_member` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `status` enum('verification','done','cancel','pay') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'verification',
  `date_add` date DEFAULT NULL,
  `photo_receipt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `bank_account` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bau_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_01_15_105324_create_roles_table', 2),
(5, '2016_01_15_114412_create_role_user_table', 2),
(6, '2016_01_26_115212_create_permissions_table', 2),
(7, '2016_01_26_115523_create_permission_role_table', 2),
(8, '2016_02_09_132439_create_permission_user_table', 2),
(9, '2017_11_04_103444_create_laravel_logger_activity_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Can View Users', 'view.users', 'Can view users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(2, 'Can Create Users', 'create.users', 'Can create new users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(3, 'Can Edit Users', 'edit.users', 'Can edit users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(4, 'Can Delete Users', 'delete.users', 'Can delete users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(5, 'Can View Crew', 'view.crew', 'Can view crew', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(6, 'Can View Agency', 'view.agency', 'Can view agency', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(2, 2, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(3, 3, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(4, 4, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(5, 5, 2, '2021-03-09 15:13:13', '2021-03-09 15:13:13', NULL),
(6, 6, 4, '2021-03-10 01:19:09', '2021-03-10 01:19:13', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `religion`
--

CREATE TABLE `religion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `religion`
--

INSERT INTO `religion` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Islam', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(2, 'Christianity', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(3, 'Buddhism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(4, 'Hinduism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(5, 'Konghucu', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(6, 'Chinese Traditional Religion', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(7, 'Ethnic Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(8, 'African Traditional Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(9, 'Sikhism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(10, 'Spiritism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(11, 'Bahaʼi', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(12, 'Jainism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(13, 'Shinto', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(14, 'Cao Dai', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(15, 'Zoroastrianism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(16, 'Tenrikyo', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(17, 'Animism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(18, 'Druze', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(19, 'Neo-Paganism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(20, 'Unitarian Universalism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(21, 'Rastafari', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(22, 'Judaism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(23, 'Agnostic', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(24, 'Unaffiliated Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(25, 'Folk Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(26, 'Atheist', '2021-03-31 06:57:39', '2021-03-31 06:57:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', 'Admin Role', 5, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(2, 'Crew', 'crew', 'Crew Role', 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(3, 'Guest', 'guest', 'Guest Role', 0, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(4, 'Agency', 'agency', 'Agency Role', 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-03-09 11:23:19', '2021-03-09 11:23:19', NULL),
(26, 2, 37, '2021-04-29 03:57:46', '2021-04-29 03:57:46', NULL),
(27, 2, 38, '2021-04-29 04:02:55', '2021-04-29 04:02:55', NULL),
(28, 4, 39, '2021-04-29 04:06:40', '2021-04-29 04:06:40', NULL),
(30, 4, 41, '2021-04-29 04:12:10', '2021-04-29 04:12:10', NULL),
(31, 4, 42, '2021-04-30 14:30:40', '2021-04-30 14:30:40', NULL),
(32, 2, 43, '2021-05-01 13:41:08', '2021-05-01 13:41:08', NULL),
(33, 4, 44, '2021-05-01 14:09:14', '2021-05-01 14:09:14', NULL),
(34, 4, 45, '2021-05-01 14:51:40', '2021-05-01 14:51:40', NULL),
(35, 4, 46, '2021-05-01 15:11:51', '2021-05-01 15:11:51', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code_service` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_id` int(11) NOT NULL,
  `types` enum('visa','passport','seamanbook','certificaterevalidation','trainingcenter','accommodation','payroll','bunker') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('verification','process','done','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'verification',
  `date_add` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`id`, `code_service`, `users_id`, `types`, `status`, `date_add`, `created_at`, `updated_at`) VALUES
(3, 'SVC162149167679', 39, 'visa', 'verification', '2021-05-20', '2021-05-20 06:21:16', '2021-05-20 06:21:16'),
(4, 'SVC162149168596', 39, 'passport', 'verification', '2021-05-20', '2021-05-20 06:21:25', '2021-05-20 06:21:25'),
(5, 'SVC162149627049', 39, 'seamanbook', 'verification', '2021-05-20', '2021-05-20 07:37:50', '2021-05-20 07:37:50'),
(6, 'SVC162149672975', 39, 'bunker', 'verification', '2021-05-20', '2021-05-20 07:45:29', '2021-05-20 07:45:29'),
(7, 'SVC162149685946', 39, 'certificaterevalidation', 'verification', '2021-05-20', '2021-05-20 07:47:39', '2021-05-20 07:47:39'),
(8, 'SVC162150937415', 38, 'certificaterevalidation', 'verification', '2021-05-20', '2021-05-20 11:16:14', '2021-05-20 11:16:14'),
(9, 'SVC162150937995', 38, 'trainingcenter', 'verification', '2021-05-20', '2021-05-20 11:16:19', '2021-05-20 11:16:19'),
(10, 'SVC162150938266', 38, 'seamanbook', 'verification', '2021-05-20', '2021-05-20 11:16:22', '2021-05-20 11:16:22'),
(11, 'SVC162150938585', 38, 'visa', 'verification', '2021-05-20', '2021-05-20 11:16:25', '2021-05-20 11:16:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` int(4) DEFAULT NULL,
  `profile` enum('done','proses','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'closed',
  `photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_account` enum('free','premium') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'free',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_ban` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `phone_code`, `profile`, `photo`, `status_account`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `is_ban`) VALUES
(1, 'administrator', 'admin@mail.com', NULL, NULL, 'closed', '', 'free', '2021-03-18 06:08:45', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', 'L0sKnWuNaMOSwxxZ5EbQccPZRo7w0JOW0kD8SMHuPx3qK1aqv2BORikTHQuK', '2021-02-25 00:39:13', '2021-02-25 00:39:13', '0'),
(37, 'crew1', 'ron1.gusya@gmail.com', '08521111111', 93, 'done', '37_1619668894.jpg', 'free', '2021-04-29 03:58:14', '$2y$10$PrHhmEZsEgfv5.27FLrhN.q/PIKBzNUh1.VQKmMu7.pavPmoYF/SS', NULL, '2021-04-29 03:57:46', '2021-05-21 14:00:58', '0'),
(38, 'crew2', 'crew2@mail.com', '0852123522', 213, 'done', '38_1619669104.png', 'free', '2021-04-29 04:03:15', '$2y$10$HuK0WZkkVRYs9JDupnUNBe294cY3WwtvGb1zKIIjuIci35QmPwqba', NULL, '2021-04-29 04:02:55', '2021-04-29 04:05:04', '0'),
(39, 'agency1', 'agency1@mail.com', '08521111111', 1684, 'done', '39_1619669373.jpg', 'free', '2021-04-29 04:06:57', '$2y$10$4RwkacbLsoe0.izB7qukrOl1dJXEGVaOgdJUGvuJ3TrhDd8DcgPou', NULL, '2021-04-29 04:06:40', '2021-04-29 04:09:33', '0'),
(41, 'agency2', 'agency2@mail.com', '085444111111', 257, 'done', '41_1619669746.jpg', 'free', '2021-04-29 04:12:23', '$2y$10$Vkku1inuARDyos8YkUdFter4mZH//2/f6DQAffpRAl7Wh1tNWnIkm', NULL, '2021-04-29 04:12:10', '2021-04-29 04:15:47', '0'),
(42, 'ShipOwner1', 'shipowner1@gmail.com', '085211112111', 213, 'done', '42_1619793202.jpg', 'free', '2021-04-30 14:31:05', '$2y$10$9wI3g8bWQhwYKLfNaB2FguHILdmlYvG/Adhtf37CsqKAbumgGvX5W', NULL, '2021-04-30 14:30:40', '2021-05-01 13:35:06', '0'),
(43, 'crew3', 'crew3@mail.com', '085454452522', 357, 'done', '43_1619877391.jpg', 'free', '2021-05-01 13:41:27', '$2y$10$RjuMhQRgs42.8lH0dOZNhOARA47A97OyoVZ8/2svmEB1bDITkIRQO', NULL, '2021-05-01 13:41:08', '2021-05-01 13:56:31', '0'),
(44, 'Agency3', 'ron.gusya1@gmail.com', '085211111210', 355, 'done', '44_1619880354.jfif', 'free', '2021-05-01 14:09:45', '$2y$10$L3n1cMP03Jppf0W2SdFaH.zmm0.EqQ/Pe7r3QMCM20UBEmL8B8eZW', NULL, '2021-05-01 14:09:14', '2021-05-01 16:05:19', '0'),
(45, 'shipowner2', 'shipowner2@mail.com', '085422212121', 213, 'done', '45_1619880839.jfif', 'free', '2021-05-01 14:52:05', '$2y$10$HOvHMmmXImutkdmf6iJPLewkaACPw27smOydN/YiqgfdCN.TgD63.', NULL, '2021-05-01 14:51:40', '2021-05-01 14:53:59', '0'),
(46, 'shipowner3', 'ron.gusya@gmail.com', '085211121111', 359, 'done', '46_1619882023.jpg', 'free', '2021-05-01 15:12:09', '$2y$10$nrvcyswmmPLx/slonMlMJOapbZaJ/NnQsFZqB1mJ.C8CVPHx7JUYC', NULL, '2021-05-01 15:11:51', '2021-05-21 14:04:28', '1');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_statistik_negara`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_statistik_negara` (
`negara` varchar(100)
,`jml` bigint(21)
,`typ` varchar(10)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_statistik_negara`
--
DROP TABLE IF EXISTS `v_statistik_negara`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_statistik_negara`  AS SELECT `agency_info`.`country` AS `negara`, count(`agency_info`.`country`) AS `jml`, 'AGENCY' AS `typ` FROM ((`users` left join `role_user` on(`role_user`.`user_id` = `users`.`id`)) left join `agency_info` on(`agency_info`.`users_id` = `users`.`id`)) WHERE `role_user`.`role_id` = 4 AND `agency_info`.`type_agency` = 'AGENCY' GROUP BY `agency_info`.`country` ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agency_info`
--
ALTER TABLE `agency_info`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `agency_invite`
--
ALTER TABLE `agency_invite`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `agency_jobs`
--
ALTER TABLE `agency_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `coc`
--
ALTER TABLE `coc`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cop`
--
ALTER TABLE `cop`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crew_apply`
--
ALTER TABLE `crew_apply`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crew_doc`
--
ALTER TABLE `crew_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crew_info`
--
ALTER TABLE `crew_info`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `home_slide`
--
ALTER TABLE `home_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `laravel_logger_activity`
--
ALTER TABLE `laravel_logger_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indeks untuk tabel `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `religion`
--
ALTER TABLE `religion`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agency_info`
--
ALTER TABLE `agency_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `agency_invite`
--
ALTER TABLE `agency_invite`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `agency_jobs`
--
ALTER TABLE `agency_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `coc`
--
ALTER TABLE `coc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `cop`
--
ALTER TABLE `cop`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT untuk tabel `crew_apply`
--
ALTER TABLE `crew_apply`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `crew_doc`
--
ALTER TABLE `crew_doc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `crew_info`
--
ALTER TABLE `crew_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `home_slide`
--
ALTER TABLE `home_slide`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `laravel_logger_activity`
--
ALTER TABLE `laravel_logger_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `membership`
--
ALTER TABLE `membership`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `religion`
--
ALTER TABLE `religion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `service`
--
ALTER TABLE `service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
