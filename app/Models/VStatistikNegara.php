<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VStatistikNegara extends Model
{
    use HasFactory;
    protected $table = 'v_statistik_negara';
}
