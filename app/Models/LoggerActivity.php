<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoggerActivity extends Model
{
    use HasFactory;
    protected $table = 'laravel_logger_activity';
}
