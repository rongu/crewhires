<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrewDoc extends Model
{
    use HasFactory;
    protected $table = 'crew_doc';
}
