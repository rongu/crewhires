@extends('layouts.backend')




@section('assets-top')
<link href="{{ url('public/assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Crew Page</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Crew
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Account Name</th>
                            <th>Fullname</th>
                            <th>Type</th>
                            <th>Status Account</th>
                            <th>Photo</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>


<div class="modal fade bs-modal-lg" id="modal-view" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Profile</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img id="v_image_id" src="{{url('public/images/profile.png')}}" class="img-responsive zoomimg" alt="" style="height: 150px;" title="Nama" >  </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> <span id="v_name_id">Name</span> </div>
                                <div class="profile-usertitle-job"> <span id="v_status_activity_id">Status Activity</span> </div>
                                <img id="v_img_bnd_id" src="{{url('public/images/no_image.png')}}" style="width: 70px;border: 1px solid #cdcdcd;">
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <button type="button" class="btn blue-ebonyclay btn-sm"><span id="v_type_crew_id">MARINE</span></button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                <ul class="nav">
        
                                    <li class="active">
                                        <a href="#">
                                            <i class="fa fa-file-text-o"></i> Curriculum Vitae </a>
                                    </li>

                                </ul>
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                        <!-- PORTLET MAIN -->
                        <div class="portlet light ">
                            <!-- STAT -->
                            <div class="row list-separated profile-stat">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 1 </div>
                                    <div class="uppercase profile-stat-text"> Apply </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 100 </div>
                                    <div class="uppercase profile-stat-text"> Agent </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 12500 </div>
                                    <div class="uppercase profile-stat-text"> Crew </div>
                                </div>
                            </div>
                            <!-- END STAT -->
                            <div>
                                <h3 class="profile-desc-title">Social Media</h3>
                                
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.facebook.com/"><span id="v_fb_id">fb</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-instagram fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.instagram.com/"><span id="v_ig_id">ig</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-send-o fa-sm font-blue-ebonyclay"></i>
                                    <a href="#"><span id="v_tg_id">tg</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.twitter.com/"><span id="v_tw_id">tw</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-whatsapp fa-sm font-blue-ebonyclay"></i>
                                    <a href="#"><span id="v_wa_id">wa</span></a>
                                </div>                                
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                        <!-- END BEGIN PROFILE SIDEBAR -->
                        <!-- BEGIN PROFILE CONTENT -->
                        <div class="profile-content">
                            <div class="row">
                                <div class="col-md-11" style="    margin-left: 25px;margin-top: 2px;">
                                    <div class="portlet light ">
                                        <div class="portlet-title tabbable-line">
                                            <div class="caption caption-md">
                                                <i class="icon-globe theme-font hide"></i>
                                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                            </div>
 
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Fullname</label>
                                                <input id="v_fullname_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Email</label>
                                                <input id="v_email_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Phone</label>
                                                <input id="v_phone_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Place & Date of Birth</label>
                                                <input id="v_birth_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Sex</label>
                                                <input id="v_sex_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Nationality</label>
                                                <input id="v_nationality_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Marital Status</label>
                                                <input id="v_marital_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Religion</label>
                                                <input id="v_religion_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>

                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Address</label>
                                                <textarea id="v_address_id" class="form-control" readonly="readonly" rows="4" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"></textarea>
                                                <!-- <input id="v_religion_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div> -->
                                                </div>
    
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Status Account</label>
                                                <input id="v_status_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>

                                             <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Portofolio</label><br>
                                                <img id="v_portofolio_id" src="{{url('public/images/portofolio/no_image.png')}}" alt="" class="img-responsive zoomimg">
                                                </div>

                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>
                <hr>
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Documentation</span>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <label class="control-label">Experienced</label>
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="v_tabel_exp">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Of Vessel</th>
                                    <th>Type Of Vessel</th>
                                    <th>Rank</th>
                                    <th>Company</th>
                                    <th>Sign On</th>                           
                                    <th>Sign Off</th>                           
                                </tr>
                            </thead>
                            <tbody>
                               
                           </tbody>
                        </table>
                        <br>
                        <label class="control-label">Certificate of Proficiency</label>
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="v_tabel_cop">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Of Certificate</th>
                                    <th>Certificate Number</th>
                                    <th>Place, Date Of Issued</th>
                                    <th>Expired Date</th>                       
                                </tr>
                            </thead>
                            <tbody>
                                
                           </tbody>
                        </table>
                        <br>
                        <label class="control-label">Certificate of Competency</label>
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="v_tabel_coc">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Of Certificate</th>
                                    <th>Certificate Number</th>
                                    <th>Place, Date Of Issued</th>
                                    <th>Expired Date</th>                       
                                </tr>
                            </thead>
                            <tbody>
                               
                           </tbody>
                        </table>
                        <br>
                        <label class="control-label">Flagstate Document</label>
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="v_tabel_flg">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Of Document</th>
                                    <th>Certificate Number</th>
                                    <th>Place, Date Of Issued</th>
                                    <th>Expired Date</th>                       
                                </tr>
                            </thead>
                            <tbody>
                                
                           </tbody>
                        </table>
                        <br>
                        <label class="control-label">Standard STCW  2010 + Flagstate Medical Form</label>
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="v_tabel_stc">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Of Document</th>
                                    <th>Document Number</th>
                                    <th>Place, Date Of Issued</th>
                                    <th>Expired Date</th>                       
                                </tr>
                            </thead>
                            <tbody>
                               
                           </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn green">Save changes</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.crew.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [6] },
            { searchable : false},                
            { render: function(data, type, row, meta) {

                if(data[3]==1){
                    var ban = "red-sunglo";
                }else{
                    var ban = "border-grey-gallery grey-cararra";
                }

                return '<a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" class="view_btn btn btn-circle btn-icon-only grey-mint" title="View"><i class="fa fa-search"></i></a><a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" class="reset_btn btn btn-circle btn-icon-only yellow-casablanca" title="Reset"><i class="fa fa-key"></i></a><a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" data-ss="'+data[3]+'" class="ban_btn btn btn-circle btn-icon-only '+ban+'" title="BAN"><i class="fa fa-ban"></i></a>'; }, targets: [6]
            },
            { render: function(data, type, row, meta) {
                return '<a href="javascript:;" class="btn green disabled uppercase">'+data+'</a>'; }, targets: [4]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/profile')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [5]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'name', name: 'name' },
            { data: 'fullname', name: 'fullname' },
            { data: 'type_crew', name: 'type_crew' },           
            { data: 'status_account', name: 'status_account' },           
            { data: 'photo', name: 'photo' },           
            { data: 'users_id', name: 'users_id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $('#sample_3').on('click','.view_btn',function(e){
        var id = e.currentTarget.dataset.id;
        var exptbl = $('#v_tabel_exp');
        var coptbl = $('#v_tabel_cop');
        var coctbl = $('#v_tabel_coc');
        
        var flgtbl = $('#v_tabel_flg');
        var stctbl = $('#v_tabel_stc');
        
        $.ajax({
            url: "{{ url('/backend/account/') }}/"+id+"/crew",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                $("#v_image_id").attr("src","{{url('public/images/profile')}}/"+data.account.photo);
                $("#v_name_id").text(data.account.name);
                $("#v_email_id").val(data.account.email);
                $("#v_phone_id").val("(+"+data.account.phone_code+") "+data.account.phone);
                $("#v_status_activity_id").text(data.info.status_activity);
                $("#v_img_bnd_id").attr('src',data.bnd);
                $("#v_type_crew_id").text(data.info.type_crew);
                
                $("#v_fullname_id").val(data.info.fullname);
                $("#v_birth_id").val(data.info.place_birth+", "+data.birth+" ("+data.age+")");

                $("#v_sex_id").val(data.info.gender=='f'?'Female':'Male');
                $("#v_nationality_id").val(data.info.nationality);
                $("#v_marital_id").val(data.info.marital);
                $("#v_religion_id").val(data.info.religion);
                $("#v_status_id").val(data.account.status_account);
                $("#v_address_id").val(data.info.address+", "+data.info.city+", "+data.info.country);

                $("#v_fb_id").text(data.info.fb==null?'-':data.info.fb);
                $("#v_ig_id").text(data.info.ig==null?'-':data.info.ig);
                $("#v_tg_id").text(data.info.tg==null?'-':data.info.tg);
                $("#v_tw_id").text(data.info.tw==null?'-':data.info.tw);
                $("#v_wa_id").text(data.info.wa==null?'-':data.info.wa);
                
                $("#v_portofolio_id").attr('src',"{{url('public/images/portofolio')}}/"+data.info.portofolio);

                exptbl.DataTable().clear().destroy();
                coptbl.DataTable().clear().destroy();
                coctbl.DataTable().clear().destroy();
                flgtbl.DataTable().clear().destroy();
                stctbl.DataTable().clear().destroy();


                for (var i = data.document.length - 1; i >= 0; i--) {
                    var rows = data.document[i];
                    if(rows.types=='exp'){

                        var vieExp = '';
                        var n_exp = 1;
                        vieExp += '<tr>';
                            vieExp += '<td>'+n_exp+'</td>';
                            vieExp += '<td>'+rows.name+'</td>';
                            vieExp += '<td>'+rows.t_vessel+'</td>';
                            vieExp += '<td>'+rows.rank+'</td>';
                            vieExp += '<td>'+rows.company+'</td>';
                            vieExp += '<td>'+rows.p_from+'</td>';
                            vieExp += '<td>'+rows.p_to+'</td>';
                        vieExp += '</tr>';
                        n_exp++;
                        $("#v_tabel_exp tbody").html(vieExp);

                    }else if(rows.types=='cop'){

                        var vieCop = '';
                        var n_cop = 1;
                        vieCop += '<tr>';
                            vieCop += '<td>'+n_cop+'</td>';
                            vieCop += '<td>'+rows.name+'</td>';
                            vieCop += '<td>'+rows.cer_doc_number+'</td>';
                            vieCop += '<td>'+rows.place+', '+rows.doi+'</td>';
                            vieCop += '<td>'+rows.ed+'</td>';
                        vieCop += '</tr>';
                        n_cop++;
                        $("#v_tabel_cop tbody").html(vieCop);

                    }else if(rows.types=='coc'){
                        var vieCoc = '';
                        var n_coc = 1;
                        vieCoc += '<tr>';
                            vieCoc += '<td>'+n_coc+'</td>';
                            vieCoc += '<td>'+rows.name+'</td>';
                            vieCoc += '<td>'+rows.cer_doc_number+'</td>';
                            vieCoc += '<td>'+rows.place+', '+rows.doi+'</td>';
                            vieCoc += '<td>'+rows.ed+'</td>';
                        vieCoc += '</tr>';
                        n_coc++;
                        $("#v_tabel_coc tbody").html(vieCoc);
                    }else if(rows.types=='flg'){

                        var vieFlg = '';
                        var n_flg = 1;
                        vieFlg += '<tr>';
                            vieFlg += '<td>'+n_flg+'</td>';
                            vieFlg += '<td>'+rows.name+'</td>';
                            vieFlg += '<td>'+rows.cer_doc_number+'</td>';
                            vieFlg += '<td>'+rows.place+', '+rows.doi+'</td>';
                            vieFlg += '<td>'+rows.ed+'</td>';
                        vieFlg += '</tr>';
                        n_flg++;
                        $("#v_tabel_flg tbody").html(vieFlg);

                    }else if(rows.types=='stc'){

                        var vieStc = '';
                        var n_stc = 1;
                        vieStc += '<tr>';
                            vieStc += '<td>'+n_stc+'</td>';
                            vieStc += '<td>'+rows.name+'</td>';
                            vieStc += '<td>'+rows.cer_doc_number+'</td>';
                            vieStc += '<td>'+rows.place+', '+rows.doi+'</td>';
                            vieStc += '<td>'+rows.ed+'</td>';
                        vieStc += '</tr>';
                        n_stc++;
                        $("#v_tabel_stc tbody").html(vieStc);

                    } 
                }


                exptbl.dataTable({paging:false,searching:false,info:false,retrieve : true});
                coptbl.dataTable({paging:false,searching:false,info:false,retrieve : true});
                coctbl.dataTable({paging:false,searching:false,info:false,retrieve : true});
                flgtbl.dataTable({paging:false,searching:false,info:false,retrieve : true});
                stctbl.dataTable({paging:false,searching:false,info:false,retrieve : true});
                
            }
        });

        $("#modal-view").modal('show');
    });
    
    $('#sample_3').on('click','.reset_btn',function(e){
        var id = e.currentTarget.dataset.id;

        swal({
          title: "Whether to Reset Password This Crew data ?",
          //text: "Crew Recommendation information will disappear from the web, are you sure you will do this process ... ",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Yes, I'm agree",
          cancelButtonText: "No, Cancle this Proses",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('/backend/reset-pass/') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            console.log(data);
                            if(data.status){
                                swal("Thank you", "You has Reset Password", "success");
                            }else{
                                swal("Sorry!!!", "User Do not Reset Password ", "error");
                            }
                            //location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Cancel", "You cancel the process of removing Crew Recommendations ... ", "error");
                }
        });
    });  


    $('#sample_3').on('click','.ban_btn',function(e){
        var id = e.currentTarget.dataset.id;
        var ss = e.currentTarget.dataset.ss;

        if(ss==1){
            var txt = 'Active';
        }else {
            var txt = 'Ban';
        
        }

        swal({
          title: "Whether to "+txt+" This Crew Access ?",
          //text: "Crew Recommendation information will disappear from the web, are you sure you will do this process ... ",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Yes, I'm agree",
          cancelButtonText: "No, Cancle this Proses",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('/backend/ban-access/') }}",
                        type : "POST",  
                        data : {id:id,"_token": "{{ csrf_token() }}"},
                        dataType: "JSON",
                        success: function(data) {
                            console.log(data);
                            if(data.status){
                                swal("Thank you", "You has "+data.msg+" Access", "success");
                            }else{
                                swal("Sorry!!!", "User Do not Banned Access ", "error");
                            }
                            //location.reload(true);
                            var oTable = table.dataTable().fnDraw();
                        }
                    });
                    
                } else {
                    swal("Cancel", "You cancel the process of "+txt+" Access Crew ... ", "error");
                }
        });
    });  

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
    
});
</script>
@endsection    