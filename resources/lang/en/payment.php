<?php 
 
return [
    'Invoice Number' => 'Invoice Number',
    'To Account Number' => 'To Account Number',
    'Proof of Payment' => 'Proof of Payment',
    'Account Number' => 'Account Number',
    'Bank Account Under the Name' => 'Bank Account Under the Name (of)',
    'Payment Verification Date' => 'Payment Verification Date',
];