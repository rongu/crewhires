@extends('layouts.backend')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }

    .label-class{
        margin-top:35px;
        background-color: #ffffff;
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Dashboard Page</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Dashboard
</h1>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="<?php echo $jml_crew;?>">0</span>

                    </h3>
                    <small>CREW</small>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                        <!-- <span class="sr-only">76% progress</span> -->
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> &nbsp; </div>
                    <div class="status-number"> &nbsp; </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="<?php echo $jml_agency;?>">0</span>
                    </h3>
                    <small>AGENCY</small>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                        <!-- <span class="sr-only">45% grow</span> -->
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> &nbsp; </div>
                    <div class="status-number"> &nbsp; </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="<?php echo $jml_so;?>">0</span>
                    </h3>
                    <small>Ship Owner</small>
                </div>
                <div class="icon">
                    <i class="fa fa-ship"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success purple-soft">
                        <!-- <span class="sr-only">56% change</span> -->
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> &nbsp; </div>
                    <div class="status-number"> &nbsp; </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption ">
                    <span class="caption-subject font-dark bold uppercase">Finance</span>
                    <span class="caption-helper">distance stats...</span>
                </div>
                <div class="actions">
                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                        <i class="fa fa-pencil"></i> Export </a>
                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                        <i class="fa fa-print"></i> Print </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="dashboard_amchart_3" class="CSSAnimationChart"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN REGIONAL STATS PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green-haze"></i>
                    <span class="caption-subject bold uppercase font-green-haze">User Map Region</span>                    
                </div>
            </div>
            <div class="portlet-body">
                <div id="mapid" style="height: 450px;"></div>
                <!-- <div id="chart_10" class="chart" style="height: 450px;"> </div> -->
            </div>
        </div>
        <!-- END REGIONAL STATS PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Feeds</span>
                </div>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="scroller" style="height: 450px;" data-always-visible="1" data-rail-visible="0">
                    <ul class="feeds">
                        @foreach($logs as $vl)
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            
                                                @if($vl->description=='Logged Out')
                                                <div class="label label-sm bg-red-mint bg-font-red-mint"><i class="fa fa-sign-out"></i></div>
                                                @elseif($vl->description=='Logged In')
                                                <div class="label label-sm bg-green-seagreen bg-font-green-seagreen"><i class="fa fa-sign-in"></i></div>
                                                @else
                                                <div class="label label-sm bg-grey-salsa bg-font-grey-salsa"><i class="fa fa-bell-o"></i></div>
                                                @endif
                                            
                                        </div>
                                        <div class="cont-col2">
                                            @if($vl->description=='Logged Out' || $vl->description=='Logged In')
                                            <div class="desc"> {{$vl->name}} </div>
                                            @elseif($vl->description=='Service')
                                            <div class="desc"> {{$vl->name}} - {{$vl->details}} </div>
                                            @else
                                            <div class="desc"> {{$vl->name}} - {{$vl->description}} </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col2"  style="width: 110px; margin-left: -110px;">
                                    <div class="date"> {{Carbon::parse($vl->created_at)->diffForHumans(['options' => Carbon::NO_ZERO_DIFF])}} </div>
                                </div>
                            </li>
                        @endforeach
                        <!-- <li>
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col1">
                                        <div class="label label-sm label-success">
                                            <i class="fa fa-bell-o"></i>
                                        </div>
                                    </div>
                                    <div class="cont-col2">
                                        <div class="desc"> You have 4 pending tasks.
                                            <span class="label label-sm label-info"> Take action
                                                <i class="fa fa-share"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date"> Just now </div>
                            </div>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-success">
                                                <i class="fa fa-bell-o"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> New version v1.4 just lunched! </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> 20 mins </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col1">
                                        <div class="label label-sm label-danger">
                                            <i class="fa fa-bolt"></i>
                                        </div>
                                    </div>
                                    <div class="cont-col2">
                                        <div class="desc"> Database server #12 overloaded. Please fix the issue. </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date"> 24 mins </div>
                            </div>
                        </li> -->
                        
                    </ul>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
@endsection

@section('assets-bottom')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB49n_oRjDn2uIes7Cfo71ZJBbAQElJ4vY&libraries=places"></script>
<script>
$(document).ready(function() {

    var cld;
    var jenis_aset_id = 0;

    $.ajax({
        url: "{{ route('backend.vs.negarasum') }}",
        type: "post",
        data: {"_token": "{{ csrf_token() }}"},
        dataType: "JSON",
        async:false,
        success: function(resultdata) {
            cld = resultdata;
            return cld;
        }   
    });

    // var markers = [
    //   ['2000', -4.441906448819741, 122.58113709907367, "data:image\/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MDAgNDAwIj4NCjxwYXRoIGZpbGw9IiNjZTExMjYiIGQ9Im0wLDBoNjAwdjIwMGgtNjAweiIvPg0KPHBhdGggZmlsbD0iI2ZmZiIgZD0ibTAsMjAwaDYwMHYyMDBoLTYwMHoiLz4NCjwvc3ZnPg0K"],
    //   ['20', 12.675953225391394, 104.91097785813749, ""],
    //   ['40', 21.303259023466406, 96.5974356653606, "data:image\/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA5MDAgNTAwIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+DQo8ZGVmcy8+DQo8ZyBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHN0cm9rZS13aWR0aD0iMS4yNSI+DQo8cGF0aCBmaWxsPSIjZGEyZDFjIiBkPSJtMCwwaDkwMHY1MDBoLTkwMHoiLz4NCjxwYXRoIGZpbGw9IiMyMDQyYTEiIGQ9Ik0gMCwwIEgzODkuNzY0IFYyNDguMDMyIEgwIHoiLz4NCjxwYXRoIGZpbGw9IiNmZmYiIGQ9Im0yMjcuMDU1OSwxMjUuMzE3N2MwLDE3LjgxMjktMTQuNjM3OCwzMi4yNjk5LTMyLjY3MzMsMzIuMjY5OS0xOC4wMzUxLDAtMzIuNjcyOS0xNC40NTctMzIuNjcyOS0zMi4yNjk5IDAtMTcuODEyOSAxNC42Mzc4LTMyLjI2OTUgMzIuNjcyOS0zMi4yNjk1IDE4LjAzNTUsMCAzMi42NzMzLDE0LjQ1NjYgMzIuNjczMywzMi4yNjk1em0tMTAxLjg2MjktNy4yMjQ0bDMuODQzNy0xNC45MzA3IDEwLjU3MDksNC4zMzQ4IDUuNzY1OC0xMi4wNDExLTkuMTI5Ni01Ljc3OTYgOS4xMjk2LTEyLjUyMjYgOC42NDg5LDguNjY5MiA5LjYwOTUtOC42NjkyLTUuNzY1OC05LjYzMyAxMy45MzM5LTcuMjI0NCAzLjM2MzQsMTIuMDQwNyAxMi45NzMzLTMuODUyOS0xLjQ0MTMtMTEuMDc3OCAxNC44OTQ5LS40ODE1LS45NjEsMTEuNTU5MiAxMy40NTM2LDQuMzM0OCAzLjM2MzQtMTIuMDQxMSAxMy40NTM2LDYuNzQyOS02LjI0NjUsMTAuMTE0NCAxMC41NzA5LDguMTg3NyA4LjE2ODEtOC42NjkyIDkuMTI5MiwxMS41NTkyLTkuNjA5NSw2Ljc0MjkgNi4yNDYxLDEyLjA0MTEgMTAuNTcwOS0zLjg1MzMgMy4zNjM0LDE0LjkzMDctMTEuMDUxMiwuOTYzMy0uNDgwNywxMy40ODU5IDExLjA1MTIsLjk2MzMtMy4zNjM0LDE1LjQxMjYtMTEuMDUwOC00LjgxNjctNC44MDUxLDEyLjUyMyA5LjEyOTIsNS43Nzk2LTkuMTI5MiwxMi4wNDA3LTcuNjg3OC04LjY2OTItMTAuNTcwNSw4LjY2OTIgNi4yNDYxLDEwLjExNDQtMTMuOTMzOSw2Ljc0My0zLjg0MzctMTEuNTU5Mi0xMi45NzMzLDMuMzcxNSAxLjQ0MTMsMTEuNTU5Mi0xNS4zNzUyLC40ODE5IDEuNDQxMy0xMS41NTk2LTEyLjQ5MjYtMy4zNzE1LTQuMzI0NCwxMS4wNzc4LTEyLjk3MzMtNi43NDMgNi4yNDY1LTkuNjMyNS0xMS4wNTEyLTguMTg4Mi04LjE2ODEsOC4xODgyLTkuNjA5OS0xMS41NTkyIDEwLjA5MDItNy4yMjQ4LTYuMjQ2MS0xMS41NTkyLTEwLjA5MDIsMy44NTI5LTQuMzI0NC0xNC45MzA3IDEyLjAxMjItLjk2MzN2LTEzLjQ4NTlsLTEyLjAxMjItMS40NDQ4eiIvPg0KPC9nPg0KPHBhdGggZmlsbD0iI2ZmZiIgZmlsbC1ydWxlPSJldmVub2RkIiBpZD0icG9seWdvbjU3OSIgc3Ryb2tlLXdpZHRoPSIxLjI1IiBkPSJtMzkwLjkwNiwyOS4xNzgtMzAuNjczLTIxLjQ3My0zMC43OTMsMjEuMjk4OCAxMC45NDQtMzUuODA2Ny0yOS43NzItMjIuNzA0NyAzNy40MzUtLjY1NjggMTIuMzk0LTM1LjMzMTEgMTIuMTkzLDM1LjQwMDggMzcuNDMxLC44Njg5LTI5LjksMjIuNTM1NyAxMC43NDEsMzUuODY4MXoiIHRyYW5zZm9ybT0ibWF0cml4KC4zNDA0NCAwIDAgLjMzMjc1IDcyLjIwNzI3IDM5LjUxMDA0KSIvPg0KPGcgZmlsbD0iI2ZmZiIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2U9IiMyMDQyYTEiPg0KPHBhdGggc3Ryb2tlLXdpZHRoPSIxLjI0NCIgZD0ibTI0MC40NDM0LDkzLjE5NjhjNi41NzQsMjMuMTMxOCAyLjAwNTMsMzUuNjQxIC4yNjI3LDM1Ljg3MDMtMi4yMzEyLS45NzY5LTIuMzk5Ni0yNC41NjMxLTExLjg5NzktMzAuNTcwMS04Ljk1NDQtNS41MTg3LTMuNjA4Ny0xMC44Nzc0LTEuMTcyNS0xMi45NTkyIDIuNDgyOC0xLjEwMzggMTAuMDEwNS0yLjA0NzkgMTIuODA3Nyw3LjY1OXoiLz4NCjxwYXRoIHN0cm9rZS13aWR0aD0iMS40NzEiIGQ9Im0yMTUuNDc0MiwxMjMuMzE2NGMtMTYuODEzNCwzOC4xOTAzLTEwLjMyNDIsNjQuMDcxOS0xNi43MzQ5LDYzLjg5LTYuNTg2OCwuMDUzOS04LjMyODktMjMuNzcyNiAzLjg0NDEtNjMuNjM1NiAxNS4zNTEyLTUwLjEwODQgMzQuODkxNi00NS45MjEyIDM3LjI1OTgtMzEuODk5Ni0xMi45MjMxLDYuMTE0MS0xNC42NzEsNy42MzQtMjQuMzY4OSwzMS42NDUyeiIvPg0KPHBhdGggc3Ryb2tlLXdpZHRoPSIxLjM4MiIgZD0ibTIzNy4wNTI2LDEzNi4wMTg5YzIuNzMxMywyNC43MjQ3LTUuNDU2MiwzNS40OTM4LTcuNTc5OCwzNS4xNi0yLjQzOTUtMS42Njk4IDYuOTA5NC0xNi45MDAyLTcuNDI4NS0zMy43MDMxLTguMjIwNi05LjQ4MjEgLjcxNzItNS45OTU4IDQuMDczNC03LjI1MTQgMy4xOTY1LS4yODQzIDkuNzQyOS00LjU5MyAxMC45MzQ5LDUuNzk0NXoiLz4NCjxwYXRoIHN0cm9rZS13aWR0aD0iMS42MzQiIGQ9Im0yMDkuNjE5NSwxNjAuNDUxOGMtNi40ODg4LDE0LjkxMDctOS4zMTU1LDMzLjA3NDktMTYuOTAyMiwzMC44NDQxLTcuODQ4MS0yLjA1NjYtLjM1MDgtMjcuNTYzNSA5LjY5NzQtNDUuMzUxNCAxMi40MDM5LTIxLjc1NTYgMzQuNjM0Mi0yNy4yNzgxIDM0LjI1OTItMTEuNjA5NC0xNS4zMjY4LDIuNDQ4OS0yMi4wNTg0LDE0LjA2NTYtMjcuMDU0NCwyNi4xMTY3eiIvPg0KPHBhdGggc3Ryb2tlLXdpZHRoPSIxLjYwNSIgZD0ibTE1My40MDEzLDEyMC42ODkyYy02LjQ3NzQsMjguMTA5NS01LjExODYsNDUuNDUyMy0zLjM1NCw0NC45NzggMi40MTYsLjI5MzQgMS42OTU0LTI2LjgzMzcgMTIuOTk0OC0zNy44ODczIDEwLjQ1OTUtMTAuNjc3NiA0LjU5MjItOC41MjE3IDIuMjA0My0xMi40OTQ3LTQuODY3Mi0xLjk3ODctOC41ODg4LTcuNzUwOC0xMS44NDUsNS40MDQxeiIvPg0KPHBhdGggc3Ryb2tlLXdpZHRoPSIxLjc0NCIgZD0ibTE3OS4yNDAyLDE1MS4wMDA5YzcuNzY2NCwxOC45NzA2IDEyLjc0NjMsNDIuMjQwMSAxOS4xMzYyLDM5LjEzMjkgNi42NDQ0LTIuODkxNC00LjAyNzEtMzUuMTgwOS0xNC43NjMxLTU4LjAxNTItOS4xNjY0LTE4LjA1MDctMTUuOTU2Ni0yMC4yNzc0LTI2LjA5Ny0xOS43MDA0IDExLjgyMjQsNC43MTA5IDE1LjY2NjEsMjMuMjQyNSAyMS43MjM5LDM4LjU4Mjd6Ii8+DQo8cGF0aCBzdHJva2Utd2lkdGg9IjEuOTQ0IiBkPSJtMTgwLjU3NTYsMTQwLjQ3MTZjNS43NywyNC4wOTQ5IDguMjU2NCw1My4wMzU0IDE1LjA0ODcsNTAuMTI0NCA3LjAyNTctMi42MTQ1LS4yMjI3LTQzLjI4MTYtOC41Njk0LTcyLjQ3MTQtNy4yODQzLTIzLjE2NjYtMTguNTAxMy0yMi43OTY1LTI4LjgwNDYtMjMuNDY1OSAxNi41MDA1LDguMjQ2MyAxNy44ODQsMjYuMzU5MSAyMi4zMjUzLDQ1LjgxMjl6Ii8+DQo8cGF0aCBzdHJva2Utd2lkdGg9IjEuNzg5IiBkPSJtMTU2LjUwOTgsOTUuNzUwNGMtMTUuMTE0MywxMi40MDU5LTE1LjAzMjIsMzcuNTQyLTEyLjE1ODUsMzcuOTM5NyAyLjk5NjIsLjUwNjIgNC42MTktMTkuODY5NiAxMy44NjA4LTI3LjUwMjEgOS43ODY0LTcuNzc2NiAxMS4yMTEtMy41NDcxIDIyLjI2NDctMi43Mi0yLjk1MjMtNi41MTUtMTEuNTcwNC0xOC4zNzU5LTIzLjk2Ny03LjcxNzd6Ii8+DQo8ZyBzdHJva2Utd2lkdGg9IjEuMjUiPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzcwODIgLS4xNjkxMyAuMTQ2ODMgLjI4ODY1IC0xMS42NDI3OCAyMS43OTc0NikiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjM4NDQ2IC0uMTM1MSAuMTIwNDEgLjMwMDY5IC02LjQxODAxIDE1LjEwMTI2KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA1OTcgLS4wMzQxOSAuMDQxMzggLjMyMTM0IDI0LjM1OCAtMzMuNjU4NzcpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDI0NyAtLjA2MzM0IC4wNjQzMSAuMzE3NTMgMTEuMjA4NjYgLTguMzQwNykiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwMjQ3IC0uMDYzMzQgLjA2NDMxIC4zMTc1MyAxMS4yMDg2NiAxLjc3MzczKSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA1NDYgLjAzOTggLS4wMTcxNSAuMzIzNTUgNTAuMjU2ODkgLTUwLjczMzY3KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA1NDYgLjAzOTggLS4wMTcxNSAuMzIzNTUgNTAuMjU2ODkgLTQwLjYxOTI1KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA2OTEgLjAxOTk1IC0uMDAxMzkgLjMyNCAzMC4xMTg3OSAtMjEuMDkxMykiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjkxIC4wMTk5NSAtLjAwMTM5IC4zMjQgMzcuMzI2MDYgLTIxLjA2ODYxKSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg4OTQgLjEyMTU1IC0uMDgyMzcgLjMxMzMxIDg0LjE1MjQxIC01NC42MjQ2NSkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjkxIC4wMTk5NSAtLjAwMTM5IC4zMjQgMjkuNjM4MyAtMTAuMDEzNjUpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDY5MSAuMDE5OTUgLS4wMDEzOSAuMzI0IDM2Ljg0NTU3IC0xMi44ODA4KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg4OTQgLjEyMTU1IC0uMDgyMzcgLjMxMzMxIDgzLjY3MTk0IC00NC41MTAyNykiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjUyNjIyIC0uMDM4NDkgLjA0OTc0IC40MDcyNSAtMjAuMDc5OTYgMzYuNDI3MTcpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDY5MSAuMDE5OTUgLS4wMDEzOSAuMzI0IDI2LjI3NDkxIDEuMDY0MDUpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDY5MSAuMDE5OTUgLS4wMDEzOSAuMzI0IDM0LjkyMzYyIC0xLjMyMTQ2KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg4OTQgLjEyMTU1IC0uMDgyMzcgLjMxMzMxIDgwLjMwODU2IC0zMy40MzI1NikiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjM5OTE1IC4wODE3NSAtLjA1MDU0IC4zMjAwMiA1Mi4zOTkxIC0xMC40OTc5NCkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjM5OTE1IC4wODE3NSAtLjA1MDU0IC4zMjAwMiA1OS41MTkwOCAtOS4zNzY3NCkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjM2NTk3IC4xNzk0MyAtLjEyODk1IC4yOTcxMSAxMTEuMDM4MTUgLTM2LjM1NTU4KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA2MjcgLS4wMzA0MSAuMDM4NCAuMzIxNzEgMjIuNzc5NDUgNTUuODY2NTkpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC4zODExMiAuMTQ0MyAtLjEwMDYzIC4zMDc5IDEwMC40NzQxOCAtMy42ODQxNikiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjk3IC4wMTg2NyAtLjAwMDM4IC4zMjQwMSA0NC41NTI3NiAzNS4yNTY3NSkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjk3IC4wMTg2NyAtLjAwMDM4IC4zMjQwMSAzNC45NDMwNiAyMy42OTc0MikiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjk3IC4wMTg2NyAtLjAwMDM4IC4zMjQwMSA0Mi4xNTAzNSAyMy42OTc0MikiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjM4OTMxIC4xMjAzMyAtLjA4MTM5IC4zMTM1NyA4Ni45NDY0NSAtMjEuMDgzMzgpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC4zODkzMSAuMTIwMzMgLS4wODEzOSAuMzEzNTcgODguODY4MzkgLTEwLjk2OTAxKSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg5MzEgLjEyMDMzIC0uMDgxMzkgLjMxMzU3IDk0LjYzNDE5IC05LjA0MjQ2KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg5MzEgLjEyMDMzIC0uMDgxMzkgLjMxMzU3IDkxLjI3MDg0IC0zMi42NDI3MSkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjM3IC4wMjkwNSAtLjAwODYxIC4zMjM4OSA2My44ODM3OCAtNDcuNTQ5NzkpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC4zODkzMSAuMTIwMzMgLS4wODEzOSAuMzEzNTcgOTMuNjczMjQgLTIyLjA0NjcpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC4zODkzMSAuMTIwMzMgLS4wODEzOSAuMzEzNTcgOTkuNDM5MDUgLTIwLjYwMTc2KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA2MzcgLjAyOTA1IC0uMDA4NjEgLjMyMzg5IDU5LjA3ODkzIC0zNy40MzUzOCkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNDUgLjA0ODY4IC0uMDI0MiAuMzIzMSA3NC4wOTYwNyAtNDQuNDI0MDgpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDYzNyAuMDI5MDUgLS4wMDg2MSAuMzIzODkgNTguMTE3OTcgLTI4LjI4NDMxKSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguNDA0NSAuMDQ4NjggLS4wMjQyIC4zMjMxIDczLjEzNTExIC0zNC4zMDk3MSkiLz4NCjxlbGxpcHNlIGN4PSIzNzIuMDY3IiBjeT0iMzk0Ljc2OSIgcng9IjExLjc3NCIgcnk9IjI1LjMxNSIgdHJhbnNmb3JtPSJtYXRyaXgoLjQwNjM3IC4wMjkwNSAtLjAwODYxIC4zMjM4OSA1OS4wNzg5MiAtMTYuMjQzMjkpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC4zODkzMSAuMTIwMzMgLS4wODEzOSAuMzEzNTcgOTcuOTk3NTkgLTM0LjU2OTM0KSIvPg0KPGVsbGlwc2UgY3g9IjM3Mi4wNjciIGN5PSIzOTQuNzY5IiByeD0iMTEuNzc0IiByeT0iMjUuMzE1IiB0cmFuc2Zvcm09Im1hdHJpeCguMzg5MzEgLjEyMDMzIC0uMDgxMzkgLjMxMzU3IDEwNC4yNDM4NiAtMzIuNjQyNzkpIi8+DQo8ZWxsaXBzZSBjeD0iMzcyLjA2NyIgY3k9IjM5NC43NjkiIHJ4PSIxMS43NzQiIHJ5PSIyNS4zMTUiIHRyYW5zZm9ybT0ibWF0cml4KC40MDQ1IC4wNDg2OCAtLjAyNDIgLjMyMzEgNzUuMDU3MDQgLTIzLjIzMjAxKSIvPg0KPC9nPg0KPC9nPg0KPHVzZSB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgaWQ9InVzZTI3NTIiIHhsaW5rOmhyZWY9IiNwb2x5Z29uNTc5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNDAuMzYwNzIgOC42Njk1MikiLz4NCjx1c2Ugd2lkdGg9IjkwMCIgaGVpZ2h0PSI1MDAiIGlkPSJ1c2UyNzU0IiB4bGluazpocmVmPSIjdXNlMjc1MiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMyLjY3Mjk2IDIzLjExODY5KSIvPg0KPHVzZSB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgaWQ9InVzZTI3NTYiIHhsaW5rOmhyZWY9IiN1c2UyNzU0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTYuMzM2NDIgMzQuNjc3OTgpIi8+DQo8dXNlIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBpZD0idXNlMjc1OCIgeGxpbms6aHJlZj0iI3VzZTI3NTYiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0uMDAwMDQgMzguNTMxMDgpIi8+DQo8dXNlIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBpZD0idXNlMjc2MCIgeGxpbms6aHJlZj0iI3VzZTI3NTgiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE2LjMzNjQ2IDM4LjUzMTA0KSIvPg0KPHVzZSB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgaWQ9InVzZTI3NjIiIHhsaW5rOmhyZWY9IiN1c2UyNzYwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMS4yMzE1MyAyNy45MzQ5NSkiLz4NCjx1c2Ugd2lkdGg9IjkwMCIgaGVpZ2h0PSI1MDAiIGlkPSJ1c2UyNzY0IiB4bGluazpocmVmPSIjdXNlMjc2MiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNDAuMzYwNjkgOS42MzI3OSkiLz4NCjx1c2Ugd2lkdGg9IjkwMCIgaGVpZ2h0PSI1MDAiIGlkPSJ1c2UyNzY2IiB4bGluazpocmVmPSIjdXNlMjc2NCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNDIuMjgyNyAtNy43MDYxNikiLz4NCjx1c2Ugd2lkdGg9IjkwMCIgaGVpZ2h0PSI1MDAiIGlkPSJ1c2UyNzY4IiB4bGluazpocmVmPSIjdXNlMjc2NiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzEuMjMxMzMgLTI3LjkzNTAzKSIvPg0KPHVzZSB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgaWQ9InVzZTI3NzAiIHhsaW5rOmhyZWY9IiN1c2UyNzY4IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNy4yOTc1MyAtMzguNTMxMDQpIi8+DQo8dXNlIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBpZD0idXNlMjc3MiIgeGxpbms6aHJlZj0iI3VzZTI3NzAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yLjg4MyAtMzcuNTY3NzgpIi8+DQo8dXNlIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBpZD0idXNlMjc3NCIgeGxpbms6aHJlZj0iI3VzZTI3NzIiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNy4yOTczNyAtMzUuNjQxMjgpIi8+DQo8dXNlIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiB4bGluazpocmVmPSIjdXNlMjc3NCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTI2LjkwNzExIC0yNS4wNDUyNCkiLz4NCjwvc3ZnPg0K"],
    //   ['60', 28.273868792935968, 83.93309629404365, "data:image\/svg+xml;base64,PCFET0NUWVBFIHN2Zz4NCjxzdmcgd2lkdGg9IjYzMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBoZWlnaHQ9Ijc3MCIgdmlld0JveD0iLTE4IC01IDcyIDg4IiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+DQo8ZGVmcz4NCjxjbGlwUGF0aCBpZD0iaGVscGVyMSI+DQo8cGF0aCBkPSJtLTIwLDIxaDUwdjMwaC01MHoiLz4NCjwvY2xpcFBhdGg+DQo8Y2xpcFBhdGggaWQ9ImhlbHBlcjIiPg0KPHBhdGggZD0ibS0yMCwyMGg1MHYzMGgtNTB6Ii8+DQo8L2NsaXBQYXRoPg0KPC9kZWZzPg0KPHBhdGggc3Ryb2tlPSIjMDAzODkzIiBzdHJva2Utd2lkdGg9IjUuMTY1IiBkPSJtLTE1LDB2ODBoNjBsLTQyLjQyNjQtNDIuNDI2NGg0Mi40MjY0eiIvPg0KPHBhdGggZmlsbD0iI2NlMTEyNiIgZD0ibS0xNSwwdjgwaDYwbC00Mi40MjY0LTQyLjQyNjRoNDIuNDI2NHoiLz4NCjxnIGZpbGw9IiNmZmYiPg0KPGc+DQo8Y2lyY2xlIGN5PSIyMy40ODMiIHI9IjExLjk1IiBjbGlwLXBhdGg9InVybCgjaGVscGVyMSkiLz4NCjxjaXJjbGUgZmlsbD0iI2NlMTEyNiIgY3k9IjE4Ljc4NyIgcj0iMTIuODQiIGNsaXAtcGF0aD0idXJsKCNoZWxwZXIyKSIvPg0KPC9nPg0KPGcgdHJhbnNmb3JtPSJtYXRyaXgoNS41NjEwNiAwIDAgNS41NjEwNiAwIDI5LjA0NDU2KSI+DQo8Y2lyY2xlIHI9IjEiLz4NCjxnIGlkPSI2amFncyI+DQo8ZyBpZD0iM2phZ3MiPg0KPHBhdGggaWQ9ImphZyIgZD0ibS4xOTUxLS45ODA4bC0uMTk1MS0uNDA4LS4xOTUxLC40MDgiIHRyYW5zZm9ybT0icm90YXRlKDExLjI1KSIvPg0KPHVzZSB4bGluazpocmVmPSIjamFnIiB0cmFuc2Zvcm09InJvdGF0ZSgyMi41KSIvPg0KPHVzZSB4bGluazpocmVmPSIjamFnIiB0cmFuc2Zvcm09InJvdGF0ZSg0NSkiLz4NCjwvZz4NCjx1c2UgeGxpbms6aHJlZj0iIzNqYWdzIiB0cmFuc2Zvcm09InJvdGF0ZSg2Ny41KSIvPg0KPC9nPg0KPHVzZSB4bGluazpocmVmPSIjNmphZ3MiIHRyYW5zZm9ybT0ic2NhbGUoLTEgMSkiLz4NCjwvZz4NCjxnIHRyYW5zZm9ybT0ibWF0cml4KDguMTQzNCAwIDAgOC4xNDM0IDAgNTguNzg2OCkiPg0KPGNpcmNsZSByPSIxIi8+DQo8ZyBpZD0iNHJheXMiPg0KPGcgaWQ9IjJyYXlzIj4NCjxwYXRoIGlkPSJyYXkiIGQ9Im0uMjU4OCwuOTY1OWwtLjI1ODgsLjYxMDgtLjI1ODgtLjYxMDgiLz4NCjx1c2UgeGxpbms6aHJlZj0iI3JheSIgdHJhbnNmb3JtPSJyb3RhdGUoMTgwKSIvPg0KPC9nPg0KPHVzZSB4bGluazpocmVmPSIjMnJheXMiIHRyYW5zZm9ybT0icm90YXRlKDkwKSIvPg0KPC9nPg0KPHVzZSB4bGluazpocmVmPSIjNHJheXMiIHRyYW5zZm9ybT0icm90YXRlKDMwKSIvPg0KPHVzZSB4bGluazpocmVmPSIjNHJheXMiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPg0KPC9nPg0KPC9nPg0KPC9zdmc+DQo="],

    // ];

    // console.log(markers);

    function initialize() {
        console.log(cld);
        var markers = JSON.parse(cld);

        var mapCanvas = document.getElementById('mapid');
        var mapOptions = {
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }     
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var infowindow = new google.maps.InfoWindow(), marker, i;
        var bounds = new google.maps.LatLngBounds(); // diluar looping
        for (i = 0; i < markers.length; i++) {
            console.log(markers[i][2]);
            pos = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(pos); // di dalam looping

            marker = new google.maps.Marker({
                position: pos,
                label: {
                    text: markers[i][0],
                    color: 'black',
                    background: 'white',
                    fontSize: '12px',
                    className: 'label-class'
                },
                //title: markers[i][0],
                //label: markers[i][0],
                //label: `${i + 1}`,
                animation: google.maps.Animation.DROP,
                map: map,
                icon: {
                  //size: new google.maps.Size(200, 200),
                  scaledSize: new google.maps.Size(32, 32),
                  origin: new google.maps.Point(0, 0),
                  url: markers[i][3],
                  anchor: new google.maps.Point(16, 16)
                },
                // icon: markers[i][3]
                
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(markers[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            map.fitBounds(bounds); // setelah looping
        }

    }


  google.maps.event.addDomListener(window, 'load', initialize);


    // var latlong = {};
    // latlong["AD"] = {
    //     "latitude": 42.5,
    //     "longitude": 1.5
    // };
    // latlong["AE"] = {
    //     "latitude": 24,
    //     "longitude": 54
    // };
    // latlong["AF"] = {
    //     "latitude": 33,
    //     "longitude": 65
    // };
    // latlong["AG"] = {
    //     "latitude": 17.05,
    //     "longitude": -61.8
    // };
    // latlong["AI"] = {
    //     "latitude": 18.25,
    //     "longitude": -63.1667
    // };
    // latlong["AL"] = {
    //     "latitude": 41,
    //     "longitude": 20
    // };
    // latlong["AM"] = {
    //     "latitude": 40,
    //     "longitude": 45
    // };
    // latlong["AN"] = {
    //     "latitude": 12.25,
    //     "longitude": -68.75
    // };
    // latlong["AO"] = {
    //     "latitude": -12.5,
    //     "longitude": 18.5
    // };
    // latlong["AP"] = {
    //     "latitude": 35,
    //     "longitude": 105
    // };
    // latlong["AQ"] = {
    //     "latitude": -90,
    //     "longitude": 0
    // };
    // latlong["AR"] = {
    //     "latitude": -34,
    //     "longitude": -64
    // };
    // latlong["AS"] = {
    //     "latitude": -14.3333,
    //     "longitude": -170
    // };
    // latlong["AT"] = {
    //     "latitude": 47.3333,
    //     "longitude": 13.3333
    // };
    // latlong["AU"] = {
    //     "latitude": -27,
    //     "longitude": 133
    // };
    // latlong["AW"] = {
    //     "latitude": 12.5,
    //     "longitude": -69.9667
    // };
    // latlong["AZ"] = {
    //     "latitude": 40.5,
    //     "longitude": 47.5
    // };
    // latlong["BA"] = {
    //     "latitude": 44,
    //     "longitude": 18
    // };
    // latlong["BB"] = {
    //     "latitude": 13.1667,
    //     "longitude": -59.5333
    // };
    // latlong["BD"] = {
    //     "latitude": 24,
    //     "longitude": 90
    // };
    // latlong["BE"] = {
    //     "latitude": 50.8333,
    //     "longitude": 4
    // };
    // latlong["BF"] = {
    //     "latitude": 13,
    //     "longitude": -2
    // };
    // latlong["BG"] = {
    //     "latitude": 43,
    //     "longitude": 25
    // };
    // latlong["BH"] = {
    //     "latitude": 26,
    //     "longitude": 50.55
    // };
    // latlong["BI"] = {
    //     "latitude": -3.5,
    //     "longitude": 30
    // };
    // latlong["BJ"] = {
    //     "latitude": 9.5,
    //     "longitude": 2.25
    // };
    // latlong["BM"] = {
    //     "latitude": 32.3333,
    //     "longitude": -64.75
    // };
    // latlong["BN"] = {
    //     "latitude": 4.5,
    //     "longitude": 114.6667
    // };
    // latlong["BO"] = {
    //     "latitude": -17,
    //     "longitude": -65
    // };
    // latlong["BR"] = {
    //     "latitude": -10,
    //     "longitude": -55
    // };
    // latlong["BS"] = {
    //     "latitude": 24.25,
    //     "longitude": -76
    // };
    // latlong["BT"] = {
    //     "latitude": 27.5,
    //     "longitude": 90.5
    // };
    // latlong["BV"] = {
    //     "latitude": -54.4333,
    //     "longitude": 3.4
    // };
    // latlong["BW"] = {
    //     "latitude": -22,
    //     "longitude": 24
    // };
    // latlong["BY"] = {
    //     "latitude": 53,
    //     "longitude": 28
    // };
    // latlong["BZ"] = {
    //     "latitude": 17.25,
    //     "longitude": -88.75
    // };
    // latlong["CA"] = {
    //     "latitude": 54,
    //     "longitude": -100
    // };
    // latlong["CC"] = {
    //     "latitude": -12.5,
    //     "longitude": 96.8333
    // };
    // latlong["CD"] = {
    //     "latitude": 0,
    //     "longitude": 25
    // };
    // latlong["CF"] = {
    //     "latitude": 7,
    //     "longitude": 21
    // };
    // latlong["CG"] = {
    //     "latitude": -1,
    //     "longitude": 15
    // };
    // latlong["CH"] = {
    //     "latitude": 47,
    //     "longitude": 8
    // };
    // latlong["CI"] = {
    //     "latitude": 8,
    //     "longitude": -5
    // };
    // latlong["CK"] = {
    //     "latitude": -21.2333,
    //     "longitude": -159.7667
    // };
    // latlong["CL"] = {
    //     "latitude": -30,
    //     "longitude": -71
    // };
    // latlong["CM"] = {
    //     "latitude": 6,
    //     "longitude": 12
    // };
    // latlong["CN"] = {
    //     "latitude": 35,
    //     "longitude": 105
    // };
    // latlong["CO"] = {
    //     "latitude": 4,
    //     "longitude": -72
    // };
    // latlong["CR"] = {
    //     "latitude": 10,
    //     "longitude": -84
    // };
    // latlong["CU"] = {
    //     "latitude": 21.5,
    //     "longitude": -80
    // };
    // latlong["CV"] = {
    //     "latitude": 16,
    //     "longitude": -24
    // };
    // latlong["CX"] = {
    //     "latitude": -10.5,
    //     "longitude": 105.6667
    // };
    // latlong["CY"] = {
    //     "latitude": 35,
    //     "longitude": 33
    // };
    // latlong["CZ"] = {
    //     "latitude": 49.75,
    //     "longitude": 15.5
    // };
    // latlong["DE"] = {
    //     "latitude": 51,
    //     "longitude": 9
    // };
    // latlong["DJ"] = {
    //     "latitude": 11.5,
    //     "longitude": 43
    // };
    // latlong["DK"] = {
    //     "latitude": 56,
    //     "longitude": 10
    // };
    // latlong["DM"] = {
    //     "latitude": 15.4167,
    //     "longitude": -61.3333
    // };
    // latlong["DO"] = {
    //     "latitude": 19,
    //     "longitude": -70.6667
    // };
    // latlong["DZ"] = {
    //     "latitude": 28,
    //     "longitude": 3
    // };
    // latlong["EC"] = {
    //     "latitude": -2,
    //     "longitude": -77.5
    // };
    // latlong["EE"] = {
    //     "latitude": 59,
    //     "longitude": 26
    // };
    // latlong["EG"] = {
    //     "latitude": 27,
    //     "longitude": 30
    // };
    // latlong["EH"] = {
    //     "latitude": 24.5,
    //     "longitude": -13
    // };
    // latlong["ER"] = {
    //     "latitude": 15,
    //     "longitude": 39
    // };
    // latlong["ES"] = {
    //     "latitude": 40,
    //     "longitude": -4
    // };
    // latlong["ET"] = {
    //     "latitude": 8,
    //     "longitude": 38
    // };
    // latlong["EU"] = {
    //     "latitude": 47,
    //     "longitude": 8
    // };
    // latlong["FI"] = {
    //     "latitude": 62,
    //     "longitude": 26
    // };
    // latlong["FJ"] = {
    //     "latitude": -18,
    //     "longitude": 175
    // };
    // latlong["FK"] = {
    //     "latitude": -51.75,
    //     "longitude": -59
    // };
    // latlong["FM"] = {
    //     "latitude": 6.9167,
    //     "longitude": 158.25
    // };
    // latlong["FO"] = {
    //     "latitude": 62,
    //     "longitude": -7
    // };
    // latlong["FR"] = {
    //     "latitude": 46,
    //     "longitude": 2
    // };
    // latlong["GA"] = {
    //     "latitude": -1,
    //     "longitude": 11.75
    // };
    // latlong["GB"] = {
    //     "latitude": 54,
    //     "longitude": -2
    // };
    // latlong["GD"] = {
    //     "latitude": 12.1167,
    //     "longitude": -61.6667
    // };
    // latlong["GE"] = {
    //     "latitude": 42,
    //     "longitude": 43.5
    // };
    // latlong["GF"] = {
    //     "latitude": 4,
    //     "longitude": -53
    // };
    // latlong["GH"] = {
    //     "latitude": 8,
    //     "longitude": -2
    // };
    // latlong["GI"] = {
    //     "latitude": 36.1833,
    //     "longitude": -5.3667
    // };
    // latlong["GL"] = {
    //     "latitude": 72,
    //     "longitude": -40
    // };
    // latlong["GM"] = {
    //     "latitude": 13.4667,
    //     "longitude": -16.5667
    // };
    // latlong["GN"] = {
    //     "latitude": 11,
    //     "longitude": -10
    // };
    // latlong["GP"] = {
    //     "latitude": 16.25,
    //     "longitude": -61.5833
    // };
    // latlong["GQ"] = {
    //     "latitude": 2,
    //     "longitude": 10
    // };
    // latlong["GR"] = {
    //     "latitude": 39,
    //     "longitude": 22
    // };
    // latlong["GS"] = {
    //     "latitude": -54.5,
    //     "longitude": -37
    // };
    // latlong["GT"] = {
    //     "latitude": 15.5,
    //     "longitude": -90.25
    // };
    // latlong["GU"] = {
    //     "latitude": 13.4667,
    //     "longitude": 144.7833
    // };
    // latlong["GW"] = {
    //     "latitude": 12,
    //     "longitude": -15
    // };
    // latlong["GY"] = {
    //     "latitude": 5,
    //     "longitude": -59
    // };
    // latlong["HK"] = {
    //     "latitude": 22.25,
    //     "longitude": 114.1667
    // };
    // latlong["HM"] = {
    //     "latitude": -53.1,
    //     "longitude": 72.5167
    // };
    // latlong["HN"] = {
    //     "latitude": 15,
    //     "longitude": -86.5
    // };
    // latlong["HR"] = {
    //     "latitude": 45.1667,
    //     "longitude": 15.5
    // };
    // latlong["HT"] = {
    //     "latitude": 19,
    //     "longitude": -72.4167
    // };
    // latlong["HU"] = {
    //     "latitude": 47,
    //     "longitude": 20
    // };
    // latlong["ID"] = {
    //     "latitude": -5,
    //     "longitude": 120
    // };
    // latlong["IE"] = {
    //     "latitude": 53,
    //     "longitude": -8
    // };
    // latlong["IL"] = {
    //     "latitude": 31.5,
    //     "longitude": 34.75
    // };
    // latlong["IN"] = {
    //     "latitude": 20,
    //     "longitude": 77
    // };
    // latlong["IO"] = {
    //     "latitude": -6,
    //     "longitude": 71.5
    // };
    // latlong["IQ"] = {
    //     "latitude": 33,
    //     "longitude": 44
    // };
    // latlong["IR"] = {
    //     "latitude": 32,
    //     "longitude": 53
    // };
    // latlong["IS"] = {
    //     "latitude": 65,
    //     "longitude": -18
    // };
    // latlong["IT"] = {
    //     "latitude": 42.8333,
    //     "longitude": 12.8333
    // };
    // latlong["JM"] = {
    //     "latitude": 18.25,
    //     "longitude": -77.5
    // };
    // latlong["JO"] = {
    //     "latitude": 31,
    //     "longitude": 36
    // };
    // latlong["JP"] = {
    //     "latitude": 36,
    //     "longitude": 138
    // };
    // latlong["KE"] = {
    //     "latitude": 1,
    //     "longitude": 38
    // };
    // latlong["KG"] = {
    //     "latitude": 41,
    //     "longitude": 75
    // };
    // latlong["KH"] = {
    //     "latitude": 13,
    //     "longitude": 105
    // };
    // latlong["KI"] = {
    //     "latitude": 1.4167,
    //     "longitude": 173
    // };
    // latlong["KM"] = {
    //     "latitude": -12.1667,
    //     "longitude": 44.25
    // };
    // latlong["KN"] = {
    //     "latitude": 17.3333,
    //     "longitude": -62.75
    // };
    // latlong["KP"] = {
    //     "latitude": 40,
    //     "longitude": 127
    // };
    // latlong["KR"] = {
    //     "latitude": 37,
    //     "longitude": 127.5
    // };
    // latlong["KW"] = {
    //     "latitude": 29.3375,
    //     "longitude": 47.6581
    // };
    // latlong["KY"] = {
    //     "latitude": 19.5,
    //     "longitude": -80.5
    // };
    // latlong["KZ"] = {
    //     "latitude": 48,
    //     "longitude": 68
    // };
    // latlong["LA"] = {
    //     "latitude": 18,
    //     "longitude": 105
    // };
    // latlong["LB"] = {
    //     "latitude": 33.8333,
    //     "longitude": 35.8333
    // };
    // latlong["LC"] = {
    //     "latitude": 13.8833,
    //     "longitude": -61.1333
    // };
    // latlong["LI"] = {
    //     "latitude": 47.1667,
    //     "longitude": 9.5333
    // };
    // latlong["LK"] = {
    //     "latitude": 7,
    //     "longitude": 81
    // };
    // latlong["LR"] = {
    //     "latitude": 6.5,
    //     "longitude": -9.5
    // };
    // latlong["LS"] = {
    //     "latitude": -29.5,
    //     "longitude": 28.5
    // };
    // latlong["LT"] = {
    //     "latitude": 55,
    //     "longitude": 24
    // };
    // latlong["LU"] = {
    //     "latitude": 49.75,
    //     "longitude": 6
    // };
    // latlong["LV"] = {
    //     "latitude": 57,
    //     "longitude": 25
    // };
    // latlong["LY"] = {
    //     "latitude": 25,
    //     "longitude": 17
    // };
    // latlong["MA"] = {
    //     "latitude": 32,
    //     "longitude": -5
    // };
    // latlong["MC"] = {
    //     "latitude": 43.7333,
    //     "longitude": 7.4
    // };
    // latlong["MD"] = {
    //     "latitude": 47,
    //     "longitude": 29
    // };
    // latlong["ME"] = {
    //     "latitude": 42.5,
    //     "longitude": 19.4
    // };
    // latlong["MG"] = {
    //     "latitude": -20,
    //     "longitude": 47
    // };
    // latlong["MH"] = {
    //     "latitude": 9,
    //     "longitude": 168
    // };
    // latlong["MK"] = {
    //     "latitude": 41.8333,
    //     "longitude": 22
    // };
    // latlong["ML"] = {
    //     "latitude": 17,
    //     "longitude": -4
    // };
    // latlong["MM"] = {
    //     "latitude": 22,
    //     "longitude": 98
    // };
    // latlong["MN"] = {
    //     "latitude": 46,
    //     "longitude": 105
    // };
    // latlong["MO"] = {
    //     "latitude": 22.1667,
    //     "longitude": 113.55
    // };
    // latlong["MP"] = {
    //     "latitude": 15.2,
    //     "longitude": 145.75
    // };
    // latlong["MQ"] = {
    //     "latitude": 14.6667,
    //     "longitude": -61
    // };
    // latlong["MR"] = {
    //     "latitude": 20,
    //     "longitude": -12
    // };
    // latlong["MS"] = {
    //     "latitude": 16.75,
    //     "longitude": -62.2
    // };
    // latlong["MT"] = {
    //     "latitude": 35.8333,
    //     "longitude": 14.5833
    // };
    // latlong["MU"] = {
    //     "latitude": -20.2833,
    //     "longitude": 57.55
    // };
    // latlong["MV"] = {
    //     "latitude": 3.25,
    //     "longitude": 73
    // };
    // latlong["MW"] = {
    //     "latitude": -13.5,
    //     "longitude": 34
    // };
    // latlong["MX"] = {
    //     "latitude": 23,
    //     "longitude": -102
    // };
    // latlong["MY"] = {
    //     "latitude": 2.5,
    //     "longitude": 112.5
    // };
    // latlong["MZ"] = {
    //     "latitude": -18.25,
    //     "longitude": 35
    // };
    // latlong["NA"] = {
    //     "latitude": -22,
    //     "longitude": 17
    // };
    // latlong["NC"] = {
    //     "latitude": -21.5,
    //     "longitude": 165.5
    // };
    // latlong["NE"] = {
    //     "latitude": 16,
    //     "longitude": 8
    // };
    // latlong["NF"] = {
    //     "latitude": -29.0333,
    //     "longitude": 167.95
    // };
    // latlong["NG"] = {
    //     "latitude": 10,
    //     "longitude": 8
    // };
    // latlong["NI"] = {
    //     "latitude": 13,
    //     "longitude": -85
    // };
    // latlong["NL"] = {
    //     "latitude": 52.5,
    //     "longitude": 5.75
    // };
    // latlong["NO"] = {
    //     "latitude": 62,
    //     "longitude": 10
    // };
    // latlong["NP"] = {
    //     "latitude": 28,
    //     "longitude": 84
    // };
    // latlong["NR"] = {
    //     "latitude": -0.5333,
    //     "longitude": 166.9167
    // };
    // latlong["NU"] = {
    //     "latitude": -19.0333,
    //     "longitude": -169.8667
    // };
    // latlong["NZ"] = {
    //     "latitude": -41,
    //     "longitude": 174
    // };
    // latlong["OM"] = {
    //     "latitude": 21,
    //     "longitude": 57
    // };
    // latlong["PA"] = {
    //     "latitude": 9,
    //     "longitude": -80
    // };
    // latlong["PE"] = {
    //     "latitude": -10,
    //     "longitude": -76
    // };
    // latlong["PF"] = {
    //     "latitude": -15,
    //     "longitude": -140
    // };
    // latlong["PG"] = {
    //     "latitude": -6,
    //     "longitude": 147
    // };
    // latlong["PH"] = {
    //     "latitude": 13,
    //     "longitude": 122
    // };
    // latlong["PK"] = {
    //     "latitude": 30,
    //     "longitude": 70
    // };
    // latlong["PL"] = {
    //     "latitude": 52,
    //     "longitude": 20
    // };
    // latlong["PM"] = {
    //     "latitude": 46.8333,
    //     "longitude": -56.3333
    // };
    // latlong["PR"] = {
    //     "latitude": 18.25,
    //     "longitude": -66.5
    // };
    // latlong["PS"] = {
    //     "latitude": 32,
    //     "longitude": 35.25
    // };
    // latlong["PT"] = {
    //     "latitude": 39.5,
    //     "longitude": -8
    // };
    // latlong["PW"] = {
    //     "latitude": 7.5,
    //     "longitude": 134.5
    // };
    // latlong["PY"] = {
    //     "latitude": -23,
    //     "longitude": -58
    // };
    // latlong["QA"] = {
    //     "latitude": 25.5,
    //     "longitude": 51.25
    // };
    // latlong["RE"] = {
    //     "latitude": -21.1,
    //     "longitude": 55.6
    // };
    // latlong["RO"] = {
    //     "latitude": 46,
    //     "longitude": 25
    // };
    // latlong["RS"] = {
    //     "latitude": 44,
    //     "longitude": 21
    // };
    // latlong["RU"] = {
    //     "latitude": 60,
    //     "longitude": 100
    // };
    // latlong["RW"] = {
    //     "latitude": -2,
    //     "longitude": 30
    // };
    // latlong["SA"] = {
    //     "latitude": 25,
    //     "longitude": 45
    // };
    // latlong["SB"] = {
    //     "latitude": -8,
    //     "longitude": 159
    // };
    // latlong["SC"] = {
    //     "latitude": -4.5833,
    //     "longitude": 55.6667
    // };
    // latlong["SD"] = {
    //     "latitude": 15,
    //     "longitude": 30
    // };
    // latlong["SE"] = {
    //     "latitude": 62,
    //     "longitude": 15
    // };
    // latlong["SG"] = {
    //     "latitude": 1.3667,
    //     "longitude": 103.8
    // };
    // latlong["SH"] = {
    //     "latitude": -15.9333,
    //     "longitude": -5.7
    // };
    // latlong["SI"] = {
    //     "latitude": 46,
    //     "longitude": 15
    // };
    // latlong["SJ"] = {
    //     "latitude": 78,
    //     "longitude": 20
    // };
    // latlong["SK"] = {
    //     "latitude": 48.6667,
    //     "longitude": 19.5
    // };
    // latlong["SL"] = {
    //     "latitude": 8.5,
    //     "longitude": -11.5
    // };
    // latlong["SM"] = {
    //     "latitude": 43.7667,
    //     "longitude": 12.4167
    // };
    // latlong["SN"] = {
    //     "latitude": 14,
    //     "longitude": -14
    // };
    // latlong["SO"] = {
    //     "latitude": 10,
    //     "longitude": 49
    // };
    // latlong["SR"] = {
    //     "latitude": 4,
    //     "longitude": -56
    // };
    // latlong["ST"] = {
    //     "latitude": 1,
    //     "longitude": 7
    // };
    // latlong["SV"] = {
    //     "latitude": 13.8333,
    //     "longitude": -88.9167
    // };
    // latlong["SY"] = {
    //     "latitude": 35,
    //     "longitude": 38
    // };
    // latlong["SZ"] = {
    //     "latitude": -26.5,
    //     "longitude": 31.5
    // };
    // latlong["TC"] = {
    //     "latitude": 21.75,
    //     "longitude": -71.5833
    // };
    // latlong["TD"] = {
    //     "latitude": 15,
    //     "longitude": 19
    // };
    // latlong["TF"] = {
    //     "latitude": -43,
    //     "longitude": 67
    // };
    // latlong["TG"] = {
    //     "latitude": 8,
    //     "longitude": 1.1667
    // };
    // latlong["TH"] = {
    //     "latitude": 15,
    //     "longitude": 100
    // };
    // latlong["TJ"] = {
    //     "latitude": 39,
    //     "longitude": 71
    // };
    // latlong["TK"] = {
    //     "latitude": -9,
    //     "longitude": -172
    // };
    // latlong["TM"] = {
    //     "latitude": 40,
    //     "longitude": 60
    // };
    // latlong["TN"] = {
    //     "latitude": 34,
    //     "longitude": 9
    // };
    // latlong["TO"] = {
    //     "latitude": -20,
    //     "longitude": -175
    // };
    // latlong["TR"] = {
    //     "latitude": 39,
    //     "longitude": 35
    // };
    // latlong["TT"] = {
    //     "latitude": 11,
    //     "longitude": -61
    // };
    // latlong["TV"] = {
    //     "latitude": -8,
    //     "longitude": 178
    // };
    // latlong["TW"] = {
    //     "latitude": 23.5,
    //     "longitude": 121
    // };
    // latlong["TZ"] = {
    //     "latitude": -6,
    //     "longitude": 35
    // };
    // latlong["UA"] = {
    //     "latitude": 49,
    //     "longitude": 32
    // };
    // latlong["UG"] = {
    //     "latitude": 1,
    //     "longitude": 32
    // };
    // latlong["UM"] = {
    //     "latitude": 19.2833,
    //     "longitude": 166.6
    // };
    // latlong["US"] = {
    //     "latitude": 38,
    //     "longitude": -97
    // };
    // latlong["UY"] = {
    //     "latitude": -33,
    //     "longitude": -56
    // };
    // latlong["UZ"] = {
    //     "latitude": 41,
    //     "longitude": 64
    // };
    // latlong["VA"] = {
    //     "latitude": 41.9,
    //     "longitude": 12.45
    // };
    // latlong["VC"] = {
    //     "latitude": 13.25,
    //     "longitude": -61.2
    // };
    // latlong["VE"] = {
    //     "latitude": 8,
    //     "longitude": -66
    // };
    // latlong["VG"] = {
    //     "latitude": 18.5,
    //     "longitude": -64.5
    // };
    // latlong["VI"] = {
    //     "latitude": 18.3333,
    //     "longitude": -64.8333
    // };
    // latlong["VN"] = {
    //     "latitude": 16,
    //     "longitude": 106
    // };
    // latlong["VU"] = {
    //     "latitude": -16,
    //     "longitude": 167
    // };
    // latlong["WF"] = {
    //     "latitude": -13.3,
    //     "longitude": -176.2
    // };
    // latlong["WS"] = {
    //     "latitude": -13.5833,
    //     "longitude": -172.3333
    // };
    // latlong["YE"] = {
    //     "latitude": 15,
    //     "longitude": 48
    // };
    // latlong["YT"] = {
    //     "latitude": -12.8333,
    //     "longitude": 45.1667
    // };
    // latlong["ZA"] = {
    //     "latitude": -29,
    //     "longitude": 24
    // };
    // latlong["ZM"] = {
    //     "latitude": -15,
    //     "longitude": 30
    // };
    // latlong["ZW"] = {
    //     "latitude": -20,
    //     "longitude": 30
    // };

    // var mapData = [{
    //     "code": "AF",
    //     "name": "Afghanistan",
    //     "value": 32358260,
    //     "color": "#eea638"
    // }, {
    //     "code": "AL",
    //     "name": "Albania",
    //     "value": 3215988,
    //     "color": "#d8854f"
    // }, {
    //     "code": "DZ",
    //     "name": "Algeria",
    //     "value": 35980193,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "AO",
    //     "name": "Angola",
    //     "value": 19618432,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "AR",
    //     "name": "Argentina",
    //     "value": 40764561,
    //     "color": "#86a965"
    // }, {
    //     "code": "AM",
    //     "name": "Armenia",
    //     "value": 3100236,
    //     "color": "#d8854f"
    // }, {
    //     "code": "AU",
    //     "name": "Australia",
    //     "value": 22605732,
    //     "color": "#8aabb0"
    // }, {
    //     "code": "AT",
    //     "name": "Austria",
    //     "value": 8413429,
    //     "color": "#d8854f"
    // }, {
    //     "code": "AZ",
    //     "name": "Azerbaijan",
    //     "value": 9306023,
    //     "color": "#d8854f"
    // }, {
    //     "code": "BH",
    //     "name": "Bahrain",
    //     "value": 1323535,
    //     "color": "#eea638"
    // }, {
    //     "code": "BD",
    //     "name": "Bangladesh",
    //     "value": 150493658,
    //     "color": "#eea638"
    // }, {
    //     "code": "BY",
    //     "name": "Belarus",
    //     "value": 9559441,
    //     "color": "#d8854f"
    // }, {
    //     "code": "BE",
    //     "name": "Belgium",
    //     "value": 10754056,
    //     "color": "#d8854f"
    // }, {
    //     "code": "BJ",
    //     "name": "Benin",
    //     "value": 9099922,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "BT",
    //     "name": "Bhutan",
    //     "value": 738267,
    //     "color": "#eea638"
    // }, {
    //     "code": "BO",
    //     "name": "Bolivia",
    //     "value": 10088108,
    //     "color": "#86a965"
    // }, {
    //     "code": "BA",
    //     "name": "Bosnia and Herzegovina",
    //     "value": 3752228,
    //     "color": "#d8854f"
    // }, {
    //     "code": "BW",
    //     "name": "Botswana",
    //     "value": 2030738,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "BR",
    //     "name": "Brazil",
    //     "value": 196655014,
    //     "color": "#86a965"
    // }, {
    //     "code": "BN",
    //     "name": "Brunei",
    //     "value": 405938,
    //     "color": "#eea638"
    // }, {
    //     "code": "BG",
    //     "name": "Bulgaria",
    //     "value": 7446135,
    //     "color": "#d8854f"
    // }, {
    //     "code": "BF",
    //     "name": "Burkina Faso",
    //     "value": 16967845,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "BI",
    //     "name": "Burundi",
    //     "value": 8575172,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "KH",
    //     "name": "Cambodia",
    //     "value": 14305183,
    //     "color": "#eea638"
    // }, {
    //     "code": "CM",
    //     "name": "Cameroon",
    //     "value": 20030362,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CA",
    //     "name": "Canada",
    //     "value": 34349561,
    //     "color": "#a7a737"
    // }, {
    //     "code": "CV",
    //     "name": "Cape Verde",
    //     "value": 500585,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CF",
    //     "name": "Central African Rep.",
    //     "value": 4486837,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "TD",
    //     "name": "Chad",
    //     "value": 11525496,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CL",
    //     "name": "Chile",
    //     "value": 17269525,
    //     "color": "#86a965"
    // }, {
    //     "code": "CN",
    //     "name": "China",
    //     "value": 1347565324,
    //     "color": "#eea638"
    // }, {
    //     "code": "CO",
    //     "name": "Colombia",
    //     "value": 46927125,
    //     "color": "#86a965"
    // }, {
    //     "code": "KM",
    //     "name": "Comoros",
    //     "value": 753943,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CD",
    //     "name": "Congo, Dem. Rep.",
    //     "value": 67757577,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CG",
    //     "name": "Congo, Rep.",
    //     "value": 4139748,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "CR",
    //     "name": "Costa Rica",
    //     "value": 4726575,
    //     "color": "#a7a737"
    // }, {
    //     "code": "CI",
    //     "name": "Cote d'Ivoire",
    //     "value": 20152894,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "HR",
    //     "name": "Croatia",
    //     "value": 4395560,
    //     "color": "#d8854f"
    // }, {
    //     "code": "CU",
    //     "name": "Cuba",
    //     "value": 11253665,
    //     "color": "#a7a737"
    // }, {
    //     "code": "CY",
    //     "name": "Cyprus",
    //     "value": 1116564,
    //     "color": "#d8854f"
    // }, {
    //     "code": "CZ",
    //     "name": "Czech Rep.",
    //     "value": 10534293,
    //     "color": "#d8854f"
    // }, {
    //     "code": "DK",
    //     "name": "Denmark",
    //     "value": 5572594,
    //     "color": "#d8854f"
    // }, {
    //     "code": "DJ",
    //     "name": "Djibouti",
    //     "value": 905564,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "DO",
    //     "name": "Dominican Rep.",
    //     "value": 10056181,
    //     "color": "#a7a737"
    // }, {
    //     "code": "EC",
    //     "name": "Ecuador",
    //     "value": 14666055,
    //     "color": "#86a965"
    // }, {
    //     "code": "EG",
    //     "name": "Egypt",
    //     "value": 82536770,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "SV",
    //     "name": "El Salvador",
    //     "value": 6227491,
    //     "color": "#a7a737"
    // }, {
    //     "code": "GQ",
    //     "name": "Equatorial Guinea",
    //     "value": 720213,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "ER",
    //     "name": "Eritrea",
    //     "value": 5415280,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "EE",
    //     "name": "Estonia",
    //     "value": 1340537,
    //     "color": "#d8854f"
    // }, {
    //     "code": "ET",
    //     "name": "Ethiopia",
    //     "value": 84734262,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "FJ",
    //     "name": "Fiji",
    //     "value": 868406,
    //     "color": "#8aabb0"
    // }, {
    //     "code": "FI",
    //     "name": "Finland",
    //     "value": 5384770,
    //     "color": "#d8854f"
    // }, {
    //     "code": "FR",
    //     "name": "France",
    //     "value": 63125894,
    //     "color": "#d8854f"
    // }, {
    //     "code": "GA",
    //     "name": "Gabon",
    //     "value": 1534262,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "GM",
    //     "name": "Gambia",
    //     "value": 1776103,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "GE",
    //     "name": "Georgia",
    //     "value": 4329026,
    //     "color": "#d8854f"
    // }, {
    //     "code": "DE",
    //     "name": "Germany",
    //     "value": 82162512,
    //     "color": "#d8854f"
    // }, {
    //     "code": "GH",
    //     "name": "Ghana",
    //     "value": 24965816,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "GR",
    //     "name": "Greece",
    //     "value": 11390031,
    //     "color": "#d8854f"
    // }, {
    //     "code": "GT",
    //     "name": "Guatemala",
    //     "value": 14757316,
    //     "color": "#a7a737"
    // }, {
    //     "code": "GN",
    //     "name": "Guinea",
    //     "value": 10221808,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "GW",
    //     "name": "Guinea-Bissau",
    //     "value": 1547061,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "GY",
    //     "name": "Guyana",
    //     "value": 756040,
    //     "color": "#86a965"
    // }, {
    //     "code": "HT",
    //     "name": "Haiti",
    //     "value": 10123787,
    //     "color": "#a7a737"
    // }, {
    //     "code": "HN",
    //     "name": "Honduras",
    //     "value": 7754687,
    //     "color": "#a7a737"
    // }, {
    //     "code": "HK",
    //     "name": "Hong Kong, China",
    //     "value": 7122187,
    //     "color": "#eea638"
    // }, {
    //     "code": "HU",
    //     "name": "Hungary",
    //     "value": 9966116,
    //     "color": "#d8854f"
    // }, {
    //     "code": "IS",
    //     "name": "Iceland",
    //     "value": 324366,
    //     "color": "#d8854f"
    // }, {
    //     "code": "IN",
    //     "name": "India",
    //     "value": 1241491960,
    //     "color": "#eea638"
    // }, {
    //     "code": "ID",
    //     "name": "Indonesia",
    //     "value": 242325638,
    //     "color": "#eea638"
    // }, {
    //     "code": "IR",
    //     "name": "Iran",
    //     "value": 74798599,
    //     "color": "#eea638"
    // }, {
    //     "code": "IQ",
    //     "name": "Iraq",
    //     "value": 32664942,
    //     "color": "#eea638"
    // }, {
    //     "code": "IE",
    //     "name": "Ireland",
    //     "value": 4525802,
    //     "color": "#d8854f"
    // }, {
    //     "code": "IL",
    //     "name": "Israel",
    //     "value": 7562194,
    //     "color": "#eea638"
    // }, {
    //     "code": "IT",
    //     "name": "Italy",
    //     "value": 60788694,
    //     "color": "#d8854f"
    // }, {
    //     "code": "JM",
    //     "name": "Jamaica",
    //     "value": 2751273,
    //     "color": "#a7a737"
    // }, {
    //     "code": "JP",
    //     "name": "Japan",
    //     "value": 126497241,
    //     "color": "#eea638"
    // }, {
    //     "code": "JO",
    //     "name": "Jordan",
    //     "value": 6330169,
    //     "color": "#eea638"
    // }, {
    //     "code": "KZ",
    //     "name": "Kazakhstan",
    //     "value": 16206750,
    //     "color": "#eea638"
    // }, {
    //     "code": "KE",
    //     "name": "Kenya",
    //     "value": 41609728,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "KP",
    //     "name": "Korea, Dem. Rep.",
    //     "value": 24451285,
    //     "color": "#eea638"
    // }, {
    //     "code": "KR",
    //     "name": "Korea, Rep.",
    //     "value": 48391343,
    //     "color": "#eea638"
    // }, {
    //     "code": "KW",
    //     "name": "Kuwait",
    //     "value": 2818042,
    //     "color": "#eea638"
    // }, {
    //     "code": "KG",
    //     "name": "Kyrgyzstan",
    //     "value": 5392580,
    //     "color": "#eea638"
    // }, {
    //     "code": "LA",
    //     "name": "Laos",
    //     "value": 6288037,
    //     "color": "#eea638"
    // }, {
    //     "code": "LV",
    //     "name": "Latvia",
    //     "value": 2243142,
    //     "color": "#d8854f"
    // }, {
    //     "code": "LB",
    //     "name": "Lebanon",
    //     "value": 4259405,
    //     "color": "#eea638"
    // }, {
    //     "code": "LS",
    //     "name": "Lesotho",
    //     "value": 2193843,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "LR",
    //     "name": "Liberia",
    //     "value": 4128572,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "LY",
    //     "name": "Libya",
    //     "value": 6422772,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "LT",
    //     "name": "Lithuania",
    //     "value": 3307481,
    //     "color": "#d8854f"
    // }, {
    //     "code": "LU",
    //     "name": "Luxembourg",
    //     "value": 515941,
    //     "color": "#d8854f"
    // }, {
    //     "code": "MK",
    //     "name": "Macedonia, FYR",
    //     "value": 2063893,
    //     "color": "#d8854f"
    // }, {
    //     "code": "MG",
    //     "name": "Madagascar",
    //     "value": 21315135,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MW",
    //     "name": "Malawi",
    //     "value": 15380888,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MY",
    //     "name": "Malaysia",
    //     "value": 28859154,
    //     "color": "#eea638"
    // }, {
    //     "code": "ML",
    //     "name": "Mali",
    //     "value": 15839538,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MR",
    //     "name": "Mauritania",
    //     "value": 3541540,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MU",
    //     "name": "Mauritius",
    //     "value": 1306593,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MX",
    //     "name": "Mexico",
    //     "value": 114793341,
    //     "color": "#a7a737"
    // }, {
    //     "code": "MD",
    //     "name": "Moldova",
    //     "value": 3544864,
    //     "color": "#d8854f"
    // }, {
    //     "code": "MN",
    //     "name": "Mongolia",
    //     "value": 2800114,
    //     "color": "#eea638"
    // }, {
    //     "code": "ME",
    //     "name": "Montenegro",
    //     "value": 632261,
    //     "color": "#d8854f"
    // }, {
    //     "code": "MA",
    //     "name": "Morocco",
    //     "value": 32272974,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MZ",
    //     "name": "Mozambique",
    //     "value": 23929708,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "MM",
    //     "name": "Myanmar",
    //     "value": 48336763,
    //     "color": "#eea638"
    // }, {
    //     "code": "NA",
    //     "name": "Namibia",
    //     "value": 2324004,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "NP",
    //     "name": "Nepal",
    //     "value": 30485798,
    //     "color": "#eea638"
    // }, {
    //     "code": "NL",
    //     "name": "Netherlands",
    //     "value": 16664746,
    //     "color": "#d8854f"
    // }, {
    //     "code": "NZ",
    //     "name": "New Zealand",
    //     "value": 4414509,
    //     "color": "#8aabb0"
    // }, {
    //     "code": "NI",
    //     "name": "Nicaragua",
    //     "value": 5869859,
    //     "color": "#a7a737"
    // }, {
    //     "code": "NE",
    //     "name": "Niger",
    //     "value": 16068994,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "NG",
    //     "name": "Nigeria",
    //     "value": 162470737,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "NO",
    //     "name": "Norway",
    //     "value": 4924848,
    //     "color": "#d8854f"
    // }, {
    //     "code": "OM",
    //     "name": "Oman",
    //     "value": 2846145,
    //     "color": "#eea638"
    // }, {
    //     "code": "PK",
    //     "name": "Pakistan",
    //     "value": 176745364,
    //     "color": "#eea638"
    // }, {
    //     "code": "PA",
    //     "name": "Panama",
    //     "value": 3571185,
    //     "color": "#a7a737"
    // }, {
    //     "code": "PG",
    //     "name": "Papua New Guinea",
    //     "value": 7013829,
    //     "color": "#8aabb0"
    // }, {
    //     "code": "PY",
    //     "name": "Paraguay",
    //     "value": 6568290,
    //     "color": "#86a965"
    // }, {
    //     "code": "PE",
    //     "name": "Peru",
    //     "value": 29399817,
    //     "color": "#86a965"
    // }, {
    //     "code": "PH",
    //     "name": "Philippines",
    //     "value": 94852030,
    //     "color": "#eea638"
    // }, {
    //     "code": "PL",
    //     "name": "Poland",
    //     "value": 38298949,
    //     "color": "#d8854f"
    // }, {
    //     "code": "PT",
    //     "name": "Portugal",
    //     "value": 10689663,
    //     "color": "#d8854f"
    // }, {
    //     "code": "PR",
    //     "name": "Puerto Rico",
    //     "value": 3745526,
    //     "color": "#a7a737"
    // }, {
    //     "code": "QA",
    //     "name": "Qatar",
    //     "value": 1870041,
    //     "color": "#eea638"
    // }, {
    //     "code": "RO",
    //     "name": "Romania",
    //     "value": 21436495,
    //     "color": "#d8854f"
    // }, {
    //     "code": "RU",
    //     "name": "Russia",
    //     "value": 142835555,
    //     "color": "#d8854f"
    // }, {
    //     "code": "RW",
    //     "name": "Rwanda",
    //     "value": 10942950,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "SA",
    //     "name": "Saudi Arabia",
    //     "value": 28082541,
    //     "color": "#eea638"
    // }, {
    //     "code": "SN",
    //     "name": "Senegal",
    //     "value": 12767556,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "RS",
    //     "name": "Serbia",
    //     "value": 9853969,
    //     "color": "#d8854f"
    // }, {
    //     "code": "SL",
    //     "name": "Sierra Leone",
    //     "value": 5997486,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "SG",
    //     "name": "Singapore",
    //     "value": 5187933,
    //     "color": "#eea638"
    // }, {
    //     "code": "SK",
    //     "name": "Slovak Republic",
    //     "value": 5471502,
    //     "color": "#d8854f"
    // }, {
    //     "code": "SI",
    //     "name": "Slovenia",
    //     "value": 2035012,
    //     "color": "#d8854f"
    // }, {
    //     "code": "SB",
    //     "name": "Solomon Islands",
    //     "value": 552267,
    //     "color": "#8aabb0"
    // }, {
    //     "code": "SO",
    //     "name": "Somalia",
    //     "value": 9556873,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "ZA",
    //     "name": "South Africa",
    //     "value": 50459978,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "ES",
    //     "name": "Spain",
    //     "value": 46454895,
    //     "color": "#d8854f"
    // }, {
    //     "code": "LK",
    //     "name": "Sri Lanka",
    //     "value": 21045394,
    //     "color": "#eea638"
    // }, {
    //     "code": "SD",
    //     "name": "Sudan",
    //     "value": 34735288,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "SR",
    //     "name": "Suriname",
    //     "value": 529419,
    //     "color": "#86a965"
    // }, {
    //     "code": "SZ",
    //     "name": "Swaziland",
    //     "value": 1203330,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "SE",
    //     "name": "Sweden",
    //     "value": 9440747,
    //     "color": "#d8854f"
    // }, {
    //     "code": "CH",
    //     "name": "Switzerland",
    //     "value": 7701690,
    //     "color": "#d8854f"
    // }, {
    //     "code": "SY",
    //     "name": "Syria",
    //     "value": 20766037,
    //     "color": "#eea638"
    // }, {
    //     "code": "TW",
    //     "name": "Taiwan",
    //     "value": 23072000,
    //     "color": "#eea638"
    // }, {
    //     "code": "TJ",
    //     "name": "Tajikistan",
    //     "value": 6976958,
    //     "color": "#eea638"
    // }, {
    //     "code": "TZ",
    //     "name": "Tanzania",
    //     "value": 46218486,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "TH",
    //     "name": "Thailand",
    //     "value": 69518555,
    //     "color": "#eea638"
    // }, {
    //     "code": "TG",
    //     "name": "Togo",
    //     "value": 6154813,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "TT",
    //     "name": "Trinidad and Tobago",
    //     "value": 1346350,
    //     "color": "#a7a737"
    // }, {
    //     "code": "TN",
    //     "name": "Tunisia",
    //     "value": 10594057,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "TR",
    //     "name": "Turkey",
    //     "value": 73639596,
    //     "color": "#d8854f"
    // }, {
    //     "code": "TM",
    //     "name": "Turkmenistan",
    //     "value": 5105301,
    //     "color": "#eea638"
    // }, {
    //     "code": "UG",
    //     "name": "Uganda",
    //     "value": 34509205,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "UA",
    //     "name": "Ukraine",
    //     "value": 45190180,
    //     "color": "#d8854f"
    // }, {
    //     "code": "AE",
    //     "name": "United Arab Emirates",
    //     "value": 7890924,
    //     "color": "#eea638"
    // }, {
    //     "code": "GB",
    //     "name": "United Kingdom",
    //     "value": 62417431,
    //     "color": "#d8854f"
    // }, {
    //     "code": "US",
    //     "name": "United States",
    //     "value": 313085380,
    //     "color": "#a7a737"
    // }, {
    //     "code": "UY",
    //     "name": "Uruguay",
    //     "value": 3380008,
    //     "color": "#86a965"
    // }, {
    //     "code": "UZ",
    //     "name": "Uzbekistan",
    //     "value": 27760267,
    //     "color": "#eea638"
    // }, {
    //     "code": "VE",
    //     "name": "Venezuela",
    //     "value": 29436891,
    //     "color": "#86a965"
    // }, {
    //     "code": "PS",
    //     "name": "West Bank and Gaza",
    //     "value": 4152369,
    //     "color": "#eea638"
    // }, {
    //     "code": "VN",
    //     "name": "Vietnam",
    //     "value": 88791996,
    //     "color": "#eea638"
    // }, {
    //     "code": "YE",
    //     "name": "Yemen, Rep.",
    //     "value": 24799880,
    //     "color": "#eea638"
    // }, {
    //     "code": "ZM",
    //     "name": "Zambia",
    //     "value": 13474959,
    //     "color": "#de4c4f"
    // }, {
    //     "code": "ZW",
    //     "name": "Zimbabwe",
    //     "value": 12754378,
    //     "color": "#de4c4f"
    // }];


    // var map;
    // var minBulletSize = 3;
    // var maxBulletSize = 70;
    // var min = Infinity;
    // var max = -Infinity;


    // // get min and max values
    // for (var i = 0; i < mapData.length; i++) {
    //     var value = mapData[i].value;
    //     if (value < min) {
    //         min = value;
    //     }
    //     if (value > max) {
    //         max = value;
    //     }
    // }

    // // build map
    // AmCharts.ready(function() {
    //     AmCharts.theme = AmCharts.themes.dark;
    //     map = new AmCharts.AmMap();
    //     map.pathToImages = App.getGlobalPluginsPath() + "amcharts/ammap/images/",

    //     map.fontFamily = 'Open Sans';
    //     map.fontSize = '13';
    //     map.color = '#888';
        
    //     //map.addTitle("Population of the World in 2011", 14);
    //     //map.addTitle("source: Gapminder", 11);
    //     map.areasSettings = {
    //         unlistedAreasColor: "#000000",
    //         unlistedAreasAlpha: 0.1
    //     };
    //     map.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>";

    //     var dataProvider = {
    //         mapVar: AmCharts.maps.worldLow,
    //         images: []
    //     }

    //     // create circle for each country
    //     for (var i = 0; i < mapData.length; i++) {
    //         var dataItem = mapData[i];
    //         var value = dataItem.value;
    //         // calculate size of a bubble
    //         var size = (value - min) / (max - min) * (maxBulletSize - minBulletSize) + minBulletSize;
    //         if (size < minBulletSize) {
    //             size = minBulletSize;
    //         }
    //         var id = dataItem.code;

    //         dataProvider.images.push({
    //             type: "circle",
    //             width: size,
    //             height: size,
    //             color: dataItem.color,
    //             longitude: latlong[id].longitude,
    //             latitude: latlong[id].latitude,
    //             title: dataItem.name,
    //             value: value
    //         });
    //     }

    //     map.dataProvider = dataProvider;

    //     map.write("chart_10");
    // });

    // $('#chart_10').closest('.portlet').find('.fullscreen').click(function() {
    //     map.invalidateSize();
    // });
    
});
</script>
@endsection    