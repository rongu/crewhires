<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailService extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = $this->data['email'];
        $subject = 'Request Service from Crewhires Website';
        $name = $this->data['name'];

        return $this->view('emails-service')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'psd' => $this->data['message'],'tgl' => $this->data['tgl'], 'name' => $this->data['name'], 'phone' => $this->data['phone'] ]);
    }
}
