<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \App\Models\RoleUser;
use \App\Models\User;
use \App\Models\CrewDoc;
use \App\Models\CrewInfo;
use \App\Models\AgencyJobs;
use \App\Models\HomeSlide;
use \App\Models\CrewApply;
use Auth,Alert,DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            $role = RoleUser::where('user_id',auth()->user()->id)->first();

            if ($role->role_id === 2) {
                
                if(Auth::user()->profile=='proses'){
                    return redirect()->route('set.profile.crew');    
                }
                

            }else if ($role->role_id === 4) {
                
                if(Auth::user()->profile=='proses'){
                    return redirect()->route('set.profile.agency');   
                }
            }else if ($role->role_id === 1) {
                return redirect()->route('backend.home');   
            }    
        }
        
         $imgslide = HomeSlide::get();
         return view('welcome',compact('imgslide'));
    }

    public function membership()
    {

        
        return view('frontend.pricing');
    }

    public function categories()
    {
        if(Auth::check()){
            $role_info = RoleUser::where('user_id',auth()->user()->id)->first();
            $role = $role_info->role_id;
            $id = auth()->user()->id;
        }else{
            $role = 0;
            $id = 0;
        }

        $users_info = CrewInfo::select('users.status_account')->leftjoin('users','users.id','crew_info.users_id')->where('crew_info.users_id',$id)->first();

        if($users_info){
            $status_account = $users_info->status_account;
        }else{
            $status_account = 'free';
        }

        $crew = CrewInfo::select('users_id','fullname','status_account','type_crew','status_activity','photo')->leftjoin('users','users.id','crew_info.users_id')->get();
        $jobs = AgencyJobs::select('agency_jobs.id as id','position','date_open','types','engine','requirement')->leftjoin('users','users.id','agency_jobs.users_id')->where('agency_jobs.status','open')->get();
        
        return view('frontend.categories',compact('crew','role','jobs','status_account','id'));
    }

    public function termCondition()
    {
        return view('frontend.term-condition');
    }    

    public function faq()
    {
        return view('frontend.faq');
    }    

    public function service()
    {
        return view('frontend.service');
    }    

    public function jobsForm()
    {
        return view('frontend.jobs-form');
    }    

    public function socialMedia()
    {
        return view('frontend.social-media');
    }

    public function pageSystemComingSoon()
    {
        return view('page-system-coming-soon');
    }

    public function profile()
    {

        $role = RoleUser::where('user_id',auth()->user()->id)->first();

        if($role->role_id == 2){//crew
            return redirect()->route('crew.profile');
        }else if($role->role_id == 4){//agency
            return redirect()->route('agency.profile');
        } 
        
    }
    
    public function crewAllList()
    {

        $codena = CrewInfo::select('users_id','fullname','status_account','type_crew','status_activity','photo','job_desc')->leftjoin('users','users.id','crew_info.users_id')->get();
        
        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

            return $codena->fullname;

        })->addColumn('photo', function($codena) {

            return $codena->photo;                

        })->addColumn('type_crew', function($codena) {

            return $codena->type_crew;                

        })->addColumn('job_description', function($codena) {

            return $codena->job_desc;                

        })->addColumn('status_activity', function($codena) {

            return $codena->status_activity;                

        })->addColumn('id', function($codena) {

            return [$codena->users_id,$codena->fullname,$codena->type_crew,$codena->status_activity];                

        })->toJson();
    }

    public function crewVacancyList()
    {
        $codena = AgencyJobs::select('agency_jobs.id as id','position','date_open','date_close','types','engine','requirement','users.photo','agency_info.companyname','agency_info.address')->leftjoin('users','users.id','agency_jobs.users_id')->leftjoin('agency_info','agency_info.users_id','agency_jobs.users_id')->where('agency_jobs.status','open')->get();
        

        return datatables()->of($codena)->addIndexColumn()->addColumn('id', function($codena) {

                return [$codena->id,$codena->position,$codena->date_open,$codena->types,$codena->engine,$codena->requirement,$codena->date_close,$codena->photo,$codena->companyname,$codena->address];                

            })->toJson();
    }

    public function crewApply(Request $request){

        if(Auth::user()->isCrew()){
            $day_apply = CrewApply::where('users_id',auth()->user()->id)->where('date_add',now())->count();

            $max = DB::table('settingan')->where('desc', '=', 'max_apply')->first();

            if ($day_apply <= $max->val){
                Alert::warning('Your maximum daily application limit has expired', 'Please upgrade your membership, Thank you.');
            }else{
                $ca = CrewApply::where('users_id', auth()->user()->id)->where('agency_jobs_id', $request->id)->doesntExist();
                if($ca){
                    $cd = new CrewApply();
                    $cd->users_id = auth()->user()->id;
                    $cd->agency_jobs_id = $request->id;
                    $cd->date_add = date('Y-m-d');
                    $cd->apply_status = 'ADD';
                    $cd->apply_by = 'USER';
                    $cd->save();  
                    Alert::success('Thank you', 'Thank you, Please wait for the next information');
                }else{
                    Alert::warning('You has apply this job', 'Thank you, you can no longer apply');
                }
            }
        }else{
            Alert::warning('Sorry', "agency can't apply job");
        }

        return redirect()->back();
    }    
}
