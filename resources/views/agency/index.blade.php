@extends('layouts.agency')

@section('assets-top')
    <link href="{{ asset('../../public/assets/pages/css/about.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .page-header .page-header-menu {
            background: #F3C200;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #F3C200;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #f3c200;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #9c7e04;
        }
    </style>

@endsection
@section('content')

<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Dashboard
                <small>dashboard us page</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            <!-- BEGIN THEME PANEL -->
            <div class="btn-group btn-theme-panel">
                <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-settings"></i>
                </a>
                <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>THEME COLORS</h3>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <ul class="theme-colors">
                                        <li class="theme-color theme-color-default" data-theme="default">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Default</span>
                                        </li>
                                        <li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Blue Hoki</span>
                                        </li>
                                        <li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Blue Steel</span>
                                        </li>
                                        <li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Orange</span>
                                        </li>
                                        <li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Yellow Crusta</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <ul class="theme-colors">
                                        <li class="theme-color theme-color-green-haze" data-theme="green-haze">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Green Haze</span>
                                        </li>
                                        <li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Red Sunglo</span>
                                        </li>
                                        <li class="theme-color theme-color-red-intense" data-theme="red-intense">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Red Intense</span>
                                        </li>
                                        <li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Purple Plum</span>
                                        </li>
                                        <li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Purple Studio</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 seperator">
                            <h3>LAYOUT</h3>
                            <ul class="theme-settings">
                                <li> Theme Style
                                    <select class="theme-setting theme-setting-style form-control input-sm input-small input-inline tooltips" data-original-title="Change theme style" data-container="body" data-placement="left">
                                        <option value="boxed" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </li>
                                <li> Layout
                                    <select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
                                        <option value="boxed" selected="selected">Boxed</option>
                                        <option value="fluid">Fluid</option>
                                    </select>
                                </li>
                                <li> Top Menu Style
                                    <select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body"
                                        data-placement="left">
                                        <option value="dark" selected="selected">Dark</option>
                                        <option value="light">Light</option>
                                    </select>
                                </li>
                                <li> Top Menu Mode
                                    <select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body"
                                        data-placement="left">
                                        <option value="fixed">Fixed</option>
                                        <option value="not-fixed" selected="selected">Not Fixed</option>
                                    </select>
                                </li>
                                <li> Mega Menu Style
                                    <select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body"
                                        data-placement="left">
                                        <option value="dark" selected="selected">Dark</option>
                                        <option value="light">Light</option>
                                    </select>
                                </li>
                                <li> Mega Menu Mode
                                    <select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body"
                                        data-placement="left">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="not-fixed">Not Fixed</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END THEME PANEL -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Pages</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>General</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <!-- BEGIN CONTENT HEADER -->
            <div class="row margin-bottom-40 about-header">
                <div class="col-md-12">
                    <h1>About Us</h1>
                    <h2>Life is either a great adventure or nothing</h2>
                    <button type="button" class="btn btn-danger">JOIN US TODAY</button>
                </div>
            </div>
            <!-- END CONTENT HEADER -->
            <!-- BEGIN CARDS -->
            <div class="row margin-bottom-20">
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="card-icon">
                            <i class="icon-user-follow font-red-sunglo theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> Best User Expierence </span>
                        </div>
                        <div class="card-desc">
                            <span> The best way to find yourself is
                                <br> to lose yourself in the service of others </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="card-icon">
                            <i class="icon-trophy font-green-haze theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> Awards Winner </span>
                        </div>
                        <div class="card-desc">
                            <span> The best way to find yourself is
                                <br> to lose yourself in the service of others </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="card-icon">
                            <i class="icon-basket font-purple-wisteria theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> eCommerce Components </span>
                        </div>
                        <div class="card-desc">
                            <span> The best way to find yourself is
                                <br> to lose yourself in the service of others </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="card-icon">
                            <i class="icon-layers font-blue theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> Adaptive Components </span>
                        </div>
                        <div class="card-desc">
                            <span> The best way to find yourself is
                                <br> to lose yourself in the service of others </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CARDS -->
            <!-- BEGIN TEXT & VIDEO -->
            <div class="row margin-bottom-40">
                <div class="col-lg-6">
                    <div class="portlet light about-text">
                        <h4>
                            <i class="fa fa-check icon-info"></i> About Metronic</h4>
                        <p class="margin-top-20"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
                            ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis
                            at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
                        <div class="row">
                            <div class="col-xs-6">
                                <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                    <li>
                                        <i class="fa fa-check"></i> Nam liber tempor cum soluta </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Lorem ipsum dolor sit amet </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                    <li>
                                        <i class="fa fa-check"></i> Nam liber tempor cum soluta </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Lorem ipsum dolor sit amet </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                </ul>
                            </div>
                        </div>
                        <div class="about-quote">
                            <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</h3>
                            <p class="about-author">Tom Hardy, 2015</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <iframe src="http://player.vimeo.com/video/22439234" style="width:100%; height:500px;border:0" allowfullscreen> </iframe>
                </div>
            </div>
            <!-- END TEXT & VIDEO -->
            <!-- BEGIN MEMBERS SUCCESS STORIES -->
            <div class="row margin-bottom-40 stories-header" data-auto-height="true">
                <div class="col-md-12">
                    <h1>Members Success Stories</h1>
                    <h2>Life is either a great adventure or nothing</h2>
                </div>
            </div>
            <div class="row margin-bottom-20 stories-cont">
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ asset('../../public/assets/pages/media/users/teambg1.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Mark Wahlberg </span>
                        </div>
                        <div class="desc">
                            <span> We are at our very best, and we are happiest, when we are fully engaged in work we enjoy on the journey toward the goal we've established for ourselves. </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ asset('../../public/assets/pages/media/users/teambg2.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Lindsay Lohan </span>
                        </div>
                        <div class="desc">
                            <span> Do what you love to do and give it your very best. Whether it's business or baseball, or the theater, or any field. If you don't love what you're doing and you can't give it your best, get out of
                                it. </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ asset('../../public/assets/pages/media/users/teambg5.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> John Travolta </span>
                        </div>
                        <div class="desc">
                            <span> To be nobody but yourself in a world which is doing its best, to make you everybody else means to fight the hardest battle which any human being can fight; and never stop fighting. </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ asset('../../public/assets/pages/media/users/teambg8.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Tom Brady </span>
                        </div>
                        <div class="desc">
                            <span> You have to accept whatever comes and the only important thing is that you meet it with courage and with the best that you have to give. Never give up, never surrender. Go all out or gain nothing.
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-40 stories-footer">
                <div class="col-md-12">
                    <button type="button" class="btn btn-danger">SEE MORE STORIES</button>
                </div>
            </div>
            <!-- END MEMBERS SUCCESS STORIES -->
            <!-- BEGIN LINKS BLOCK -->
            <div class="row about-links-cont" data-auto-height="true">
                <div class="col-md-6 about-links">
                    <div class="row">
                        <div class="col-sm-6 about-links-item">
                            <h4>UX & Design</h4>
                            <ul>
                                <li>
                                    <a href="#">Ui Features</a>
                                </li>
                                <li>
                                    <a href="#">Ui Components</a>
                                </li>
                                <li>
                                    <a href="#">Flat UI Colors</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 about-links-item">
                            <h4>eCommerce</h4>
                            <ul>
                                <li>
                                    <a href="#">Dashboard</a>
                                </li>
                                <li>
                                    <a href="#">Orders</a>
                                </li>
                                <li>
                                    <a href="#">Products</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 about-links-item">
                            <h4>Page Layouts</h4>
                            <ul>
                                <li>
                                    <a href="#">Boxed Page</a>
                                </li>
                                <li>
                                    <a href="#">Full Width</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 about-links-item">
                            <h4>Form Stuff</h4>
                            <ul>
                                <li>
                                    <a href="#">Material Forms</a>
                                </li>
                                <li>
                                    <a href="#">Form Wizard</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 about-links-item">
                            <h4>Charts</h4>
                            <ul>
                                <li>
                                    <a href="#">amChart</a>
                                </li>
                                <li>
                                    <a href="#">Flotchart</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 about-links-item">
                            <h4>Portlets</h4>
                            <ul>
                                <li>
                                    <a href="#">General Portlets</a>
                                </li>
                                <li>
                                    <a href="#">Ajax Portlets</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="about-image" style="background: url({{ asset('../../public/assets/pages/media/works/img4.jpg') }}) center no-repeat;"></div>
                </div>
            </div>
            <!-- END LINKS BLOCK -->
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {


    
});
</script>
@endsection    