<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Profile Agency | {{ config('app.name', 'Crewhires') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #1 for blank page layout"
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/layouts/layout/css/themes/light.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" /> 
        <style type="text/css">
            .page-content-wrapper .page-content{
                margin-left: 0px;
            }

            .select2-container--bootstrap .select2-selection {
                -webkit-box-shadow: inherit;
                box-shadow: inset inherit;
                background-color: inherit;
                border: 1px solid #c2cad8;
                border-radius: 4px;
                color: #555;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                outline: 0;
                border-left: 0px;
                border-right: 0px;
                border-top: 0px;
            }

            .nav>li>a {
                padding: 10px 0px 10px 5px;
            }

            .mt-repeater .mt-repeater-item {
                border-bottom: 3px dashed #4c87b9;
            }

            .form-wizard .steps>li>a.step>.number {
                background-color: #eee;
                text-align: center!important;
                padding: 9px 13px 13px;
                margin-right: 2px;
                height: 40px;
                width: 40px;
                -webkit-border-radius: 50%!important;
                -moz-border-radius: 50%!important;
                border-radius: 50%!important;
            }
        </style>
        
    
    </head>


    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-boxed">
        <div class="page-wrapper">
            
            <div class="clearfix"> </div>
            
            <div class="container">
                
                <div class="page-container">
                    
                    <div class="page-content-wrapper">
                        
                        <div class="page-content">
                            
                            <div class="portlet light portlet-form" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Setup Profile <span id="titleheader_id">Agency / Ship Owner</span> -
                                            <span class="step-title"> Step 1 of 5 </span>
                                        </span>
                                    </div>
                                    
                                </div>
                                <div class="portlet-body form">

                                    <form id="submit_form" role="form" action="{{route('profile.create.wizard.agency')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <ul class="nav nav-pills nav-justified steps">
                                                    <li>
                                                        <a href="#tab1" data-toggle="tab" class="step">
                                                            <span class="number"> 1 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Type Setup </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab2" data-toggle="tab" class="step">
                                                            <span class="number"> 2 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Profile Setup </span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#tab4" data-toggle="tab" class="step">
                                                            <span class="number"> 3 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Job List </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab5" data-toggle="tab" class="step">
                                                            <span class="number"> 4 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Sosmed </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab6" data-toggle="tab" class="step">
                                                            <span class="number"> 5 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Confirm </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div id="bar" class="progress progress-striped" role="progressbar">
                                                    <div class="progress-bar progress-bar-success bg-blue-soft bg-font-blue-soft"> </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="alert alert-danger display-none">
                                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                    <div class="alert alert-success display-none">
                                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Provide your type account details</h3>                                                        


                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Type Account
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <select name="type_agency" id="type_agency_id" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="AGENCY">AGENCY</option>
                                                                    <option value="SHIP OWNER">SHIP OWNER</option>
                                                                </select>
                                                                <span class="help-block"> Provide your type agency </span>
                                                            </div>
                                                        </div>

                                                    
                                                        
                                                    </div>
                                                    <div class="tab-pane" id="tab2">
                                                        <h3 class="block">Provide your profile details</h3>
                                                        
                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Photo
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn red btn-outline btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input id="upload" type="file" name="file"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Username
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="name" value="{{$user->name}}" />
                                                                <span class="help-block"> Provide your username </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Company Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="companyname" value="" />
                                                                <span class="help-block"> Provide your Company Name </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Email
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="email" value="{{$user->email}}"/>
                                                                <span class="help-block"> Provide your email </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-2">
                                                                <select name="phone_code" id="phone_code_id" class="form-control" data-placeholder="">
                                                                    <option></option>
                                                                    @foreach(json_decode($calling, true) as $key => $value)
                                                                    <option value="{{$value['calling_code']}}">+{{$value['calling_code']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="phone" value="{{$user->phone}}"/>
                                                                <span class="help-block"> Provide your phone number </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Address
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="address" rows="4"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">City/Town
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="city" />
                                                                <span class="help-block"> Provide your city or town </span>
                                                            </div>
                                                        </div>                                                        

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Country
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <select name="country" id="country_list" class="form-control" data-placeholder="">
                                                                    <option></option>
                                                                    @foreach(json_decode($ctr, true) as $key => $value)
                                                                        <option value="{{$value['country']}}">{{$value['country']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">PIC
                                                                 <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="pic" />
                                                                <span class="help-block"> Provide your PIC </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">PIC Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-2">
                                                                <select name="pic_phone_code" id="pic_phone_code_id" class="form-control" data-placeholder="">
                                                                    <option></option>
                                                                    @foreach(json_decode($calling, true) as $key => $value)
                                                                    <option value="{{$value['calling_code']}}">+{{$value['calling_code']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="pic_phone"/>
                                                                <span class="help-block"> Provide your PIC phone number </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Document
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn red btn-outline btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input  type="file" name="file_doc"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Portofolio
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn red btn-outline btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input  type="file" name="file_portofolio"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">SIUP
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn red btn-outline btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input  type="file" name="file_siup"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="formsiuppak_id" class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">SIUPPAK
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn red btn-outline btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input  type="file" name="file_siuppak"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>


                                                    <div class="tab-pane" id="tab4">
                                                        <h3 class="block">Job list Setup</h3>
                                                        
                                                        
                                                        <div class="form-group form-md-line-input" style="background-color: #FFFFFF;">
                                                            <label class="control-label col-md-12" style="text-align: left;">Jobs
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-12">
                                                                

                                                                
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="jl">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Position</label>
                                                                                    <input name="jl_po" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Type Vessel</label>
                                                                                    <input name="jl_ty" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Engine</label>
                                                                                    <input name="jl_en" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-12">
                                                                                    <label class="control-label">Requirement</label>
                                                                                    <!-- <input name="jl_re" type="text" class="form-control" />  -->
                                                                                    <textarea class="wysihtml5 form-control" rows="6" name="jl_re"></textarea>
                                                                                </div>              
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Duration Recruit</label>
                                                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                    <input name="jl_open" type="text" class="form-control">
                                                                                    <span class="input-group-addon"> to </span>
                                                                                    <input name="jl_close" type="text" class="form-control"></div>
                                                                                </div>
                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i> Add </a>
                                                                </div>


                                                            </div>
                                                        </div>

                                                        

                                                    </div>

                                                    <div class="tab-pane" id="tab5">
                                                        <h3 class="block">Social Media <span id="titlesosmed_id">Agency</span></h3>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Facebook
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="fb" placeholder="@account name" />
                                                                <span class="help-block"> Provide your facebook </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Instagram
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="ig" placeholder="@account name" />
                                                                <span class="help-block"> Provide your Instagram </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Telegram
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="tg" placeholder="Phone Number" />
                                                                <span class="help-block"> Provide your Telegram </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">Twitter
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="tw" placeholder="@account name" />
                                                                <span class="help-block"> Provide your Twitter </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            <label class="control-label col-md-4">WhatsApp
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="wa" placeholder="Phone Number" />
                                                                <span class="help-block"> Provide your WhatsApp </span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="tab-pane" id="tab6">
                                                        <h3 class="block">Confirm your account</h3>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Type Account:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="type_agency"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Photo:</label>
                                                            <div class="col-md-4">
                                                                <div class="fileinput fileinput-exists"> 
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                                                        <img id="imgview">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="name"> </p>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Company Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="companyname"> </p>
                                                            </div>
                                                        </div>
                                                        

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Email:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="email"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Phone:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="phone_code"> </p>
                                                                <p class="form-control-static" data-display="phone"> </p>
                                                            </div>
                                                        </div>

  
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Address:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="address"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="city"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="country"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">PIC:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="pic"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">PIC Phone Number:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="pic_phone_code"> </p>
                                                                <p class="form-control-static" data-display="pic_phone"> </p>
                                                            </div>
                                                        </div>
                                                        

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back </a>
                                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                        <button href="javascript:;" class="btn green button-submit" type="submit"> Submit
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                   
                </div>
                
            </div>
            <div class="container">

                <div class="page-footer">
                    <div class="page-footer-inner"> &nbsp;
                    </div>
                    
                </div>

            </div>
        </div>

        
        
        

        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll-master/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>

        <script type="text/javascript">

        $(document).ready(function() {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            if (!jQuery().wysihtml5) {
                return;
            }



            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
                //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
            }



            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,

                width: 'auto', 
                
                escapeMarkup: function (m) {
                    return m;
                }
            });            

            $("#phone_code_id").select2({
                placeholder: "Select",
                allowClear: true,

                width: 'auto', 
                
                escapeMarkup: function (m) {
                    return m;
                }
            });            

            $("#pic_phone_code_id").select2({
                placeholder: "Select",
                allowClear: true,

                width: 'auto', 
                
                escapeMarkup: function (m) {
                    return m;
                }
            });



            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //type
                    type_agency: {
                        required: true
                    },
                   
                    //profile
                    file: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    companyname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    //status
                    pic: {
                        required: true
                    },
                    pic_phone: {
                        required: true
                    },
                    phone_code: {
                        required: true
                    },
                    pic_phone_code: {
                        required: true
                    },

                    file_doc: {
                        required: true
                    },
                    file_portofolio: {
                        required: true
                    },
                    file_siup: {
                        required: true
                    },
                    
                    
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab6 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    console.dir(input);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }

                    if (input.is(":text") || input.is("textarea")) {

                        $(this).html(input.val());

                    } else if (input.is("select")) {

                        $(this).html(input.find('option:selected').text());

                    } else if (input.is(":radio") && input.is(":checked")) {

                        $(this).html(input.attr("data-title"));

                    } else if ($(this).attr("data-display") == 'payment[]') {

                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));

                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;

                $("#titleheader_id").text($("#type_agency_id").val());
                $("#titlesosmed_id").text($("#type_agency_id").val());
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    console.log(index);

                    if(index==1){
                        var ty = $("#type_agency_id").val();
                        if(ty=='AGENCY'){
                            $("#formsiuppak_id").show();
                            
                        }else{
                            $("#formsiuppak_id").hide();
                            
                        }
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                //alert('Finished! Hope you like it :)');
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });



            $('.mt-repeater').each(function(){
                $(this).repeater({
                    show: function () {

                        $(this).slideDown();  


                        $(".date-picker").datepicker();
                        $('.wysihtml5').summernote({height: 300});
                        // if ($('.wysihtml5').size() > 0) {
                        //     $('.wysihtml5').wysihtml5({
                        //     "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
                        //     });
                        // }

                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {
                        $('.wysihtml5').summernote({height: 300});
                        // if ($('.wysihtml5').size() > 0) {
                        //     $('.wysihtml5').wysihtml5({
                        //     "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
                        //     });
                        // }  
                    }

                });
            });

            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            $('#phone_code_id', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            $('#pic_phone_code_id', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            $('#upload').change(function(){
                var input = this;
                var url = $(this).val();
                
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                console.log(ext);
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "jfif")) 
                 {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                       $('#imgview').attr('src', e.target.result);
                    }
                   reader.readAsDataURL(input.files[0]);
                }
                else
                {
                  //$('#img').attr('src', '/assets/no_preview.png');
                }
              });

        });

            
        </script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
    </body>

</html>