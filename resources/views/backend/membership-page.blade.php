@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }

    .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Membership</a>

        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Membership
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>INV Number</th>
                            <th>Account Name</th>                            
                            <th>Phone Number</th>                            
                            <th>Date</th>                            
                            <th>Payment Date</th>                            
                            <th>&nbsp;</th>                            
                            <th>&nbsp;</th>                            
                            <th>Bank Account</th>                            
                            <th>Payment</th>                            
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>

<div id="modalPayment" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form role="form" action="{{route('backend.membership.payment')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="id" type="hidden" class="form-control" id="id_id" readonly="readonly">
        <input name="users_id" type="hidden" class="form-control" id="users_id" readonly="readonly">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation Payment</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="inv" type="text" class="form-control" id="inv_id" readonly="readonly">
                        <label for="inv_id">{{ __('payment.Invoice Number')}}</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <select id="status_id" name="status" class="form-control select2">
                            <option></option>
                           
                            <option value="done">Done</option>
                            <option value="cancel">Cancel</option>

                            
                        </select>
                        <label for="status_id">Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

   ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.membership.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [10] },
            { searchable : false},                

            { render: function(data, type, row, meta) {

                if(data==='verification'){
                    var nn = 'grey-cararra';
                }else if(data==='done'){
                    var nn = 'blue-soft';
                }else if(data==='pay'){
                    var nn = 'green-seagreen';
                }else if(data==='cancel'){
                    var nn = 'yellow-mint';
                }

                return '<a href="javascript:;" class="btn '+nn+' uppercase btn-block" style="cursor: inherit;">'+data+'</a>'; }, targets: [6]
            },
            { render: function(data, type, row, meta) {
                if(data){
                    return '<img src="{{url('public/images/inv_member')}}/'+data+'" height="40px" class="zoomimg">'; 
                }else{
                    return '<img src="{{url('public/images/no_image.png')}}" height="40px" class="zoomimg">'; 
                }
                
                }, targets: [7]
            },{ render: function(data, type, row, meta) {
                return '<a href="javascript:;" data-id="'+data[0]+'" data-users_id="'+data[1]+'" data-inv="'+data[2]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [10]
            },
           
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'code_member', name: 'code_member' },            
            { data: 'name', name: 'name' },
            { data: 'phone', name: 'phone' },
            { data: 'date_add', name: 'date_add' },
            { data: 'date_paid', name: 'date_paid' },
            { data: 'status', name: 'status' },
            { data: 'photo_receipt', name: 'photo_receipt' },
            { data: 'bank_account', name: 'bank_account' },
            { data: 'memberpay', name: 'memberpay' },
            { data: 'users_id', name: 'users_id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var users_id = e.currentTarget.dataset.users_id;
        var inv = e.currentTarget.dataset.inv;

        console.log(id, users_id);
        $("#id_id").val(id);
        $("#inv_id").val(inv);
        $("#users_id").val(users_id);
        $("#modalPayment").modal('show');
        
    });

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);

    $("#status_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });
})

</script>
@endsection    