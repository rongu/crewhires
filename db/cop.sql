-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Feb 2021 pada 07.07
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crewhires`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cop`
--

CREATE TABLE `cop` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','general','deck departement','engine departement') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cop`
--

INSERT INTO `cop` (`id`, `name`, `codes`, `types`, `created_at`, `updated_at`) VALUES
(1, 'None', '', 'none', '2021-02-28 05:51:19', '2021-02-28 05:51:26'),
(2, 'Basic Safety Training', 'BST', 'general', '2021-02-28 05:51:24', '2021-02-28 05:51:29'),
(3, 'Proficiency Survival Craft and Rescue Boat', 'SCRB', 'general', '2021-02-28 05:51:36', '2021-02-28 05:51:36'),
(4, 'Advanced Fire Fighting', 'AFF', 'general', '2021-02-28 05:52:05', '2021-02-28 05:52:05'),
(5, 'Medical First Aid', 'MFA', 'general', '2021-02-28 05:52:30', '2021-02-28 05:52:30'),
(6, 'Medical Care Onboard Ship', 'MC', 'general', '2021-02-28 05:52:58', '2021-02-28 05:52:58'),
(7, 'Security Awareness Training', 'SAT', 'general', '2021-02-28 05:53:22', '2021-02-28 05:53:22'),
(8, 'Seaferer With Designated Security Duties', 'SDSD', 'general', '2021-02-28 05:53:54', '2021-02-28 05:53:54'),
(9, 'Ship Security Officer', 'SSO', 'general', '2021-02-28 05:54:21', '2021-02-28 05:54:21'),
(10, 'Bridge Resource Management', 'BRM', 'deck departement', '2021-02-28 05:54:55', '2021-02-28 05:54:55'),
(11, 'ARPA Simulator', '', 'deck departement', '2021-02-28 05:55:21', '2021-02-28 05:55:21'),
(12, 'RADAR Simulator', '', 'deck departement', '2021-02-28 05:55:42', '2021-02-28 05:55:42'),
(13, 'Electronic Chart Display', 'ECDIS', 'deck departement', '2021-02-28 05:56:08', '2021-02-28 05:56:08'),
(14, 'Global Maritime Distress Safety System', 'GMDSS', 'deck departement', '2021-02-28 05:56:55', '2021-02-28 05:56:55'),
(15, 'General Operator Certificate', 'GOC Book', 'deck departement', '2021-02-28 05:57:20', '2021-02-28 05:57:20'),
(16, 'Basic Training for Liquified Gas Tanker Cargo Operations', 'BLGT', 'general', '2021-02-28 05:58:07', '2021-02-28 05:58:07'),
(17, 'Advanced Training for Liquified Gas Tanker Cargo Operations', 'ALGT', 'general', '2021-02-28 05:59:17', '2021-02-28 05:59:17'),
(18, 'Basic Training for Oil and Chemical Tanker Cargo Operations', 'BOCT', 'general', '2021-02-28 06:00:16', '2021-02-28 06:00:16'),
(19, 'DP Basic', '', 'deck departement', '2021-02-28 06:01:07', '2021-02-28 06:01:07'),
(20, 'DP Advanced', '', 'deck departement', '2021-02-28 06:01:27', '2021-02-28 06:01:27'),
(21, 'Full DP', '', 'deck departement', '2021-02-28 06:01:50', '2021-02-28 06:01:50'),
(22, 'DP Maintenance', '', 'engine departement', '2021-02-28 06:02:05', '2021-02-28 06:02:05'),
(23, 'Engine Resources Management', 'ERM', 'engine departement', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(24, 'Marine High Voltage', 'MHV', 'deck departement', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(25, 'Crowd Management', 'CM', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(26, 'Food Handling', '', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(27, 'Food Safety', '', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(28, 'Helicopter Under Escape Training', 'HUET', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(29, 'Basic Offshore Safety Induction & Emergency Training', 'BOSIET', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39'),
(30, 'International Safety Management Code', 'ISM Code', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `cop`
--
ALTER TABLE `cop`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cop`
--
ALTER TABLE `cop`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
