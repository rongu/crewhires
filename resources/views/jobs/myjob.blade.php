

@extends('layouts.frontend')
@section('title','My job')
@section('desc','Halaman ini menyatakan my job')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')

    <link href="{{ url('public/assets/pages/css/search.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }
        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>My Job
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>My Job</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="search-page search-content-1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-container ">
                                    <ul>
                                        @foreach($jb as $vl)
                                            <li class="search-item clearfix">
                                                <a href="javascriptt:;">
                                                    @if($vl->agencyphoto)
                                                        <img src="{{asset('../../public/images/profile')}}/{{$vl->agencyphoto}}" />
                                                    @else
                                                        <img src="{{asset('../../public/images/no_image.png')}}" />
                                                    @endif

                                                    
                                                </a>
                                                <div class="search-content">
                                                    <h2 class="search-title">
                                                        <a href="javascript:;">{{$vl->position}}</a>
                                                    </h2>
                                                    <p class="search-desc">{!!$vl->requirement!!}</p>
                                                    <div>
                                                        <ul class="list-inline" style="padding-bottom: 13px;">                                                           
                                                                
                                                            <li>
                                                                Type : <span class="label label-info"> {{$vl->types}} </span></li>

                                                            <li>
                                                                Engine: <span class="label label-info"> {{$vl->engine}} </span></li>                                                            
                                                        </ul>
                                                    </div>
                                                    <div>
                                                        <ul class="list-inline" style="padding-bottom: 13px;">
                                                            <li>
                                                                Apply Status : <span class="label label-success"> {{$vl->apply_status}} </span></li>

                                                            
                                                        </ul>
                                                    </div>
                                                    <div>
                                                        <ul class="list-inline">
                                                            <li>
                                                                <i class="fa fa-user"></i> {{$vl->agencyname}} </li>
                                                            <li>
                                                                <i class="fa fa-calendar-times-o"></i> {{Carbon::parse($vl->date_close)->format('m/d/Y')}}</li>
                                                            <li>
                                                                <i class="fa fa-briefcase"></i> {{$vl->jmlcount}} </li>
                                                            
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                    <div class="search-pagination">
                                        <ul class="pagination">
                                            <li class="page-active">
                                                <a href="javascriptt:;"> 1 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 2 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 3 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 4 </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    