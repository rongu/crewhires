@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Frontend</a>

        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Frontend
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-photo"></i>
            <span class="caption-subject bold uppercase"> Slider</span>

        </div>
        
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Images</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-5">
                <div class="caption" style="margin-bottom: 10px">
                <span class="caption-subject bold uppercase">Form Input</span>

                </div>
                <form id="submit_form_slider" role="form" action="{{route('backend.frontend.create.slider')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}                
                <input type="hidden" class="form-control" id="slide_id" name="id" value="0">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" id="title_id" name="title">
                        <label for="title_id">Title</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>

                    <div class="form-group form-md-line-input">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            

                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                
                                <img  id="img_ex" src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                
                            </div>   
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                         
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="file"> </span>
                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green">Submit</button>
                                <a id="delete_btn" class="btn red-mint btn-outline sbold uppercase btn-sm">Delete</a> 
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                </form>
            </div>
           
        </div>
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

    $("#delete_btn").hide();

    var form_s = $('#submit_form_slider');
    var error = $('.alert-danger', form_s);
    var success = $('.alert-success', form_s);

    form_s.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            title: {
                required: true
            },
            

            
        },
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_s[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

   ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.frontend.list.slider')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    //scrollCollapse: true,        
    responsive:     false,  
    stateSave:      false,
    columnDefs: [
            { orderable: false, targets: [3] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                return '<a href="javascript:;" data-id="'+data[0]+'" data-title="'+data[1]+'" data-images="'+data[2]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [3]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/slide')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [2]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'title', name: 'title' },
            { data: 'images', name: 'images' },
            { data: 'images_id', name: 'images_id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    // lengthMenu: [
    //     [10, 15, 20, -1],
    //     [10, 15, 20, "All"] // change per page values here
    // ],
    // // set the initial value
    // pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $("#sample_3").on('click','.edit_btn',function(e){
        var id = e.currentTarget.dataset.id;
        var title = e.currentTarget.dataset.title;
        var images = e.currentTarget.dataset.images;
        
        if(images!="null"){

            var gmb = "{{ url('public/images/slide') }}/"+images;
            $("#img_ex").attr('src',gmb); 

        }else{
            var gmb = "{{ url('public/images/no_image.png') }}";
            $("#img_ex").attr('src',gmb); 
        }

        $("#slide_id").val(id);
        $("#title_id").val(title);
        $("#delete_btn").show();
    });

    $("#delete_btn").on('click',function(e){
        var id = $("#slide_id").val();
        console.log(id);
        //console.log(sa_btnClass);
        swal({
          title: "Apakah akan menghapus data Slide Ini?",
          text: "informasi Slide akan hilang dari web, apakah anda yakin akan melakukan proses ini...",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Ya, Setuju hapus",
          cancelButtonText: "Tidak, Batal hapus",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('backend/frontend-destroy-slider') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            if(data){
                                swal("Terimakasih", "Anda telah menghapus data", "success");
                            }else{
                                swal("Maaf!!!", "User Tidak Memiliki akses Hapus", "error");
                            }
                            location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Batal", "Anda membatalkan proses hapus Employee...", "error");
                }
        });
    });

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
})

</script>
@endsection    