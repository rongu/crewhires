@extends('layouts.backend')




@section('assets-top')
<link href="{{ url('public/assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Agency / Ship Owner</a>

        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Agency / Ship Owner
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <div class="portlet-title">
        <div class="actions">
            <div class="btn-group">
                <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-list-ul"></i> <span id="typ_id" class="text-capitalize">-</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:;" class="btn-filter-type" data-typ="all">
                            <i class="fa fa-list-ul"></i> All </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-filter-type" data-typ="agency">
                            <i class="fa fa-briefcase"></i> Agency </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-filter-type" data-typ="shipowner">
                            <i class="fa fa-ship"></i> Ship Owner </a>
                    </li>
                    
                </ul>
            </div>
            
        </div>    
    </div>
    

    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Account Name</th>
                            <th>Company Name</th>
                            <th>Type</th>
                            <th>Status Account</th>
                            <th>Logo</th>
                            <th>SIUP</th>
                            <th>SIUPPAK</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>


<div class="modal fade bs-modal-lg" id="modal-view" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Profile</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img id="v_image_id" src="{{url('public/images/profile.png')}}" class="img-responsive zoomimg" alt="" style="height: 150px;" title="Nama" >  </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> <span id="v_name_id">Name</span> </div>
                                <img id="v_img_bnd_id" src="{{url('public/images/no_image.png')}}" style="width: 70px;border: 1px solid #cdcdcd;">
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <button type="button" class="btn blue-ebonyclay btn-sm"><span id="v_type_agency_id">MARINE</span></button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
<!--                                 <ul class="nav">
        
                                    <li class="active">
                                        <a href="#">
                                            <i class="fa fa-file-text-o"></i> Curriculum Vitae </a>
                                    </li>

                                </ul> -->
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                        <!-- PORTLET MAIN -->
                        <div class="portlet light ">
                            <!-- STAT -->
                            <div class="row list-separated profile-stat">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 1 </div>
                                    <div class="uppercase profile-stat-text"> Apply </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 100 </div>
                                    <div class="uppercase profile-stat-text"> Agent </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 12500 </div>
                                    <div class="uppercase profile-stat-text"> Crew </div>
                                </div>
                            </div>
                            <!-- END STAT -->
                            <div>
                                <h3 class="profile-desc-title">Social Media</h3>
                                
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.facebook.com/"><span id="v_fb_id">fb</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-instagram fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.instagram.com/"><span id="v_ig_id">ig</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-send-o fa-sm font-blue-ebonyclay"></i>
                                    <a href="#"><span id="v_tg_id">tg</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.twitter.com/"><span id="v_tw_id">tw</span></a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-whatsapp fa-sm font-blue-ebonyclay"></i>
                                    <a href="#"><span id="v_wa_id">wa</span></a>
                                </div>                                
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                        <!-- END BEGIN PROFILE SIDEBAR -->
                        <!-- BEGIN PROFILE CONTENT -->
                        <div class="profile-content">
                            <div class="row">
                                <div class="col-md-11" style="    margin-left: 25px;margin-top: 2px;">
                                    <div class="portlet light ">
                                        <div class="portlet-title tabbable-line">
                                            <div class="caption caption-md">
                                                <i class="icon-globe theme-font hide"></i>
                                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                            </div>
 
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Company Name</label>
                                                <input id="v_c_name_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Email</label>
                                                <input id="v_email_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Phone</label>
                                                <input id="v_phone_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Address</label>
                                                <textarea id="v_address_id" class="form-control" readonly="readonly" rows="4" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"></textarea>
                                                <!-- <input id="v_religion_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div> -->
                                                </div>
    
                                            <hr>

                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">PIC</label>
                                                <input id="v_pic_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">PIC Phone</label>
                                                <input id="v_pic_phone_id" type="text" class="form-control" value="-" readonly="readonly" style="border: 1px solid #ffffff;background: #ffffff;text-transform: capitalize;"> </div>
                                            

                                             <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Portofolio</label><br>
                                                <img id="v_portofolio_id" src="{{url('public/images/portofolio/no_image.png')}}" alt="" class="img-responsive zoomimg">
                                                </div>

                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">Document</label><br>
                                                <img id="v_document_id" src="{{url('public/images/document/no_image.png')}}" alt="" class="img-responsive zoomimg">
                                                </div>

                                            <div class="form-group">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">SIUP</label><br>
                                                <img id="v_siup_id" src="{{url('public/images/siup/no_image.png')}}" alt="" class="img-responsive zoomimg">
                                                </div>

                                            <div class="form-group" id="row_siuppak_id">
                                                <label class="control-label" style="margin-top: 1px;font-weight: 300;font-style: italic;">SIUPPAK</label><br>
                                                <img id="v_siuppak_id" src="{{url('public/images/siuppak/no_image.png')}}" alt="" class="img-responsive zoomimg">
                                                </div>

                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>
                <hr>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn green">Save changes</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

    var typ = 'all';

    $("#typ_id").text(typ);

    $(".btn-filter-type").on('click',function(e){
        typ = e.currentTarget.dataset.typ;
        $("#typ_id").text(typ);    
        var oTable = table.dataTable().fnDraw();

    })

   ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.agency.list')}}",  
        method:'POST',   
        data :  function(data) {
            data.typ = typ;

            data._token= "{{ csrf_token() }}"
        }              
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [8] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                if(data[3]==1){
                    var ban = "red-sunglo";
                }else{
                    var ban = "border-grey-gallery grey-cararra";
                }
                return '<a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" class="view_btn btn btn-circle btn-icon-only grey-mint" title="View"><i class="fa fa-search"></i></a><a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" class="reset_btn btn btn-circle btn-icon-only yellow-casablanca" title="Reset"><i class="fa fa-key"></i></a><a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-type_agency="'+data[2]+'" data-ss="'+data[3]+'" class="ban_btn btn btn-circle btn-icon-only '+ban+'" title="BAN"><i class="fa fa-ban"></i></a>'; }, targets: [8]
            },
            { render: function(data, type, row, meta) {
                return '<a href="javascript:;" class="btn green disabled uppercase">'+data+'</a>'; }, targets: [4]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/profile')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [5]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/siup')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [6]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/siuppak')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [7]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'name', name: 'name' },
            { data: 'companyname', name: 'companyname' },
            { data: 'type_agency', name: 'type_agency' },           
            { data: 'status_account', name: 'status_account' },           
            { data: 'photo', name: 'photo' },           
            { data: 'siup', name: 'siup' },           
            { data: 'siuppak', name: 'siuppak' },           
            { data: 'users_id', name: 'users_id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $('#sample_3').on('click','.reset_btn',function(e){
        var id = e.currentTarget.dataset.id;

        swal({
          title: "Whether to Reset Password This Agency/Shipowner data ?",
          //text: "Crew Recommendation information will disappear from the web, are you sure you will do this process ... ",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Yes, I'm agree",
          cancelButtonText: "No, Cancle this Proses",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('/backend/reset-pass/') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            console.log(data);
                            if(data.status){
                                swal("Thank you", "You has Reset Password", "success");
                            }else{
                                swal("Sorry!!!", "User Do not Reset Password ", "error");
                            }
                            //location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Cancel", "You cancel the process of removing Crew Recommendations ... ", "error");
                }
        });
    });   

    $('#sample_3').on('click','.view_btn',function(e){
        var id = e.currentTarget.dataset.id;

        $.ajax({
            url: "{{ url('/backend/account/') }}/"+id+"/agency",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                $("#v_image_id").attr("src","{{url('public/images/profile')}}/"+data.account.photo);
                $("#v_name_id").text(data.account.name);
                $("#v_email_id").val(data.account.email);
                $("#v_phone_id").val("(+"+data.account.phone_code+") "+data.account.phone);
                $("#v_type_agency_id").text(data.info.type_agency);
                $("#v_img_bnd_id").attr('src',data.bnd);
                
                
                $("#v_c_name_id").val(data.info.companyname);                
                $("#v_address_id").val(data.info.address+", "+data.info.city+", "+data.info.country);

                $("#v_pic_id").val(data.info.pic);
                //$("#v_pic_phone_code_id").val(data.info.pic_phone_code);
                $("#v_pic_phone_id").val("(+"+data.info.pic_phone_code+") "+data.info.pic_phone);

                $("#v_fb_id").text(data.info.fb==null?'-':data.info.fb);
                $("#v_ig_id").text(data.info.ig==null?'-':data.info.ig);
                $("#v_tg_id").text(data.info.tg==null?'-':data.info.tg);
                $("#v_tw_id").text(data.info.tw==null?'-':data.info.tw);
                $("#v_wa_id").text(data.info.wa==null?'-':data.info.wa);
                
                $("#v_portofolio_id").attr('src',"{{url('public/images/portofolio')}}/"+data.info.portofolio);
                $("#v_document_id").attr('src',"{{url('public/images/document')}}/"+data.info.document);
                $("#v_siup_id").attr('src',"{{url('public/images/siup')}}/"+data.info.siup);
                $("#v_siuppak_id").attr('src',"{{url('public/images/siuppak')}}/"+data.info.siuppak);

               if(data.info.type_agency=="AGENCY"){
                    $("#row_siuppak_id").show();
               }else if(data.info.type_agency=="SHIP OWNER"){
                    $("#row_siuppak_id").hide();
               }
                
            }
        });


        $("#modal-view").modal('show');
    });

    $('#sample_3').on('click','.ban_btn',function(e){
        var id = e.currentTarget.dataset.id;
        var ss = e.currentTarget.dataset.ss;

        if(ss==1){
            var txt = 'Active';
        }else {
            var txt = 'Ban';
        
        }

        swal({
          title: "Whether to "+txt+" This Agency/Ship Onwer Access ?",
          //text: "Crew Recommendation information will disappear from the web, are you sure you will do this process ... ",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Yes, I'm agree",
          cancelButtonText: "No, Cancle this Proses",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('/backend/ban-access/') }}",
                        type : "POST",  
                        data : {id:id,"_token": "{{ csrf_token() }}"},
                        dataType: "JSON",
                        success: function(data) {
                            console.log(data);
                            if(data.status){
                                swal("Thank you", "You has "+data.msg+" Access", "success");
                            }else{
                                swal("Sorry!!!", "User Do not Banned Access ", "error");
                            }
                            //location.reload(true);
                            var oTable = table.dataTable().fnDraw();
                        }
                    });
                    
                } else {
                    swal("Cancel", "You cancel the process of "+txt+" Access Agency/Ship Onwer ... ", "error");
                }
        });
    });  

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
})

</script>
@endsection    