<?php
 
namespace App\Http\Controllers;
 
use App;
use Illuminate\Http\Request;
 
class LocalizationController extends Controller
{
    /**
     * Switch Language.
     *
     * @param string $language Language code.
     * 
     * @return Response
     */
    public function switch($locale = '')
    {
        App::setlocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }
}