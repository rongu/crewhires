

@extends('layouts.frontend')

@section('title','My Invitation')
@section('desc','Halaman ini menyatakan My Invitation')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }
        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>My Invitation
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>My Invitation</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="row" style="background-color: #fff;border: 1px solid #ddd;padding: 10px;-webkit-border-radius: 0 0 4px 4px;-moz-border-radius: 0 0 4px 4px;-ms-border-radius: 0 0 4px 4px;-o-border-radius: 0 0 4px 4px;border-radius: 0 0 4px 4px;">
                <div class="col-md-12">
                    <div class="search-page">
                        <div class="row search-table">
                            <div class="col-md-12">
                                @foreach($agency as $vl)
                                    <div class="col-md-3">                                        
                                        <div class="mt-widget-1">
                                            @if($vl->status_account=='premium')
                                            <div class="mt-icon">
                                                <a href="#">
                                                    <i class="fa fa-certificate" style="color: #17d200"></i>
                                                </a>
                                            </div>
                                            @endif
                                            <div class="mt-img">
                                                @if($vl->photo)

                                                    @if(Auth::check())
                                                    <img src="{{ url('public/images/profile/') }}/{{$vl->photo}}" width="150px" height="150px" class="zoomimg"> 
                                                    @else
                                                    <img src="{{ url('public/images/profile/') }}/{{$vl->photo}}" width="150px" height="150px" class="bg-image"> 
                                                    @endif
                                                
                                                @else
                                                <img src="{{ url('public/images/profile.png') }}" width="150px" height="150px"> 
                                                @endif
                                            </div>
                                            <div class="mt-body">
                                                <h3 class="mt-username">
                                                    {{$vl->companyname}}<br>
                                                    <img src="{{App\Http\Controllers\JobsController::getCountry($vl->country)['flag_base64']}}" style="width: 20px;border: 1px solid #cdcdcd;">
                                                </h3>                                                
                                                <div class="mt-stats" style="max-height: 57px;height: 57px;overflow: hidden;text-overflow: ellipsis;font-size: .9em;margin-left: 3px; margin-right: 3px;margin-top: 10px;">
                                                    {{$vl->address}}
                                                </div>
                                                <div class="mt-stats" style="margin-top: 10px;">
                                                    <div class="btn-group btn-group btn-group-justified">
                                                        <div class="btn btn-lg font-blue-madison">
                                                            {{$vl->type_agency}}
                                                        </div>
                                                        <div class="btn btn-lg font-grey-mint" style="overflow: hidden;text-overflow: ellipsis;">
                                                            {{$vl->pic}}
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="mt-stats" style="margin:0px">
                                                    <div class="btn-group btn-group btn-group-justified">
                                                       
                                                            <a href="{{route('crew.invitation.destroy',$vl->id)}}" class="btn btn-lg bg-font-red-sunglo red-sunglo">          
                                                                Reject
                                                            </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    