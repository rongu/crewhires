<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('/cache', function () {
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    echo "sukses";
});

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/page-system-coming-soon', [App\Http\Controllers\HomeController::class, 'pageSystemComingSoon'])->name('page.system.coming.soon');

Route::get('/categories', [App\Http\Controllers\HomeController::class, 'categories'])->name('categories');
Route::get('/categories-crew-vacancy-list', [App\Http\Controllers\HomeController::class, 'crewVacancyList'])->name('categories.crew.vacancy.list');
Route::get('/categories-crew-all-list', [App\Http\Controllers\HomeController::class, 'crewAllList'])->name('categories.crew.all.list');
Route::post('/categories-crew_apply', [App\Http\Controllers\HomeController::class, 'crewApply'])->name('categories.crew.apply');
Route::get('/membership', [App\Http\Controllers\HomeController::class, 'membership'])->name('membership')->middleware('banned');
Route::get('/membership-activation', [App\Http\Controllers\FrontController::class, 'membershipActivation'])->name('membership.activation')->middleware(['verified']);
Route::get('/membership-verification', [App\Http\Controllers\FrontController::class, 'membershipVerification'])->name('membership.verification');
Route::post('/membership-payment', [App\Http\Controllers\FrontController::class, 'membershipPayment'])->name('membership.payment');
Route::get('/payment-list', [App\Http\Controllers\FrontController::class, 'paymentList'])->name('payment.list')->middleware(['verified']);
Route::get('/payment-delete/{id}', [App\Http\Controllers\FrontController::class, 'paymentDelete'])->name('payment.delete');
Route::get('/payment-edit/{id}', [App\Http\Controllers\FrontController::class, 'paymentEdit'])->name('payment.edit');
Route::get('/term-and-condition', [App\Http\Controllers\HomeController::class, 'termCondition'])->name('term.condition');
Route::get('/faq', [App\Http\Controllers\HomeController::class, 'faq'])->name('faq');
Route::get('/service', [App\Http\Controllers\HomeController::class, 'service'])->name('service');
Route::get('/service/{type}', [App\Http\Controllers\FrontController::class, 'serviceRequest'])->name('service.req');

//Route::get('/jobs-form', [App\Http\Controllers\HomeController::class, 'jobsForm'])->name('jobs.form');
//Route::get('/social-media', [App\Http\Controllers\HomeController::class, 'socialMedia'])->name('social.media');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');
Route::get('/snk', [App\Http\Controllers\HomeController::class, 'snk'])->name('snk');


Route::get('/backend', function(){return redirect()->route('backend.login');})->name('backend');
Route::get('/backend/login', [App\Http\Controllers\Backend\LoginController::class, 'index'])->name('backend.login');
Route::get('/backend/home', [App\Http\Controllers\Backend\BackendController::class, 'index'])->name('backend.home');
Route::get('/backend/crew', [App\Http\Controllers\Backend\BackendController::class, 'crewPage'])->name('backend.crew.page');
Route::get('/backend/agency', [App\Http\Controllers\Backend\BackendController::class, 'agencyPage'])->name('backend.agency.page');

Route::get('/backend/crew-vacancy', [App\Http\Controllers\Backend\BackendController::class, 'crewVacancyPage'])->name('backend.crew.vacancy');
Route::get('/backend/crew-vacacny-list', [App\Http\Controllers\Backend\BackendController::class, 'crewVacancyList'])->name('backend.crew.vacancy.list');
Route::post('/backend/crew-vacancy-apply-list', [App\Http\Controllers\Backend\BackendController::class, 'crewVacancyApplyList'])->name('backend.crew.vacancy.apply.list');

Route::get('/backend/frontend', [App\Http\Controllers\Backend\BackendController::class, 'frontendPage'])->name('backend.frontend.page');
Route::get('/backend/frontend-list-slider', [App\Http\Controllers\Backend\BackendController::class, 'frontendListSlider'])->name('backend.frontend.list.slider');
Route::post('/backend/frontend-create-slider', [App\Http\Controllers\Backend\BackendController::class, 'frontendCreateSlider'])->name('backend.frontend.create.slider');
Route::get('/backend/frontend-destroy-slider/{id}', [App\Http\Controllers\Backend\BackendController::class, 'frontendDestroySlider'])->name('backend.frontend.destroy.slider');
Route::get('/backend/invitation', [App\Http\Controllers\Backend\BackendController::class, 'invitationPage'])->name('backend.invitation.page');
Route::get('/backend/invitation-list', [App\Http\Controllers\Backend\BackendController::class, 'invitationList'])->name('backend.invitation.list');
Route::post('/backend/invitation-permission', [App\Http\Controllers\Backend\BackendController::class, 'invitationPermission'])->name('backend.invitation.permission');

Route::get('/backend/crew-apply', [App\Http\Controllers\Backend\BackendController::class, 'crewApplyPage'])->name('backend.crew.apply');
Route::get('/backend/crew-apply-list', [App\Http\Controllers\Backend\BackendController::class, 'crewApplyList'])->name('backend.crew.apply.list');
Route::post('/backend/crew-apply-approval', [App\Http\Controllers\Backend\BackendController::class, 'crewApplyApproval'])->name('backend.crew.apply.approval');

Route::get('/backend/crew-recommendation', [App\Http\Controllers\Backend\BackendController::class, 'crewRecommendationPage'])->name('backend.crew.recommendation');
Route::get('/backend/crew-recommendation-list', [App\Http\Controllers\Backend\BackendController::class, 'crewRecommendationList'])->name('backend.crew.recommendation.list');
Route::get('/backend/crew-recommendation-destroy/{id}', [App\Http\Controllers\Backend\BackendController::class, 'crewRecommendationDestroy'])->name('crew.recommendation.destroy');
Route::post('/backend/crew-recommendation-create', [App\Http\Controllers\Backend\BackendController::class, 'crewRecommendationCreate'])->name('crew.recommendation.create');
Route::post('/backend/crew-recommendation-get-position', [App\Http\Controllers\Backend\BackendController::class, 'getposition'])->name('crew.recommendation.get.position');
Route::post('/backend/crew-recommendation-get-crew', [App\Http\Controllers\Backend\BackendController::class, 'getcrew'])->name('crew.recommendation.get.crew');

Route::get('/backend/service', [App\Http\Controllers\Backend\BackendController::class, 'servicePage'])->name('backend.service.page');
Route::get('/backend/service-list', [App\Http\Controllers\Backend\BackendController::class, 'serviceList'])->name('backend.service.list');

Route::post('/backend/agency-list', [App\Http\Controllers\Backend\BackendController::class, 'agencyList'])->name('backend.agency.list');
Route::get('/backend/reset-pass/{id}', [App\Http\Controllers\Backend\BackendController::class, 'resetPassword'])->name('backend.reset.pass');
Route::post('/backend/ban-access', [App\Http\Controllers\Backend\BackendController::class, 'banAccess'])->name('backend.ban.access');
Route::get('/backend/crew-list', [App\Http\Controllers\Backend\BackendController::class, 'crewList'])->name('backend.crew.list');
Route::get('/backend/membership', [App\Http\Controllers\Backend\BackendController::class, 'membershipPage'])->name('backend.membership.page');
Route::get('/backend/membership-list', [App\Http\Controllers\Backend\BackendController::class, 'membershipList'])->name('backend.membership.list');
Route::post('/backend/membership-payment-activate', [App\Http\Controllers\Backend\BackendController::class, 'membershipPaymentActivate'])->name('backend.membership.payment');

Route::get('lang/{language}', [App\Http\Controllers\LocalizationController::class, 'switch'])->name('localization.switch');

Route::get('/set-profile-crew', [App\Http\Controllers\FrontController::class, 'index'])->middleware(['auth','verified'])->name('set.profile.crew');
Route::post('/store-profile-crew', [App\Http\Controllers\FrontController::class, 'storeCrew'])->name('profile.create.wizard');


Route::get('/crew/profile', [App\Http\Controllers\JobsController::class, 'profile'])->name('crew.profile')->middleware(['permission:view.crew','verified']);//->middleware(['auth','verified'])
Route::get('/get-job_desc/{type_crew}', [App\Http\Controllers\FrontController::class, 'getJobDesc'])->name('get.jobdesc');
Route::post('/crew/store-update-crew', [App\Http\Controllers\JobsController::class, 'updateCrew'])->name('profile.update.crew');
Route::get('/crew/store-delete-crew-doc/{id}', [App\Http\Controllers\JobsController::class, 'deleteDocCrew'])->name('profile.delete.doc.crew');
Route::get('/crew/my-job', [App\Http\Controllers\JobsController::class, 'myJob'])->name('crew.myjob')->middleware(['permission:view.crew','verified']);
Route::get('/crew/my-invitation', [App\Http\Controllers\JobsController::class, 'myInvitation'])->name('crew.invitation')->middleware(['permission:view.crew','verified']);
Route::get('/crew/my-invitation-destroy/{id}', [App\Http\Controllers\JobsController::class, 'myInvitationDestroy'])->name('crew.invitation.destroy');


Route::get('/set-profile-agency', [App\Http\Controllers\FrontController::class, 'agency'])->middleware(['auth','verified'])->name('set.profile.agency');
Route::post('/store-profile-agency', [App\Http\Controllers\FrontController::class, 'storeAgency'])->name('profile.create.wizard.agency');

// Route::get('/agency/home', [App\Http\Controllers\AgencyController::class, 'index'])->name('agency.home');
Route::get('/agency/profile', [App\Http\Controllers\AgencyController::class, 'profile'])->name('agency.profile')->middleware('permission:view.agency');
Route::post('/agency/store-update-agency', [App\Http\Controllers\AgencyController::class, 'updateAgency'])->name('profile.update.agency');
Route::get('/agency/store-delete-agency-doc/{id}', [App\Http\Controllers\AgencyController::class, 'deleteJobsAgency'])->name('profile.delete.job.agency');
Route::post('/agency/store-update-agency-byid', [App\Http\Controllers\AgencyController::class, 'updateAgencyByid'])->name('profile.update.agency.byid');
Route::get('/agency/my-job', [App\Http\Controllers\AgencyController::class, 'myJob'])->name('agency.myjob')->middleware(['permission:view.agency','verified']);
Route::post('/agency/my-job-create', [App\Http\Controllers\AgencyController::class, 'myJobCreate'])->name('agency.myjob.create');
Route::post('/agency/my-job-apply', [App\Http\Controllers\AgencyController::class, 'myJobApply'])->name('agency.myjob.apply');
Route::get('/agency/my-invitation', [App\Http\Controllers\AgencyController::class, 'myInvitation'])->name('agency.invitation')->middleware(['permission:view.agency','verified']);
Route::get('/agency/my-invitation-destroy/{id}', [App\Http\Controllers\AgencyController::class, 'myInvitationDestroy'])->name('agency.invitation.destroy');

Route::get('/crew-cv/{id}', [App\Http\Controllers\FrontController::class, 'crewCV'])->name('crew.cv');

Route::get('/crew/apply-job/{id}', [App\Http\Controllers\FrontController::class, 'applyCrew'])->name('crew.apply')->middleware(['verified']);
Route::get('/crew/apply-job-view/{id}', [App\Http\Controllers\FrontController::class, 'jobsViewCrew'])->name('crew.jobs.view');
Route::get('/agency/invite/{id}', [App\Http\Controllers\FrontController::class, 'inviteAgency'])->name('agency.invite');

Route::get('/backend/account/{id}/{typ}', [App\Http\Controllers\Backend\BackendController::class, 'getAccountInfo'])->name('backend.account');
Route::get('/banned', [App\Http\Controllers\Backend\BackendController::class, 'banned'])->name('banned');

Route::post('/backend/vs/negarasum', [App\Http\Controllers\Backend\BackendController::class, 'vStatistikNegaraSum'])->name('backend.vs.negarasum');





