

@extends('layouts.frontend')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    <link href="{{ asset('../../public/assets/pages/css/invoice-2.min.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
            max-height: 180px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Curriculum Vitae
                <small>Curriculum Vitae Crew</small>
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Pages</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Crew</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="invoice-content-2 ">
                 <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="3" width="220px" class="invoice-title text-center sbold" style="background-color: #ffffff;border: 1px solid #ffffff;"><img class="login-logo" src="{{ asset('../../public/images/Logo_crewhires.png') }}" width="100px" /></th>
                            <th class="invoice-title text-center sbold" style="background-color: #ffffff;border: 1px solid #ffffff;">&nbsp;</th>
                        </tr>
                        <tr>
                            <th class="invoice-title text-center sbold" style="background-color: #4596bb"><h4><b>CURRICULUM VITAE</b></h4></th>

                        </tr><tr>
                            <th class="invoice-title text-center sbold" style="background-color: #ffffff;border: 1px solid #ffffff;">&nbsp;</th>
                            
                        </tr>
                    </thead>
                </table>
                <div class="row invoice-head">
                    
                    <div class="col-md-7 col-xs-7">
                        <div class="company-address">
                            <h4><b>PERSONAL DATA</b></h4>
                             <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" width="165px" class="text-left">Name</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left uppercase">{{$inf->fullname}}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Place & Date of Birth</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->place_birth}}, {{Carbon::parse($inf->birth)->format('m/d/Y')}} ( {{Carbon::parse($inf->birth)->diff(\Carbon\Carbon::now())->format('%y years')}})</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Sex</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->gender=='m'?'Male':'Female'}}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Nationality</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->nationality}}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Marital Status</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->marital}}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Religion</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->religion}}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">Salary</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-center">:</td>
                                        <td style="padding: 1px 8px 2px 8px;" class="text-left">{{$inf->currency}}&nbsp;{{$inf->salary}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-5 col-xs-5">
                        <div class="invoice-logo">
                            <img src="{{ asset('../../public/images/profile') }}/{{$usr->photo}}" class="img-responsive zoomimg" alt="" class="img-responsive">
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-xs-12 table-responsive">


                        <h4><i class="fa fa-asterisk"></i> <b>DOCUMENT</b></h4>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr style="background-color: #4596bb">
                                    <th class="invoice-title text-center sbold">Document</th>
                                    <th class="invoice-title text-center sbold">Number</th>
                                    <th class="invoice-title text-center sbold">Place, Date Of Issued</th>
                                    <th class="invoice-title text-center sbold">Expired Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($doc as $vlk)
                                @php $i=1; @endphp
                                @if($vlk->types=='flg' || $vlk->types=='stc')
                                    <tr>
                                        <td class="text-left">{{$vlk->name}}</td>
                                        <td class="text-center">{{$vlk->cer_doc_number}}</td>
                                        <td class="text-left">{{$vlk->place}}, {{Carbon::parse($vlk->doi)->format('m/d/Y')}}</td>
                                        <td class="text-center">{{Carbon::parse($vlk->ed)->format('m/d/Y')}}</td>
                                    </tr>
                                @endif
                                @php $i++; @endphp
                                @endforeach

                                
                            </tbody>
                        </table>
                        <br>
                        <h4><i class="fa fa-asterisk"></i> <b>CERTIFICATE OF COMPTENCY AND PROFICIENCY</b></h4>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr style="background-color: #4596bb">
                                    <th class="invoice-title text-center sbold">Name Of Certificate</th>
                                    <th class="invoice-title text-center sbold">Certificate Number</th>
                                    <th class="invoice-title text-center sbold">Place, Date Of Issued</th>
                                    <th class="invoice-title text-center sbold">Expired Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($doc as $vlk)
                                @php $i=1; @endphp
                                @if($vlk->types=='cop' || $vlk->types=='coc')
                                    <tr>
                                        <td class="text-left">{{$vlk->name}}</td>
                                        <td class="text-center">{{$vlk->cer_doc_number}}</td>
                                        <td class="text-left">{{$vlk->place}}, {{Carbon::parse($vlk->doi)->format('m/d/Y')}}</td>
                                        <td class="text-center">{{Carbon::parse($vlk->ed)->format('m/d/Y')}}</td>
                                    </tr>
                                @endif
                                @php $i++; @endphp
                                @endforeach

                               
                            </tbody>
                        </table>
                        <br>
                        <h4><i class="fa fa-asterisk"></i> <b>WORKING  EXPERINCE</b></h4>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr style="background-color: #4596bb">
                                    <th class="invoice-title text-center sbold" rowspan="2">No</th>
                                    <th class="invoice-title text-center sbold" rowspan="2">Name Of Vessel</th>
                                    <th class="invoice-title text-center sbold" rowspan="2">Type of Vessel</th>
                                    <th class="invoice-title text-center sbold" rowspan="2">Rank</th>
                                    <th class="invoice-title text-center sbold" rowspan="2">Company</th>
                                    <th class="invoice-title text-center sbold" colspan="2">Period</th>
                                </tr>
                                <tr style="background-color: #4596bb">
                                    <th class="invoice-title text-center sbold">Sign on</th>
                                    <th class="invoice-title text-center sbold">Sign off</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($doc as $vlk)
                                @php $i=1; @endphp
                                @if($vlk->types=='exp')
                                    <tr>
                                        <td class="text-left">{{$i}}</td>
                                        <td class="text-left">{{$vlk->name}}</td>
                                        <td class="text-center">{{$vlk->t_vessel}}</td>
                                        <td class="text-center">{{$vlk->rank}}</td>
                                        <td class="text-center">{{$vlk->company}}</td>
                                        <td class="text-center">{{Carbon::parse($vlk->p_from)->format('m/d/Y')}}</td>
                                        <td class="text-center">{{Carbon::parse($vlk->p_to)->format('m/d/Y')}}</td>
                                    </tr>
                                @endif
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <span>Yours Faithfully</span>
                        <br>
                        <br>
                        <br>
                        <span>{{$inf->fullname}}</span>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{route('agency.invite',$inf->users_id)}}" class="btn btn-lg bg-font-blue-dark blue-dark">Invite</a>
                        <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="javascript:window.print();">Print</a>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

        ezoom.onInit($('.zoomimg'), {
            hideControlBtn: true,
            onClose: function (result) {
                console.log(result);
            },
            onRotate: function (result) {
                console.log(result);
            },
        });

});
</script>
@endsection    