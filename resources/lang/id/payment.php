<?php 
 
return [
    'Invoice Number' => 'Nomor faktur',
    'To Account Number' => 'Ke Nomor Rekening',
    'Proof of Payment' => 'Bukti pembayaran',
    'Account Number' => 'Nomor rekening',
    'Bank Account Under the Name' => 'Rekening Bank Atas Nama',
    'Payment Verification Date' => 'Tanggal Verification pembayaran',
];