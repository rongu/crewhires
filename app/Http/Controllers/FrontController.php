<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\RoleUser;
use \App\Models\Country;
use \App\Models\MasterCop;
use \App\Models\MasterCoc;
use \App\Models\CrewDoc;
use \App\Models\CrewInfo;
use \App\Models\AgencyInfo;
use \App\Models\AgencyJobs;
use \App\Models\CrewApply;
use \App\Models\AgencyInvite;
use \App\Models\Service;
use \App\Models\Membership;
use \App\Models\Religion;
use App\Mail\EmailService;
use Session,Alert,Auth,DB;
use Illuminate\Support\Facades\Mail;

use jeremykenedy\LaravelLogger\App\Http\Traits\ActivityLogger;

class FrontController extends Controller
{
    use ActivityLogger;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::select('name','email','phone')->where('id', auth()->user()->id)->first();
        $mastercop = MasterCop::select('name','codes')->get();
        $mastercoc = MasterCoc::select('name')->get();
        $reli = Religion::select('name')->get();
        //$ctr = Country::get();
        $ctr = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");
        $calling = file_get_contents("vendor/samayo/country-json/src/country-by-calling-code.json");

        return view('setprofile-crew',compact('user','mastercop','mastercoc','ctr','calling','reli'));
    }    

    public function agency()
    {
        $user = User::select('name','email','phone')->where('id', auth()->user()->id)->first();
        $mastercop = MasterCop::select('name','codes')->get();
        $mastercoc = MasterCoc::select('name')->get();

        $calling = file_get_contents("vendor/samayo/country-json/src/country-by-calling-code.json");
        $ctr = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");

        return view('setprofile-agency',compact('user','mastercop','mastercoc','calling','ctr'));
    }

    public function storeAgency(Request $request)
    {
        $type_agency = $request->type_agency;
        $name = $request->name;
        $companyname = $request->companyname;
        $email = $request->email;
        $phone = $request->phone;
        $phone_code = $request->phone_code;
        $address = $request->address;
        $city = $request->city;
        $country = $request->country;
        $pic = $request->pic;
        $pic_phone = $request->pic_phone;
        $pic_phone_code = $request->pic_phone_code;
        
        $file = $request->file('file');
        if($file){
            $nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
    
            $tujuan_upload = 'public/images/profile';
            $file->move($tujuan_upload,$nama_file);
        }        

        $file_portofolio = $request->file('file_portofolio');
        if($file_portofolio){
            $nama_file_porto = 'port_'.auth()->user()->id.'_'.time() . '.' . $file_portofolio->getClientOriginalExtension();
            $tujuan_upload = 'public/images/portofolio';
            $file_portofolio->move($tujuan_upload,$nama_file_porto);
        }

        $file_doc = $request->file('file_doc');
        if($file_doc){
            $nama_file_doc = 'doc_'.auth()->user()->id.'_'.time() . '.' . $file_doc->getClientOriginalExtension();
            $tujuan_upload = 'public/images/document';
            $file_doc->move($tujuan_upload,$nama_file_doc);
        }

        $file_siup = $request->file('file_siup');
        if($file_siup){
            $nama_file_siup = 'siup_'.auth()->user()->id.'_'.time() . '.' . $file_siup->getClientOriginalExtension();
            $tujuan_upload = 'public/images/siup';
            $file_siup->move($tujuan_upload,$nama_file_siup);
        }

        $file_siuppak = $request->file('file_siuppak');
        if($file_siuppak){
            $nama_file_siuppak = 'siuppak_'.auth()->user()->id.'_'.time() . '.' . $file_siuppak->getClientOriginalExtension();
            $tujuan_upload = 'public/images/siuppak';
            $file_siuppak->move($tujuan_upload,$nama_file_siuppak);
        }


        $fb = $request->fb;
        $ig = $request->ig;
        $tg = $request->tg;
        $tw = $request->tw;
        $wa = $request->wa;

        $jl = $request->jl; 

        //echo $type_agency.'-'.$name.'-'.$companyname.'-'.$email.'-'. $phone.'-'.$address.'-'.$city.'-'.$country.'-'. $pic.'-'.$pic_phone.'-'.$file.'-'.$file_doc.'-'.$file_portofolio.'-'.$fb.'-'.$ig.'-'.$tg.'-'.$tw.'-'.$wa.'<br>';

                //info crew
        $crewinfo = AgencyInfo::where('users_id', auth()->user()->id)->doesntExist();
        if($crewinfo){
            $cwi = new AgencyInfo();
            $cwi->users_id = auth()->user()->id;
            $cwi->companyname = $companyname;
            $cwi->type_agency = $type_agency;
            $cwi->address = $address;  
            $cwi->city = $city;  
            $cwi->country = $country;  
            $cwi->pic = $pic;  
            $cwi->pic_phone = $pic_phone;  
            $cwi->pic_phone_code = $pic_phone_code;  
            $cwi->fb = $fb;  
            $cwi->ig = $ig;  
            $cwi->tg = $tg;  
            $cwi->tw = $tw;  
            $cwi->wa = $wa;

            if($file_portofolio){
                $cwi->portofolio = $nama_file_porto; 
            }

            if($file_doc){
                $cwi->document = $nama_file_doc; 
            }

            if($file_siup){
                $cwi->siup = $nama_file_siup; 
            }

            if($file_siuppak){
                $cwi->siuppak = $nama_file_siuppak; 
            }

            $cwi->save(); 
        }

                //info user
        $pts = User::findOrFail(auth()->user()->id);
        $pts->name = $name;
        $pts->email = $email;
        $pts->phone = $phone;
        $pts->phone_code = $phone_code;
        $pts->profile = 'done';
        if($file){
            $pts->photo = $nama_file;
        }
        $pts->update();

        foreach ($jl as $kee) {

            //echo $kee['jl_po'].' '.$kee['jl_ty'].' '.$kee['jl_en'].' '.$kee['jl_re'].'<br>';
            if(!empty($kee['jl_po'])){
                $cd = new AgencyJobs();
                $cd->users_id = auth()->user()->id;
                $cd->position = $kee['jl_po'];
                $cd->types = $kee['jl_ty'];
                $cd->engine = $kee['jl_en'];
                $cd->requirement = $kee['jl_re'];
                $cd->status = 'open';
                $cd->date_open = date('Y-m-d',strtotime($kee['jl_open']));
                $cd->date_close = date('Y-m-d',strtotime($kee['jl_close']));
                $cd->save();  
            }


        } 

        return redirect()->route('home');
    }

    public function storeCrew(Request $request)
    {
        $type_crew = $request->type_crew;
        $job_id = $request->job_id;
        $fullname = $request->fullname;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $phone_code = $request->phone_code;
        $place = $request->place;
        $marital = $request->marital;
        $religion = $request->religion;
        $gender = $request->gender;
        $birth = $request->birth;
        $address = $request->address;
        $city = $request->city;
        $country = $request->country;
        $nationality = $request->nationality;
        $currency = $request->currency;
        $salary = $request->salary;
        $seaferer = $request->seaferer;
        $status_activity = $request->status_activity;
        $fb = $request->fb;
        $ig = $request->ig;
        $tg = $request->tg;
        $tw = $request->tw;
        $wa = $request->wa; 

        $exp = $request->exp;
        $cop = $request->cop;
        $coc = $request->coc;
        $flg = $request->flg;
        $stc = $request->stc;

        $file = $request->file('file');
        if($file){
            $nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
    
            $tujuan_upload = 'public/images/profile';
            $file->move($tujuan_upload,$nama_file);
        }        

        $file_portofolio = $request->file('file_portofolio');
        if($file_portofolio){
            $nama_file_porto = auth()->user()->id.'_'.time() . '.' . $file_portofolio->getClientOriginalExtension();
            $tujuan_upload = 'public/images/portofolio';
            $file_portofolio->move($tujuan_upload,$nama_file_porto);
        }

        //info crew
        $crewinfo = CrewInfo::where('users_id', auth()->user()->id)->doesntExist();
        if($crewinfo){
            $cwi = new CrewInfo();
            $cwi->users_id = auth()->user()->id;
            $cwi->fullname = $fullname;
            $cwi->place_birth = $place;
            $cwi->marital = $marital;
            $cwi->religion = $religion;
            $cwi->type_crew = $type_crew;       
            $cwi->job_desc = $job_id;       
            $cwi->birth = date('Y-m-d',strtotime($birth));
            $cwi->gender = $gender;  
            $cwi->address = $address;  
            $cwi->city = $city;  
            $cwi->country = $country;  
            $cwi->nationality = $nationality;  
            $cwi->currency = $currency;  
            $cwi->salary = $salary;  
            $cwi->seaferer = $seaferer;  
            $cwi->status_activity = $status_activity;  
            $cwi->fb = $fb;  
            $cwi->ig = $ig;  
            $cwi->tg = $tg;  
            $cwi->tw = $tw;  
            $cwi->wa = $wa;  
             
            if($file_portofolio){
                $cwi->portofolio = $nama_file_porto; 
            }
            $cwi->save(); 
        }

        //info user
        $pts = User::findOrFail(auth()->user()->id);
        $pts->name = $name;
        $pts->email = $email;
        $pts->phone = $phone;
        $pts->phone_code = $phone_code;
        $pts->profile = 'done';
        if($file){
            $pts->photo = $nama_file;
        }
        $pts->update();


        //exp
        foreach ($exp as $kee) {

            if(!empty($kee['exp_nov'])){
                $cd = new CrewDoc();
                $cd->users_id = auth()->user()->id;
                $cd->name = $kee['exp_nov'];
                $cd->types = 'exp';
                $cd->t_vessel = $kee['exp_tv'];
                $cd->rank = $kee['exp_rank'];
                $cd->company = $kee['exp_com'];
                $cd->p_from = date('Y-m-d',strtotime($kee['exp_from']));
                $cd->p_to = date('Y-m-d',strtotime($kee['exp_to']));
                $cd->save();  
            }


        }        
        //cop
        foreach ($cop as $kee) {

            if($kee['cop_noc']!='None'){
                $cd = new CrewDoc();
                $cd->users_id = auth()->user()->id;
                $cd->name = $kee['cop_noc'];
                $cd->types = 'cop';
                $cd->cer_doc_number = $kee['cop_cn'];
                $cd->place = $kee['cop_pl'];
                $cd->doi = date('Y-m-d',strtotime($kee['cop_doi']));
                $cd->ed = date('Y-m-d',strtotime($kee['cop_ed']));
                $cd->save();  
            }


        }
        //coc
        foreach ($coc as $kee) {

            if($kee['coc_noc']!='None'){
                $cd = new CrewDoc();
                $cd->users_id = auth()->user()->id;
                $cd->name = $kee['coc_noc'];
                $cd->types = 'coc';
                $cd->cer_doc_number = $kee['coc_cn'];
                $cd->place = $kee['coc_pl'];
                $cd->doi = date('Y-m-d',strtotime($kee['coc_doi']));
                $cd->ed = date('Y-m-d',strtotime($kee['coc_ed']));
                $cd->save();  
            }


        }

        //flg
        foreach ($flg as $kee) {

            if(!empty($kee['flg_nod'])){
                $cd = new CrewDoc();
                $cd->users_id = auth()->user()->id;
                $cd->name = $kee['flg_nod'];
                $cd->types = 'flg';
                $cd->cer_doc_number = $kee['flg_cn'];
                $cd->place = $kee['flg_pl'];
                $cd->doi = date('Y-m-d',strtotime($kee['flg_doi']));
                $cd->ed = date('Y-m-d',strtotime($kee['flg_ed']));
                $cd->save();  
            }


        }

        //flg
        foreach ($stc as $kee) {

            if(!empty($kee['stc_nod'])){
                $cd = new CrewDoc();
                $cd->users_id = auth()->user()->id;
                $cd->name = $kee['stc_nod'];
                $cd->types = 'stc';
                $cd->cer_doc_number = $kee['stc_dn'];
                $cd->place = $kee['stc_pl'];
                $cd->doi = date('Y-m-d',strtotime($kee['stc_doi']));
                $cd->ed = date('Y-m-d',strtotime($kee['stc_ed']));
                $cd->save();  
            }


        }

    return redirect()->route('home');

    }

    public function crewCV($id){
        $usr_ex = User::where('id',$id)->exists();
        if($usr_ex){
            $usr = User::where('id',$id)->first();
            $doc = CrewDoc::where('users_id',$id)->get();
            $inf = CrewInfo::where('users_id',$id)->first();
            $country = Country::select('name','codes')->where('codes',$inf->country)->first();
            $nationality = Country::select('name','codes')->where('codes',$inf->nationality)->first();

            $role = RoleUser::where('user_id',auth()->user()->id)->first();

            if($role->role_id==2){
                
                if($id==auth()->user()->id){
                    return view('jobs.curriculumvitae',compact('usr','doc','inf','country','nationality'));
                }else{
                    return redirect()->back();
                }

                

            }else{
                $status = User::where('id',auth()->user()->id)->first();
                if($status->status_account=='premium'){
                    return view('jobs.curriculumvitae_agencypremium',compact('usr','doc','inf','country','nationality'));
                }else{
                    return view('jobs.curriculumvitae_agencyfree',compact('usr','doc','inf','country','nationality'));
                }
            }
        }else{
            return redirect()->route('categories');
        }
    }

    public function applyCrew($id){

        if(Auth::user()->isCrew()){
            $day_apply = CrewApply::where('users_id',auth()->user()->id)->where('date_add',now())->count();

            $max = DB::table('settingan')->where('desc', '=', 'max_apply')->first();

            if ($day_apply <= $max->val){
                Alert::warning('Your maximum daily application limit has expired', 'Please upgrade your membership, Thank you.');
            }else{

                $ca = CrewApply::where('users_id', auth()->user()->id)->where('agency_jobs_id', $id)->doesntExist();
                if($ca){
                    $cd = new CrewApply();
                    $cd->users_id = auth()->user()->id;
                    $cd->agency_jobs_id = $id;
                    $cd->date_add = date('Y-m-d');
                    $cd->apply_status = 'ADD';
                    $cd->save();  
                    Alert::success('Thank you', 'Thank you, Please wait for the next information');
                }else{
                    Alert::warning('You has apply this job', 'Thank you, you can no longer apply');
                }
            }
        }else{
            Alert::warning('Sorry', "agency can't apply job");
        }

        return redirect()->back();
    }    

    public function jobsViewCrew($id){

        return redirect()->back();
    }

    public function inviteAgency($id){

        $day_invite = AgencyInvite::where('users_id',auth()->user()->id)->where('date_add',date("Y-m-d"))->count();

        $max = DB::table('settingan')->where('desc', '=', 'max_invite')->first();

        if ($day_invite <= $max->val){
            Alert::warning('Your maximum daily invitation limit has expired', 'Please upgrade your membership, Thank you.');
        }else{
        
            $ca = AgencyInvite::where('users_id', auth()->user()->id)->where('crew_id', $id)->doesntExist();
            if($ca){
                $cd = new AgencyInvite();
                $cd->users_id = auth()->user()->id;
                $cd->crew_id = $id;
                $cd->date_add = date('Y-m-d');
                $cd->save();  
                Alert::success('Thank you', 'Crew was added in your invitation');
            }else{
                Alert::warning('Sorry', 'Crew was added in your invitation before');
            }
        }

        return redirect()->back();
    }

    public function serviceRequest($type){
        
        // $ca = Service::where('users_id', auth()->user()->id)->where('types', $type)->whereDate('date_add',date('Y-m-d'))->doesntExist();
        // if($ca){
            $usr = User::where('id', auth()->user()->id)->first();
            
            $noinv = 'SVC'.Carbon::now()->timestamp.rand(10,99);

            $cd = new Service();
            $cd->code_service = $noinv;
            $cd->users_id = auth()->user()->id;
            $cd->types = $type;
            $cd->status = 'verification';
            $cd->date_add = date('Y-m-d');
            $cd->save();  
            Alert::success('Thank you', 'Thank you, for request service');


            if($type=='visa'){
                $ket = "[".$noinv."] Request Visa";
            }else if($type=='passport'){
                $ket = "[".$noinv."] Request Passport";
            }else if($type=='seamanbook'){
                $ket = "[".$noinv."] Request Seaman's Book";
            }else if($type=='certificaterevalidation'){
                $ket = "[".$noinv."] Request Certificate Revalidation";
            }else if($type=='trainingcenter'){
                $ket = "[".$noinv."] Request Training Center";
            }else if($type=='accommodation'){
                $ket = "[".$noinv."] Request Accommodation";
            }else if($type=='payroll'){
                $ket = "[".$noinv."] Request Payroll";
            }else if($type=='bunker'){
                $ket = "[".$noinv."] Request Bunker";
            } 
            ActivityLogger::activity("Service", $ket);
        // }else{
        //     Alert::warning('Sorry', 'for request service was add in the day');
        // }
        
        $pho = substr($usr->phone,0,1);
        
        if ($pho == 0){
            $telp = '+'.$usr->phone_code.substr($usr->phone,1);
        }else{
            $telp = '+'.$usr->phone_code.$usr->phone;
        }
        
        $data = array('message' => $ket, 'name' => $usr->name,'tgl' => date('d m Y'), 'phone' => $telp,'email' => $usr->email);
        
        Mail::to('admin@crewhires.com')->send(new EmailService($data));

        return redirect()->back();
    }    

    public function membershipActivation(){
        
        // $ca = Service::where('users_id', auth()->user()->id)->where('types', $type)->whereDate('date_add',date('Y-m-d'))->doesntExist();
        // if($ca){
            $cd = new Membership();
            $cd->code_member = 'MB'.Carbon::now()->timestamp.rand(10,99);
            $cd->users_id = auth()->user()->id;
            $cd->status = 'verification';
            $cd->date_add = date('Y-m-d');
            $cd->save();  
            Alert::success('Thank you', 'Thank you, for get membership');
        // }else{
        //     Alert::warning('Sorry', 'for request service was add in the day');
        // }

        return redirect()->route('membership.verification');
    }

    public function membershipVerification(){

        $mm = Membership::where('status','verification')->where('users_id',auth()->user()->id)->get();
        $act = "add";
        return view('frontend.membership-paid',compact('mm','act'));

    }

    public function paymentEdit($id){

        $mm = Membership::where('id',$id)->first();
        $act = "edit";
        return view('frontend.membership-paid',compact('mm','act'));

    }

    public function membershipPayment(Request $request){

        $file = $request->file('file');
        if($file){
            $nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
    
            $tujuan_upload = 'public/images/inv_member';
            $file->move($tujuan_upload,$nama_file);
        }    

        $pts = Membership::findOrFail($request->inv_mem);        
        $pts->bank_account = $request->rek_to;
        $pts->bau_name = $request->bau_name;
        $pts->account_number = $request->account_number;
        $pts->status = 'pay';
        $pts->date_paid = date('Y-m-d');

        if($file){
            $pts->photo_receipt = $nama_file;
        }

        $pts->update();

        Alert::success('Thank You', 'Thank you, for payment membership, waiting for verification');
        return redirect()->route('payment.list');
    }

    public function paymentList(){
        $mm = Membership::where('users_id',auth()->user()->id)->get();
        return view('frontend.membership-list',compact('mm'));

    }

    public function paymentDelete($id){
        Membership::where('id', $id)->delete();
        Alert::success('success', 'Thank you, invoice was deleted');

        return redirect()->back();  
    }

    public function chechPremium(){
        
    }

    public function getJobDesc($type_crew){

        $data = DB::table('job_desc')->where('dept', '=', $type_crew)->orderBy('job_description','ASC')->get();
        
        return response()->json($data);
    }
}
