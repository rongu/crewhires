<?php 
 
return [
    'p1' => 'Selamat bergabung dengan www.crewhires.com. Sebagai pusat penyedia informasi lowongan pekerjaan khusus untuk pelaut dan crew hotel secara professional berbasis situs web, kami berperan sebagai fasilitator pencocokan dan komunikasi lapangan kerja antara pencari kerja laut dan perusahaan pelayaran/pemilik kapal/agensi awak kapal baik yang akan beroperasi di perairan Indonesia maupun di luar Indonesia. Kami juga berperan sebagai media services pengurusan dokumen-dokumen pelaut yang dibutuhkan  secara online system.',
    'p2' => 'Berangkat dari profesional kerja di beberapa perusahaan pelayaran terkemuka selama bertahun-tahun,  www.crewhires.com didirikan pada tahun 2020 oleh tim profesional yang mempunyai kualifikasi dan kompenten pada bidangnya sehingga memastikan seluruh anggota yang bergabung di portal ini mendapatkan manfaat secara maksimal.',
];