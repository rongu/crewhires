@extends('layouts.frontend')

@section('title','Categories')
@section('desc','Halaman ini menyatakan Categories')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')


    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }

        .mt-widget-1 {
            border: 1px solid #e7ecf1;
            text-align: center;
            position: relative;
            margin-bottom: 30px;
        }

        .tabbable-custom>.nav-tabs>li.active {
            border-top: 3px solid #32c5d2;
            margin-top: 0;
            position: relative;
        }

        .mt-username{
            text-transform: capitalize;
        }

        .bg-image {

          filter: blur(10px);
          -webkit-filter: blur(10px);
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
        }

    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Categories
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">

        <div class="page-content-inner">

            <div class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active">
                        <a href="#tab_1_1_1" data-toggle="tab"> Crew Directory  </a>
                    </li>
                    <li>
                        <a href="#tab_1_1_2" data-toggle="tab"> Crew Vacancy </a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1_1">
                        <br>
                        <input type="hidden" class="form-control" id="id_role" value="<?php echo $role;?>">
                        <input type="hidden" class="form-control" id="id_login" value="<?php echo $id;?>">
                        <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Photo</th>                            
                                    <th>Fullname</th>                            
                                    <th>Type Crew</th>                            
                                    <th>Job Experience</th>                            
                                    <th>Status Crew</th>                            
                                    <th>&nbsp;</th>                            
                                </tr>
                            </thead>
                            <tbody>
                                
                           </tbody>
                        </table>
                        <!-- <div class="search-page search-content-4">
                            <div class="search-bar bordered">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                                <button class="btn green-soft uppercase bold" type="button">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>-->
                            <!-- <div class="search-table table-responsive" style="overflow-x:hidden;">
                                <div class="row">

                                    @foreach($crew as $vl)
                                        <div class="col-md-3">                                        
                                            <div class="mt-widget-1">
                                                @if($vl->status_account=='premium')
                                                <div class="mt-icon">
                                                    <a href="#">
                                                        <i class="fa fa-certificate" style="color: #17d200"></i>
                                                    </a>
                                                </div>
                                                @endif
                                                <div class="mt-img">
                                                    @if($vl->photo)

                                                        @if(Auth::check())
                                                        <img src="{{ url('public/images/profile/') }}/{{$vl->photo}}" width="150px" height="150px" class="zoomimg"> 
                                                        @else
                                                        <img src="{{ url('public/images/profile/') }}/{{$vl->photo}}" width="150px" height="150px" class="bg-image"> 
                                                        @endif
                                                    
                                                    @else
                                                    <img src="{{ url('public/images/profile.png') }}" width="150px" height="150px"> 
                                                    @endif
                                                </div>
                                                <div class="mt-body">
                                                    <h3 class="mt-username">
                                                        <a href="{{route('crew.cv',$vl->users_id)}}">{{$vl->fullname}}</a>
                                                    </h3>                                                
                                                    <div class="mt-stats">
                                                        <div class="btn-group btn-group btn-group-justified">
                                                            <div class="btn btn-lg font-blue-madison">
                                                                {{$vl->type_crew}}
                                                            </div>
                                                            <div class="btn btn-lg font-grey-mint">
                                                                {{$vl->status_activity}}
                                                            </div>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="mt-stats" style="margin:0px">
                                                        <div class="btn-group btn-group btn-group-justified">
                                                            @if($role==4)
                                                                <a href="{{route('agency.invite',$vl->users_id)}}" class="btn btn-lg bg-font-blue-dark blue-dark">          
                                                                    Invite
                                                                </a>
                                                            @else

                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="tab-pane" id="tab_1_1_2">
                        <br>
                        <div class="search-page search-content-4">
                            <div class="search-bar bordered">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                                <button class="btn green-soft uppercase bold" type="button">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <!-- <div class="table-scrollable table-scrollable-borderless" style="overflow-y: visible;"> -->
                                        <table class="table table-striped table-hover" id="sample_3" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Crew Vacancy</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                           </tbody>
                                        </table>
                                    <!-- </div> -->
                                </div>
                                <div class="col-lg-8">
                                    <form role="form" action="{{route('categories.crew.apply')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                                <input name="id" type="hidden" class="form-control" id="id_id" readonly="readonly">
                                                <p>
                                                    @if($status_account!='free')
                                                        <span id="photo_id"></span><br>
                                                        <b><span id="companyname_id"></span></b> <br>
                                                        <span id="address_id"></span><br><br>
                                                    @endif
                                                    <h3><b><span id="position_id"></span></b></h3><br>
                                                    Tanggal Pendaftaran:<br>
                                                    <span id="open_id"></span> - <span id="close_id"></span>
                                                </p>
                                                <p>
                                                    <b>Deskripsi Pekerjaan :</b><br>
                                                    <span id="requirement_id"></span>
                                                </p>
                                                <br><br>
                                                <p>
                                                    <b>Spesifik Kekosongan</b><br>
                                                    <span id="types_id"></span> - <span id="engine_id"></span>    
                                                </p>
                                                @if(($role!=4))
                                                    <button type="submit" class="btn green">Apply</button>
                                                @endif
                                        </form>
                                </div>
                            </div>    
                        </div>
                    </div>
                    
                </div>
            </div>


            
        </div>
        
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

     var table = $('#sample_1');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('categories.crew.all.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [5] },
            { searchable : false},                

            { render: function(data, type, row, meta) {

                var rol = document.getElementById("id_role").value;

                if(rol==='4'){

                    var btn = '<a href="javascript:;"data-id="'+data[0]+'" class="btn btn-outline btn-circle btn-sm purple edit_btn"><i class="fa fa-search"></i> Detail </a>';

                }else{
                    var btn = '';
                }

                return btn; }, targets: [5]
            },{ render: function(data, type, row, meta) {

                var id_login = document.getElementById("id_login").value;

                if(data == ''){
                    var foto = '<img src="{{url('public/images/profile.png')}}" height="80px" class="zoomimg" style="border-radius: 50% !important;">';
                }else{
                    if(id_login!='0'){
                        var foto = '<img src="{{url('public/images/profile')}}/'+data+'" width="80px" height="80px" class="zoomimg" style="border-radius: 50% !important;">';
                    }else{
                        var foto = '<img src="{{url('public/images/profile')}}/'+data+'" width="80px" height="80px" class="bg-image" style="border-radius: 50% !important;">';   
                    }
                }

                return foto; }, targets: [0]
            },
           
    ],
    columns: [
            { data: 'photo', name: 'photo' },            
            { data: 'fullname', name: 'fullname' },            
            { data: 'type_crew', name: 'type_crew' },
            { data: 'job_desc', name: 'job_desc' },
            { data: 'status_activity', name: 'status_activity' },
            { data: 'id', name: 'id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $("#sample_1").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;

        window.open('crew-cv/'+id,'_self',false);
    });

    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('categories.crew.vacancy.list')}}",                  
    },

    scrollY: 400,
    deferRender: true,
    scroller: {
        loadingIndicator: true
    },
    scrollX:        true,
    // scrollCollapse: true,        
    responsive:     true,  
    stateSave:      false,
    searching: false,
    columnDefs: [
            { orderable: false, targets: [0] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                // return '<a href="javascript:;" data-id="'+data[0]+'" data-position="'+data[1]+'" data-date_open="'+data[2]+'" data-types="'+data[3]+'" data-engine="'+data[4]+'" data-requirement="'+data[5]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [0]
                return '<a href="javascript:;"data-id="'+data[0]+'" data-position="'+data[1]+'" data-date_open="'+data[2]+'" data-types="'+data[3]+'" data-engine="'+data[4]+'" data-requirement="'+data[5]+'" data-date_close="'+data[6]+'" data-photo="'+data[7]+'" data-companyname="'+data[8]+'" data-address="'+data[9]+'" class="edit_btn"><p><h3>'+data[1]+'</h3><br><b>Spesialisasi Pekerjaan</b><br>'+data[3]+' - '+data[4]+'<br><br>Buka Pendaftaran<br>'+data[2]+' - '+data[6]+'</p></a>'; }, targets: [0]

            },
    ],
    columns: [
            { data: 'id', name: 'id' },
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    setTimeout(function(){ $("#sample_3>tbody>tr:first a.edit_btn").trigger('click'); }, 500);  

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var position = e.currentTarget.dataset.position;
        var date_open = e.currentTarget.dataset.date_open;
        var types = e.currentTarget.dataset.types;
        var engine = e.currentTarget.dataset.engine;
        var requirement = e.currentTarget.dataset.requirement;
        var date_close = e.currentTarget.dataset.date_close;
        var photo = "<img src='{{ url('public/images/profile/') }}/"+e.currentTarget.dataset.photo+"' width='150px' height='150px' class='zoomimg'>";
        var companyname = e.currentTarget.dataset.companyname;
        var address = e.currentTarget.dataset.address;

        $("#id_id").val(id);
        $("#position_id").text(position);
        $("#requirement_id").html(requirement);
        $("#open_id").text(date_open);
        $("#close_id").text(date_close);
        $("#types_id").text(types);
        $("#engine_id").text(engine);
        $("#companyname_id").text(companyname);
        $("#address_id").text(address);
        $("#photo_id").html(photo);
        
    });

    //swal("Terimakasih", "Anda telah menghapus data", "success");
        ezoom.onInit($('.zoomimg'), {
            hideControlBtn: true,
            onClose: function (result) {
                console.log(result);
            },
            onRotate: function (result) {
                console.log(result);
            },
        });

});
</script>
@endsection    