<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\MasterCop;
use \App\Models\MasterCoc;
use \App\Models\CrewDoc;
use \App\Models\CrewInfo;
use \App\Models\Country;
use \App\Models\AgencyJobs;
use \App\Models\AgencyInfo;
use \App\Models\Religion;
use Illuminate\Support\Facades\Hash;
use Session,DB;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
    	$usr = User::where('id',auth()->user()->id)->first();
    	$doc = CrewDoc::where('users_id',auth()->user()->id)->get();
    	$inf = CrewInfo::where('users_id',auth()->user()->id)->first();
		$mastercop = MasterCop::select('name','codes')->get();
		$mastercoc = MasterCoc::select('name')->get();
    	//$cty = Country::get();

    	$cty = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");
        $getBendera = (new static)->getCountry($inf->nationality);
        $bnd = $getBendera['flag_base64'];
        $calling = file_get_contents("vendor/samayo/country-json/src/country-by-calling-code.json");
        $reli = Religion::select('name')->get();
		$job_desc = DB::table('job_desc')->where('dept', '=', $inf->type_crew)->orderBy('job_description')->get();

    	return view('jobs.myprofile',compact('usr','doc','inf','cty','mastercop','mastercoc','bnd','calling','reli','job_desc'));
    }

    static public function getCountry($keys){

        $file = file_get_contents("vendor/samayo/country-json/src/country-by-flag.json");
        foreach (json_decode($file, true) as $key => $value) {
            if($value['country']===$keys){
                return $value;
            }
        }

    }

    public function deleteDocCrew($id){
          
        CrewDoc::where('id', $id)->delete();
        Session::flash('success', 'Document was deleted');

        return redirect()->back();
    }
    
    public function updateCrew(Request $request)
    {
        $form_t = $request->form_t;

        if($form_t==='profile'){
            $fullname = $request->fullname;
            $email = $request->email;
            $phone = $request->phone;
            $phone_code = $request->phone_code;
            $place = $request->place;
            $marital = $request->marital;
            $religion = $request->religion;
            $gender = $request->gender;
            $birth = $request->birth;
            $address = $request->address;
            $city = $request->city;
            $country = $request->country;
            $nationality = $request->nationality;
            $currency = $request->currency;
        	$salary = $request->salary;

            $cif_d = CrewInfo::select('id')->where('users_id', auth()->user()->id)->first();

            $cif = CrewInfo::findOrFail($cif_d->id);
            $cif->fullname = $fullname;
            $cif->place_birth = $place;
            $cif->birth = date('Y-m-d',strtotime($birth));
            $cif->marital = $marital;  
            $cif->religion = $religion;  
            $cif->gender = $gender;  
            $cif->address = $address;  
            $cif->city = $city;  
            $cif->country = $country;   
            $cif->nationality = $nationality;
            $cif->currency = $currency;  
            $cif->salary = $salary;     
            $cif->update(); 

            $usr = User::findOrFail(auth()->user()->id);
            $usr->email = $email;  
            $usr->phone = $phone;   
            $usr->phone_code = $phone_code;   
            $usr->update(); 

            Session::flash('success', 'Profile was updated');

        }else if($form_t==='account'){
        	$name = $request->name;
        	$password = $request->password;
        	
        	$usr = User::findOrFail(auth()->user()->id);
            $usr->name = $name;
            if($password){
            	$usr->password = Hash::make($password);	
            }  
               
            $usr->update(); 

            Session::flash('success', 'Account was updated');

        }else if($form_t==='photo'){
			$file = $request->file('file');
			if($file){
				$nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
				
				$tujuan_upload = 'public/images/profile';
				$file->move($tujuan_upload,$nama_file);

				$usr = User::findOrFail(auth()->user()->id);
				$usr->photo = $nama_file;
				$usr->update(); 

			}      	


            Session::flash('success', 'Photo was updated');

        }else if($form_t==='portofolio'){

			$file = $request->file('file_portofolio');
			if($file){
				$nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
				
				$tujuan_upload = 'public/images/portofolio';
				$file->move($tujuan_upload,$nama_file);

				$cif_d = CrewInfo::select('id')->where('users_id', auth()->user()->id)->first();
				$cif = CrewInfo::findOrFail($cif_d->id);
				$cif->portofolio = $nama_file;
				$cif->update(); 

			}      	


            Session::flash('success', 'Portofolio was updated');
            
        }else if($form_t==='sosmed'){
        	$fb = $request->fb;
        	$ig = $request->ig;
        	$tele = $request->tele;
        	$tw = $request->tw;
        	$wa = $request->wa;

			$cif_d = CrewInfo::select('id')->where('users_id', auth()->user()->id)->first();
			$cif = CrewInfo::findOrFail($cif_d->id);
			$cif->fb = $fb;
			$cif->ig = $ig;
			$cif->tg = $tele;
			$cif->tw = $tw;
			$cif->wa = $wa;
			
			$cif->update(); 
			Session::flash('success', 'Sosmed was updated');

        }else if($form_t==='info'){

        	$type_crew = $request->type_crew;
        	$job_id = $request->job_id;
        	$seaferer = $request->seaferer;
        	$status_activity = $request->status_activity;

			$cif_d = CrewInfo::select('id')->where('users_id', auth()->user()->id)->first();
			$cif = CrewInfo::findOrFail($cif_d->id);
			$cif->type_crew = $type_crew;
			$cif->job_desc = $job_id;
			$cif->seaferer = $seaferer;
			$cif->status_activity = $status_activity;

			
			$cif->update(); 
			Session::flash('success', 'Crew Status was updated');

        }else if($form_t==='document'){

			$exp = $request->exp;
			$cop = $request->cop;
			$coc = $request->coc;
			$flg = $request->flg;
			$stc = $request->stc;

        	//exp
	        foreach ($exp as $kee) {

	            if(!empty($kee['exp_nov'])){
	                $cd = new CrewDoc();
	                $cd->users_id = auth()->user()->id;
	                $cd->name = $kee['exp_nov'];
	                $cd->types = 'exp';
	                $cd->t_vessel = $kee['exp_tv'];
	                $cd->rank = $kee['exp_rank'];
	                $cd->company = $kee['exp_com'];
	                $cd->p_from = date('Y-m-d',strtotime($kee['exp_from']));
	                $cd->p_to = date('Y-m-d',strtotime($kee['exp_to']));
	                $cd->save();  
	            }


	        }        
	        //cop
	        foreach ($cop as $kee) {

	            if($kee['cop_noc']!='None'){
	                $cd = new CrewDoc();
	                $cd->users_id = auth()->user()->id;
	                $cd->name = $kee['cop_noc'];
	                $cd->types = 'cop';
	                $cd->cer_doc_number = $kee['cop_cn'];
	                $cd->place = $kee['cop_pl'];
	                $cd->doi = date('Y-m-d',strtotime($kee['cop_doi']));
	                $cd->ed = date('Y-m-d',strtotime($kee['cop_ed']));
	                $cd->save();  
	            }


	        }
	        //coc
	        foreach ($coc as $kee) {

	            if($kee['coc_noc']!='None'){
	                $cd = new CrewDoc();
	                $cd->users_id = auth()->user()->id;
	                $cd->name = $kee['coc_noc'];
	                $cd->types = 'coc';
	                $cd->cer_doc_number = $kee['coc_cn'];
	                $cd->place = $kee['coc_pl'];
	                $cd->doi = date('Y-m-d',strtotime($kee['coc_doi']));
	                $cd->ed = date('Y-m-d',strtotime($kee['coc_ed']));
	                $cd->save();  
	            }


	        }

	        //flg
	        foreach ($flg as $kee) {

	            if(!empty($kee['flg_nod'])){
	                $cd = new CrewDoc();
	                $cd->users_id = auth()->user()->id;
	                $cd->name = $kee['flg_nod'];
	                $cd->types = 'flg';
	                $cd->cer_doc_number = $kee['flg_cn'];
	                $cd->place = $kee['flg_pl'];
	                $cd->doi = date('Y-m-d',strtotime($kee['flg_doi']));
	                $cd->ed = date('Y-m-d',strtotime($kee['flg_ed']));
	                $cd->save();  
	            }


	        }

	        //flg
	        foreach ($stc as $kee) {

	            if(!empty($kee['stc_nod'])){
	                $cd = new CrewDoc();
	                $cd->users_id = auth()->user()->id;
	                $cd->name = $kee['stc_nod'];
	                $cd->types = 'stc';
	                $cd->cer_doc_number = $kee['stc_dn'];
	                $cd->place = $kee['stc_pl'];
	                $cd->doi = date('Y-m-d',strtotime($kee['stc_doi']));
	                $cd->ed = date('Y-m-d',strtotime($kee['stc_ed']));
	                $cd->save();  
	            }


	        }

	        Session::flash('success', 'Document was updated');

        }
        


        return redirect()->back();

    }

    public function myJob()
    {
    	$jb = AgencyJobs::select('position','types','engine','requirement','ag.name as agencyname','ag.photo as agencyphoto','date_close','crew_apply.apply_status',DB::raw('(select count(id) as jml from crew_apply cpa where cpa.agency_jobs_id = crew_apply.agency_jobs_id ) as jmlcount'))->leftjoin('crew_apply','crew_apply.agency_jobs_id','agency_jobs.id')->leftjoin('users as ag','ag.id','agency_jobs.users_id')->where('crew_apply.users_id',auth()->user()->id)->get();
        return view('jobs.myjob',compact('jb'));
    }

    public function myInvitation()
    {
    	$agency = AgencyInfo::select('agency_invite.id as id','agency_info.users_id as users_id','photo','name','companyname','type_agency','status_account','address','pic','pic_phone','country')->leftjoin('users','users.id','agency_info.users_id')->leftjoin('agency_invite','agency_invite.users_id','agency_info.users_id')->where('agency_invite.crew_id',auth()->user()->id)->get();

        return view('jobs.myinvitation',compact('agency'));
    }
    
}