-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Feb 2021 pada 07.36
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crewhires`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `coc`
--

CREATE TABLE `coc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','deck department','engine department','ratings deck','ratings engine','cook engine') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `coc`
--

INSERT INTO `coc` (`id`, `name`, `types`, `created_at`, `updated_at`) VALUES
(1, 'COC  Deck Officer Class I', 'deck department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(2, 'COC  Deck Officer Class II', 'deck department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(3, 'COC  Deck Officer Class III', 'deck department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(4, 'COC  Deck Officer Class IV', 'deck department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(5, 'COC  Deck Officer Class V', 'deck department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(6, 'COC Engineer Officer Class I', 'engine department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(7, 'COC Engineer Officer Class II', 'engine department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(8, 'COC Engineer Officer Class III', 'engine department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(9, 'COC Engineer Officer Class IV', 'engine department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(10, 'COC Engineer Officer Class V', 'engine department', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(11, 'Ratings As Able Deck Seaferer', 'ratings deck', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(12, 'Ratings As Able Engine Seaferer', 'ratings engine', '2021-02-28 06:25:05', '2021-02-28 06:25:05'),
(13, 'Ship\'\\s Cook MLC 2006', 'cook engine', '2021-02-28 06:25:05', '2021-02-28 06:25:05');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `coc`
--
ALTER TABLE `coc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `coc`
--
ALTER TABLE `coc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
