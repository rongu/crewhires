@extends('layouts.app-login')

@section('title', 'Register Crew & Agency')

@section('content')

<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset mt-login-5-bsfix">
            <div class="login-bg" style="background-image:url(../assets/pages/img/login/bg1.jpg)">
                <img class="login-logo" src="{{ asset('../public/images/Logo_crewhires.png') }}" width="70px" /> </div>
        </div>
        <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content">
                <h1>Registration</h1>
                <!-- <p> Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p> -->
                
                

                <div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#tab_1_1_1" data-toggle="tab"> Crew </a>
                        </li>
                        <li>
                            <a href="#tab_1_1_2" data-toggle="tab"> Agency/ship Owner </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_1">
                            <form id="register-crew" class="login-form" method="post" action="{{ route('register') }}">
                            @csrf
                            <input type="hidden" name="types" value="crew">
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control form-control-solid placeholder-no-fix form-group @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="email" class="form-control form-control-solid placeholder-no-fix form-group @error('email') has-error @enderror" name="email" autocomplete="off" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input id="phone" type="text" class="form-control form-control-solid placeholder-no-fix form-group @error('phone') has-error @enderror" name="phone" autocomplete="off" placeholder="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input id="password_id_crew" type="password" class="form-control form-control-solid placeholder-no-fix form-group @error('password') has-error @enderror" name="password"  placeholder="Password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror    
                                
                                </div>
                                <div class="col-xs-6">

                                    <input id="password-confirm_crew" type="password" class="form-control form-control-solid placeholder-no-fix form-group" name="password_confirmation" required placeholder="Confirm Password" autocomplete="new-password">

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    &nbsp;
                                </div>
                                <div class="col-sm-8 text-right">
                                    
                                    <button class="btn green" type="submit">Sign Up</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="tab_1_1_2">
                            <form id="register-agency" class="login-form" method="post" action="{{ route('register') }}">
                            @csrf
                            <input type="hidden" name="types" value="agency">

                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control form-control-solid placeholder-no-fix form-group @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="email" class="form-control form-control-solid placeholder-no-fix form-group @error('email') has-error @enderror" name="email" autocomplete="off" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input id="phone" type="text" class="form-control form-control-solid placeholder-no-fix form-group @error('phone') has-error @enderror" name="phone" autocomplete="off" placeholder="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input id="password_id_agency" type="password" class="form-control form-control-solid placeholder-no-fix form-group @error('password') has-error @enderror" name="password"  placeholder="Password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror    
                                
                                </div>
                                <div class="col-xs-6">

                                    <input id="password-confirm_agency" type="password" class="form-control form-control-solid placeholder-no-fix form-group" name="password_confirmation" required placeholder="Confirm Password" autocomplete="new-password">

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    &nbsp;
                                </div>
                                <div class="col-sm-8 text-right">
                                    
                                    <button class="btn green" type="submit">Sign Up</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
                    

                    
                    
                
                
            </div>
            <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-5 bs-reset">
                        <ul class="login-social">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-7 bs-reset">
                        <div class="login-copyright text-right">
                            <p>Copyright &copy; Crewhires</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('assets-bottom')

<script>
$(document).ready(function() {

    $('#register-crew').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            email: {
                required: true
            },
            phone: {
                required: true
            },
            password: {
                required: true
            },
            password_confirmation: {
                minlength: 2,
                equalTo: "#password_id_crew"
            },

        },

        messages: {
            email: {
                required: "Email is required."
            },
            phone: {
                required: "Phone is required."
            },
            password: {
                required: "Password is required."
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit   
            $('.alert-danger', $('#register-crew')).show();
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function(form) {
            form.submit(); // form validation success, call ajax form submit
        }
    });

    $('#register-crew input').keypress(function(e) {
        if (e.which == 13) {
            if ($('#register-crew').validate().form()) {
                $('#register-crew').submit(); //form validation success, call ajax form submit
            }
            return false;
        }
    });

    $('.forget-form input').keypress(function(e) {
        if (e.which == 13) {
            if ($('.forget-form').validate().form()) {
                $('.forget-form').submit();
            }
            return false;
        }
    });

    $('.login-bg').backstretch([
        "public/images/bg/bg1.jpg",
        "public/images/bg/bg2.jpg",
        "public/images/bg/bg3.jpg"
        ], {
        fade: 1000,
        duration: 8000
        }
    );

    $('.forget-form').hide();

    $('#forget-password').click(function(){
        $('#register-crew').hide();
        $('.forget-form').show();
    });

    $('#back-btn').click(function(){
        $('#register-crew').show();
        $('.forget-form').hide();
    });

    $('#register-agency').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            email: {
                required: true
            },
            phone: {
                required: true
            },
            password: {
                required: true
            },
            password_confirmation: {
                minlength: 2,
                equalTo: "#password_id_agency"
            },

        },

        messages: {
            email: {
                required: "Email is required."
            },
            phone: {
                required: "Phone is required."
            },
            password: {
                required: "Password is required."
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit   
            $('.alert-danger', $('#register-crew')).show();
        },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function(form) {
            form.submit(); // form validation success, call ajax form submit
        }
    });

    $('#register-agency input').keypress(function(e) {
        if (e.which == 13) {
            if ($('#register-agency').validate().form()) {
                $('#register-agency').submit(); //form validation success, call ajax form submit
            }
            return false;
        }
    });

});
</script>    
@endsection