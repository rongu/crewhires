

@extends('layouts.frontend')

@section('title','My job')
@section('desc','Halaman ini menyatakan my job')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }
        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>My Job
                
            </h1>
        </div>
        <div class="page-toolbar">
            <!-- BEGIN THEME PANEL -->
            <div class="btn-group btn-theme-panel">
                <a class="btn btn-lg bold" data-toggle="modal" href="#basic">
                    <i class="fa fa-plus-circle" style="color: #32c5d2"></i> Add
                </a>                
            </div>
            <!-- END THEME PANEL -->
        </div>
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <form id="submit_form_job" role="form" action="{{route('agency.myjob.create')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}  
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New Job</h4>
            </div>
            <div class="modal-body">
               
                <div class="form-body">

                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Position</label>
                        <input name="jl_po" type="text" class="form-control" />
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input has-success">
                                <label class="control-label">Type Vessel</label>
                                <input name="jl_ty" type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group  form-md-line-input has-success">
                                <label class="control-label">Engine</label>
                                <input name="jl_en" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    
                    

                    

                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Requirement</label>
                                            <!-- <input name="jl_re" type="text" class="form-control" />  -->
                        <textarea class="wysihtml5 form-control" rows="6" name="jl_re"></textarea>
                    </div>

                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Duration Recruit</label>
                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                        <input name="jl_open" type="text" class="form-control">
                        <span class="input-group-addon"> to </span>
                        <input name="jl_close" type="text" class="form-control"></div>
                    </div>                    
                </div>
                
                
            

            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </form>
    <!-- /.modal-dialog -->
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>My Job</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="search-page search-content-1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-container ">
                                    <ul>
                                        @php $i=1; @endphp
                                        @foreach($jb as $vl)
                                            <li class="search-item clearfix">
                                                <a href="javascript:;" class="btn btn-lg default pull-left">
                                                    <h3>{{$i++}}</h3>
                                                </a>
                                                <div class="search-content">
                                                    <h2 class="search-title">
                                                        <a href="javascript:;">{{$vl->position}}</a>
                                                    </h2>
                                                    <p class="search-desc">{!!$vl->requirement!!}</p>
                                                    <div>
                                                        <ul class="list-inline" style="padding-bottom: 13px;">                                                           
                                                                
                                                            <li>
                                                                Type : <span class="label label-info"> {{$vl->types}} </span></li>

                                                            <li>
                                                                Engine: <span class="label label-info"> {{$vl->engine}} </span></li>                                                            
                                                        </ul>
                                                    </div>
                                                    <div>    
                                                        <ul class="list-inline">
                                                            <li><a class="collapsed btn btn-link btn-only-icon" data-toggle="collapse"  href="#collapse_{{$i}}" aria-expanded="false"> <i class="fa fa-user"></i> {{$vl->jmlcount}} </a></li> 
                                                                
                                                            <li>
                                                                <i class="fa fa-calendar-check-o"></i> {{Carbon::parse($vl->date_open)->format('m/d/Y')}}</li>

                                                            <li>
                                                                <i class="fa fa-calendar-times-o"></i> {{Carbon::parse($vl->date_close)->format('m/d/Y')}}</li>
                                                            

                                                            
                                                        </ul>
                                                    </div>
                                                    <div id="collapse_{{$i}}" class="collapse" aria-expanded="false" data-id="{{$vl->id}}">
                                                        <div class="panel-body">
                                                            <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_{{$vl->id}}">
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Account Name</th>
                                                                        <th>Fullname</th>
                                                                        <th>Type</th>
                                                                        <th>Photo</th>
                                                                                                 
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    
                                                               </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                    <div class="search-pagination">
                                        <ul class="pagination">
                                            <li class="page-active">
                                                <a href="javascriptt:;"> 1 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 2 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 3 </a>
                                            </li>
                                            <li>
                                                <a href="javascriptt:;"> 4 </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    $('.wysihtml5').summernote({height: 300});
    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }

    $(document).on('hidden.bs.collapse', function (e) {
      // do something…
      
      console.log("dd "+e.target.dataset.id);

        var id = e.target.dataset.id;

        var table = $('#sample_'+id);
        var oTable = table.dataTable().fnDestroy();
    });

    $(document).on('show.bs.collapse', function (e) {
      // do something…
      
      console.log("pp "+e.target.dataset.id);

        var id = e.target.dataset.id;

        var table = $('#sample_'+id);

        var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        serverSide: true,
        ajax: {
            url: "{{route('agency.myjob.apply')}}",
            type : "POST",
            data :  function(data) {
                data.id = id;
                
                data._token= "{{ csrf_token() }}"
            }                    
        },

        scrollY:        400,
        deferRender:    true,
        scroller:       true,
        deferRender:    true,
        scrollX:        true,
              
        responsive:     false,  
        stateSave:      false,
        columnDefs: [
                { orderable: false, targets: [4] },
                { searchable : false},                

                { render: function(data, type, row, meta) {
                    return '<img src="{{url('public/images/profile')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [4]
                },

                { render: function(data, type, row, meta) {
                    return '<a href="{{url('crew-cv')}}/'+data[0]+'" target="_blank">'+data[1]+'</a>'; }, targets: [1]
                },
        ],
        columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'name', name: 'name' },
                { data: 'fullname', name: 'fullname' },
                { data: 'type_crew', name: 'type_crew' },                    
                { data: 'photo', name: 'photo' },     
            ],
        // "order": [
        //     [0, 'asc']
        // ],

        // lengthMenu: [
        //     [10, 15, 20, -1],
        //     [10, 15, 20, "All"] // change per page values here
        // ],
        // // set the initial value
        // pageLength: 10,

        dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        });

        
        ezoom.onInit($('.zoomimg'), {
            hideControlBtn: true,
            onClose: function (result) {
                console.log(result);
            },
            onRotate: function (result) {
                console.log(result);
            },
        });

    })

    var form_p = $('#submit_form_job');
    var error = $('.alert-danger', form_p);
    var success = $('.alert-success', form_p);

    form_p.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            jl_po: {
                required: true
            },
            jl_ty: {
                required: true
            },
            jl_en: {
                required: true
            },
            
            jl_open: {
                required: true
            },
            jl_close: {
                required: true
            },
            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_p[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    


});
</script>
@endsection    