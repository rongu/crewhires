<?php 
 
return [
    'h1' => 'Job Vacancy Information',
    'd1' => 'Job vacancies information on various types of ships that are always up to date and can be accessed easily from various platforms (iOS, Android, PC, Laptop. ',
    'h2' => 'Opportunity to work in reputed companies',
    'd2' => 'Gives you a great opportunity to get information on job vacancies and better salaries in well-known companies.',
    'h3' => 'Document Management Facility',
    'd3' => 'Make it easier for the crew to take care of the necessary documents before boarding the ship officially, openly and on time.',
    'h4' => 'Data Privacy Policy',
    'd4' => 'The database at www.crewhires.com has a high level of security, with access through detailed verification from our side so that it can be ensured to be safe.',
];