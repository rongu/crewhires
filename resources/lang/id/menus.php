<?php 
 
return [
    'home' => 'beranda',
    'categories' => 'kategori',
    'membership' => 'keanggotaan',
    'service' => 'layanan',
    'social media' => 'media sosial',
    'login' => 'masuk',
    'register' => 'daftar',
    'dashboard' => 'dasbor',
];