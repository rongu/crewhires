<?php 
 
return [
    'h1' => 'Informasi Lowongan Pekerjaan',
    'd1' => 'Informasi lowongan pekerjaan di berbagai tipe kapal yang selalu up to date dan dapat diakses dengan mudah dari berbagai platform (iOS, Android, PC, Laptop.',
    'h2' => 'Kesempatan bekerja di Perusahaan ternama',
    'd2' => 'Memberikan Anda kesempatan besar mendapatkan informasi lowongan pekerjaan dan gaji yang lebih baik di perusahaan ternama.',
    'h3' => 'Fasilitas Pengurusan Dokumen',
    'd3' => 'Memudahkan para awak kapal untuk mengurus dokumen-dokumen yang diperlukan sebelum naik ke kapal secara resmi, terbuka dan tepat waktu.',
    'h4' => 'Kebijakan Privasi Data',
    'd4' => 'Database di www.crewhires.com memiliki tingkat keamanan yang tinggi, dengan akses melalui verifikasi detail dari pihak kami sehingga dapat dipastikan aman.',
];