-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2021 at 05:09 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crewhires`
--

-- --------------------------------------------------------

--
-- Table structure for table `agency_info`
--

CREATE TABLE `agency_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `companyname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_agency` enum('AGENCY','SHIP OWNER') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic_phone_code` int(4) DEFAULT NULL,
  `fb` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tg` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portofolio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siup` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siuppak` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agency_info`
--

INSERT INTO `agency_info` (`id`, `users_id`, `companyname`, `type_agency`, `address`, `city`, `country`, `pic`, `pic_phone`, `pic_phone_code`, `fb`, `ig`, `tg`, `tw`, `wa`, `portofolio`, `document`, `siup`, `siuppak`, `created_at`, `updated_at`) VALUES
(1, 39, 'PT laut Biru', 'AGENCY', 'Jl Sempurna jaya', 'townroy', 'Mauritania', 'Hura', '0852111111', 506, 'a', 'b', 'c', 'd', 'e', 'port_39_1619669373.jpg', 'doc_39_1619669373.jpg', 'siup_39_1619669373.png', 'siuppak_39_1619669373.jpg', '2021-04-29 04:09:33', '2021-04-29 04:09:33'),
(2, 41, 'PT Dermaga Dunia', 'AGENCY', 'Jl pahlawan dunia', 'Ferita', 'Cyprus', 'Uyahhh', '06585521255', 355, NULL, NULL, NULL, NULL, NULL, 'port_41_1619669746.png', 'doc_41_1619669746.jpg', 'siup_41_1619669746.jpg', 'siuppak_41_1619669746.png', '2021-04-29 04:15:46', '2021-04-29 04:15:46'),
(3, 42, 'PT ARISNAM', 'SHIP OWNER', 'JL juanda Raya', 'Besta', 'Gambia', 'Jeku', '02521252225', 213, NULL, NULL, NULL, NULL, NULL, 'port_42_1619793202.png', 'doc_42_1619793202.jpg', NULL, NULL, '2021-04-30 14:33:22', '2021-04-30 14:33:22'),
(4, 44, 'PT kejora Timur', 'AGENCY', 'JL bangka', 'Jakarta', 'Indonesia', 'Iwan Banaran', '085211111', 213, NULL, NULL, NULL, NULL, NULL, 'port_44_1619880354.jpg', 'doc_44_1619880354.jpg', 'siup_44_1619880354.jpg', NULL, '2021-05-01 14:45:54', '2021-05-01 14:45:54'),
(5, 45, 'SHIPOWNER PESDRAD', 'SHIP OWNER', 'Jl bandung raya nomor 5566', 'bandung', 'Ghana', 'Jiuki Vardks', '085545554', 253, NULL, NULL, NULL, NULL, NULL, 'port_45_1619880839.jpg', 'doc_45_1619880839.jpg', 'siup_45_1619880839.jpg', NULL, '2021-05-01 14:53:59', '2021-05-01 14:53:59'),
(6, 46, 'SHIPOWNER GERIA', 'SHIP OWNER', 'Jl Canjur Raya Nomor 6569', 'Cimahai', 'Ghana', 'Keow nue', '0855545555', 57, NULL, NULL, NULL, NULL, NULL, 'port_46_1619882023.jpg', 'doc_46_1619882023.jpg', 'siup_46_1619882023.jpg', NULL, '2021-05-01 15:13:43', '2021-05-01 15:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `agency_invite`
--

CREATE TABLE `agency_invite` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `crew_id` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `status_invitation` enum('ADD','APPROVE','DELETE','REJECT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADD',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `agency_jobs`
--

CREATE TABLE `agency_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `engine` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirement` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_open` date DEFAULT NULL,
  `date_close` date DEFAULT NULL,
  `status` enum('open','close') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agency_jobs`
--

INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(1, 39, 'STAFF TEKNIK & PRODUKSI', 'Bn', 'Kxxx', '<div class=\"FYwKg _2MJ7O_0 _36UVG_0\" data-automation=\"jobDetailsHeader\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><div class=\"FYwKg _2Bz3E _1aq_D_0 _1lyEa BDgXc_0 _3ucB1_0 _28vkp_0\" style=\"margin: 0px 0px 0px -12px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-direction: row;\"><div class=\"FYwKg _3VCZm _1uk_1 _3Ve9Z _2li9R\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 530.656px; min-width: 0px; flex: 0 0 66.6667%;\"><div class=\"FYwKg _1GAuD zoxBO_0 JNERa_0 hLznC_0 _34Q9p_0 _2Bz3E rNAgI _1tfFt\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 302.328px; justify-content: flex-start;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _2Cp5K _3ftyQ _1lyEa\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative; display: flex; flex-direction: column;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _17IyL_0 sDUog_0 _1yTEl_0\" style=\"margin: 0px; padding: 0px 20px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"detailsTitle\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r UJoTY_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h1 class=\"FYwKg C6ZIU_0 _3nVJR_0 _642YY_0 _2DNlq_0 _2k6I7_0\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">STAFF TEKNIK &amp; PRODUKSI</h1></div><div class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _2CELK_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">PT Unichem Candi Indonesia</span></div></div></div></div></div></div></div></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _2Cp5K _3ftyQ _1lyEa _1xsO__0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative; display: flex; flex-direction: row; align-items: center;\"><div class=\"FYwKg _3VCZm _3qNSL_0 bMBHP_0 sDUog_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 32px 20px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 518.656px;\"><div class=\"FYwKg d7v3r _3122U_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Gresik</span></div></div><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">IDR&nbsp;4.300.000 - IDR&nbsp;4.500.000</span></div><div class=\"FYwKg _11hx2_0\" style=\"margin: 0px; padding: 4px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Ditayangkan pada 27-Apr-21</span></div></div></div></div></div></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3Ve9Z f3ti9\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 265.328px; min-width: 0px; flex: 0 0 33.3333%;\"><div class=\"FYwKg _1GAuD zoxBO_0 JNERa_0 hLznC_0 _34Q9p_0 _2Bz3E rNAgI _1tfFt\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 302.328px; justify-content: flex-start;\"></div></div></div></div><div class=\"FYwKg _2MJ7O_0 _3gJU3_0 _1yPon_0 _36Yi4_0 _1WtCj_0 FLByR_0 _2QIfI_0\" style=\"margin: 0px; padding: 48px 24px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">S1 TEKNIK INDUSTRI / TEKNIK MESIN / TEKNIK ELEKTRO</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">FRESH GRADUATED / PENGALAMAN LEBIH DIUTAMAKAN</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">MAHIR MS. OFFICE, TERUTAMA MS. EXCEL, MS. WORD, MS. POWER POIN</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">DIUTAMAKAN PAHAM ISO 9001 MANAJEMEN MUTU, FSSC, 5R DAN K3</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">DOMISILI GRESIK DAN SEKITARNYA LEBIH DIUTAMAKAN</li></ul></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tidak terspesifikasi</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><a href=\"https://www.jobstreet.co.id/id/job-search/engineering-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Teknik\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Teknik</a>,&nbsp;<a href=\"https://www.jobstreet.co.id/id/job-search/electrical-engineering-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Teknik Elektro\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Teknik Elektro</a></span></div></div></div></div></div></div></div></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3VCZm _36Yi4_0 _1WtCj_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 48px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px;\"><div class=\"FYwKg _2Cp5K\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative;\"><div class=\"FYwKg _3VCZm _3ra6Y _1vY2t_0 TfmPI_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px; position: absolute; height: 1px; background: rgb(216, 220, 226);\"></div></div></div><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Tentang Perusahaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: 700;\">P.T. Unichem Candi Industri</span>&nbsp;is a leader in producing special minerals and chemicals for oil &amp; gas industry, mining and other related services to various industries.</div><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px; text-align: justify;\">Our mission is to continually provide these and other services by promoting a reputation for excellence and value while fully anticipating, then recognizing the needs and expectations of our customers.</div><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px; text-align: justify;\">Our market focus is directed towards providing unequaled service to our customers, co-workers and investors through recognition of the processes that are of vital importance to them and the goals, which much be established and achieved to continually progress toward total reliability and satisfaction.</div></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan Perusahaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Ukuran Perusahaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">51 - 200 pekerja</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Waktu Proses Lamaran</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">29 hari</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Industri</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Produk Konsumen/Barang konsumen yang bergerak cepat</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tunjangan dan Lain-lain</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Asuransi kesehatan,&nbsp;Parkir,&nbsp;Formil (contoh: Kemeja + Dasi),&nbsp;Monday - Saturday</span></div></div></div></div></div></div></div></div></div></div></div></div></div>', '2021-04-01', '2021-04-30', 'open', '2021-04-29 04:09:33', '2021-04-29 04:09:33');
INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(2, 41, 'Warehouse Supervisor', 'Kl', 'uyss', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\"><span style=\"font-weight: 700;\">Why Join Us?</span><span style=\"font-weight: 700;\">&nbsp;</span>Shipper is on a mission to invent a new standard for online shipping in Indonesia. We have assembled the largest asset-light delivery network to make shipping simple, reliable, and cost competitive. With over 17,000 islands, 2,500 logistics providers, and a fast growing e-commerce market, Indonesia is one of the most challenging markets to build a great online shipping experience and we are primed to be a part of the solution. We are privileged to have some of the best global investors including Lightspeed, Y Combinator, Sequoia Capital India, DST Global Partners. We are looking for passionate and outstanding individuals to join our team.<br><span style=\"font-weight: 700;\">What You Will Do</span><br><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Maintaining inbound and outbound process in the warehouse</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Organizing, coordinating, and supervising all subordinate task to fit with the company\'s SOP</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Managing all resource for warehouse &amp; transportation activity including people, vehicle, and equipment</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Improving activity in the warehouse, include for forecasting</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Making reports and analysis of warehouse performance</li></ul><br><span style=\"font-weight: 700;\">What Are the Requirements</span><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Diploma degree of any major</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Having experience in Logistic/Supply Chain/Warehouse at least 2 year</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Understanding inventory management and method</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Clear understanding of logistic &amp; warehouse</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Having excellent skill (s) in communication, leadership, analytical thinking &amp; problem solving</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Candidate who can join as soon as possible will be prioritized</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Willing to be placed in any warehouse location</li></ul></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Supervisor/Koordinator</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 90.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Sertifikat Professional, D3 (Diploma), D4 (Diploma), Sarjana (S1)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">2 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Pelayanan</span></font>,&nbsp;<font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Logistik/Rantai Pasokan</span></font></span></div></div></div></div></div></div></div></div></div>', '2021-01-01', '2021-06-30', 'open', '2021-04-29 04:15:47', '2021-04-29 04:15:47'),
(3, 41, 'Customer Service', 'Znnnm', 'Klllsk', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0 _2FwxQ_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"job-details-job-highlights\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Keuntungan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul class=\"FYwKg _302h6 d7v3r UJoTY_0\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; list-style: none;\"><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Lingkungan kerja yang baik. Prospek untuk maju dan berkembang</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">mendapatkan training dan pengetahuan</span></div></div></li></ul></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div style=\"padding-top: 12px; padding-bottom: 12px; margin: 0px;\">Persyaratan :<ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Kandidat minimal lulusan SLTA/sederajat</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kesehatan yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kemampuan komunikasi verbal</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki passion di bidang customer service</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki kemampuan problem solving yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Berpenampilan rapi</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Bisa berbahasa Inggris minimal pasif</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Memiliki pengalaman sebagai customer service minimal 1 tahun (fresh graduate silahkan boleh melamar)</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Ramah dan senang melayani customer</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Maksimal berusia 24 tahun, memiliki berat dan tinggi yang ideal</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Memiliki attitude yang baik</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc; text-align: justify;\">Dapat mengoperasikan komputer.</li></ul></div></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tidak terspesifikasi</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">1 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><a href=\"https://www.jobstreet.co.id/id/job-search/services-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Pelayanan\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Pelayanan</a>,&nbsp;<a href=\"https://www.jobstreet.co.id/id/job-search/customer-service-jobs/\" rel=\"nofollow\" class=\"_27Shq_0 qbDva _2CELK_0 FYwKg _2k9O2 _2Nj6k\" title=\"Limit results to Layanan Pelanggan\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(39, 101, 207); cursor: pointer;\">Layanan Pelanggan</a></span></div></div></div></div></div></div></div></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3VCZm _36Yi4_0 _1WtCj_0 _2Bz3E\" style=\"margin: 0px; padding: 0px 0px 48px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 736px;\"><div class=\"FYwKg _2Cp5K\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; position: relative;\"></div></div></div>', '2021-04-01', '2021-06-05', 'open', '2021-04-29 04:15:47', '2021-04-29 04:15:47');
INSERT INTO `agency_jobs` (`id`, `users_id`, `position`, `types`, `engine`, `requirement`, `date_open`, `date_close`, `status`, `created_at`, `updated_at`) VALUES
(4, 42, 'Export Customer Service & Documentation Semarang Office', 'KLOA', 'JKLKI', '<div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg\" data-automation=\"job-details-job-highlights\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Keuntungan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><ul class=\"FYwKg _302h6 d7v3r UJoTY_0\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; list-style: none;\"><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Top 10 NVOCC from Asia to USA</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Reliable Partner to all Carriers</span></div></div></li><li class=\"FYwKg _6Gmbl_0\" style=\"margin: 0px; padding: 20px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex;\"><div class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\"><div class=\"FYwKg _3ftyQ _3O7rA mY9oq _26CPL_0\" aria-hidden=\"true\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; align-items: center; user-select: none; height: 24px;\"><div class=\"FYwKg _1A6vC _25blN _2FgpS\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; border-radius: 50%; background: currentcolor; width: 4px; height: 4px;\"></div></div></div><div class=\"FYwKg _3VCZm _1uk_1 _3RqUb_0\" style=\"margin: 0px; padding: 0px 0px 0px 12px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; width: 732px; min-width: 0px;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Convenience Working Place Atmosphere</span></div></div></li></ul></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Deskripsi Pekerjaan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div data-automation=\"jobDescription\" class=\"vDEj0_0\" style=\"font-family: inherit;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><div class=\"FYwKg\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><p style=\"padding-top: 12px; padding-bottom: 12px; margin-bottom: 0px;\">Job Description:</p><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Export administration and customer service</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Follow SOP in performance of all operation processes</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Co-ordinate with overseas offices, partner agents and customers for timely shipment</li></ul><p style=\"padding-top: 12px; padding-bottom: 12px; margin-bottom: 0px;\">Job requirement:</p><ul style=\"padding-bottom: 12px;\"><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Candidate must possess at least Bachelor\'s Degree in Business Studies/Administration/Management, Logistic/Transportation or equivalent.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Required language(s):&nbsp;English, Bahasa Indonesia</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Must have at least 2&nbsp;Year(s) of working experience in international freight forwarding is required for this position.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Required Skill(s): Microsoft Office, Forwarding Application</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Preferably Staff (non-management &amp; non-supervisor) specialized in Customer Service or equivalent.</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Having a good communication skill and capable in English communication</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Good computer skill</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Able to work under pressure and deadline</li><li style=\"padding-bottom: 4px; margin-left: 20px; list-style: disc;\">Responsible and can work proactively&nbsp;and can work in team</li></ul></div></span></div></div></div></div><div class=\"FYwKg _3gJU3_0 _1yPon_0\" style=\"margin: 0px; padding: 48px 0px 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; font-family: &quot;Times New Roman&quot;; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3aoZS_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><h4 class=\"FYwKg C6ZIU_0 _3nVJR_0 _2VCbC_0 _2DNlq_0 _1VMf3_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: 28px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; color: rgb(30, 34, 43);\">Informasi Tambahan</h4></div><div class=\"FYwKg fB92N_0\" style=\"margin: 0px; padding: 24px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg d7v3r _3BZ6E_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg _3ftyQ _1q9J3 _3gDk-_0\" style=\"margin: 0px 0px 0px -32px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: flex; flex-wrap: wrap;\"><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Tingkat Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pegawai (non-manajemen &amp; non-supervisor)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Kualifikasi</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Sarjana (S1)</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Pengalaman Kerja</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">2 tahun</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Jenis Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Penuh Waktu</span></div></div></div></div><div class=\"FYwKg _1uk_1 _2fqoM_0 _1hqiH_0\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; min-width: 0px; flex: 0 0 50%;\"><div class=\"FYwKg _1GAuD _3gJU3_0 _3wXaW_0\" style=\"margin: 0px; padding: 32px 0px 0px 32px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent; height: 66.7812px;\"><div class=\"FYwKg d7v3r _2uGS9_0\" style=\"margin: 0px; padding: 1px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\">Spesialisasi Pekerjaan</span></div><div class=\"FYwKg zoxBO_0\" style=\"margin: 0px; padding: 12px 0px 0px; border: 0px; font: inherit; vertical-align: baseline; -webkit-tap-highlight-color: transparent;\"><span class=\"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _2WTa0_0\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: Roboto, &quot;Helvetica Neue&quot;, HelveticaNeue, Helvetica, Arial, sans-serif; vertical-align: baseline; -webkit-tap-highlight-color: transparent; display: block; color: rgb(30, 34, 43);\"><font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Pelayanan</span></font>,&nbsp;<font color=\"#2765cf\" face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; -webkit-tap-highlight-color: transparent; cursor: pointer;\">Logistik/Rantai Pasokan</span></font></span></div></div></div></div></div></div></div></div></div>', '2021-04-01', '2021-08-01', 'open', '2021-04-30 14:33:22', '2021-04-30 14:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `coc`
--

CREATE TABLE `coc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','deck department','engine department','ratings deck','ratings engine','cook engine') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coc`
--

INSERT INTO `coc` (`id`, `name`, `types`, `created_at`, `updated_at`) VALUES
(1, 'None', 'none', '2021-02-28 06:37:59', '2021-02-28 06:37:59'),
(2, 'COC  Deck Officer Class I', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(3, 'COC  Deck Officer Class II', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(4, 'COC  Deck Officer Class III', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(5, 'COC  Deck Officer Class IV', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(6, 'COC  Deck Officer Class V', 'deck department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(7, 'COC Engineer Officer Class I', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(8, 'COC Engineer Officer Class II', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(9, 'COC Engineer Officer Class III', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(10, 'COC Engineer Officer Class IV', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(11, 'COC Engineer Officer Class V', 'engine department', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(12, 'Ratings As Able Deck Seaferer', 'ratings deck', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(13, 'Ratings As Able Engine Seaferer', 'ratings engine', '2021-02-27 23:25:05', '2021-02-27 23:25:05'),
(14, 'Ship\'s Cook MLC 2006', 'cook engine', '2021-02-27 23:25:05', '2021-02-27 23:25:05');

-- --------------------------------------------------------

--
-- Table structure for table `cop`
--

CREATE TABLE `cop` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` enum('none','general','deck department','engine department') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cop`
--

INSERT INTO `cop` (`id`, `name`, `codes`, `types`, `created_at`, `updated_at`) VALUES
(1, 'None', '', 'none', '2021-02-28 05:51:19', '2021-02-28 05:51:26'),
(2, 'Basic Safety Training', 'BST', 'general', '2021-02-28 05:51:24', '2021-02-28 05:51:29'),
(3, 'Proficiency Survival Craft and Rescue Boat', 'SCRB', 'general', '2021-02-28 05:51:36', '2021-02-28 05:51:36'),
(4, 'Advanced Fire Fighting', 'AFF', 'general', '2021-02-28 05:52:05', '2021-02-28 05:52:05'),
(5, 'Medical First Aid', 'MFA', 'general', '2021-02-28 05:52:30', '2021-02-28 05:52:30'),
(6, 'Medical Care Onboard Ship', 'MC', 'general', '2021-02-28 05:52:58', '2021-02-28 05:52:58'),
(7, 'Security Awareness Training', 'SAT', 'general', '2021-02-28 05:53:22', '2021-02-28 05:53:22'),
(8, 'Seaferer With Designated Security Duties', 'SDSD', 'general', '2021-02-28 05:53:54', '2021-02-28 05:53:54'),
(9, 'Ship Security Officer', 'SSO', 'general', '2021-02-28 05:54:21', '2021-02-28 05:54:21'),
(10, 'Bridge Resource Management', 'BRM', 'deck department', '2021-02-28 05:54:55', '2021-02-28 05:54:55'),
(11, 'ARPA Simulator', '', 'deck department', '2021-02-28 05:55:21', '2021-02-28 05:55:21'),
(12, 'RADAR Simulator', '', 'deck department', '2021-02-28 05:55:42', '2021-02-28 05:55:42'),
(13, 'Electronic Chart Display', 'ECDIS', 'deck department', '2021-02-28 05:56:08', '2021-02-28 05:56:08'),
(14, 'Global Maritime Distress Safety System', 'GMDSS', 'deck department', '2021-02-28 05:56:55', '2021-02-28 05:56:55'),
(15, 'General Operator Certificate', 'GOC Book', 'deck department', '2021-02-28 05:57:20', '2021-02-28 05:57:20'),
(16, 'Basic Training for Liquified Gas Tanker Cargo Operations', 'BLGT', 'general', '2021-02-28 05:58:07', '2021-02-28 05:58:07'),
(17, 'Advanced Training for Liquified Gas Tanker Cargo Operations', 'ALGT', 'general', '2021-02-28 05:59:17', '2021-02-28 05:59:17'),
(18, 'Basic Training for Oil and Chemical Tanker Cargo Operations', 'BOCT', 'general', '2021-02-28 06:00:16', '2021-02-28 06:00:16'),
(19, 'DP Basic', '', 'deck department', '2021-02-28 06:01:07', '2021-02-28 06:01:07'),
(20, 'DP Advanced', '', 'deck department', '2021-02-28 06:01:27', '2021-02-28 06:01:27'),
(21, 'Full DP', '', 'deck department', '2021-02-28 06:01:50', '2021-02-28 06:01:50'),
(22, 'DP Maintenance', '', 'engine department', '2021-02-28 06:02:05', '2021-02-28 06:02:05'),
(23, 'Engine Resources Management', 'ERM', 'engine department', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(24, 'Marine High Voltage', 'MHV', 'deck department', '2021-02-28 06:02:20', '2021-02-28 06:02:20'),
(25, 'Crowd Management', 'CM', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(26, 'Food Handling', '', 'general', '2021-02-28 06:03:10', '2021-02-28 06:03:10'),
(27, 'Food Safety', '', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(28, 'Helicopter Under Escape Training', 'HUET', 'general', '2021-02-28 06:03:55', '2021-02-28 06:03:55'),
(29, 'Basic Offshore Safety Induction & Emergency Training', 'BOSIET', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39'),
(30, 'International Safety Management Code', 'ISM Code', 'general', '2021-02-28 06:04:39', '2021-02-28 06:04:39');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `codes`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'AF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(2, 'Albania', 'AL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(3, 'Algeria', 'DZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(4, 'American Samoa', 'AS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(5, 'Andorra', 'AD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(6, 'Angola', 'AO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(7, 'Anguilla', 'AI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(8, 'Argentina', 'AR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(9, 'Armenia', 'AM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(10, 'Aruba', 'AW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(11, 'Australia', 'AU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(12, 'Austria', 'AT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(13, 'Azerbaijan', 'AZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(14, 'Bahamas', 'BS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(15, 'Bahrain', 'BH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(16, 'Bangladesh', 'BD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(17, 'Barbados', 'BB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(18, 'Belarus', 'BY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(19, 'Belgium', 'BE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(20, 'Belize', 'BZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(21, 'Benin', 'BJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(22, 'Bermuda', 'BM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(23, 'Bhutan', 'BT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(24, 'Bolivia', 'BO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(25, 'Bosnia and Herzegowina', 'BA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(26, 'Botswana', 'BW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(27, 'Bouvet Island', 'BV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(28, 'Brazil', 'BR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(29, 'British Indian Ocean Territory', 'IO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(30, 'Brunei Darussalam', 'BN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(31, 'Bulgaria', 'BG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(32, 'Burkina Faso', 'BF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(33, 'Burundi', 'BI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(34, 'Cambodia', 'KH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(35, 'Cameroon', 'CM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(36, 'Canada', 'CA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(37, 'Cape Verde', 'CV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(38, 'Cayman Islands', 'KY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(39, 'Central African Republic', 'CF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(40, 'Chad', 'TD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(41, 'Chile', 'CL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(42, 'China', 'CN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(43, 'Christmas Island', 'CX', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(44, 'Cocos (Keeling) Islands', 'CC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(45, 'Colombia', 'CO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(46, 'Comoros', 'KM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(47, 'Congo', 'CG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(48, 'Congo, the Democratic Republic of the', 'CD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(49, 'Cook Islands', 'CK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(50, 'Costa Rica', 'CR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(51, 'Cote d\'Ivoire', 'CI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(52, 'Croatia', 'HR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(53, 'Cuba', 'CU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(54, 'Cyprus', 'CY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(55, 'Czech Republic', 'CZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(56, 'Denmark', 'DK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(57, 'Djibouti', 'DJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(58, 'Dominica', 'DM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(59, 'Dominican Republic', 'DO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(60, 'Ecuador', 'EC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(61, 'Egypt', 'EG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(62, 'El Salvador', 'SV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(63, 'Equatorial Guinea', 'GQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(64, 'Eritrea', 'ER', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(65, 'Estonia', 'EE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(66, 'Ethiopia', 'ET', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(67, 'Falkland Islands (Malvinas)', 'FK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(68, 'Faroe Islands', 'FO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(69, 'Fiji', 'FJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(70, 'Finland', 'FI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(71, 'France', 'FR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(72, 'French Guiana', 'GF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(73, 'French Polynesia', 'PF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(74, 'French Southern Territories', 'TF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(75, 'Gabon', 'GA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(76, 'Gambia', 'GM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(77, 'Georgia', 'GE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(78, 'Germany', 'DE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(79, 'Ghana', 'GH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(80, 'Gibraltar', 'GI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(81, 'Greece', 'GR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(82, 'Greenland', 'GL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(83, 'Grenada', 'GD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(84, 'Guadeloupe', 'GP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(85, 'Guam', 'GU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(86, 'Guatemala', 'GT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(87, 'Guinea', 'GN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(88, 'Guinea-Bissau', 'GW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(89, 'Guyana', 'GY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(90, 'Haiti', 'HT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(91, 'Heard and Mc Donald Islands', 'HM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(92, 'Holy See (Vatican City State)', 'VA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(93, 'Honduras', 'HN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(94, 'Hong Kong', 'HK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(95, 'Hungary', 'HU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(96, 'Iceland', 'IS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(97, 'India', 'IN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(98, 'Indonesia', 'ID', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(99, 'Iran (Islamic Republic of)', 'IR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(100, 'Iraq', 'IQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(101, 'Ireland', 'IE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(102, 'Israel', 'IL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(103, 'Italy', 'IT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(104, 'Jamaica', 'JM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(105, 'Japan', 'JP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(106, 'Jordan', 'JO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(107, 'Kazakhstan', 'KZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(108, 'Kenya', 'KE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(109, 'Kiribati', 'KI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(110, 'Korea, Democratic People\'s Republic of', 'KP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(111, 'Korea, Republic of', 'KR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(112, 'Kuwait', 'KW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(113, 'Kyrgyzstan', 'KG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(114, 'Lao People\'s Democratic Republic', 'LA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(115, 'Latvia', 'LV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(116, 'Lebanon', 'LB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(117, 'Lesotho', 'LS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(118, 'Liberia', 'LR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(119, 'Libyan Arab Jamahiriya', 'LY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(120, 'Liechtenstein', 'LI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(121, 'Lithuania', 'LT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(122, 'Luxembourg', 'LU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(123, 'Macau', 'MO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(124, 'Macedonia, The Former Yugoslav Republic of', 'MK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(125, 'Madagascar', 'MG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(126, 'Malawi', 'MW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(127, 'Malaysia', 'MY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(128, 'Maldives', 'MV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(129, 'Mali', 'ML', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(130, 'Malta', 'MT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(131, 'Marshall Islands', 'MH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(132, 'Martinique', 'MQ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(133, 'Mauritania', 'MR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(134, 'Mauritius', 'MU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(135, 'Mayotte', 'YT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(136, 'Mexico', 'MX', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(137, 'Micronesia, Federated States of', 'FM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(138, 'Moldova, Republic of', 'MD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(139, 'Monaco', 'MC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(140, 'Mongolia', 'MN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(141, 'Montserrat', 'MS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(142, 'Morocco', 'MA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(143, 'Mozambique', 'MZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(144, 'Myanmar', 'MM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(145, 'Namibia', 'NA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(146, 'Nauru', 'NR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(147, 'Nepal', 'NP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(148, 'Netherlands', 'NL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(149, 'Netherlands Antilles', 'AN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(150, 'New Caledonia', 'NC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(151, 'New Zealand', 'NZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(152, 'Nicaragua', 'NI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(153, 'Niger', 'NE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(154, 'Nigeria', 'NG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(155, 'Niue', 'NU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(156, 'Norfolk Island', 'NF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(157, 'Northern Mariana Islands', 'MP', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(158, 'Norway', 'NO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(159, 'Oman', 'OM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(160, 'Pakistan', 'PK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(161, 'Palau', 'PW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(162, 'Panama', 'PA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(163, 'Papua New Guinea', 'PG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(164, 'Paraguay', 'PY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(165, 'Peru', 'PE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(166, 'Philippines', 'PH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(167, 'Pitcairn', 'PN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(168, 'Poland', 'PL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(169, 'Portugal', 'PT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(170, 'Puerto Rico', 'PR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(171, 'Qatar', 'QA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(172, 'Reunion', 'RE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(173, 'Romania', 'RO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(174, 'Russian Federation', 'RU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(175, 'Rwanda', 'RW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(176, 'Saint Kitts and Nevis', 'KN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(177, 'Saint LUCIA', 'LC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(178, 'Saint Vincent and the Grenadines', 'VC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(179, 'Samoa', 'WS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(180, 'San Marino', 'SM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(181, 'Sao Tome and Principe', 'ST', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(182, 'Saudi Arabia', 'SA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(183, 'Senegal', 'SN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(184, 'Seychelles', 'SC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(185, 'Sierra Leone', 'SL', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(186, 'Singapore', 'SG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(187, 'Slovakia (Slovak Republic)', 'SK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(188, 'Slovenia', 'SI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(189, 'Solomon Islands', 'SB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(190, 'Somalia', 'SO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(191, 'South Africa', 'ZA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(192, 'South Georgia and the South Sandwich Islands', 'GS', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(193, 'Spain', 'ES', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(194, 'Sri Lanka', 'LK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(195, 'St. Helena', 'SH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(196, 'St. Pierre and Miquelon', 'PM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(197, 'Sudan', 'SD', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(198, 'Suriname', 'SR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(199, 'Svalbard and Jan Mayen Islands', 'SJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(200, 'Swaziland', 'SZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(201, 'Sweden', 'SE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(202, 'Switzerland', 'CH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(203, 'Syrian Arab Republic', 'SY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(204, 'Taiwan, Province of China', 'TW', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(205, 'Tajikistan', 'TJ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(206, 'Tanzania, United Republic of', 'TZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(207, 'Thailand', 'TH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(208, 'Togo', 'TG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(209, 'Tokelau', 'TK', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(210, 'Tonga', 'TO', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(211, 'Trinidad and Tobago', 'TT', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(212, 'Tunisia', 'TN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(213, 'Turkey', 'TR', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(214, 'Turkmenistan', 'TM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(215, 'Turks and Caicos Islands', 'TC', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(216, 'Tuvalu', 'TV', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(217, 'Uganda', 'UG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(218, 'Ukraine', 'UA', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(219, 'United Arab Emirates', 'AE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(220, 'United Kingdom', 'GB', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(221, 'United States', 'US', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(222, 'United States Minor Outlying Islands', 'UM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(223, 'Uruguay', 'UY', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(224, 'Uzbekistan', 'UZ', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(225, 'Vanuatu', 'VU', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(226, 'Venezuela', 'VE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(227, 'Viet Nam', 'VN', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(228, 'Virgin Islands (British)', 'VG', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(229, 'Virgin Islands (U.S.)', 'VI', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(230, 'Wallis and Futuna Islands', 'WF', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(231, 'Western Sahara', 'EH', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(232, 'Yemen', 'YE', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(233, 'Zambia', 'ZM', '2021-03-02 09:59:28', '2021-03-02 09:59:28'),
(234, 'Zimbabwe', 'ZW', '2021-03-02 09:59:28', '2021-03-02 09:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `crew_apply`
--

CREATE TABLE `crew_apply` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `agency_jobs_id` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `apply_status` enum('ADD','APPROVE','REJECT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADD',
  `apply_by` enum('USER','ADMIN') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crew_apply`
--

INSERT INTO `crew_apply` (`id`, `users_id`, `agency_jobs_id`, `date_add`, `apply_status`, `apply_by`, `created_at`, `updated_at`) VALUES
(4, 38, 3, '2021-04-29', 'APPROVE', 'ADMIN', '2021-04-29 11:22:12', '2021-04-29 11:22:12'),
(5, 37, 1, '2021-07-16', 'ADD', 'USER', '2021-07-16 05:13:12', '2021-07-16 05:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `crew_doc`
--

CREATE TABLE `crew_doc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cer_doc_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `types` enum('exp','cop','coc','flg','stc') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doi` date DEFAULT NULL,
  `ed` date DEFAULT NULL,
  `rank` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_vessel` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_from` date DEFAULT NULL,
  `p_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crew_doc`
--

INSERT INTO `crew_doc` (`id`, `users_id`, `name`, `cer_doc_number`, `types`, `place`, `doi`, `ed`, `rank`, `t_vessel`, `company`, `p_from`, `p_to`, `created_at`, `updated_at`) VALUES
(1, 37, 'xxxxxx', NULL, 'exp', NULL, NULL, NULL, 'xxx', 'xxx', 'xxxx', '2020-01-01', '2022-03-01', '2021-04-29 04:01:34', '2021-04-29 04:01:34'),
(2, 37, 'Proficiency Survival Craft and Rescue Boat', 'ssssssss', 'cop', 'sss', '2018-02-01', '2024-10-01', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(3, 37, 'COC  Deck Officer Class I', 'cccccc', 'coc', 'ccc', '2021-01-01', '2021-10-02', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(4, 37, 'zzz', 'zzzz', 'flg', 'zzz', '2021-04-01', '2021-07-09', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35'),
(5, 37, 'qqq', 'qqq', 'stc', 'qqq', '2021-03-01', '2022-01-01', NULL, NULL, NULL, NULL, NULL, '2021-04-29 04:01:35', '2021-04-29 04:01:35');

-- --------------------------------------------------------

--
-- Table structure for table `crew_info`
--

CREATE TABLE `crew_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `fullname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_crew` enum('MARINE','CRUISE') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `place_birth` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital` enum('Single','Married','Divorced','Separated','Widowed') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('m','f') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` enum('IDR','USD') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `seaferer` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_activity` enum('STANDBY','AVAILABLE','ON BOARD') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tg` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portofolio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crew_info`
--

INSERT INTO `crew_info` (`id`, `users_id`, `fullname`, `type_crew`, `job_desc`, `birth`, `place_birth`, `marital`, `religion`, `gender`, `address`, `city`, `country`, `nationality`, `currency`, `salary`, `seaferer`, `status_activity`, `fb`, `ig`, `tg`, `tw`, `wa`, `portofolio`, `created_at`, `updated_at`) VALUES
(1, 37, 'Wawan', 'CRUISE', 'AZ MICHAEL`S CLUB PIANIST/VOC', '1990-02-28', 'Cbbr', 'Single', 'Islam', 'm', 'Jln stok land no 30', 'Ziak', 'Scotland', 'Scotland', 'USD', '3500.00', NULL, 'STANDBY', NULL, NULL, NULL, NULL, NULL, '37_1619668894.png', '2021-04-29 04:01:34', '2021-07-16 07:05:39'),
(2, 38, 'Marni', 'MARINE', 'GMTS FITTER', '1999-02-10', 'Ciza', 'Single', 'Islam', 'f', 'Jl vrank kx', 'Cvzz', 'Germany', 'Germany', 'IDR', '16000000.00', NULL, 'STANDBY', 'fb1', 'ig1', 'tg1', 'tw1', 'wa1', '38_1619669104.png', '2021-04-29 04:05:04', '2021-04-29 04:05:04'),
(3, 43, 'Marsha Ivarzya', 'CRUISE', 'EXECUTIVE SOUS CHEF', '1992-06-18', 'Bogor', 'Single', 'Islam', 'f', 'Jln Bangka Raya No. 55 Jakarta', 'Jakarta', 'Fiji Islands', 'Ghana', 'USD', '7500.00', NULL, 'STANDBY', NULL, NULL, NULL, NULL, NULL, '43_1619877391.jpg', '2021-05-01 13:56:31', '2021-05-01 14:07:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_slide`
--

CREATE TABLE `home_slide` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_slide`
--

INSERT INTO `home_slide` (`id`, `title`, `images`, `created_at`, `updated_at`) VALUES
(1, 'Gambar Satu', 'gambar1.jpg', '2021-03-28 04:42:15', '2021-03-28 04:42:15'),
(2, 'Gambar Duas', '1_1617000604.jpg', '2021-03-28 04:42:15', '2021-03-29 06:50:04'),
(3, 'Gambar Tiga', '1_1624355816.jpg', '2021-03-28 04:42:15', '2021-06-22 09:56:56'),
(4, 'Gambar Empat', 'gambar4.jpg', '2021-03-28 04:42:15', '2021-03-28 04:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `job_desc`
--

CREATE TABLE `job_desc` (
  `id` int(11) NOT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `job_description` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_desc`
--

INSERT INTO `job_desc` (`id`, `dept`, `job_description`, `created_at`, `updated_at`) VALUES
(1, 'CRUISE', 'AZ ADMIN PURSER - PAYROLL', NULL, NULL),
(2, 'CRUISE', 'AZ L&D CONSULTANT', NULL, NULL),
(3, 'CRUISE', 'AZ - ASST LAUNDRY MASTER', NULL, NULL),
(4, 'CRUISE', 'AZ - DRY CLEANING SPEC', NULL, NULL),
(5, 'CRUISE', 'AZ LAUNDRY ATTENDANT', NULL, NULL),
(6, 'CRUISE', 'AZ - LAUNDRY MASTER', NULL, NULL),
(7, 'CRUISE', 'AZ - WASH SPECIALIST', NULL, NULL),
(8, 'CRUISE', 'AZ CELEBRITY ONLINE TECHNICIAN', NULL, NULL),
(9, 'CRUISE', 'AZ IT ASSISTANT', NULL, NULL),
(10, 'CRUISE', 'AZ IT MANAGER', NULL, NULL),
(11, 'CRUISE', 'F and B ADMIN ASSISTANT', NULL, NULL),
(12, 'CRUISE', 'AZ ASSISTANT FOOD MANAGER', NULL, NULL),
(13, 'CRUISE', 'AZ CHEF DE PARTIE 1 - PANTRY', NULL, NULL),
(14, 'CRUISE', 'AZ CHEF DE PARTIE 1 - PASTRY', NULL, NULL),
(15, 'CRUISE', 'AZ CHEF DE PARTIE 1 SUSHI', NULL, NULL),
(16, 'CRUISE', 'AZ CHEF DE PARTIE 2 - BAKER', NULL, NULL),
(17, 'CRUISE', 'AZ CHEF DE PARTIE 2 - PANTRY', NULL, NULL),
(18, 'CRUISE', 'AZ CHEF DE PARTIE 2 - PASTRY', NULL, NULL),
(19, 'CRUISE', 'AZ CHEF DE PARTIE 2 - BUTCHER', NULL, NULL),
(20, 'CRUISE', 'AZ CHEF DE PARTIE 3 - BAKER', NULL, NULL),
(21, 'CRUISE', 'CHEF DE PARTIE 3 - HEAD BUFFET', NULL, NULL),
(22, 'CRUISE', 'AZ CHEF DE PARTIE 3 - PANTRY', NULL, NULL),
(23, 'CRUISE', 'AZ CHEF DE PARTIE 3 - PASTRY', NULL, NULL),
(24, 'CRUISE', 'AZ CHEF DE PARTIE 3 - BUTCHER', NULL, NULL),
(25, 'CRUISE', 'AZ CHEF DE PARTIE 1', NULL, NULL),
(26, 'CRUISE', 'AZ CHEF DE PARTIE 2', NULL, NULL),
(27, 'CRUISE', 'AZ CHEF DE PARTIE 3', NULL, NULL),
(28, 'CRUISE', 'AZ EXEC SOUS CHEF', NULL, NULL),
(29, 'CRUISE', 'AZ EXECUTIVE CHEF', NULL, NULL),
(30, 'CRUISE', 'F and B MANAGER', NULL, NULL),
(31, 'CRUISE', 'AZ F&B SANITATION CONTROLLER', NULL, NULL),
(32, 'CRUISE', 'AZ COMMIS 1 - BAKER', NULL, NULL),
(33, 'CRUISE', 'AZ COMMIS 1 - PANTRY', NULL, NULL),
(34, 'CRUISE', 'AZ COMMIS 1 - PASTRY', NULL, NULL),
(35, 'CRUISE', 'AZ COMMIS 1 - SUSHI', NULL, NULL),
(36, 'CRUISE', 'AZ COMMIS 1 - BUTCHER', NULL, NULL),
(37, 'CRUISE', 'AZ COMMIS 2- BAKER', NULL, NULL),
(38, 'CRUISE', 'AZ COMMIS 2 - PANTRY', NULL, NULL),
(39, 'CRUISE', 'AZ COMMIS 2 - PASTRY', NULL, NULL),
(40, 'CRUISE', 'AZ COMMIS 2 - BUTCHER', NULL, NULL),
(41, 'CRUISE', 'AZ COMMIS 1', NULL, NULL),
(42, 'CRUISE', 'AZ COMMIS 2', NULL, NULL),
(43, 'CRUISE', 'AZ PASTRY SOUS CHEF', NULL, NULL),
(44, 'CRUISE', 'AZ SOUS CHEF', NULL, NULL),
(45, 'CRUISE', 'AZ TVL CORP EXECUTIVE CHEF', NULL, NULL),
(46, 'CRUISE', 'AZ GALLEY STEWARD', NULL, NULL),
(47, 'CRUISE', 'AZ GALLEY STEWARD HEAD', NULL, NULL),
(48, 'CRUISE', 'ASSISTANT MANAGER DINING ROOM', NULL, NULL),
(49, 'CRUISE', 'ASST MGR. SPECIALTY DINING', NULL, NULL),
(50, 'CRUISE', 'AZ ASSISTANT WAITER', NULL, NULL),
(51, 'CRUISE', 'HEAD WAITER', NULL, NULL),
(52, 'CRUISE', 'MANAGER SPECIALTY DINING', NULL, NULL),
(53, 'CRUISE', 'AZ RESTAURANT HOST/ESS', NULL, NULL),
(54, 'CRUISE', 'AZ RESTAURANT MANAGER', NULL, NULL),
(55, 'CRUISE', 'AZ MESS ATTENDANT', NULL, NULL),
(56, 'CRUISE', 'AZ UTILITY CLEANER SPECIALTY', NULL, NULL),
(57, 'CRUISE', 'AZ WAITER', NULL, NULL),
(58, 'CRUISE', 'AZ BAR MANAGER', NULL, NULL),
(59, 'CRUISE', 'AZ ASSISTANT BAR MANAGER', NULL, NULL),
(60, 'CRUISE', 'AZ BARTENDER', NULL, NULL),
(61, 'CRUISE', 'AZ HEAD BARTENDER', NULL, NULL),
(62, 'CRUISE', 'AZ CELLAR MASTER', NULL, NULL),
(63, 'CRUISE', 'AZ BAR SERVER', NULL, NULL),
(64, 'CRUISE', 'AZ SOMMELIER', NULL, NULL),
(65, 'CRUISE', 'AZ UTILITY BAR', NULL, NULL),
(66, 'CRUISE', 'AZ ASSISTANT CASINO MANAGER', NULL, NULL),
(67, 'CRUISE', 'AZ CASINO ATTENDANT', NULL, NULL),
(68, 'CRUISE', 'AZ CASINO TRAINEE', NULL, NULL),
(69, 'CRUISE', 'AZ CASH DESK MANAGER', NULL, NULL),
(70, 'CRUISE', 'AZ CASINO SENIOR SUPERVISOR', NULL, NULL),
(71, 'CRUISE', 'AZ CASINO SUPERVISOR', NULL, NULL),
(72, 'CRUISE', 'AZ CASINO DEALER 1', NULL, NULL),
(73, 'CRUISE', 'AZ CASINO DEALER 2', NULL, NULL),
(74, 'CRUISE', 'AZ CASINO DEALER 3', NULL, NULL),
(75, 'CRUISE', 'AZ CASINO DEALER', NULL, NULL),
(76, 'CRUISE', 'AZ CASINO MANAGER', NULL, NULL),
(77, 'CRUISE', 'AZ CASHIER', NULL, NULL),
(78, 'CRUISE', 'AZ SLOT MANAGER', NULL, NULL),
(79, 'CRUISE', 'AZ SLOT TECHNICIAN', NULL, NULL),
(80, 'CRUISE', 'ASST SHORE EXCURSION MGR ', NULL, NULL),
(81, 'CRUISE', 'AZ SHORE EXCURSION MANAGER', NULL, NULL),
(82, 'CRUISE', 'AZ SHORE EXCURSION STAFF', NULL, NULL),
(83, 'CRUISE', 'AZ BAR STOREKEEPER UTILITY', NULL, NULL),
(84, 'CRUISE', 'AZ HOTEL STOREKEEPER', NULL, NULL),
(85, 'CRUISE', 'AZ INVENTORY MANAGER', NULL, NULL),
(86, 'CRUISE', 'AZ F and B PROVISION MASTER', NULL, NULL),
(87, 'CRUISE', 'AZ FOOD STOREKEEPER', NULL, NULL),
(88, 'CRUISE', 'AZ HOTEL SUPPLY SUPERVISOR', NULL, NULL),
(89, 'CRUISE', 'AZ ASST FOOD STOREKEEPER', NULL, NULL),
(90, 'CRUISE', 'AZ ASSISTANT HOTEL STOREKEEPER', NULL, NULL),
(91, 'CRUISE', 'AZ SPORTS COORDINATOR', NULL, NULL),
(92, 'CRUISE', 'AZ DISC JOCKEY', NULL, NULL),
(93, 'CRUISE', 'AZ CRUISE DIRECTOR', NULL, NULL),
(94, 'CRUISE', 'AZ ASST CRUISE DIRECTOR', NULL, NULL),
(95, 'CRUISE', 'AZ LEAD PERFORMER', NULL, NULL),
(96, 'CRUISE', 'AZ PERFORMER', NULL, NULL),
(97, 'CRUISE', 'AZ MICHAEL`S CLUB PIANIST/VOC', NULL, NULL),
(98, 'CRUISE', 'AZ GUITARIST/VOCALIST', NULL, NULL),
(99, 'CRUISE', 'AZ STRING QUARTET MUSICIAN', NULL, NULL),
(100, 'CRUISE', 'AZ STRING TRIO MUSICIAN', NULL, NULL),
(101, 'CRUISE', 'AZAMARA CLEANER', NULL, NULL),
(102, 'CRUISE', 'AZ BUTLER', NULL, NULL),
(103, 'CRUISE', 'AZ ASST STATEROOM ATTENDANT', NULL, NULL),
(104, 'CRUISE', 'AZ CHIEF HOUSEKEEPER', NULL, NULL),
(105, 'CRUISE', 'AZ HOUSEKEEPING PANTRY ATTEND', NULL, NULL),
(106, 'CRUISE', 'AZ ASSISTANT CHIEF HOUSEKEEPER', NULL, NULL),
(107, 'CRUISE', 'AZ PUBLIC AREA SUP ATT (NIGHT)', NULL, NULL),
(108, 'CRUISE', 'AZ STATEROOM ATTENDANT', NULL, NULL),
(109, 'CRUISE', 'AZ TAILOR', NULL, NULL),
(110, 'CRUISE', 'AZ FINANCIAL CONTROLLER', NULL, NULL),
(111, 'CRUISE', 'AZ JUNIOR PAYROLL PURSER', NULL, NULL),
(112, 'CRUISE', 'AZ GUEST RELATIONS MANAGER', NULL, NULL),
(113, 'CRUISE', 'AZ ADMIN PURSER - GUEST', NULL, NULL),
(114, 'CRUISE', 'CRUISE SLS/CAPTAINS CLUB/EVNT', NULL, NULL),
(115, 'CRUISE', 'PRINT and LAYOUT SPECIALIST', NULL, NULL),
(116, 'CRUISE', 'AZ ASST. GUEST RELATIONS MGR', NULL, NULL),
(117, 'CRUISE', 'AZ ASSOCIATE HOTEL DIRECTOR', NULL, NULL),
(118, 'CRUISE', 'AZ ADMIN PURSER  DOCUMENTATION', NULL, NULL),
(119, 'CRUISE', 'AZ BELL ATTENDANT', NULL, NULL),
(120, 'CRUISE', 'AZ CONCIERGE', NULL, NULL),
(121, 'CRUISE', 'AZ GUEST RELATIONS OFFICER', NULL, NULL),
(122, 'CRUISE', 'AZ HOTEL ADMIN ASSISTANT', NULL, NULL),
(123, 'CRUISE', 'AZ HOTEL DIRECTOR', NULL, NULL),
(124, 'CRUISE', 'AZ JR. GUEST RELATIONS OFFICER', NULL, NULL),
(125, 'CRUISE', 'AZ CRUISE SALES MANAGER', NULL, NULL),
(126, 'CRUISE', 'AZ CRUISE SALES ASSOCIATE', NULL, NULL),
(127, 'CRUISE', 'AZ AV MANAGER', NULL, NULL),
(128, 'CRUISE', 'AZ AV OPERATOR - SOUND', NULL, NULL),
(129, 'CRUISE', 'AZ BROADCAST MANAGER', NULL, NULL),
(130, 'CRUISE', 'AZ BROADCAST OPERATOR', NULL, NULL),
(131, 'CRUISE', 'HORTICULTURIST', NULL, NULL),
(132, 'CRUISE', 'POOL MONITOR', NULL, NULL),
(133, 'CRUISE', 'FLEET F&B DIRECTOR', NULL, NULL),
(134, 'CRUISE', 'FLEET REVENUE MANAGER', NULL, NULL),
(135, 'CRUISE', 'TRAVELING CRUISE SALES MANAGER', NULL, NULL),
(136, 'CRUISE', 'TRAVELING EXECUTIVE CHEF', NULL, NULL),
(137, 'CRUISE', 'TRAVELING F&B COST ANALYST', NULL, NULL),
(138, 'CRUISE', 'TRAVELING HOTEL DIRECTOR', NULL, NULL),
(139, 'CRUISE', 'TRAVELING LAUNDRY MANAGER     ', NULL, NULL),
(140, 'CRUISE', 'TRAVELING CHIEF HOUSEKEEPER', NULL, NULL),
(141, 'CRUISE', 'ASST. LAUNDRY MASTER - CEL', NULL, NULL),
(142, 'CRUISE', 'LAUNDRY MASTER - CELEBRITY', NULL, NULL),
(143, 'CRUISE', 'LAUNDRY ATTENDANT - CEL', NULL, NULL),
(144, 'CRUISE', 'DRY CLEANING SPEC - CEL', NULL, NULL),
(145, 'CRUISE', 'WASH SPECIALIST - CEL', NULL, NULL),
(146, 'CRUISE', 'IT ASSISTANT MANAGER', NULL, NULL),
(147, 'CRUISE', 'IT MANAGER', NULL, NULL),
(148, 'CRUISE', 'IT OFFICER', NULL, NULL),
(149, 'CRUISE', 'IT INFRASTRUCTURE SPECIALIST', NULL, NULL),
(150, 'CRUISE', 'F&B ADMINISTRATIVE ASSISTANT', NULL, NULL),
(151, 'CRUISE', 'ASST GALLEY OPERATIONS MGR', NULL, NULL),
(152, 'CRUISE', 'ASSISTANT PASTRY CHEF', NULL, NULL),
(153, 'CRUISE', 'CULINARY ADMINISTRATIVE ASST', NULL, NULL),
(154, 'CRUISE', 'CDP BAKER', NULL, NULL),
(155, 'CRUISE', 'EXEC SOUS CHEF PASTRY & BAKERY', NULL, NULL),
(156, 'CRUISE', 'EXECUTIVE CHEF', NULL, NULL),
(157, 'CRUISE', 'EXEC SOUS CHEF', NULL, NULL),
(158, 'CRUISE', 'F&B DIRECTOR', NULL, NULL),
(159, 'CRUISE', 'GALLEY OPERATIONS MANAGER', NULL, NULL),
(160, 'CRUISE', 'CHEF DE CUISINE', NULL, NULL),
(161, 'CRUISE', 'CDP BUTCHER', NULL, NULL),
(162, 'CRUISE', '1ST COOK BAKERY', NULL, NULL),
(163, 'CRUISE', '1ST COOK', NULL, NULL),
(164, 'CRUISE', '1ST COOK SUSHI', NULL, NULL),
(165, 'CRUISE', '1ST COOK PANTRY', NULL, NULL),
(166, 'CRUISE', '1ST COOK PASTRY', NULL, NULL),
(167, 'CRUISE', '1ST COOK BUTCHER', NULL, NULL),
(168, 'CRUISE', '2ND COOK BAKER', NULL, NULL),
(169, 'CRUISE', '2ND COOK', NULL, NULL),
(170, 'CRUISE', '2ND COOK SUSHI', NULL, NULL),
(171, 'CRUISE', '2ND COOK PANTRY', NULL, NULL),
(172, 'CRUISE', '2ND COOK PASTRY', NULL, NULL),
(173, 'CRUISE', '2ND COOK BUTCHER', NULL, NULL),
(174, 'CRUISE', '3RD COOK BAKERY', NULL, NULL),
(175, 'CRUISE', '3RD COOK', NULL, NULL),
(176, 'CRUISE', '3RD COOK SUSHI', NULL, NULL),
(177, 'CRUISE', '3RD COOK PANTRY', NULL, NULL),
(178, 'CRUISE', '3RD COOK PASTRY', NULL, NULL),
(179, 'CRUISE', '4TH COOK BAKERY', NULL, NULL),
(180, 'CRUISE', '4TH COOK', NULL, NULL),
(181, 'CRUISE', '4TH COOK PANTRY', NULL, NULL),
(182, 'CRUISE', '4TH COOK PASTRY', NULL, NULL),
(183, 'CRUISE', 'CHEF DE PARTIE (CDP)', NULL, NULL),
(184, 'CRUISE', 'CDP CREW CHEF', NULL, NULL),
(185, 'CRUISE', 'JR SOUS CHEF', NULL, NULL),
(186, 'CRUISE', 'PANTRY DECORATOR', NULL, NULL),
(187, 'CRUISE', 'PANTRY CHEF', NULL, NULL),
(188, 'CRUISE', 'SOUS CHEF', NULL, NULL),
(189, 'CRUISE', 'UTILITY CLEANER', NULL, NULL),
(190, 'CRUISE', 'ASSISTANT MAITRE D`', NULL, NULL),
(191, 'CRUISE', 'HEADWAITER CASUAL DINING', NULL, NULL),
(192, 'CRUISE', 'ASST WAITER LUMINAE', NULL, NULL),
(193, 'CRUISE', 'ASST WAITER', NULL, NULL),
(194, 'CRUISE', 'COMIS DE RANG', NULL, NULL),
(195, 'CRUISE', 'CHEF DE RANG', NULL, NULL),
(196, 'CRUISE', 'RESTAURANT OPERATIONS MANAGER', NULL, NULL),
(197, 'CRUISE', 'HEAD CHEF DE RANG', NULL, NULL),
(198, 'CRUISE', 'MAITRE D` SPECIALTY RESTAURANT', NULL, NULL),
(199, 'CRUISE', 'ROOM SERVICE ATTENDANT', NULL, NULL),
(200, 'CRUISE', 'ROOM SERVICE MANAGER', NULL, NULL),
(201, 'CRUISE', 'ROOM SERVICE OPERATOR', NULL, NULL),
(202, 'CRUISE', 'SNACK ATTENDANT CASUAL DINING', NULL, NULL),
(203, 'CRUISE', 'WAITER LUMINAE', NULL, NULL),
(204, 'CRUISE', 'WAITER', NULL, NULL),
(205, 'CRUISE', 'RESTAURANT HOST/ESS', NULL, NULL),
(206, 'CRUISE', 'SR. ASST. BEVERAGE MANAGER', NULL, NULL),
(207, 'CRUISE', 'SENIOR BEVERAGE MANAGER', NULL, NULL),
(208, 'CRUISE', 'BEVERAGE OPERATIONS MANAGER', NULL, NULL),
(209, 'CRUISE', 'ASSISTANT BEVERAGE MANAGER', NULL, NULL),
(210, 'CRUISE', 'BARISTA', NULL, NULL),
(211, 'CRUISE', 'FLAIR BARTENDER', NULL, NULL),
(212, 'CRUISE', 'MOLECULAR BARTENDER', NULL, NULL),
(213, 'CRUISE', 'BARTENDER', NULL, NULL),
(214, 'CRUISE', 'BAR SERVER', NULL, NULL),
(215, 'CRUISE', 'CELLAR MASTER', NULL, NULL),
(216, 'CRUISE', 'PRIVATE BEVERAGE ATTENDANT', NULL, NULL),
(217, 'CRUISE', 'SENIOR BARTENDER', NULL, NULL),
(218, 'CRUISE', 'SENIOR BARISTA', NULL, NULL),
(219, 'CRUISE', 'SENIOR FLAIR BARTENDER', NULL, NULL),
(220, 'CRUISE', 'SENIOR MOLECULAR BARTENDER', NULL, NULL),
(221, 'CRUISE', 'UTILITY BAR', NULL, NULL),
(222, 'CRUISE', 'SOMMELIER', NULL, NULL),
(223, 'CRUISE', 'ASSISTANT CASINO MANAGER', NULL, NULL),
(224, 'CRUISE', 'CASINO SUPERVISOR', NULL, NULL),
(225, 'CRUISE', 'CELEBRITY CASINO ATTENDANT', NULL, NULL),
(226, 'CRUISE', 'CELEBRITY CASINO HOST', NULL, NULL),
(227, 'CRUISE', 'CASH DESK MANAGER', NULL, NULL),
(228, 'CRUISE', 'CASINO MANAGER', NULL, NULL),
(229, 'CRUISE', 'CASHIER', NULL, NULL),
(230, 'CRUISE', 'CASINO DEALER', NULL, NULL),
(231, 'CRUISE', 'DEALER 1', NULL, NULL),
(232, 'CRUISE', 'DEALER 2', NULL, NULL),
(233, 'CRUISE', 'DEALER 3', NULL, NULL),
(234, 'CRUISE', 'SLOT TECHNICIAN', NULL, NULL),
(235, 'CRUISE', 'SLOT MANAGER', NULL, NULL),
(236, 'CRUISE', 'TRAVELING CASINO MANAGER', NULL, NULL),
(237, 'CRUISE', 'PHOTO MANAGER ', NULL, NULL),
(238, 'CRUISE', 'ASST. PHOTO MANAGER', NULL, NULL),
(239, 'CRUISE', 'LAB TECHNICIAN', NULL, NULL),
(240, 'CRUISE', 'STUDIO ARTIST', NULL, NULL),
(241, 'CRUISE', 'SENIOR PHOTOGRAPHER', NULL, NULL),
(242, 'CRUISE', 'PHOTOGRAPHER ', NULL, NULL),
(243, 'CRUISE', 'SHORE EXCURSION STAFF', NULL, NULL),
(244, 'CRUISE', 'ASST. SHOREX MANAGER', NULL, NULL),
(245, 'CRUISE', 'DESTINATION CONCIERGE', NULL, NULL),
(246, 'CRUISE', 'SHORE EXCURSION MANAGER', NULL, NULL),
(247, 'CRUISE', 'HOTEL STOREKEEPER', NULL, NULL),
(248, 'CRUISE', 'FOOD STOREKEEPER', NULL, NULL),
(249, 'CRUISE', 'INVENTORY CONTROL ASST', NULL, NULL),
(250, 'CRUISE', 'INVENTORY MANAGER', NULL, NULL),
(251, 'CRUISE', 'F&B PROVISION MASTER', NULL, NULL),
(252, 'CRUISE', 'BAR STOREKEEPER UTILITY', NULL, NULL),
(253, 'CRUISE', 'ASST FOOD STOREKEEPER', NULL, NULL),
(254, 'CRUISE', 'ACTIVITIES HOST', NULL, NULL),
(255, 'CRUISE', 'ACTIVITY MANAGER', NULL, NULL),
(256, 'CRUISE', 'CRUISE DIRECTOR CEL', NULL, NULL),
(257, 'CRUISE', 'DISC JOCKEY', NULL, NULL),
(258, 'CRUISE', 'YOUTH COUNSELOR', NULL, NULL),
(259, 'CRUISE', 'YOUTH COUNSELOR SEASONAL', NULL, NULL),
(260, 'CRUISE', 'YOUTH PROGRAM MANAGER', NULL, NULL),
(261, 'CRUISE', 'BROADCAST OPERATOR', NULL, NULL),
(262, 'CRUISE', 'BROADCAST MANAGER', NULL, NULL),
(263, 'CRUISE', 'DIGITAL SIGN. & ITV SPECIALIST', NULL, NULL),
(264, 'CRUISE', 'HEAD ENTERTAINMENT OPERATOR', NULL, NULL),
(265, 'CRUISE', 'ENTERTAINMENT OPERATOR - FLY', NULL, NULL),
(266, 'CRUISE', 'ENTERTAINMENT OPERATOR - LIGHT', NULL, NULL),
(267, 'CRUISE', 'ENTERTAINMENT OPERATOR', NULL, NULL),
(268, 'CRUISE', 'ENTERTAINMENT OPERATOR - SOUND', NULL, NULL),
(269, 'CRUISE', 'ENTERTAINMENT STAFF - FLY', NULL, NULL),
(270, 'CRUISE', 'ENTERTAINMENT STAFF - LIGHTING', NULL, NULL),
(271, 'CRUISE', 'ENTERTERTAINMENT. STAFF - PROD', NULL, NULL),
(272, 'CRUISE', 'ENTERTAINMENT STAGE STAFF', NULL, NULL),
(273, 'CRUISE', 'ENTERTAINMENT STAFF - SOUND', NULL, NULL),
(274, 'CRUISE', 'ENT TECHNICAL DIRECTOR', NULL, NULL),
(275, 'CRUISE', 'STAGE & PRODUCTION MANAGER', NULL, NULL),
(276, 'CRUISE', 'STAGE & PRODUCTION MANAGER B', NULL, NULL),
(277, 'CRUISE', 'SR. STAGE & PROD. MGR', NULL, NULL),
(278, 'CRUISE', 'BAND MASTER/MUSICAL DIRECTOR', NULL, NULL),
(279, 'CRUISE', 'ORCHESTRA MUSICIAN', NULL, NULL),
(280, 'CRUISE', 'PREMIER MUSICIAN', NULL, NULL),
(281, 'CRUISE', 'SOLO MUSICIAN', NULL, NULL),
(282, 'CRUISE', 'VENUE MUSICIAN', NULL, NULL),
(283, 'CRUISE', 'AERIAL FLYERS/ADAGIO', NULL, NULL),
(284, 'CRUISE', 'AERIAL QUAD PERFORMER', NULL, NULL),
(285, 'CRUISE', 'SPECIALTY PRODUCTION ACT', NULL, NULL),
(286, 'CRUISE', 'SPECIALTY PRODUCTION DANCER', NULL, NULL),
(287, 'CRUISE', 'SPECIALTY VOCALIST', NULL, NULL),
(288, 'CRUISE', 'PRODUCTION CAST DANCE CAPTAIN', NULL, NULL),
(289, 'CRUISE', 'PREMIERE SPECIALTY ACT', NULL, NULL),
(290, 'CRUISE', 'PHYSICAL COMEDIAN', NULL, NULL),
(291, 'CRUISE', 'PRODUCTION CAST SINGER/DANCER', NULL, NULL),
(292, 'CRUISE', 'PRODUCTION CAST SINGER', NULL, NULL),
(293, 'CRUISE', 'SENIOR BROADCAST MANAGER', NULL, NULL),
(294, 'CRUISE', 'TRVL SR. BROADCAST MGR', NULL, NULL),
(295, 'CRUISE', 'CELEBRITY CLEANER', NULL, NULL),
(296, 'CRUISE', 'CHIEF HOUSEKEEPER', NULL, NULL),
(297, 'CRUISE', 'SR. ASST. CHIEF HOUSEKEEPER', NULL, NULL),
(298, 'CRUISE', 'LINEN UNIFORM KEEPER', NULL, NULL),
(299, 'CRUISE', 'DECK ATTENDANT', NULL, NULL),
(300, 'CRUISE', 'ECCR STAFF ATTENDANT', NULL, NULL),
(301, 'CRUISE', 'HOUSEKEEPING COORDINATOR', NULL, NULL),
(302, 'CRUISE', 'ASSISTANT CHIEF HOUSEKEEPER', NULL, NULL),
(303, 'CRUISE', 'HOUSEKEEPING SUPERVISOR - CEL', NULL, NULL),
(304, 'CRUISE', 'HOUSEKEEPING SUP DECK', NULL, NULL),
(305, 'CRUISE', 'NIGHT ATTENDANT', NULL, NULL),
(306, 'CRUISE', 'STATEROOM ATTENDANT', NULL, NULL),
(307, 'CRUISE', 'STAFF ATTENDANT', NULL, NULL),
(308, 'CRUISE', 'TAILOR UNIFORM KEEPER', NULL, NULL),
(309, 'CRUISE', 'ADMINISTRATION PURSER - PAYROL', NULL, NULL),
(310, 'CRUISE', 'APPRENTICE PURSER', NULL, NULL),
(311, 'CRUISE', 'FINANCIAL CONTROLLER', NULL, NULL),
(312, 'CRUISE', 'JUNIOR PAYROLL PURSER', NULL, NULL),
(313, 'CRUISE', 'ADMIN PURSER-GUEST ACCOUNTS', NULL, NULL),
(314, 'CRUISE', 'ASSIST. CRUISE SALES MANAGER', NULL, NULL),
(315, 'CRUISE', 'ASST GUEST RELATIONS MANAGER', NULL, NULL),
(316, 'CRUISE', 'ASSOCIATE HOTEL DIRECTOR', NULL, NULL),
(317, 'CRUISE', 'ADMINISTRATION PURSER - DOCUME', NULL, NULL),
(318, 'CRUISE', 'BUTLER', NULL, NULL),
(319, 'CRUISE', 'CAPTAIN`S CLUB HOST/ESS', NULL, NULL),
(320, 'CRUISE', 'CRUISE SALES ASSOCIATE', NULL, NULL),
(321, 'CRUISE', 'MANAGER CRUISE SALES', NULL, NULL),
(322, 'CRUISE', 'DIRECTOR REVENUE & MARKETING', NULL, NULL),
(323, 'CRUISE', 'CONCIERGE', NULL, NULL),
(324, 'CRUISE', 'DESKTOP PUBLISHER', NULL, NULL),
(325, 'CRUISE', 'INTERNATIONAL HOST/ESS', NULL, NULL),
(326, 'CRUISE', 'GUEST RELATIONS MANAGER', NULL, NULL),
(327, 'CRUISE', 'EVENT COORDINATOR', NULL, NULL),
(328, 'CRUISE', 'GUEST RELATIONS OFFICER', NULL, NULL),
(329, 'CRUISE', 'HOTEL ADMINISTRATIVE ASSISTANT', NULL, NULL),
(330, 'CRUISE', 'HEAD BUTLER', NULL, NULL),
(331, 'CRUISE', 'HOTEL DIRECTOR', NULL, NULL),
(332, 'CRUISE', 'ILOUNGE ASSISTANT', NULL, NULL),
(333, 'CRUISE', 'MACOLOGIST', NULL, NULL),
(334, 'CRUISE', 'ILOUNGE MANAGER', NULL, NULL),
(335, 'CRUISE', 'JR. GUEST RELATIONS OFFICER', NULL, NULL),
(336, 'CRUISE', 'MICHAELS CLUB CONCIERGE', NULL, NULL),
(337, 'CRUISE', 'ONBOARD MARKETING MANAGER', NULL, NULL),
(338, 'CRUISE', 'PRINTER', NULL, NULL),
(339, 'CRUISE', 'SR DESKTOP PUBLISHER', NULL, NULL),
(340, 'CRUISE', 'TRAVELING GUEST RELATIONS MGR', NULL, NULL),
(341, 'CRUISE', 'FRONT DESK MANAGER', NULL, NULL),
(342, 'CRUISE', 'SUITE MANAGER', NULL, NULL),
(343, 'CRUISE', 'ONBOARD DIGITAL MANAGER', NULL, NULL),
(344, 'CRUISE', 'L&D CONSULTANT', NULL, NULL),
(345, 'CRUISE', 'PL ASST HOTEL CONTROLLER', NULL, NULL),
(346, 'CRUISE', 'PL F&B MANAGER', NULL, NULL),
(347, 'CRUISE', 'PL HOTEL CONTROLLER', NULL, NULL),
(348, 'CRUISE', 'PL HOTEL DIRECTOR', NULL, NULL),
(349, 'CRUISE', 'PL IT ASSISTANT', NULL, NULL),
(350, 'CRUISE', 'PL IT CAFE MANAGER', NULL, NULL),
(351, 'CRUISE', 'PL IT FLEET MANAGER', NULL, NULL),
(352, 'CRUISE', 'PL IT MANAGER', NULL, NULL),
(353, 'CRUISE', 'PL IT SENIOR MANAGER', NULL, NULL),
(354, 'CRUISE', 'PL MARKETING & REVENUE MANAGER', NULL, NULL),
(355, 'CRUISE', 'PL PRINTER', NULL, NULL),
(356, 'CRUISE', 'PL EXECUTIVE CHEF', NULL, NULL),
(357, 'CRUISE', 'PL PASTRY CHEF', NULL, NULL),
(358, 'CRUISE', 'PL SOUS CHEF', NULL, NULL),
(359, 'CRUISE', 'PL 1ST COOK', NULL, NULL),
(360, 'CRUISE', 'PL 1ST PASTRY', NULL, NULL),
(361, 'CRUISE', 'PL 2ND COOK', NULL, NULL),
(362, 'CRUISE', 'PL 2ND COOK BAKER', NULL, NULL),
(363, 'CRUISE', 'PL 2ND COOK  BUTCHER', NULL, NULL),
(364, 'CRUISE', 'PL 2ND COOK PASTRY', NULL, NULL),
(365, 'CRUISE', 'PL 3RD COOK', NULL, NULL),
(366, 'CRUISE', 'PL 3RD COOK BAKER', NULL, NULL),
(367, 'CRUISE', 'PL 3RD COOK BUTCHER', NULL, NULL),
(368, 'CRUISE', 'PL 3RD COOK PASTRY', NULL, NULL),
(369, 'CRUISE', 'PL ASST KITCHEN STEWARD', NULL, NULL),
(370, 'CRUISE', 'PL CHEF TOURNANT', NULL, NULL),
(371, 'CRUISE', 'PL COOK UTILITY/COOK RUNNER', NULL, NULL),
(372, 'CRUISE', 'PL COFFEEMAN', NULL, NULL),
(373, 'CRUISE', 'PL GALLEY CLEANER', NULL, NULL),
(374, 'CRUISE', 'PL HEAD BAKER', NULL, NULL),
(375, 'CRUISE', 'PL HEAD BUTCHER', NULL, NULL),
(376, 'CRUISE', 'PL KITCHEN STEWARD', NULL, NULL),
(377, 'CRUISE', 'PL TRVL CORP PASTRY CHEF', NULL, NULL),
(378, 'CRUISE', 'PL TRVL CORP EXEC. CHEF', NULL, NULL),
(379, 'CRUISE', 'TVRL HTL COST CTRL & EFFIC MGR', NULL, NULL),
(380, 'CRUISE', 'PL ASST RESTAURANT MANAGER', NULL, NULL),
(381, 'CRUISE', 'PL ASST WAITER', NULL, NULL),
(382, 'CRUISE', 'PL CREW MESSMAN', NULL, NULL),
(383, 'CRUISE', 'PL HEAD WAITER', NULL, NULL),
(384, 'CRUISE', 'PL LIDO MANAGER', NULL, NULL),
(385, 'CRUISE', 'PL OFFICER/STAFF MESSMAN', NULL, NULL),
(386, 'CRUISE', 'PL RESTAURANT MANAGER', NULL, NULL),
(387, 'CRUISE', 'PL WAITER', NULL, NULL),
(388, 'CRUISE', 'PL ASST BAR MANAGER', NULL, NULL),
(389, 'CRUISE', 'PL BAR MANAGER', NULL, NULL),
(390, 'CRUISE', 'PL BARTENDER', NULL, NULL),
(391, 'CRUISE', 'PL BAR UTILITY', NULL, NULL),
(392, 'CRUISE', 'PL BAR WAITER', NULL, NULL),
(393, 'CRUISE', 'PL CREW BARTENDER', NULL, NULL),
(394, 'CRUISE', 'PL HEAD BARTENDER', NULL, NULL),
(395, 'CRUISE', 'PL WINE STOREKEEPER', NULL, NULL),
(396, 'CRUISE', 'PL CASINO CASHIER', NULL, NULL),
(397, 'CRUISE', 'PL CASINO CROUPIER', NULL, NULL),
(398, 'CRUISE', 'PL CASINO HOSTESS', NULL, NULL),
(399, 'CRUISE', 'PL CASINO MANAGER', NULL, NULL),
(400, 'CRUISE', 'PL CASINO SLOT TECHNICIAN', NULL, NULL),
(401, 'CRUISE', 'PL CASINO SUPERVISOR', NULL, NULL),
(402, 'CRUISE', 'PL CRUISE SALES MANAGER', NULL, NULL),
(403, 'CRUISE', 'PL SHORE EXCURSION MANAGER', NULL, NULL),
(404, 'CRUISE', 'PL SHORE EXCURSION STAFF', NULL, NULL),
(405, 'CRUISE', 'PL ASST CRUISE DIRECTOR', NULL, NULL),
(406, 'CRUISE', 'PL ASST STAGE MANAGER', NULL, NULL),
(407, 'CRUISE', 'PL CRUISE DIRECTOR', NULL, NULL),
(408, 'CRUISE', 'PL CRUISE STAFF', NULL, NULL),
(409, 'CRUISE', 'PL DISC JOCKEY', NULL, NULL),
(410, 'CRUISE', 'PL EXTRA SINGER', NULL, NULL),
(411, 'CRUISE', 'PL HEAD CRUISE STAFF', NULL, NULL),
(412, 'CRUISE', 'PL HEAD ARTIST', NULL, NULL),
(413, 'CRUISE', 'PL ARTIST', NULL, NULL),
(414, 'CRUISE', 'PL MC/HUMORISTA/COMEDIAN', NULL, NULL),
(415, 'CRUISE', 'PL MUSICIAN', NULL, NULL),
(416, 'CRUISE', 'PL SENIOR ARTIST', NULL, NULL),
(417, 'CRUISE', 'PL STAGE MANAGER', NULL, NULL),
(418, 'CRUISE', 'PL YOUTH STAFF', NULL, NULL),
(419, 'CRUISE', 'PL YOUTH STAFF SENIOR', NULL, NULL),
(420, 'CRUISE', 'PL TRAVELING ARTISTIC SUPERVIS', NULL, NULL),
(421, 'CRUISE', 'PL TRAVELING MUSICAL DIRECTOR', NULL, NULL),
(422, 'CRUISE', 'PL TRVL A/V & BROADCAST COORD', NULL, NULL),
(423, 'CRUISE', 'PL ASST HOUSEKEEPER', NULL, NULL),
(424, 'CRUISE', 'PL ASST LAUNDRY MASTER', NULL, NULL),
(425, 'CRUISE', 'PL BELLMAN', NULL, NULL),
(426, 'CRUISE', 'PL CHIEF CREW STEWARD', NULL, NULL),
(427, 'CRUISE', 'PL CHIEF HOUSEKEEPER', NULL, NULL),
(428, 'CRUISE', 'PL CABIN STEWARD', NULL, NULL),
(429, 'CRUISE', 'PL GENERAL PURPOSE UTILITY', NULL, NULL),
(430, 'CRUISE', 'PL HEAD GNRL PURPOSE UTILITY', NULL, NULL),
(431, 'CRUISE', 'PL LAUNDRYMAN', NULL, NULL),
(432, 'CRUISE', 'PL LAUNDRY MASTER', NULL, NULL),
(433, 'CRUISE', 'PL LINENKEEPER/TAILOR', NULL, NULL),
(434, 'CRUISE', 'PL OFFICERS/STAFF STEWARD', NULL, NULL),
(435, 'CRUISE', 'PL SENIOR ASST HOUSEKEEPER', NULL, NULL),
(436, 'CRUISE', 'PL ASST ACCOUNTANT', NULL, NULL),
(437, 'CRUISE', 'PL ASST ACCOUNTANT PAYROLL', NULL, NULL),
(438, 'CRUISE', 'PL CHIEF ACCOUNTANT', NULL, NULL),
(439, 'CRUISE', 'PL ASST PURSER', NULL, NULL),
(440, 'CRUISE', 'PL CHIEF PURSER', NULL, NULL),
(441, 'CRUISE', 'PL FRONT DESK MANAGER', NULL, NULL),
(442, 'CRUISE', 'PL GROUP COORDINATOR', NULL, NULL),
(443, 'CRUISE', 'PL HOSTESS GRAND CLASS', NULL, NULL),
(444, 'CRUISE', 'PL HOTEL ADMINISTRATIVE ASSIST', NULL, NULL),
(445, 'CRUISE', 'PL INTERNATIONAL HOSTESS', NULL, NULL),
(446, 'CRUISE', 'PL TRV GUEST RELATIONS MANAGER', NULL, NULL),
(447, 'CRUISE', 'PL TRV GAMING & RETAIL OPS MGR', NULL, NULL),
(448, 'CRUISE', 'PL TVL PUBLISHING COORD', NULL, NULL),
(449, 'CRUISE', 'PL HR CREW ADMINISTRATOR', NULL, NULL),
(450, 'CRUISE', 'PL HUMAN RESOURCES ASSISTANT', NULL, NULL),
(451, 'CRUISE', 'PL HUMAN RESOURCES MANAGER', NULL, NULL),
(452, 'CRUISE', 'PL ASSISTANT HOTEL STOREKEEPER', NULL, NULL),
(453, 'CRUISE', 'PL F&B STOREKEEPER', NULL, NULL),
(454, 'CRUISE', 'PL HOTEL INVENTORY MANAGER', NULL, NULL),
(455, 'CRUISE', 'PL HOTEL STOREKEEPER', NULL, NULL),
(456, 'CRUISE', 'PL PROVISION MASTER', NULL, NULL),
(457, 'CRUISE', 'PL STOREKEEPER', NULL, NULL),
(458, 'CRUISE', 'PL STORE UTILITY', NULL, NULL),
(459, 'CRUISE', 'NBM ASST. EXECUTIVE HOUSEKEEPER', NULL, NULL),
(460, 'CRUISE', 'NBM CLEANING SPECIALIST', NULL, NULL),
(461, 'CRUISE', 'NBM EXECUTIVE HOUSEKEEPER', NULL, NULL),
(462, 'CRUISE', 'NBM FOOD AND BEVERAGE DIRECTOR', NULL, NULL),
(463, 'CRUISE', 'NBM GUEST ADMIN OFFICER', NULL, NULL),
(464, 'CRUISE', 'NBM GUEST SERVICES MANAGER', NULL, NULL),
(465, 'CRUISE', 'NBM HOTEL DIRECTOR', NULL, NULL),
(466, 'CRUISE', 'NBM HUMAN RESOURCES MANAGER', NULL, NULL),
(467, 'CRUISE', 'NBM CREW ADMIN MANAGER', NULL, NULL),
(468, 'CRUISE', 'NBM OPERATIONS MANAGER', NULL, NULL),
(469, 'CRUISE', 'TRVL BEVERAGE OPS MGR', NULL, NULL),
(470, 'CRUISE', 'FLEET GUEST SERVICES MANAGER', NULL, NULL),
(471, 'CRUISE', 'FLEET FINANCIAL CONTROLLER', NULL, NULL),
(472, 'CRUISE', 'TRVL RESTAURANT OPS MGR', NULL, NULL),
(473, 'CRUISE', 'TRAVELING CREW ADMIN MANAGER', NULL, NULL),
(474, 'CRUISE', 'TRAVELING CORPORATE CHEF', NULL, NULL),
(475, 'CRUISE', 'TVL CORPORATE PASTRY CHEF', NULL, NULL),
(476, 'CRUISE', 'TRVL F&B DIRECTOR', NULL, NULL),
(477, 'CRUISE', 'TRVL CRUISE SALES MANAGER', NULL, NULL),
(478, 'CRUISE', 'TRVL SR HEAD BROADCAST TECH', NULL, NULL),
(479, 'CRUISE', 'TVL SR SOUND AND LIGHT TECH', NULL, NULL),
(480, 'CRUISE', 'TRAVEL STAGE PROD MGR RIGGING', NULL, NULL),
(481, 'CRUISE', 'FLEET EXECUTIVE HOUSEKEEPER', NULL, NULL),
(482, 'CRUISE', 'FLEET LAUNDRY MASTER ', NULL, NULL),
(483, 'CRUISE', 'TRVLNG CLEANING SPECIALIST', NULL, NULL),
(484, 'CRUISE', 'SR TRAVELING REVENUE MANAGER', NULL, NULL),
(485, 'CRUISE', 'ASSISTANT LAUNDRY MASTER', NULL, NULL),
(486, 'CRUISE', 'LAUNDRY ATTENDANT', NULL, NULL),
(487, 'CRUISE', 'LAUNDRY MASTER', NULL, NULL),
(488, 'CRUISE', 'ASSISTANT SYSTEMS MANAGER I', NULL, NULL),
(489, 'CRUISE', 'ASSISTANT SYSTEMS MANAGER II', NULL, NULL),
(490, 'CRUISE', 'ASSISTANT SYSTEMS MANAGER III', NULL, NULL),
(491, 'CRUISE', 'IT OPERATIONS MANAGER', NULL, NULL),
(492, 'CRUISE', 'IT INFRASTRUCTURE & OPS MGR', NULL, NULL),
(493, 'CRUISE', 'SYSTEMS MANAGER', NULL, NULL),
(494, 'CRUISE', 'BAKER-ASST', NULL, NULL),
(495, 'CRUISE', 'CDP2 BAKER', NULL, NULL),
(496, 'CRUISE', 'CDP3 BAKER', NULL, NULL),
(497, 'CRUISE', 'BAKER-TRAINEE', NULL, NULL),
(498, 'CRUISE', 'COMMIS 1 BUTCHER', NULL, NULL),
(499, 'CRUISE', 'CDP2 BUTCHER', NULL, NULL),
(500, 'CRUISE', 'COMMIS 2 BUTCHER', NULL, NULL),
(501, 'CRUISE', 'CHEF DE PARTIE  1 SUSHI', NULL, NULL),
(502, 'CRUISE', 'CHEF DE PARTIE  2SUSHI', NULL, NULL),
(503, 'CRUISE', 'CHEF DE PARTIE  3 SUSHI', NULL, NULL),
(504, 'CRUISE', 'CHEF DE PARTIE BAKER', NULL, NULL),
(505, 'CRUISE', 'CHEF DE PARTIE BUTCHER', NULL, NULL),
(506, 'CRUISE', 'CHEF DE PARTIE-1', NULL, NULL),
(507, 'CRUISE', 'CHEF DE PARTIE-2', NULL, NULL),
(508, 'CRUISE', 'CHEF DE PARTIE 3', NULL, NULL),
(509, 'CRUISE', 'CHEF DE PARTIE GANDEMANGER', NULL, NULL),
(510, 'CRUISE', 'CHEF DE PARTIE PASTRY', NULL, NULL),
(511, 'CRUISE', 'COMMIS 1 SUSHI', NULL, NULL),
(512, 'CRUISE', 'COMMIS 2 SUSHI', NULL, NULL),
(513, 'CRUISE', 'CULINARY OPERATIONS COORD', NULL, NULL),
(514, 'CRUISE', 'JR CULINARY OPERATIONS COORD', NULL, NULL),
(515, 'CRUISE', 'COMMIS - 1', NULL, NULL),
(516, 'CRUISE', 'COMMIS - 2', NULL, NULL),
(517, 'CRUISE', 'EXECUTIVE CHEF 3', NULL, NULL),
(518, 'CRUISE', 'EXECUTIVE CHEF (OASIS CLASS)', NULL, NULL),
(519, 'CRUISE', 'EXECUTIVE SOUS CHEF', NULL, NULL),
(520, 'CRUISE', 'FOOD & BEVERAGE ADMIN ASST', NULL, NULL),
(521, 'CRUISE', 'GALLEY STEWARDING SPECIALIST', NULL, NULL),
(522, 'CRUISE', 'FOOD AND BEVERAGE DIRECTOR', NULL, NULL),
(523, 'CRUISE', 'FOOD & BEVERAGE MANAGER', NULL, NULL),
(524, 'CRUISE', 'F&B SANITATION CONTROLLER', NULL, NULL),
(525, 'CRUISE', 'COMMIS 1 PASTRY', NULL, NULL),
(526, 'CRUISE', 'PASTRY COOK', NULL, NULL),
(527, 'CRUISE', 'CDP2 PASTRY', NULL, NULL),
(528, 'CRUISE', 'PASTRY COOK-TRAINEE', NULL, NULL),
(529, 'CRUISE', 'PASTRY SOUS CHEF', NULL, NULL),
(530, 'CRUISE', 'CDP2 PANTRY', NULL, NULL),
(531, 'CRUISE', 'PANTRY COOK-ASST', NULL, NULL),
(532, 'CRUISE', 'COMMIS 2 PANTRY', NULL, NULL),
(533, 'CRUISE', 'RELIEF EXECUTIVE CHEF', NULL, NULL),
(534, 'CRUISE', 'TEPPAN CHEF', NULL, NULL),
(535, 'CRUISE', 'GALLEY STEWARD', NULL, NULL),
(536, 'CRUISE', 'GALLEY STEWARD HEAD', NULL, NULL),
(537, 'CRUISE', 'ASSISTAND MAITRE`D', NULL, NULL),
(538, 'CRUISE', 'ASST MGR DINING ROOM', NULL, NULL),
(539, 'CRUISE', 'MAITRE`D', NULL, NULL),
(540, 'CRUISE', 'RESTAURANT TRAINING MANAGER', NULL, NULL),
(541, 'CRUISE', 'RESTAURANT ATTENDANT', NULL, NULL),
(542, 'CRUISE', 'PHONE OPERATOR ROOM SVC', NULL, NULL),
(543, 'CRUISE', 'RESTAURANT SUPERVISOR', NULL, NULL),
(544, 'CRUISE', 'WAITER-ASST', NULL, NULL),
(545, 'CRUISE', 'BEVERAGE OPERATIONS SUPERVISOR', NULL, NULL),
(546, 'CRUISE', 'BEVERAGE MANAGER', NULL, NULL),
(547, 'CRUISE', 'ASSISTANT BEVERAGE  MGR', NULL, NULL),
(548, 'CRUISE', 'PLAYMAKERS MANAGER            ', NULL, NULL),
(549, 'CRUISE', 'SENIOR ASST. BAR MANAGER', NULL, NULL),
(550, 'CRUISE', 'SENIOR BEVERAGE MANAGER', NULL, NULL),
(551, 'CRUISE', 'STARBUCKS TEAM LEADER', NULL, NULL),
(552, 'CRUISE', 'BAR UTILITY', NULL, NULL),
(553, 'CRUISE', 'LEAD BARTENDER', NULL, NULL),
(554, 'CRUISE', 'WINE TENDER', NULL, NULL),
(555, 'CRUISE', 'BEVERAGE OPERATIONS COORD', NULL, NULL),
(556, 'CRUISE', 'LOYALTY CRUISE SALES ASSOCIATE', NULL, NULL),
(557, 'CRUISE', 'LOYALTY CRUISE SALES ASST.MANAGER', NULL, NULL),
(558, 'CRUISE', 'LOYALTY CRUISE SALES MANAGER', NULL, NULL),
(559, 'CRUISE', 'ASSISTANT CASINO HOST', NULL, NULL),
(560, 'CRUISE', 'CASINO HOST', NULL, NULL),
(561, 'CRUISE', 'ASST. CASINO MANAGER', NULL, NULL),
(562, 'CRUISE', 'CASHIER TRAINEE', NULL, NULL),
(563, 'CRUISE', 'CASINO INSPECTOR', NULL, NULL),
(564, 'CRUISE', 'COMMUNICATION SERVICES SPEC', NULL, NULL),
(565, 'CRUISE', 'DEALER - ASIA', NULL, NULL),
(566, 'CRUISE', 'DEALER-A', NULL, NULL),
(567, 'CRUISE', 'DEALER-D', NULL, NULL),
(568, 'CRUISE', 'DEALER A', NULL, NULL),
(569, 'CRUISE', 'DEALER D', NULL, NULL),
(570, 'CRUISE', 'MARKETING & REVENUE MANAGER', NULL, NULL),
(571, 'CRUISE', 'CASINO SLOT ATTENDANT', NULL, NULL),
(572, 'CRUISE', 'AQUATICS ACTIVITY STAFF', NULL, NULL),
(573, 'CRUISE', 'DIVE MANAGER-ASST.', NULL, NULL),
(574, 'CRUISE', 'DIVE INSTRUCTOR', NULL, NULL),
(575, 'CRUISE', 'DIVE MANAGER', NULL, NULL),
(576, 'CRUISE', 'EXPLORATIONS! PORT LECTURER', NULL, NULL),
(577, 'CRUISE', 'EXPLORATIONS MANAGER', NULL, NULL),
(578, 'CRUISE', 'EXPLORATIONS STAFF', NULL, NULL),
(579, 'CRUISE', 'PARASAIL BOAT OPERATOR', NULL, NULL),
(580, 'CRUISE', 'PRIVATE DESTINATION DIVE MGR', NULL, NULL),
(581, 'CRUISE', 'PRIVATE DESTINATION LIFEGUARD', NULL, NULL),
(582, 'CRUISE', 'ASST. SHORE EXCURSIONS MANAGER', NULL, NULL),
(583, 'CRUISE', 'TRVL SHORE EXCURSION MGR', NULL, NULL),
(584, 'CRUISE', 'INVENTORY CONTROL SPECIALIST', NULL, NULL),
(585, 'CRUISE', 'PROVISION MASTER', NULL, NULL),
(586, 'CRUISE', 'PROVISION STOREKEEPER', NULL, NULL),
(587, 'CRUISE', 'STOREKEEPER-HOTEL', NULL, NULL),
(588, 'CRUISE', 'STOREKEEPER-ASST. HOTEL', NULL, NULL),
(589, 'CRUISE', 'PROVISION UTILITY', NULL, NULL),
(590, 'CRUISE', 'AQUA CAST SUPPORT DIVER', NULL, NULL),
(591, 'CRUISE', 'DIG SIGNAGE & ITV CONTENT SPEC', NULL, NULL),
(592, 'CRUISE', 'ENTERTAINMENT RIGGING SPECIALI', NULL, NULL),
(593, 'CRUISE', 'LIGHT TECHNICIAN', NULL, NULL),
(594, 'CRUISE', 'LOUNGE TECHNICIAN', NULL, NULL),
(595, 'CRUISE', 'STAGE & PROD. MGR AQUA THEATER', NULL, NULL),
(596, 'CRUISE', 'STAGE MANAGER STUDIO B', NULL, NULL),
(597, 'CRUISE', 'STAGE & PRODUCTION MGR', NULL, NULL),
(598, 'CRUISE', 'HEAD SOUND & LIGHT TECH.', NULL, NULL),
(599, 'CRUISE', 'SOUND TECHNICIAN', NULL, NULL),
(600, 'CRUISE', 'AQUATIC STAGE STAFF', NULL, NULL),
(601, 'CRUISE', 'STAGE STAFF', NULL, NULL),
(602, 'CRUISE', 'HEAD STAGE STAFF', NULL, NULL),
(603, 'CRUISE', 'TECHNICAL STAGE STAFF', NULL, NULL),
(604, 'CRUISE', 'RIGGING SUPPORT STAGE STAFF', NULL, NULL),
(605, 'CRUISE', 'BROADCAST TECHNICIAN', NULL, NULL),
(606, 'CRUISE', 'HEAD BROADCAST TECHNICIAN', NULL, NULL),
(607, 'CRUISE', 'ACTIVITIES MANAGER', NULL, NULL),
(608, 'CRUISE', 'ADVENTURE OCEAN MANAGER', NULL, NULL),
(609, 'CRUISE', 'ADVENTURE OCEAN MNG ASST', NULL, NULL),
(610, 'CRUISE', 'CRUISE PROGRAMS ADMINISTRATOR', NULL, NULL),
(611, 'CRUISE', 'CRUISE ENTERTAINMENT STAFF', NULL, NULL),
(612, 'CRUISE', 'CRUISE STAFF', NULL, NULL),
(613, 'CRUISE', 'CRUISE DIRECTOR', NULL, NULL),
(614, 'CRUISE', 'SPORTS MANAGER', NULL, NULL),
(615, 'CRUISE', 'SPORTS STAFF', NULL, NULL),
(616, 'CRUISE', 'SPORTS & FITNESS SUPERVISOR', NULL, NULL),
(617, 'CRUISE', 'LEAD SPORTS STAFF IFLY', NULL, NULL),
(618, 'CRUISE', 'YOUTH STAFF', NULL, NULL),
(619, 'CRUISE', 'YOUTH STAFF-SEASONAL', NULL, NULL),
(620, 'CRUISE', 'ACOUSTIC BASS-STRING', NULL, NULL),
(621, 'CRUISE', 'ACORDIAN/PIANO-STRING', NULL, NULL),
(622, 'CRUISE', 'AQUA CAST DIVE CAPTAIN', NULL, NULL),
(623, 'CRUISE', 'ACROBAT TUMBLER', NULL, NULL),
(624, 'CRUISE', 'FEATURED AERIALIST', NULL, NULL),
(625, 'CRUISE', 'ACOUSTIC GUITAR-STRING', NULL, NULL),
(626, 'CRUISE', 'AQUA CAST DANCER', NULL, NULL),
(627, 'CRUISE', 'AQUA CAST DIVER/SWIMMER', NULL, NULL),
(628, 'CRUISE', 'FEATURED AQUA THEATER ACT', NULL, NULL),
(629, 'CRUISE', 'BASS ELECTRIC', NULL, NULL),
(630, 'CRUISE', 'BAND LDR ACOUSTIC BASS-STRING', NULL, NULL),
(631, 'CRUISE', 'BAND LDR-ACORDIAN/PIANO-STRING', NULL, NULL),
(632, 'CRUISE', 'BAND LDR-ACOUSTIC GUITAR STRNG', NULL, NULL),
(633, 'CRUISE', 'BAND LEADER-ALTO SAX', NULL, NULL),
(634, 'CRUISE', 'BAND LEADER-BASS ELECTRIC', NULL, NULL),
(635, 'CRUISE', 'BAND LEADER-CALYPSO BASS', NULL, NULL),
(636, 'CRUISE', 'BAND LEADER-CALYPSO DRUMS', NULL, NULL),
(637, 'CRUISE', 'BAND LEADER-CALYPSO GUITAR', NULL, NULL),
(638, 'CRUISE', 'BAND LEADER-CLARINET', NULL, NULL),
(639, 'CRUISE', 'BAND LEADER-DRUMS', NULL, NULL),
(640, 'CRUISE', 'BAND LEADER-FLUTE', NULL, NULL),
(641, 'CRUISE', 'BAND LEADER-GUITAR ELECTRIC', NULL, NULL),
(642, 'CRUISE', 'BAND LEADER-CALYPSO KEYBOARD', NULL, NULL),
(643, 'CRUISE', 'BAND LEADER-STEEL DRUMS/PANS', NULL, NULL),
(644, 'CRUISE', 'BAND LEADER-PIANO', NULL, NULL),
(645, 'CRUISE', 'BAND LEADER-CALYPSO SINGER', NULL, NULL),
(646, 'CRUISE', 'BAND LEADER-TROMBONE', NULL, NULL),
(647, 'CRUISE', 'BAND LEADER-TENOR SAX', NULL, NULL),
(648, 'CRUISE', 'BAND LEADER LEAD TRUMPET', NULL, NULL),
(649, 'CRUISE', 'BAND LEADER-VIOLIN', NULL, NULL),
(650, 'CRUISE', 'CALYPSO BASS', NULL, NULL),
(651, 'CRUISE', 'CALYPSO DRUMS', NULL, NULL),
(652, 'CRUISE', 'CALYPSO GUITAR', NULL, NULL),
(653, 'CRUISE', 'CHARACTER PERFORMER COORDINATO', NULL, NULL),
(654, 'CRUISE', 'CHARACTER PERFORMER', NULL, NULL),
(655, 'CRUISE', 'CHICAGO CONDUCTOR & INTERMISSI', NULL, NULL),
(656, 'CRUISE', 'CALYPSO KEYBOARD', NULL, NULL),
(657, 'CRUISE', 'CLARINET', NULL, NULL),
(658, 'CRUISE', 'CLASSICAL GUITARIST', NULL, NULL),
(659, 'CRUISE', 'CALYPSO SINGER', NULL, NULL),
(660, 'CRUISE', 'DANCE CAPTAIN', NULL, NULL),
(661, 'CRUISE', 'DRUMS', NULL, NULL),
(662, 'CRUISE', 'FTD BMX FLATLAND SPECIALIST', NULL, NULL),
(663, 'CRUISE', 'FLUTE', NULL, NULL),
(664, 'CRUISE', 'FEATURED ACTOR', NULL, NULL),
(665, 'CRUISE', 'FEATURED BALLROOM DANCERS', NULL, NULL),
(666, 'CRUISE', 'FEATURED HIGH DIVERS', NULL, NULL),
(667, 'CRUISE', 'FEATURED SINGER', NULL, NULL),
(668, 'CRUISE', 'GUITAR-ELECTRIC', NULL, NULL),
(669, 'CRUISE', 'FEATURED HAND BALANCING ACT', NULL, NULL),
(670, 'CRUISE', 'INTERMISSIONIST PIANO', NULL, NULL),
(671, 'CRUISE', 'LEAD ALTO SAX', NULL, NULL),
(672, 'CRUISE', 'LEAD TRUMPET', NULL, NULL),
(673, 'CRUISE', 'MUSICAL DIRECTOR-ALTO SAX', NULL, NULL),
(674, 'CRUISE', 'MUSICAL DIRECTOR-BASS ELEC.', NULL, NULL),
(675, 'CRUISE', 'MUSICAL DIRECTOR-DRUMS', NULL, NULL),
(676, 'CRUISE', 'MUSICAL DIRECTOR-GUITAR ELEC.', NULL, NULL),
(677, 'CRUISE', 'MUSICAL DIRECTOR-LEAD TRUMPET', NULL, NULL),
(678, 'CRUISE', 'MUSICAL DIRECTOR-PIANO', NULL, NULL),
(679, 'CRUISE', 'MUSICAL DIRECTOR-TROMBONE', NULL, NULL),
(680, 'CRUISE', 'MUSICAL DIRECTOR-TENOR SAX', NULL, NULL),
(681, 'CRUISE', 'MUSICAL DIRECTOR-TRUMPET', NULL, NULL),
(682, 'CRUISE', 'MUSICIAN DOUBLER', NULL, NULL),
(683, 'CRUISE', 'STEEL DRUMS/PANS', NULL, NULL),
(684, 'CRUISE', 'PIANO', NULL, NULL),
(685, 'CRUISE', 'ROYAL PLAYERS  / COMEDIC HOST', NULL, NULL),
(686, 'CRUISE', 'ACROBAT', NULL, NULL),
(687, 'CRUISE', 'REVUE CAST DANCER', NULL, NULL),
(688, 'CRUISE', 'RC - DANCER VOCALIST', NULL, NULL),
(689, 'CRUISE', 'REVUE CAST ICE COMPANY MGR', NULL, NULL),
(690, 'CRUISE', 'REVUE CAST ICE SKATERS', NULL, NULL),
(691, 'CRUISE', 'REVUE CAST-LEAD VOCAL', NULL, NULL),
(692, 'CRUISE', 'RC ACRO CAPTAIN', NULL, NULL),
(693, 'CRUISE', 'REVUE CAST SINGER', NULL, NULL),
(694, 'CRUISE', 'FEATURED REHEARSAL TRAINING', NULL, NULL),
(695, 'CRUISE', 'RC REHEARSAL TRAINING', NULL, NULL),
(696, 'CRUISE', 'TWO70 DANCE CAPTAIN', NULL, NULL),
(697, 'CRUISE', 'TWO70 ENSEMBLE PERFORMER', NULL, NULL),
(698, 'CRUISE', 'TWO70 VOCALIST PERFORMER', NULL, NULL),
(699, 'CRUISE', 'SLACKLINE/HIGHLINE SPEC PERF', NULL, NULL),
(700, 'CRUISE', 'TROMBONE', NULL, NULL),
(701, 'CRUISE', 'TENOR SAX', NULL, NULL),
(702, 'CRUISE', 'TRUMPET', NULL, NULL),
(703, 'CRUISE', 'VIOLIN', NULL, NULL),
(704, 'CRUISE', 'FLEET HOTEL MAINTENANCE MGR', NULL, NULL),
(705, 'CRUISE', 'ASST. EXECUTIVE HOUSEKEEPER', NULL, NULL),
(706, 'CRUISE', 'LOFT APPRENTICE', NULL, NULL),
(707, 'CRUISE', 'BELL & OFFICER ATTENDANT', NULL, NULL),
(708, 'CRUISE', 'PUBLIC AREA ATTENDANT         ', NULL, NULL),
(709, 'CRUISE', 'CLEANER-TAILOR', NULL, NULL),
(710, 'CRUISE', 'DECK SUPERVISOR', NULL, NULL),
(711, 'CRUISE', 'EXECUTIVE HOUSEKEEPER', NULL, NULL),
(712, 'CRUISE', 'HEAD CLEANER', NULL, NULL),
(713, 'CRUISE', 'ASSISTANT CLEANING SPECIALIST', NULL, NULL),
(714, 'CRUISE', 'CLEANING SPECIALIST', NULL, NULL),
(715, 'CRUISE', 'HOUSEKEEPING ADMIN ASST', NULL, NULL),
(716, 'CRUISE', 'LINENKEEPER-ASST', NULL, NULL),
(717, 'CRUISE', 'LINENKEEPER/LAUNDRYMASTER', NULL, NULL),
(718, 'CRUISE', 'LOFT ATTENDANT', NULL, NULL),
(719, 'CRUISE', 'HEAD POOL ATTENDANT', NULL, NULL),
(720, 'CRUISE', 'SR. DECK SUPERVISOR', NULL, NULL),
(721, 'CRUISE', 'CREW PAYROLL MANAGER', NULL, NULL),
(722, 'CRUISE', '2/PURSER, ADMINISTRATION', NULL, NULL),
(723, 'CRUISE', '2/PURSER, PAYROLL', NULL, NULL),
(724, 'CRUISE', 'ASST. FRONT DESK MANAGER', NULL, NULL),
(725, 'CRUISE', 'INTL AMBASSADOR', NULL, NULL),
(726, 'CRUISE', 'GUEST ADMINISTRATION OFFICER', NULL, NULL),
(727, 'CRUISE', 'GOLDEN CONCIERGE', NULL, NULL),
(728, 'CRUISE', 'GUEST DEPARTURE OFFICER', NULL, NULL),
(729, 'CRUISE', 'GROUP COORDINATOR', NULL, NULL),
(730, 'CRUISE', 'GUEST SERVICES MANAGER', NULL, NULL),
(731, 'CRUISE', 'GUEST SERVICES OFFICER', NULL, NULL),
(732, 'CRUISE', 'SENIOR GUEST SERVICES OFFICER', NULL, NULL),
(733, 'CRUISE', 'GUEST SERVICES OFFICER 2', NULL, NULL),
(734, 'CRUISE', 'HOTEL ADMIN ASST', NULL, NULL),
(735, 'CRUISE', 'HOTEL-DIRECTOR', NULL, NULL),
(736, 'CRUISE', 'ASSOCIATE INTL AMBASSADOR', NULL, NULL),
(737, 'CRUISE', 'INTL GROUP COORDINATOR', NULL, NULL),
(738, 'CRUISE', 'LOYALTY AMBASSADOR', NULL, NULL),
(739, 'CRUISE', 'MANAGER HOTEL OPERATIONS', NULL, NULL),
(740, 'CRUISE', 'PURSER PRINTER', NULL, NULL),
(741, 'CRUISE', 'ONBOARD DIGITAL MANAGER', NULL, NULL),
(742, 'CRUISE', 'ROYAL GENIE', NULL, NULL),
(743, 'CRUISE', 'ASST MGR TRAINING& DEVELOPMENT', NULL, NULL),
(744, 'CRUISE', 'CREW ADMIN MANAGER', NULL, NULL),
(745, 'CRUISE', 'HUMAN RESOURCES MANAGER 1', NULL, NULL),
(746, 'CRUISE', 'HUMAN RESOURCES MANAGER', NULL, NULL),
(747, 'CRUISE', 'HUMAN RESOURCES SPECIALIST', NULL, NULL),
(748, 'CRUISE', 'CREW ADMINISTRATOR', NULL, NULL),
(749, 'CRUISE', 'CREW RELATIONS SPECIALIST', NULL, NULL),
(750, 'CRUISE', 'TRAINING & DEVELOPMENT MGR', NULL, NULL),
(751, 'CRUISE', 'PD ISLAND DOCTOR', NULL, NULL),
(752, 'CRUISE', 'PD SITE NURSE', NULL, NULL),
(753, 'CRUISE', 'PD LAUNDRY SPECIALIST', NULL, NULL),
(754, 'CRUISE', 'PD AQUATICS GUIDE', NULL, NULL),
(755, 'CRUISE', 'PD AQUATICS GENERAL WORKER', NULL, NULL),
(756, 'CRUISE', 'PD  BALLOON OPS PILOT', NULL, NULL),
(757, 'CRUISE', 'PD BALLOON OPS COPILOT', NULL, NULL),
(758, 'CRUISE', 'PD BALLOON OPS', NULL, NULL),
(759, 'CRUISE', 'PD DIVE INSTRUCTOR', NULL, NULL),
(760, 'CRUISE', 'PD GEN WORKERS / ATTENDANTS', NULL, NULL),
(761, 'CRUISE', 'PD COOK ASST COMMIS', NULL, NULL),
(762, 'CRUISE', 'PD ATTENDANT KITCHEN', NULL, NULL),
(763, 'CRUISE', 'PD SOUS CHEF', NULL, NULL),
(764, 'CRUISE', 'PD BEVERAGE MANAGER', NULL, NULL),
(765, 'CRUISE', 'PD EXECUTIVE CHEF', NULL, NULL),
(766, 'CRUISE', 'PD EXEC SOUS CHEF', NULL, NULL),
(767, 'CRUISE', 'PD F& B DIRECTOR', NULL, NULL),
(768, 'CRUISE', 'PD F & B VENUE MGR', NULL, NULL),
(769, 'CRUISE', 'PD CHEF DE PARTIE', NULL, NULL),
(770, 'CRUISE', 'PD START UP F & B DIRECTOR', NULL, NULL),
(771, 'CRUISE', 'PD CLUB SERVER/HOST', NULL, NULL),
(772, 'CRUISE', 'PD RESTAURANT ATTENDANTS', NULL, NULL),
(773, 'CRUISE', 'PD ASSISTANT BAR MANAGER', NULL, NULL),
(774, 'CRUISE', 'PD BAR ATTENDANT', NULL, NULL),
(775, 'CRUISE', 'PD BAR SERVER', NULL, NULL),
(776, 'CRUISE', 'PD BARTENDER', NULL, NULL),
(777, 'CRUISE', 'PD BEVERAGE SUPERVISOR', NULL, NULL),
(778, 'CRUISE', 'PD LEAD BARTENDER', NULL, NULL),
(779, 'CRUISE', 'PD STARBUCKS BARISTA', NULL, NULL),
(780, 'CRUISE', 'PD STARBUCKS SUPERVISOR', NULL, NULL),
(781, 'CRUISE', 'PD ASST EXCURSION MGR', NULL, NULL),
(782, 'CRUISE', 'PD ISLAND ENT MANAGER', NULL, NULL),
(783, 'CRUISE', 'PD ACTIVITIES STAFF', NULL, NULL),
(784, 'CRUISE', 'PD CABANA ATTENDANT', NULL, NULL),
(785, 'CRUISE', 'PD CABANA MANAGER', NULL, NULL),
(786, 'CRUISE', 'PD CRUISE ENT STAFF', NULL, NULL),
(787, 'CRUISE', 'PD MECHANICAL ELEC PLUMBER', NULL, NULL),
(788, 'CRUISE', 'PD ISLAND OPS SUPERVISOR', NULL, NULL),
(789, 'CRUISE', 'PD SHOREX MANAGER', NULL, NULL),
(790, 'CRUISE', 'PD ISLAND TECH OFFICER', NULL, NULL),
(791, 'CRUISE', 'PD JET SKI CHASE', NULL, NULL),
(792, 'CRUISE', 'PD KAYAK TOUR STAFF', NULL, NULL),
(793, 'CRUISE', 'PD LEAD SHORE EXCURSIONS', NULL, NULL),
(794, 'CRUISE', 'PD AQUATIC & ACTIVITIES MGR', NULL, NULL),
(795, 'CRUISE', 'PD TROLLEY DRIVER', NULL, NULL),
(796, 'CRUISE', 'PD TRAM DRIVER', NULL, NULL),
(797, 'CRUISE', 'PD ASST STOREKEEPER', NULL, NULL),
(798, 'CRUISE', 'PD INVENTORY CTRL SPECIALIST', NULL, NULL),
(799, 'CRUISE', 'PD INVENTORY MGR', NULL, NULL),
(800, 'CRUISE', 'PD FOOD SAFETY OFF', NULL, NULL),
(801, 'CRUISE', 'PD DISC JOCKEY', NULL, NULL),
(802, 'CRUISE', 'PD LOUNGE TECHNICIAN', NULL, NULL),
(803, 'CRUISE', 'PD SPORTS STAFF', NULL, NULL),
(804, 'CRUISE', 'PD CAPT JACK`S GUITAR ENT', NULL, NULL),
(805, 'CRUISE', 'PD CALYPSO BAND MUSICIAN', NULL, NULL),
(806, 'CRUISE', 'PD CAPTAIN JACK', NULL, NULL),
(807, 'CRUISE', 'PD CAPTAIN JILL', NULL, NULL),
(808, 'CRUISE', 'PD JUNKANOO DANCER', NULL, NULL),
(809, 'CRUISE', 'PD OFFICE ADMIN', NULL, NULL),
(810, 'CRUISE', 'PD HOUSEKEEPER', NULL, NULL),
(811, 'CRUISE', 'PD LEAD HOUSEKEEPER', NULL, NULL),
(812, 'CRUISE', 'PD RESTROOM ATTENDANT', NULL, NULL),
(813, 'CRUISE', 'PD OPERATIONS DIRECTOR', NULL, NULL),
(814, 'CRUISE', 'PD FINANCIAL CONTROLLER', NULL, NULL),
(815, 'CRUISE', 'PD GUEST EXPERIENCE MGR', NULL, NULL),
(816, 'CRUISE', 'PD GUEST SERV WATER PARK', NULL, NULL),
(817, 'CRUISE', 'PD GENERAL MANAGER', NULL, NULL),
(818, 'CRUISE', 'PD PROJECT MANAGER', NULL, NULL),
(819, 'CRUISE', 'PD OPS MANAGER', NULL, NULL),
(820, 'CRUISE', 'PD PROB RES & BRAND STAND MGR', NULL, NULL),
(821, 'CRUISE', 'PD PAYROLL MANAGER', NULL, NULL),
(822, 'CRUISE', 'PD START UP GENERAL MGR', NULL, NULL),
(823, 'CRUISE', 'PD CREW ADMIN OFFICER', NULL, NULL),
(824, 'CRUISE', 'PD HR ADMINISTRATOR', NULL, NULL),
(825, 'CRUISE', 'PD HR MANAGER', NULL, NULL),
(826, 'CRUISE', 'PD HR SPECIALIST', NULL, NULL),
(827, 'CRUISE', 'PD TRAINING MGR', NULL, NULL),
(828, 'MARINE', 'AEP 1ST ENGINEER', NULL, NULL),
(829, 'MARINE', 'AEP 2ND ENGINEER', NULL, NULL),
(830, 'MARINE', 'AEP 3RD ENGINEER', NULL, NULL),
(831, 'MARINE', 'AEP JR ENGINE MAN', NULL, NULL),
(832, 'MARINE', 'AEP JR SEAMAN', NULL, NULL),
(833, 'MARINE', 'AEP CHIEF ENGINEER', NULL, NULL),
(834, 'MARINE', 'AEP SR SAFETY OFFICER', NULL, NULL),
(835, 'MARINE', 'AEP STAFF CHIEF ENGINEER', NULL, NULL),
(836, 'MARINE', 'AEP SAFETY OFFICER', NULL, NULL),
(837, 'MARINE', 'AEP SAFETY OFFICER JR', NULL, NULL),
(838, 'MARINE', 'GMTS 1ST ASST. CARPENTER/VARNI', NULL, NULL),
(839, 'MARINE', 'GMTS 1ST ASST ELECTRICIAN', NULL, NULL),
(840, 'MARINE', 'GMTS 1ST ELECTRICAL ENGINEER', NULL, NULL),
(841, 'MARINE', 'GMTS 1ST ENGINEER', NULL, NULL),
(842, 'MARINE', 'GMTS 1ST ENGINEER POWERPLANT', NULL, NULL),
(843, 'MARINE', 'GMTS 1ST OFFICER', NULL, NULL),
(844, 'MARINE', 'GMTS 2ND ASST. CARPENTER/VARNI', NULL, NULL),
(845, 'MARINE', 'GMTS 2ND ASST ELECTRICIAN', NULL, NULL),
(846, 'MARINE', 'GMTS 2ND ASST. REPAIRMAN', NULL, NULL),
(847, 'MARINE', 'GMTS 2ND ELECTRICAL ENGINEER', NULL, NULL),
(848, 'MARINE', 'GMTS 2ND ENGINEER', NULL, NULL),
(849, 'MARINE', 'GMTS 2ND OFFICER', NULL, NULL),
(850, 'MARINE', 'GMTS 2ND ASST. REF. TECHNICIAN', NULL, NULL),
(851, 'MARINE', 'GMTS 3RD ASST. CARPENTER/VARNI', NULL, NULL),
(852, 'MARINE', 'GMTS 3RD ASST ELECTRICIAN', NULL, NULL),
(853, 'MARINE', 'GMTS 3RD ASST. REPAIRMAN', NULL, NULL),
(854, 'MARINE', 'GMTS 3RD ELECTRICAL ENGINEER', NULL, NULL),
(855, 'MARINE', 'GMTS 3RD ENGINEER', NULL, NULL),
(856, 'MARINE', 'GMTS 3RD ASST. REF. TECHNICIAN', NULL, NULL),
(857, 'MARINE', 'GMTS ABLE SEAMEN', NULL, NULL),
(858, 'MARINE', 'GMTS AWP CHIEF ENGINEER', NULL, NULL),
(859, 'MARINE', 'GMTS ASST ELECTRICIAN', NULL, NULL),
(860, 'MARINE', 'GMTS AEP ENGINEER', NULL, NULL),
(861, 'MARINE', 'GMTS ASST. REPAIRMAN', NULL, NULL),
(862, 'MARINE', 'GMTS ASST. REF. TECHNICIAN', NULL, NULL),
(863, 'MARINE', 'GMTS AWP SR. SYSTEM SPECIALIST', NULL, NULL),
(864, 'MARINE', 'GMTS AWP SYSTEM SPECIALIST', NULL, NULL),
(865, 'MARINE', 'GMTS BOATSWAIN', NULL, NULL),
(866, 'MARINE', 'GMTS CHIEF ELECTRICAL ENGINEER', NULL, NULL),
(867, 'MARINE', 'GMTS CHIEF ENGINEER', NULL, NULL),
(868, 'MARINE', 'GMTS CHIEF ENGINEER POWERPLANT', NULL, NULL),
(869, 'MARINE', 'GMTS CHIEF REF. ENGINEER', NULL, NULL),
(870, 'MARINE', 'GMTS CARPENTER SUPERVISOR', NULL, NULL),
(871, 'MARINE', 'CHIEF OFFICER SAFETY CEL', NULL, NULL),
(872, 'MARINE', 'CHIEF OFFICER SAFETY RCI', NULL, NULL),
(873, 'MARINE', 'GMTS CARPENTER/VARNISHER', NULL, NULL),
(874, 'MARINE', 'GMTS DECK TRAINEE', NULL, NULL),
(875, 'MARINE', 'GMTS ENGINE/DIESEL MECHANIC', NULL, NULL),
(876, 'MARINE', 'GMTS ELECTRONICS ENGINEER', NULL, NULL),
(877, 'MARINE', 'GMTS ENGINE FOREMAN', NULL, NULL),
(878, 'MARINE', 'GMTS ENVIRONMENTAL OFFICER', NULL, NULL),
(879, 'MARINE', 'GMTS ELECTRICAL SUPERVISOR', NULL, NULL),
(880, 'MARINE', 'GMTS ENGINE TRAINEE', NULL, NULL),
(881, 'MARINE', 'GMTS FITTER', NULL, NULL),
(882, 'MARINE', 'GMTS HOTEL MAINTENANCE MGR', NULL, NULL),
(883, 'MARINE', 'GMTS LEAD HVAC SPECIALIST', NULL, NULL),
(884, 'MARINE', 'GMTS HVAC SPECIALIST', NULL, NULL),
(885, 'MARINE', 'GMTS INJURY PREVENTION OFFICER', NULL, NULL),
(886, 'MARINE', 'GMTS TRVL INVENTORY TECH SPEC', NULL, NULL),
(887, 'MARINE', 'GMTS JR. ENGINE MAN', NULL, NULL),
(888, 'MARINE', 'GMTS JR SEAMEN', NULL, NULL),
(889, 'MARINE', 'GMTS LEAD ENGINEER POWERPLANT', NULL, NULL),
(890, 'MARINE', 'GMTS MECHANIC CERTIFIED', NULL, NULL),
(891, 'MARINE', 'GMTS MECHANIC CERTIFIED LEAD', NULL, NULL),
(892, 'MARINE', 'GMTS MECHANIC', NULL, NULL),
(893, 'MARINE', 'GMTS MECHANIC LEAD', NULL, NULL),
(894, 'MARINE', 'GMTS MARINE ADMIN ASSISTANT', NULL, NULL),
(895, 'MARINE', 'GMTS MOTORMAN', NULL, NULL),
(896, 'MARINE', 'GMTS MARINE SERVICE ENGINEER', NULL, NULL),
(897, 'MARINE', 'GMTS OILER', NULL, NULL),
(898, 'MARINE', 'GMTS TRAINER (OPERATIONS/AMOS)', NULL, NULL),
(899, 'MARINE', 'GMTS ORDINARY SEAMEN', NULL, NULL),
(900, 'MARINE', 'GMTS PRJ HOTEL MAINTENANCE MGR', NULL, NULL),
(901, 'MARINE', 'GMTS PLUMBER', NULL, NULL),
(902, 'MARINE', 'GMTS PROJECT MANAGER', NULL, NULL),
(903, 'MARINE', 'GMTS POWERPLANT OFFICER', NULL, NULL),
(904, 'MARINE', 'GMTS  REF. ENGINEER', NULL, NULL),
(905, 'MARINE', 'GMTS REPAIRMAN', NULL, NULL),
(906, 'MARINE', 'GMTS REF. MAINTENANCE TECH', NULL, NULL),
(907, 'MARINE', 'GMTS OFFICER', NULL, NULL),
(908, 'MARINE', 'GMTS REPAIRMAN SUPERVISOR', NULL, NULL),
(909, 'MARINE', 'GMTS STAFF CHIEF ENGINEER', NULL, NULL),
(910, 'MARINE', 'GMTS SR ENGINE/DIESEL MECHANIC', NULL, NULL),
(911, 'MARINE', 'GMTS STAFF CHIEF ENG PWRPLANT', NULL, NULL),
(912, 'MARINE', 'GMTS SAFETY OFFICER', NULL, NULL),
(913, 'MARINE', 'GMTS SENIOR GUARANTEE ENGINEER', NULL, NULL),
(914, 'MARINE', 'GMTS SR SAFETY OFFICER', NULL, NULL),
(915, 'MARINE', 'GMTS STAFF CAPTAIN', NULL, NULL),
(916, 'MARINE', 'GMTS TRV 1ST ENG. SANITATION', NULL, NULL),
(917, 'MARINE', 'GMTS  TRVL ACCOM. MAINT. MGR', NULL, NULL),
(918, 'MARINE', 'GMTS  TRVL CHIEF ELEC. ENG.', NULL, NULL),
(919, 'MARINE', 'GMTS TRVL CHIEF REF. ENGINEER', NULL, NULL),
(920, 'MARINE', 'GMTS TRVL ENV. OFFICER', NULL, NULL),
(921, 'MARINE', 'GMTS TECH ENGINE STOREKEEPER', NULL, NULL),
(922, 'MARINE', 'GMTS GUARANTEE ENGINEER', NULL, NULL),
(923, 'MARINE', 'GMTS TECHNICAL SUPPORT OFC 1', NULL, NULL),
(924, 'MARINE', 'GMTS TECHNICAL SUPPORT OFC 2', NULL, NULL),
(925, 'MARINE', 'GMTS TRVL SAFETY MGR', NULL, NULL),
(926, 'MARINE', 'GMTS TECH SUPPORT STOREKEEPER', NULL, NULL),
(927, 'MARINE', 'GMTS TVL CAPTAIN', NULL, NULL),
(928, 'MARINE', 'GMTS UPHOLSTERER', NULL, NULL),
(929, 'MARINE', 'REFURBISH CARPET INSTALLER', NULL, NULL),
(930, 'MARINE', 'REFURBISHING-1/ASST.CARPENTER', NULL, NULL),
(931, 'MARINE', 'REFURBISHING-2/ASST.CARPENTER', NULL, NULL),
(932, 'MARINE', 'REFURBISHING-3/ASST.CARPENTER', NULL, NULL),
(933, 'MARINE', 'REFURBISHING-CARPENTER', NULL, NULL),
(934, 'MARINE', 'REFURBISH CARPENTER SUPERVISOR', NULL, NULL),
(935, 'MARINE', 'REFURBISH PAINTER/ARTISTIC', NULL, NULL),
(936, 'MARINE', '1ST ASST REPAIRMAN            ', NULL, NULL),
(937, 'MARINE', 'REFURBISHING-2/ASST.REPAIRMAN', NULL, NULL),
(938, 'MARINE', '3RD ASST. REPAIRMAN', NULL, NULL),
(939, 'MARINE', 'REFURBISH-REPAIRMAN SUPERVISOR', NULL, NULL),
(940, 'MARINE', 'REFURBISHING-3/AST.UPHOLSTERER', NULL, NULL),
(941, 'MARINE', 'REFURBISHING-UPHOLSTERER', NULL, NULL),
(942, 'MARINE', 'UPHOLSTERY SUPERVISOR', NULL, NULL),
(943, 'MARINE', 'AZ SAILOR A/B', NULL, NULL),
(944, 'MARINE', 'AZ SAILOR O/S', NULL, NULL),
(945, 'MARINE', 'AZ DECK CADET (APPREN OFFICER)', NULL, NULL),
(946, 'MARINE', 'AZ DECK BOSUN', NULL, NULL),
(947, 'MARINE', 'AZ CHIEF OFFICER/SAFETY (MAST)', NULL, NULL),
(948, 'MARINE', 'AZ ENVIRONMENTAL OFFICER', NULL, NULL),
(949, 'MARINE', 'AZ CHIEF OFFICER (CHIEF TKT)', NULL, NULL),
(950, 'MARINE', 'AZ CHIEF OFFICER  (MASTER TKT)', NULL, NULL),
(951, 'MARINE', 'AZ CHIEF OFFICER/SAFETY(CHIEF', NULL, NULL),
(952, 'MARINE', 'AZ MASTER', NULL, NULL),
(953, 'MARINE', '1ST OFFICER - AZAMARA', NULL, NULL),
(954, 'MARINE', '2ND OFFICER - AZAMARA', NULL, NULL),
(955, 'MARINE', '3RD OFFICER', NULL, NULL),
(956, 'MARINE', 'AZ STAFF CAPTAIN', NULL, NULL),
(957, 'MARINE', 'AZ CREW WELFARE SPECIALIST', NULL, NULL),
(958, 'MARINE', 'AZ CREW ADMINISTRATOR', NULL, NULL),
(959, 'MARINE', 'AZ HUMAN RESOURCES MANAGER', NULL, NULL),
(960, 'MARINE', 'AZ HUMAN RESOURCES SPECIALIST', NULL, NULL),
(961, 'MARINE', 'AZ 2ND ENGINEER (CHIEF TICKET)', NULL, NULL),
(962, 'MARINE', 'AZ 2ND ENGINEER (2ND TICKET)', NULL, NULL),
(963, 'MARINE', 'AZ 3RD ENGINEER (2ND TICKET)', NULL, NULL),
(964, 'MARINE', 'AZ  A/C ENGINEER', NULL, NULL),
(965, 'MARINE', 'AZ APPRENTICE ENG/ENGINE CADET', NULL, NULL),
(966, 'MARINE', 'AZ AMOS CONTROLLER', NULL, NULL),
(967, 'MARINE', 'AZ CHIEF ENGINEER', NULL, NULL),
(968, 'MARINE', 'AZ ELECTRICIAN CADET', NULL, NULL),
(969, 'MARINE', 'AZ ENGINE CADET', NULL, NULL),
(970, 'MARINE', 'AZ 3RD ENGINEER (3RD TICKET)', NULL, NULL),
(971, 'MARINE', 'HOTEL REPAIRMAN', NULL, NULL),
(972, 'MARINE', 'HOTEL REPAIRMAN 1', NULL, NULL),
(973, 'MARINE', '1ST ENGINEER - ACCOMMODATION', NULL, NULL),
(974, 'MARINE', 'AZ JOINER', NULL, NULL);
INSERT INTO `job_desc` (`id`, `dept`, `job_description`, `created_at`, `updated_at`) VALUES
(975, 'MARINE', 'AZ MOTORMAN', NULL, NULL),
(976, 'MARINE', 'MECHANIC', NULL, NULL),
(977, 'MARINE', 'MECHANIC 1', NULL, NULL),
(978, 'MARINE', 'AZ MOTORMAN 1', NULL, NULL),
(979, 'MARINE', 'AZ PLUMBER', NULL, NULL),
(980, 'MARINE', 'REF ASSISTANT', NULL, NULL),
(981, 'MARINE', 'REF ASSISTANT 1', NULL, NULL),
(982, 'MARINE', 'REPAIRMAN', NULL, NULL),
(983, 'MARINE', 'REPAIRMAN 1', NULL, NULL),
(984, 'MARINE', 'AZ SANITATION TECHNICIAN', NULL, NULL),
(985, 'MARINE', 'AZ STAFF CHIEF ENGINEER', NULL, NULL),
(986, 'MARINE', 'SANITATION REPAIRMAN', NULL, NULL),
(987, 'MARINE', 'AZ UPHOLSTERER', NULL, NULL),
(988, 'MARINE', 'AZ VARNISHER', NULL, NULL),
(989, 'MARINE', 'AZ WIPER', NULL, NULL),
(990, 'MARINE', 'AZ 1ST ELECTRICIAN', NULL, NULL),
(991, 'MARINE', 'AZ 2ND ELECTRICIAN', NULL, NULL),
(992, 'MARINE', 'AZ CHIEF ELECTRICIAN', NULL, NULL),
(993, 'MARINE', 'ASSISTANT ELECTRICIAN', NULL, NULL),
(994, 'MARINE', 'ASSISTANT ELECTRICIAN 1', NULL, NULL),
(995, 'MARINE', 'AZ CHIEF SECURITY OFFICER', NULL, NULL),
(996, 'MARINE', 'AZ FLEET SECURITY OFFICER', NULL, NULL),
(997, 'MARINE', 'AZ SECURITY GUARD SUPERVISOR', NULL, NULL),
(998, 'MARINE', 'AZ SECURITY OFFICER', NULL, NULL),
(999, 'MARINE', 'AZ SECURITY GUARD', NULL, NULL),
(1000, 'MARINE', 'SR. DOCTOR', NULL, NULL),
(1001, 'MARINE', 'AZ NURSE', NULL, NULL),
(1002, 'MARINE', 'DOCTOR', NULL, NULL),
(1003, 'MARINE', 'CREW ADMINISTRATOR', NULL, NULL),
(1004, 'MARINE', 'CREW  WELFARE SPECIALIST', NULL, NULL),
(1005, 'MARINE', 'CELEBRITY MEDICAL SECRETARY', NULL, NULL),
(1006, 'MARINE', 'CHIEF NURSE CELEBRITY', NULL, NULL),
(1007, 'MARINE', 'HUMAN RESOURCES MANAGER', NULL, NULL),
(1008, 'MARINE', 'CELEBRITY NURSE', NULL, NULL),
(1009, 'MARINE', 'ASST. LAWNKEEPER', NULL, NULL),
(1010, 'MARINE', 'APPR.OFFICER/DECK CADET', NULL, NULL),
(1011, 'MARINE', 'DECK BOSUN', NULL, NULL),
(1012, 'MARINE', '1ST OFFICER - CELEBRITY', NULL, NULL),
(1013, 'MARINE', '2ND OFFICER - CELEBRITY', NULL, NULL),
(1014, 'MARINE', 'CHIEF OFFICER - SAFETY MASTER', NULL, NULL),
(1015, 'MARINE', 'CHIEF OFFICER - DECK', NULL, NULL),
(1016, 'MARINE', 'CHIEF OFFICER - MASTER', NULL, NULL),
(1017, 'MARINE', 'CHIEF OFFICER - SAFETY', NULL, NULL),
(1018, 'MARINE', 'CEL UK DECK CADET', NULL, NULL),
(1019, 'MARINE', 'ENVIRONMENTAL OFFICER CEL', NULL, NULL),
(1020, 'MARINE', 'LAWNKEEPER', NULL, NULL),
(1021, 'MARINE', 'MARINE ADMINISTRATIVE ASSISTAN', NULL, NULL),
(1022, 'MARINE', 'MASTER', NULL, NULL),
(1023, 'MARINE', 'PUB HEALTH & SAFETY SPECIALIST', NULL, NULL),
(1024, 'MARINE', 'SAILOR A/B', NULL, NULL),
(1025, 'MARINE', 'SAILOR O/S', NULL, NULL),
(1026, 'MARINE', 'SAFETY ADMINISTRATOR', NULL, NULL),
(1027, 'MARINE', 'STAFF CAPTAIN CELEBRITY', NULL, NULL),
(1028, 'MARINE', 'TRAVELING GUARANTEE ENGINEER', NULL, NULL),
(1029, 'MARINE', '1ST ENGINEER', NULL, NULL),
(1030, 'MARINE', '2ND ENGINEER CHIEF', NULL, NULL),
(1031, 'MARINE', '2ND ENGINEER CELEBRITY', NULL, NULL),
(1032, 'MARINE', '3RD ENGINEER 2ND TICKET', NULL, NULL),
(1033, 'MARINE', '3RD ENGINEER CELEBRITY', NULL, NULL),
(1034, 'MARINE', 'A/C ENGINEER', NULL, NULL),
(1035, 'MARINE', 'APPRENTICE ENGINEER/ENGINE CAD', NULL, NULL),
(1036, 'MARINE', 'ACCOMMODATIONS MAINTENANCE MGR', NULL, NULL),
(1037, 'MARINE', 'AMOS CONTROLLER', NULL, NULL),
(1038, 'MARINE', 'ACCOMODATIONS MAINTENANCE SUP.', NULL, NULL),
(1039, 'MARINE', 'TRVL. TECHNICAL SUPPORT OFFICER', NULL, NULL),
(1040, 'MARINE', 'CHIEF ENGINEER CELEBRITY', NULL, NULL),
(1041, 'MARINE', 'CEL UK ENGINE CADET', NULL, NULL),
(1042, 'MARINE', 'TECHNICAL ASSISTANT', NULL, NULL),
(1043, 'MARINE', 'FITTER A/C', NULL, NULL),
(1044, 'MARINE', 'FITTER DECK', NULL, NULL),
(1045, 'MARINE', 'FITTER ENGINE FOREMAN', NULL, NULL),
(1046, 'MARINE', 'FITTER ENGINE', NULL, NULL),
(1047, 'MARINE', 'FITTER REPAIRMAN', NULL, NULL),
(1048, 'MARINE', 'FITTER TURNER', NULL, NULL),
(1049, 'MARINE', 'JOINER', NULL, NULL),
(1050, 'MARINE', 'MOTORMAN 1 CELEBRITY', NULL, NULL),
(1051, 'MARINE', 'MOTORMAN', NULL, NULL),
(1052, 'MARINE', 'OILER CELEBRITY', NULL, NULL),
(1053, 'MARINE', 'PLUMBER CELEBRITY', NULL, NULL),
(1054, 'MARINE', 'REFRIGERATION ENGINEER', NULL, NULL),
(1055, 'MARINE', 'SANITATION ENGINEER', NULL, NULL),
(1056, 'MARINE', 'STAFF ENGINEER', NULL, NULL),
(1057, 'MARINE', 'UPHOLSTERER CELEBRITY', NULL, NULL),
(1058, 'MARINE', 'VARNISHER', NULL, NULL),
(1059, 'MARINE', 'WIPERS', NULL, NULL),
(1060, 'MARINE', '1ST ELECTRICAL ENGINEER', NULL, NULL),
(1061, 'MARINE', '2ND ELECTRICAL ENGINEER', NULL, NULL),
(1062, 'MARINE', '3RD ELECTRICAL ENGINEER', NULL, NULL),
(1063, 'MARINE', 'ASSISTANT ELECTRICIAN 2', NULL, NULL),
(1064, 'MARINE', 'ASSISTANT ELECTRICIAN 3', NULL, NULL),
(1065, 'MARINE', 'CHIEF ELECTRICAL ENGINEER', NULL, NULL),
(1066, 'MARINE', 'CHIEF SECURITY OFC CELEBRITY', NULL, NULL),
(1067, 'MARINE', 'CEL FLEET SECURITY OFFICER', NULL, NULL),
(1068, 'MARINE', 'SECURITY OFFICER CELEBRITY', NULL, NULL),
(1069, 'MARINE', 'SECURITY GUARD CELEBRITY', NULL, NULL),
(1070, 'MARINE', 'SECURITY GUARD SUPERVISOR', NULL, NULL),
(1071, 'MARINE', 'PL 1ST OFFICER', NULL, NULL),
(1072, 'MARINE', 'PL 2ND OFFICER', NULL, NULL),
(1073, 'MARINE', 'PL AB SAILOR', NULL, NULL),
(1074, 'MARINE', 'PL ASST SANITATION ENGINEER', NULL, NULL),
(1075, 'MARINE', 'PL BOSUN', NULL, NULL),
(1076, 'MARINE', 'PL CHIEF OFICER SAFETY', NULL, NULL),
(1077, 'MARINE', 'PL CHIEF NURSE', NULL, NULL),
(1078, 'MARINE', 'PL CHIEF SECURITY OFFICER', NULL, NULL),
(1079, 'MARINE', 'PL DECK APPRENTICE OFFICER', NULL, NULL),
(1080, 'MARINE', 'PL DEPUTY CHIEF SECURITY OFFIC', NULL, NULL),
(1081, 'MARINE', 'PL DOCTOR', NULL, NULL),
(1082, 'MARINE', 'PL ENVIRON. & PUBLIC HEALTH OF', NULL, NULL),
(1083, 'MARINE', 'PL GUEST SECURITY SUPERVISOR', NULL, NULL),
(1084, 'MARINE', 'L&D CONSULTANT', NULL, NULL),
(1085, 'MARINE', 'PL MARINE ADMIN ASSISTANT', NULL, NULL),
(1086, 'MARINE', 'PL MASTER', NULL, NULL),
(1087, 'MARINE', 'PL SECURITY GUARD', NULL, NULL),
(1088, 'MARINE', 'PL NURSE', NULL, NULL),
(1089, 'MARINE', 'PL ORDINARY SEAMAN', NULL, NULL),
(1090, 'MARINE', 'PL QUARTERMASTER', NULL, NULL),
(1091, 'MARINE', 'PL SANTITATION ENGINEER ', NULL, NULL),
(1092, 'MARINE', 'PL SENIOR DOCTOR', NULL, NULL),
(1093, 'MARINE', 'PL SECURITY SUPERVISOR', NULL, NULL),
(1094, 'MARINE', 'PL STAFF CAPTAIN', NULL, NULL),
(1095, 'MARINE', 'PL SANITATION UTILITY', NULL, NULL),
(1096, 'MARINE', 'PL UPHOLSTERER', NULL, NULL),
(1097, 'MARINE', 'PL 1ST ELECTRICAL ENGINEER', NULL, NULL),
(1098, 'MARINE', 'PL 1ST ENGINEER', NULL, NULL),
(1099, 'MARINE', 'PL 2ND  ELECTRICAL ENGINEER', NULL, NULL),
(1100, 'MARINE', 'PL 2ND ENGINEER', NULL, NULL),
(1101, 'MARINE', 'PL 3RD ELECTRICAL ENGINEER', NULL, NULL),
(1102, 'MARINE', 'PL 3RD ENGINEER', NULL, NULL),
(1103, 'MARINE', 'PL CHIEF REF ENGINEER', NULL, NULL),
(1104, 'MARINE', 'PL A/C MECHANIC', NULL, NULL),
(1105, 'MARINE', 'PL ASSISTANT ELECTRICIAN 1', NULL, NULL),
(1106, 'MARINE', 'PL ASSISTANT ELECTRICIAN 2', NULL, NULL),
(1107, 'MARINE', 'PL ASSISTANT ELECTRICIAN 3', NULL, NULL),
(1108, 'MARINE', 'PL ASST ELECTRONIC OFFICER', NULL, NULL),
(1109, 'MARINE', 'PL ACCOMODATION MAINT SUPV.', NULL, NULL),
(1110, 'MARINE', 'PL APPRENTICE ENGINEER', NULL, NULL),
(1111, 'MARINE', 'PL CHIEF ELECTRICAL ENGINEER', NULL, NULL),
(1112, 'MARINE', 'PL CHIEF ENGINEER', NULL, NULL),
(1113, 'MARINE', 'PL DECK MECHANIC', NULL, NULL),
(1114, 'MARINE', 'PL ELECTRONIC ENGINEER', NULL, NULL),
(1115, 'MARINE', 'PL ENGINE FOREMAN', NULL, NULL),
(1116, 'MARINE', 'PL ENGINE MECHANIC', NULL, NULL),
(1117, 'MARINE', 'PL ENGINE STOREKEEPER', NULL, NULL),
(1118, 'MARINE', 'PL ELECTRONIC OFFICER', NULL, NULL),
(1119, 'MARINE', 'PL ENGINE PLUMER', NULL, NULL),
(1120, 'MARINE', 'PL FITTER REPAIRMAN', NULL, NULL),
(1121, 'MARINE', 'PL HOTEL MAINTENANCE MANAGER', NULL, NULL),
(1122, 'MARINE', 'PL INCINERATOR MAN', NULL, NULL),
(1123, 'MARINE', 'PL JOINER', NULL, NULL),
(1124, 'MARINE', 'PL OILER', NULL, NULL),
(1125, 'MARINE', 'PL MACHINIST', NULL, NULL),
(1126, 'MARINE', 'PL MOTORMAN', NULL, NULL),
(1127, 'MARINE', 'PL PLUMBER', NULL, NULL),
(1128, 'MARINE', 'PL STAFF CHIEF ENGINEER', NULL, NULL),
(1129, 'MARINE', 'PL TECHNICAL/AMOS CONTROLLER', NULL, NULL),
(1130, 'MARINE', 'PL VARNISHER', NULL, NULL),
(1131, 'MARINE', 'PL WIPER', NULL, NULL),
(1132, 'MARINE', '2ND BOSUN', NULL, NULL),
(1133, 'MARINE', 'ABLE SEAMAN', NULL, NULL),
(1134, 'MARINE', 'BOSUN', NULL, NULL),
(1135, 'MARINE', 'DECK REPAIRMAN', NULL, NULL),
(1136, 'MARINE', 'DECK REPAIRMAN 1ST ASST.', NULL, NULL),
(1137, 'MARINE', 'DECK REPAIRMAN 2ND ASST.', NULL, NULL),
(1138, 'MARINE', 'DECK REPAIRMAN 3rd ASST.', NULL, NULL),
(1139, 'MARINE', 'JUNIOR SEAMAN', NULL, NULL),
(1140, 'MARINE', 'LIFEGUARD SUPERVISOR', NULL, NULL),
(1141, 'MARINE', 'LIFEGUARD STAFF', NULL, NULL),
(1142, 'MARINE', 'ORDINARY SEAMAN', NULL, NULL),
(1143, 'MARINE', 'QUARTERMASTER', NULL, NULL),
(1144, 'MARINE', 'SITE SUPERVISOR', NULL, NULL),
(1145, 'MARINE', 'TENDER OPERATOR', NULL, NULL),
(1146, 'MARINE', 'DIESEL MECHANIC', NULL, NULL),
(1147, 'MARINE', 'JR. ENGINE MAN', NULL, NULL),
(1148, 'MARINE', 'ENGINE FOREMAN', NULL, NULL),
(1149, 'MARINE', 'INCINERATOR OPERATOR', NULL, NULL),
(1150, 'MARINE', 'INCINERATOR OPERATOR 1', NULL, NULL),
(1151, 'MARINE', '1ST INCINERATOR TECH', NULL, NULL),
(1152, 'MARINE', 'INCINERATOR TECHNICIAN', NULL, NULL),
(1153, 'MARINE', 'OILER', NULL, NULL),
(1154, 'MARINE', '2ND ASST.REFRIGERATOR ENGINEER', NULL, NULL),
(1155, 'MARINE', '3RD ASST.REFRIGERATOR ENGINEER', NULL, NULL),
(1156, 'MARINE', 'ASST. REFRIG. ENGINEER', NULL, NULL),
(1157, 'MARINE', '1ST ASST. REPAIRMAN', NULL, NULL),
(1158, 'MARINE', '2ND ASST. REPAIRMAN', NULL, NULL),
(1159, 'MARINE', 'TECHNICAL STOREKEEPER', NULL, NULL),
(1160, 'MARINE', 'TECHNICAL STOREKEEPER 1', NULL, NULL),
(1161, 'MARINE', '1ST ASST. ELECTRICIAN', NULL, NULL),
(1162, 'MARINE', '2ND ASST. ELECTRICIAN', NULL, NULL),
(1163, 'MARINE', '3RD ASST. ELECTRICIAN', NULL, NULL),
(1164, 'MARINE', 'ASST. ELECTRICIAN', NULL, NULL),
(1165, 'MARINE', 'DEPUTY SECURITY OFFICER', NULL, NULL),
(1166, 'MARINE', 'FLEET SECURITY AUDITOR', NULL, NULL),
(1167, 'MARINE', 'GUEST SECURITY SUPERVISOR', NULL, NULL),
(1168, 'MARINE', 'SECURITY GUARD', NULL, NULL),
(1169, 'MARINE', 'SECURITY SUPERVISOR', NULL, NULL),
(1170, 'MARINE', 'CHIEF SECURITY OFFICER', NULL, NULL),
(1171, 'MARINE', 'FLEET SECURITY OFFICER', NULL, NULL),
(1172, 'MARINE', 'DECK APPRENTICE OFFICER', NULL, NULL),
(1173, 'MARINE', 'ENVIRONMENTAL OFFICER', NULL, NULL),
(1174, 'MARINE', 'INJURY PREVENTION OFFICER', NULL, NULL),
(1175, 'MARINE', 'LIFEGUARD MANAGER-INSTRUCTOR', NULL, NULL),
(1176, 'MARINE', 'MARINE ADMIN. ASSISTANT', NULL, NULL),
(1177, 'MARINE', 'CAPTAIN', NULL, NULL),
(1178, 'MARINE', '1ST OFFICER', NULL, NULL),
(1179, 'MARINE', '2ND OFFICER', NULL, NULL),
(1180, 'MARINE', 'CHIEF OFFICER', NULL, NULL),
(1181, 'MARINE', 'CHIEF OFFICER SAFETY', NULL, NULL),
(1182, 'MARINE', 'STAFF CAPTAIN', NULL, NULL),
(1183, 'MARINE', 'UK CADET', NULL, NULL),
(1184, 'MARINE', '3RD ENGINEER SR. 2ND TICKET', NULL, NULL),
(1185, 'MARINE', '2ND ENGINEER', NULL, NULL),
(1186, 'MARINE', '2ND ENGINEER CHIEF TICKET', NULL, NULL),
(1187, 'MARINE', '2ND ENGINEER SR', NULL, NULL),
(1188, 'MARINE', '2ND ENGINEER SR CHIEF TICKET', NULL, NULL),
(1189, 'MARINE', '3RD ENGINEER', NULL, NULL),
(1190, 'MARINE', '3RD ENGINEER SR', NULL, NULL),
(1191, 'MARINE', 'CHIEF ENGINEER', NULL, NULL),
(1192, 'MARINE', 'ENGINE CADET', NULL, NULL),
(1193, 'MARINE', 'STAFF CHIEF ENGINEER', NULL, NULL),
(1194, 'MARINE', 'GUARANTEE ADMIN OFFICER', NULL, NULL),
(1195, 'MARINE', 'GUARANTEE ENGINEER', NULL, NULL),
(1196, 'MARINE', '1ST REFRIGERATION ENGINEER', NULL, NULL),
(1197, 'MARINE', 'CHIEF REFRIGERATION ENGINEER', NULL, NULL),
(1198, 'MARINE', 'REF ENG EXPANSION', NULL, NULL),
(1199, 'MARINE', 'SENIOR GUARANTEE ENGINE', NULL, NULL),
(1200, 'MARINE', 'UK CADET ENGINE', NULL, NULL),
(1201, 'MARINE', 'ELECTRONICS ENGINEER', NULL, NULL),
(1202, 'MARINE', '4TH ELECTRICAL ENGINEER', NULL, NULL),
(1203, 'MARINE', 'ELECTRICIAN CADET', NULL, NULL),
(1204, 'MARINE', 'UK CADET ELECTRICAL', NULL, NULL),
(1205, 'MARINE', 'MEDICAL SECRETARY', NULL, NULL),
(1206, 'MARINE', 'CHIEF NURSE', NULL, NULL),
(1207, 'MARINE', 'NURSE', NULL, NULL),
(1208, 'MARINE', 'PHYSIOTHERAPIST', NULL, NULL),
(1209, 'MARINE', 'ARTISTIC PAINTER', NULL, NULL),
(1210, 'MARINE', 'REPAIRMAN AQUA', NULL, NULL),
(1211, 'MARINE', '1ST ASST REPAIRMAN AQUA', NULL, NULL),
(1212, 'MARINE', 'ASST. REPAIRMAN - AQUA', NULL, NULL),
(1213, 'MARINE', '1ST ASST CARPENTER', NULL, NULL),
(1214, 'MARINE', '2ND ASST CARPENTER', NULL, NULL),
(1215, 'MARINE', '3RD ASST CARPENTER', NULL, NULL),
(1216, 'MARINE', 'ASSISTANT CARPENTER', NULL, NULL),
(1217, 'MARINE', 'CARPENTER SUPERVISOR', NULL, NULL),
(1218, 'MARINE', 'DISPATCHER', NULL, NULL),
(1219, 'MARINE', 'LAUNDRY TECHNICIAN', NULL, NULL),
(1220, 'MARINE', 'ADMINISTRATIVE ASSISTANT', NULL, NULL),
(1221, 'MARINE', '1ST ASST REPAIRMAN', NULL, NULL),
(1222, 'MARINE', '2ND ASST REPAIRMAN', NULL, NULL),
(1223, 'MARINE', '3RD ASST REPAIRMAN', NULL, NULL),
(1224, 'MARINE', 'REPAIR ASSISTANT', NULL, NULL),
(1225, 'MARINE', 'REPAIRMAN SUPERVISOR-AQUA', NULL, NULL),
(1226, 'MARINE', 'REPAIR SUPERVISOR', NULL, NULL),
(1227, 'MARINE', '2ND ASST UPHOLSTERER', NULL, NULL),
(1228, 'MARINE', '3RD ASST UPHOLSTERER', NULL, NULL),
(1229, 'MARINE', 'UPHOLSTERER', NULL, NULL),
(1230, 'MARINE', 'HOTEL MAINT. MGR-1ST ENG EQUIV', NULL, NULL),
(1231, 'MARINE', 'SR HOTEL MAINTENANCE MANAGER', NULL, NULL),
(1232, 'MARINE', 'HORTICULTURIST', NULL, NULL),
(1233, 'MARINE', 'LANDSCAPE SPECIALIST', NULL, NULL),
(1234, 'MARINE', 'DECK CADET', NULL, NULL),
(1235, 'MARINE', 'OBSERVER CADET', NULL, NULL),
(1236, 'MARINE', 'MOTORMAN 1', NULL, NULL),
(1237, 'MARINE', 'PLUMBER', NULL, NULL),
(1238, 'MARINE', 'ACCOMMODATIONS MAINTENANCE MG', NULL, NULL),
(1239, 'MARINE', 'TECHNICAL SUPPORT OFFICER', NULL, NULL),
(1240, 'MARINE', 'TUI CHIEF ELECTRICIAN', NULL, NULL),
(1241, 'MARINE', 'SECURITY OFFICER', NULL, NULL),
(1242, 'MARINE', 'PD ASST LIFEGUARD OPS MGR', NULL, NULL),
(1243, 'MARINE', 'PD CUSTOMS OPS SPECIALIST', NULL, NULL),
(1244, 'MARINE', 'PD CARPENTER', NULL, NULL),
(1245, 'MARINE', 'PD DISPATCHER', NULL, NULL),
(1246, 'MARINE', 'PD FACILITIES MAINT LEAD', NULL, NULL),
(1247, 'MARINE', 'PD LEAD LIFE GUARD TR', NULL, NULL),
(1248, 'MARINE', 'PD LEAD LIFEGUARD', NULL, NULL),
(1249, 'MARINE', 'PD LIFEGUARD OPS MGR', NULL, NULL),
(1250, 'MARINE', 'PD PORT CAPT SAFETY', NULL, NULL),
(1251, 'MARINE', 'PD ENGINEERING DIRECTOR', NULL, NULL),
(1252, 'MARINE', 'PD PLUMBER', NULL, NULL),
(1253, 'MARINE', 'PD POOL & PARK MAINT TECH', NULL, NULL),
(1254, 'MARINE', 'PD ENV OFFICER', NULL, NULL),
(1255, 'MARINE', 'PD SR LIFEGUARD', NULL, NULL),
(1256, 'MARINE', 'PD UPHOLSTERER', NULL, NULL),
(1257, 'MARINE', 'PD AUTO AQUATICS MECHANIC', NULL, NULL),
(1258, 'MARINE', 'PD A/C TECHNICIAN', NULL, NULL),
(1259, 'MARINE', 'PD AMOS CONTROLLER', NULL, NULL),
(1260, 'MARINE', 'PD ASST ENGINEERING MGR', NULL, NULL),
(1261, 'MARINE', 'PD ELECTRICIAN', NULL, NULL),
(1262, 'MARINE', 'PD ELECTRONIC ENG', NULL, NULL),
(1263, 'MARINE', 'PD FIBER GLASS REPAIRMAN', NULL, NULL),
(1264, 'MARINE', 'PD HEAVY EQUIPMENT OPERATOR', NULL, NULL),
(1265, 'MARINE', 'PD MECHANIC', NULL, NULL),
(1266, 'MARINE', 'PD INCINERATOR OPS', NULL, NULL),
(1267, 'MARINE', 'PD ISLAND TECH  MGR', NULL, NULL),
(1268, 'MARINE', 'PD REFRIGERATION TECH', NULL, NULL),
(1269, 'MARINE', 'PD TECH STOREKEEPER', NULL, NULL),
(1270, 'MARINE', 'PD TECHNICAL SUPERVISOR', NULL, NULL),
(1271, 'MARINE', 'PD SR. LINESMAN', NULL, NULL),
(1272, 'MARINE', 'PD GUEST SECURITY SUP', NULL, NULL),
(1273, 'MARINE', 'PD CHIEF SECURITY OFF', NULL, NULL),
(1274, 'MARINE', 'PD LIFEGUARDS', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `laravel_logger_activity`
--

CREATE TABLE `laravel_logger_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `route` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ipAddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userAgent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `methodType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `laravel_logger_activity`
--

INSERT INTO `laravel_logger_activity` (`id`, `description`, `details`, `userType`, `userId`, `route`, `ipAddress`, `userAgent`, `locale`, `referer`, `methodType`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/home', 'POST', '2021-05-20 07:36:13', '2021-05-20 07:36:13', NULL),
(2, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'POST', '2021-05-20 07:37:06', '2021-05-20 07:37:06', NULL),
(3, 'Service', '', 'Registered', 39, 'http://localhost/crewhires/service/bunker', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 07:45:29', '2021-05-20 07:45:29', NULL),
(4, 'Service', 'certificaterevalidation', 'Registered', 39, 'http://localhost/crewhires/service/certificaterevalidation', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 07:47:39', '2021-05-20 07:47:39', NULL),
(5, 'Logged In', NULL, 'Registered', 38, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'POST', '2021-05-20 11:16:14', '2021-05-20 11:16:14', NULL),
(6, 'Service', '[SVC162150937415] Request Certificate Revalidation', 'Registered', 38, 'http://localhost/crewhires/service/certificaterevalidation', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'GET', '2021-05-20 11:16:14', '2021-05-20 11:16:14', NULL),
(7, 'Service', '[SVC162150937995] Request Training Center', 'Registered', 38, 'http://localhost/crewhires/service/trainingcenter', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:19', '2021-05-20 11:16:19', NULL),
(8, 'Service', '[SVC162150938266] Request Seaman\'s Book', 'Registered', 38, 'http://localhost/crewhires/service/seamanbook', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:22', '2021-05-20 11:16:22', NULL),
(9, 'Service', '[SVC162150938585] Request Visa', 'Registered', 38, 'http://localhost/crewhires/service/visa', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 OPR/76.0.4017.123', 'en-US,en;q=0.9', 'http://localhost/crewhires/service', 'GET', '2021-05-20 11:16:25', '2021-05-20 11:16:25', NULL),
(10, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7', 'http://localhost/crewhires/login', 'POST', '2021-05-21 02:14:23', '2021-05-21 02:14:23', NULL),
(11, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7', 'http://localhost/crewhires/login', 'POST', '2021-05-21 13:13:28', '2021-05-21 13:13:28', NULL),
(12, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-06-29 05:43:39', '2021-06-29 05:43:39', NULL),
(13, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-06-29 08:47:20', '2021-06-29 08:47:20', NULL),
(14, 'Logged Out', NULL, 'Registered', 1, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/backend/agency', 'POST', '2021-06-29 09:18:46', '2021-06-29 09:18:46', NULL),
(15, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-07 09:28:01', '2021-07-07 09:28:01', NULL),
(16, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-13 03:01:57', '2021-07-13 03:01:57', NULL),
(17, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-13 03:02:14', '2021-07-13 03:02:14', NULL),
(18, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-13 03:02:19', '2021-07-13 03:02:19', NULL),
(19, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-13 03:03:38', '2021-07-13 03:03:38', NULL),
(20, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-13 03:09:22', '2021-07-13 03:09:22', NULL),
(21, 'Service', '[SVC162614576223] Request Visa', 'Registered', 1, 'http://localhost/crewhires/service/visa', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'GET', '2021-07-13 03:09:22', '2021-07-13 03:09:22', NULL),
(22, 'Logged Out', NULL, 'Registered', 1, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/backend/service', 'POST', '2021-07-13 05:25:16', '2021-07-13 05:25:16', NULL),
(23, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 08:53:05', '2021-07-15 08:53:05', NULL),
(24, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 08:53:24', '2021-07-15 08:53:24', NULL),
(25, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 08:53:44', '2021-07-15 08:53:44', NULL),
(26, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 10:19:48', '2021-07-15 10:19:48', NULL),
(27, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:20:07', '2021-07-15 10:20:07', NULL),
(28, 'Logged In', NULL, 'Registered', 47, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:20:19', '2021-07-15 10:20:19', NULL),
(29, 'Logged Out', NULL, 'Registered', 47, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 10:29:36', '2021-07-15 10:29:36', NULL),
(30, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:29:46', '2021-07-15 10:29:46', NULL),
(31, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 10:30:42', '2021-07-15 10:30:42', NULL),
(32, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:30:48', '2021-07-15 10:30:48', NULL),
(33, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:30:53', '2021-07-15 10:30:53', NULL),
(34, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:31:03', '2021-07-15 10:31:03', NULL),
(35, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 10:44:10', '2021-07-15 10:44:10', NULL),
(36, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:44:16', '2021-07-15 10:44:16', NULL),
(37, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 10:52:13', '2021-07-15 10:52:13', NULL),
(38, 'Logged In', NULL, 'Registered', 47, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 10:52:24', '2021-07-15 10:52:24', NULL),
(39, 'Logged Out', NULL, 'Registered', 47, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 11:00:37', '2021-07-15 11:00:37', NULL),
(40, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 11:00:44', '2021-07-15 11:00:44', NULL),
(41, 'Logged Out', NULL, 'Registered', 1, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/backend/agency', 'POST', '2021-07-15 11:06:42', '2021-07-15 11:06:42', NULL),
(42, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 11:06:48', '2021-07-15 11:06:48', NULL),
(43, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 11:13:29', '2021-07-15 11:13:29', NULL),
(44, 'Logged In', NULL, 'Registered', 47, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 11:13:37', '2021-07-15 11:13:37', NULL),
(45, 'Logged Out', NULL, 'Registered', 47, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 11:13:44', '2021-07-15 11:13:44', NULL),
(46, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 11:13:57', '2021-07-15 11:13:57', NULL),
(47, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/home', 'POST', '2021-07-15 12:03:50', '2021-07-15 12:03:50', NULL),
(48, 'Logged In', NULL, 'Registered', 1, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 12:03:57', '2021-07-15 12:03:57', NULL),
(49, 'Logged Out', NULL, 'Registered', 1, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/backend/crew-vacancy', 'POST', '2021-07-15 12:04:33', '2021-07-15 12:04:33', NULL),
(50, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 12:04:42', '2021-07-15 12:04:42', NULL),
(51, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-15 12:05:10', '2021-07-15 12:05:10', NULL),
(52, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-15 12:05:32', '2021-07-15 12:05:32', NULL),
(53, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'en-US,en;q=0.9', 'http://localhost/crewhires/login', 'POST', '2021-07-15 13:03:10', '2021-07-15 13:03:10', NULL),
(54, 'Logged In', NULL, 'Registered', 47, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 03:26:24', '2021-07-16 03:26:24', NULL),
(55, 'Logged Out', NULL, 'Registered', 47, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/email/verify', 'POST', '2021-07-16 03:31:33', '2021-07-16 03:31:33', NULL),
(56, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 03:31:45', '2021-07-16 03:31:45', NULL),
(57, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-16 04:03:43', '2021-07-16 04:03:43', NULL),
(58, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 04:03:49', '2021-07-16 04:03:49', NULL),
(59, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/crew-cv/37', 'POST', '2021-07-16 04:04:06', '2021-07-16 04:04:06', NULL),
(60, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 04:04:17', '2021-07-16 04:04:17', NULL),
(61, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 04:04:20', '2021-07-16 04:04:20', NULL),
(62, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 04:04:26', '2021-07-16 04:04:26', NULL),
(63, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/home', 'POST', '2021-07-16 05:12:30', '2021-07-16 05:12:30', NULL),
(64, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 05:12:59', '2021-07-16 05:12:59', NULL),
(65, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-16 06:10:40', '2021-07-16 06:10:40', NULL),
(66, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 06:10:55', '2021-07-16 06:10:55', NULL),
(67, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/categories', 'POST', '2021-07-16 06:44:21', '2021-07-16 06:44:21', NULL),
(68, 'Logged In', NULL, 'Registered', 39, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 06:44:30', '2021-07-16 06:44:30', NULL),
(69, 'Logged Out', NULL, 'Registered', 39, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/membership', 'POST', '2021-07-16 07:05:22', '2021-07-16 07:05:22', NULL),
(70, 'Logged In', NULL, 'Registered', 37, 'http://localhost/crewhires/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/login', 'POST', '2021-07-16 07:05:29', '2021-07-16 07:05:29', NULL),
(71, 'Logged Out', NULL, 'Registered', 37, 'http://localhost/crewhires/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', 'en-US,en;q=0.5', 'http://localhost/crewhires/crew/profile', 'POST', '2021-07-16 07:08:02', '2021-07-16 07:08:02', NULL),
(72, 'Logged In', NULL, 'Registered', 37, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:10:33', '2021-07-16 07:10:33', NULL),
(73, 'Logged Out', NULL, 'Registered', 37, 'http://192.168.100.100/crewhires/logout', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/service', 'POST', '2021-07-16 07:11:51', '2021-07-16 07:11:51', NULL),
(74, 'Logged In', NULL, 'Registered', 38, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:12:10', '2021-07-16 07:12:10', NULL),
(75, 'Logged Out', NULL, 'Registered', 38, 'http://192.168.100.100/crewhires/logout', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/membership', 'POST', '2021-07-16 07:12:56', '2021-07-16 07:12:56', NULL),
(76, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:13:16', '2021-07-16 07:13:16', NULL),
(77, 'Logged In', NULL, 'Registered', 41, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:13:25', '2021-07-16 07:13:25', NULL),
(78, 'Logged Out', NULL, 'Registered', 41, 'http://192.168.100.100/crewhires/logout', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/crew-cv/37', 'POST', '2021-07-16 07:13:44', '2021-07-16 07:13:44', NULL),
(79, 'Failed Login Attempt', NULL, 'Guest', NULL, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:13:59', '2021-07-16 07:13:59', NULL),
(80, 'Logged In', NULL, 'Registered', 39, 'http://192.168.100.100/crewhires/login', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/login', 'POST', '2021-07-16 07:14:28', '2021-07-16 07:14:28', NULL),
(81, 'Logged Out', NULL, 'Registered', 39, 'http://192.168.100.100/crewhires/logout', '192.168.100.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.227', 'en-US,en;q=0.9', 'http://192.168.100.100/crewhires/crew-cv/37', 'POST', '2021-07-16 07:15:23', '2021-07-16 07:15:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code_member` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `status` enum('verification','done','cancel','pay') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'verification',
  `date_add` date DEFAULT NULL,
  `photo_receipt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `bank_account` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bau_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_01_15_105324_create_roles_table', 2),
(5, '2016_01_15_114412_create_role_user_table', 2),
(6, '2016_01_26_115212_create_permissions_table', 2),
(7, '2016_01_26_115523_create_permission_role_table', 2),
(8, '2016_02_09_132439_create_permission_user_table', 2),
(9, '2017_11_04_103444_create_laravel_logger_activity_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Can View Users', 'view.users', 'Can view users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(2, 'Can Create Users', 'create.users', 'Can create new users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(3, 'Can Edit Users', 'edit.users', 'Can edit users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(4, 'Can Delete Users', 'delete.users', 'Can delete users', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(5, 'Can View Crew', 'view.crew', 'Can view crew', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL),
(6, 'Can View Agency', 'view.agency', 'Can view agency', 'Permission', '2021-03-09 03:58:15', '2021-03-09 03:58:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(2, 2, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(3, 3, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(4, 4, 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(5, 5, 2, '2021-03-09 15:13:13', '2021-03-09 15:13:13', NULL),
(6, 6, 4, '2021-03-10 01:19:09', '2021-03-10 01:19:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE `religion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Islam', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(2, 'Christianity', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(3, 'Buddhism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(4, 'Hinduism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(5, 'Konghucu', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(6, 'Chinese Traditional Religion', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(7, 'Ethnic Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(8, 'African Traditional Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(9, 'Sikhism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(10, 'Spiritism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(11, 'Bahaʼi', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(12, 'Jainism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(13, 'Shinto', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(14, 'Cao Dai', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(15, 'Zoroastrianism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(16, 'Tenrikyo', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(17, 'Animism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(18, 'Druze', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(19, 'Neo-Paganism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(20, 'Unitarian Universalism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(21, 'Rastafari', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(22, 'Judaism', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(23, 'Agnostic', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(24, 'Unaffiliated Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(25, 'Folk Religions', '2021-03-31 06:57:39', '2021-03-31 06:57:39'),
(26, 'Atheist', '2021-03-31 06:57:39', '2021-03-31 06:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', 'Admin Role', 5, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(2, 'Crew', 'crew', 'Crew Role', 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(3, 'Guest', 'guest', 'Guest Role', 0, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL),
(4, 'Agency', 'agency', 'Agency Role', 1, '2021-03-09 03:58:16', '2021-03-09 03:58:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-03-09 11:23:19', '2021-03-09 11:23:19', NULL),
(26, 2, 37, '2021-04-29 03:57:46', '2021-04-29 03:57:46', NULL),
(27, 2, 38, '2021-04-29 04:02:55', '2021-04-29 04:02:55', NULL),
(28, 4, 39, '2021-04-29 04:06:40', '2021-04-29 04:06:40', NULL),
(30, 4, 41, '2021-04-29 04:12:10', '2021-04-29 04:12:10', NULL),
(31, 4, 42, '2021-04-30 14:30:40', '2021-04-30 14:30:40', NULL),
(32, 2, 43, '2021-05-01 13:41:08', '2021-05-01 13:41:08', NULL),
(33, 4, 44, '2021-05-01 14:09:14', '2021-05-01 14:09:14', NULL),
(34, 4, 45, '2021-05-01 14:51:40', '2021-05-01 14:51:40', NULL),
(35, 4, 46, '2021-05-01 15:11:51', '2021-05-01 15:11:51', NULL),
(36, 2, 47, '2021-07-13 03:04:22', '2021-07-13 03:04:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code_service` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_id` int(11) NOT NULL,
  `types` enum('visa','passport','seamanbook','certificaterevalidation','trainingcenter','accommodation','payroll','bunker') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('verification','process','done','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'verification',
  `date_add` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `code_service`, `users_id`, `types`, `status`, `date_add`, `created_at`, `updated_at`) VALUES
(3, 'SVC162149167679', 39, 'visa', 'verification', '2021-05-20', '2021-05-20 06:21:16', '2021-05-20 06:21:16'),
(4, 'SVC162149168596', 39, 'passport', 'verification', '2021-05-20', '2021-05-20 06:21:25', '2021-05-20 06:21:25'),
(5, 'SVC162149627049', 39, 'seamanbook', 'verification', '2021-05-20', '2021-05-20 07:37:50', '2021-05-20 07:37:50'),
(6, 'SVC162149672975', 39, 'bunker', 'verification', '2021-05-20', '2021-05-20 07:45:29', '2021-05-20 07:45:29'),
(7, 'SVC162149685946', 39, 'certificaterevalidation', 'verification', '2021-05-20', '2021-05-20 07:47:39', '2021-05-20 07:47:39'),
(8, 'SVC162150937415', 38, 'certificaterevalidation', 'verification', '2021-05-20', '2021-05-20 11:16:14', '2021-05-20 11:16:14'),
(9, 'SVC162150937995', 38, 'trainingcenter', 'verification', '2021-05-20', '2021-05-20 11:16:19', '2021-05-20 11:16:19'),
(10, 'SVC162150938266', 38, 'seamanbook', 'verification', '2021-05-20', '2021-05-20 11:16:22', '2021-05-20 11:16:22'),
(11, 'SVC162150938585', 38, 'visa', 'verification', '2021-05-20', '2021-05-20 11:16:25', '2021-05-20 11:16:25'),
(12, 'SVC162614576223', 1, 'visa', 'verification', '2021-07-13', '2021-07-13 03:09:22', '2021-07-13 03:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `settingan`
--

CREATE TABLE `settingan` (
  `id` int(11) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settingan`
--

INSERT INTO `settingan` (`id`, `desc`, `val`, `created_at`, `updated_at`) VALUES
(1, 'max_apply', '0', NULL, NULL),
(2, 'max_invite', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` int(4) DEFAULT NULL,
  `profile` enum('done','proses','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'closed',
  `photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_account` enum('free','premium') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'free',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_ban` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `phone_code`, `profile`, `photo`, `status_account`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `is_ban`) VALUES
(1, 'administrator', 'admin@mail.com', NULL, NULL, 'closed', '', 'free', '2021-03-18 06:08:45', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', 'ZAIA3JD74JpAoObvNO66cYUoZLE8xbQTKpdgAFKDLVgAOIe9n4MlJAMT9H7R', '2021-02-25 00:39:13', '2021-02-25 00:39:13', '0'),
(37, 'crew1', 'ron1.gusya@gmail.com', '08521111111', 93, 'done', '', 'free', '2021-04-29 03:58:14', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', NULL, '2021-04-29 03:57:46', '2021-05-21 14:00:58', '0'),
(38, 'crew2', 'crew2@mail.com', '0852123522', 213, 'done', '38_1619669104.png', 'premium', '2021-04-29 04:03:15', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', NULL, '2021-04-29 04:02:55', '2021-04-29 04:05:04', '0'),
(39, 'agency1', 'agency1@mail.com', '08521111111', 1684, 'done', '39_1619669373.jpg', 'free', '2021-04-29 04:06:57', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', NULL, '2021-04-29 04:06:40', '2021-04-29 04:09:33', '0'),
(41, 'agency2', 'agency2@mail.com', '085444111111', 257, 'done', '41_1619669746.jpg', 'premium', '2021-04-29 04:12:23', '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', NULL, '2021-04-29 04:12:10', '2021-04-29 04:15:47', '0'),
(42, 'ShipOwner1', 'shipowner1@gmail.com', '085211112111', 213, 'done', '42_1619793202.jpg', 'free', '2021-04-30 14:31:05', '$2y$10$9wI3g8bWQhwYKLfNaB2FguHILdmlYvG/Adhtf37CsqKAbumgGvX5W', NULL, '2021-04-30 14:30:40', '2021-05-01 13:35:06', '0'),
(43, 'crew3', 'crew3@mail.com', '085454452522', 357, 'done', '43_1619877391.jpg', 'free', '2021-05-01 13:41:27', '$2y$10$RjuMhQRgs42.8lH0dOZNhOARA47A97OyoVZ8/2svmEB1bDITkIRQO', NULL, '2021-05-01 13:41:08', '2021-05-01 13:56:31', '0'),
(44, 'Agency3', 'ron.gusya1@gmail.com', '085211111210', 355, 'done', '44_1619880354.jfif', 'free', '2021-05-01 14:09:45', '$2y$10$L3n1cMP03Jppf0W2SdFaH.zmm0.EqQ/Pe7r3QMCM20UBEmL8B8eZW', NULL, '2021-05-01 14:09:14', '2021-05-01 16:05:19', '0'),
(45, 'shipowner2', 'shipowner2@mail.com', '085422212121', 213, 'done', '45_1619880839.jfif', 'free', '2021-05-01 14:52:05', '$2y$10$HOvHMmmXImutkdmf6iJPLewkaACPw27smOydN/YiqgfdCN.TgD63.', NULL, '2021-05-01 14:51:40', '2021-05-01 14:53:59', '0'),
(46, 'shipowner3', 'ron.gusya@gmail.com', '085211121111', 359, 'done', '46_1619882023.jpg', 'free', '2021-05-01 15:12:09', '$2y$10$nrvcyswmmPLx/slonMlMJOapbZaJ/NnQsFZqB1mJ.C8CVPHx7JUYC', NULL, '2021-05-01 15:11:51', '2021-05-21 14:04:28', '1'),
(47, 'tatang suherman', 'tatang.suherman07@gmail.com', '085317749211', NULL, 'proses', NULL, 'free', NULL, '$2y$10$YGdKzPV5ikHqZI5b4gzBPeTPlTlCbRfHeL7iniBLO91ITtJO9zQQS', NULL, '2021-07-13 03:04:22', '2021-07-13 03:04:22', '0');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_statistik_negara`
-- (See below for the actual view)
--
CREATE TABLE `v_statistik_negara` (
`negara` varchar(100)
,`jml` bigint(21)
,`typ` varchar(6)
);

-- --------------------------------------------------------

--
-- Structure for view `v_statistik_negara`
--
DROP TABLE IF EXISTS `v_statistik_negara`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_statistik_negara`  AS  select `agency_info`.`country` AS `negara`,count(`agency_info`.`country`) AS `jml`,'AGENCY' AS `typ` from ((`users` left join `role_user` on(`role_user`.`user_id` = `users`.`id`)) left join `agency_info` on(`agency_info`.`users_id` = `users`.`id`)) where `role_user`.`role_id` = 4 and `agency_info`.`type_agency` = 'AGENCY' group by `agency_info`.`country` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agency_info`
--
ALTER TABLE `agency_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agency_invite`
--
ALTER TABLE `agency_invite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agency_jobs`
--
ALTER TABLE `agency_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coc`
--
ALTER TABLE `coc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cop`
--
ALTER TABLE `cop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crew_apply`
--
ALTER TABLE `crew_apply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crew_doc`
--
ALTER TABLE `crew_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crew_info`
--
ALTER TABLE `crew_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `home_slide`
--
ALTER TABLE `home_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_desc`
--
ALTER TABLE `job_desc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_logger_activity`
--
ALTER TABLE `laravel_logger_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `religion`
--
ALTER TABLE `religion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settingan`
--
ALTER TABLE `settingan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agency_info`
--
ALTER TABLE `agency_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `agency_invite`
--
ALTER TABLE `agency_invite`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `agency_jobs`
--
ALTER TABLE `agency_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coc`
--
ALTER TABLE `coc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cop`
--
ALTER TABLE `cop`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT for table `crew_apply`
--
ALTER TABLE `crew_apply`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `crew_doc`
--
ALTER TABLE `crew_doc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `crew_info`
--
ALTER TABLE `crew_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_slide`
--
ALTER TABLE `home_slide`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_desc`
--
ALTER TABLE `job_desc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1275;

--
-- AUTO_INCREMENT for table `laravel_logger_activity`
--
ALTER TABLE `laravel_logger_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `religion`
--
ALTER TABLE `religion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `settingan`
--
ALTER TABLE `settingan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
