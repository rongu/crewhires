<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use \App\Models\Country;
use \App\Models\AgencyJobs;
use \App\Models\AgencyInfo;
use \App\Models\AgencyInvite;
use \App\Models\CrewInfo;
use Illuminate\Support\Facades\Hash;
use Session,DB;

class AgencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    	return view('agency.index');
    }

    public function profile()
    {
        $usr = User::where('id',auth()->user()->id)->first();
        $job = AgencyJobs::where('users_id',auth()->user()->id)->get();
        $inf = AgencyInfo::where('users_id',auth()->user()->id)->first();

        //$cty = Country::get();

        $cty = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");
        $getBendera = (new static)->getCountry($inf->country);
        $bnd = $getBendera['flag_base64'];
        $calling = file_get_contents("vendor/samayo/country-json/src/country-by-calling-code.json");

        return view('agency.myprofile',compact('usr','job','inf','cty','bnd','calling'));
    }

    private function getCountry($keys){

        $file = file_get_contents("vendor/samayo/country-json/src/country-by-flag.json");
        foreach (json_decode($file, true) as $key => $value) {
            if($value['country']===$keys){
                return $value;
            }
        }

    }

    public function updateAgency(Request $request)
    {
        $form_t = $request->form_t;

        if($form_t==='profile'){
            $companyname = $request->companyname;
            $email = $request->email;
            $phone = $request->phone;
            $phone_code = $request->phone_code;
            $address = $request->address;
            $city = $request->city;
            $country = $request->country;

            $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();

            $cif = AgencyInfo::findOrFail($cif_d->id);
            $cif->companyname = $companyname;
            $cif->address = $address;  
            $cif->city = $city;  
            $cif->country = $country;   
            $cif->update(); 

            $usr = User::findOrFail(auth()->user()->id);
            $usr->email = $email;  
            $usr->phone = $phone;   
            $usr->phone_code = $phone_code;   
            $usr->update(); 

            Session::flash('success', 'Profile was updated');

        }else if($form_t==='account'){
            $name = $request->name;
            $password = $request->password;
            
            $usr = User::findOrFail(auth()->user()->id);
            $usr->name = $name;
            if($password){
                $usr->password = Hash::make($password); 
            }  
               
            $usr->update(); 

            Session::flash('success', 'Account was updated');

        }else if($form_t==='photo'){
            $file = $request->file('file');
            if($file){
                $nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
                
                $tujuan_upload = 'public/images/profile';
                $file->move($tujuan_upload,$nama_file);

                $usr = User::findOrFail(auth()->user()->id);
                $usr->photo = $nama_file;
                $usr->update(); 

            }       


            Session::flash('success', 'Photo was updated');

        }else if($form_t==='portofolio'){

            $file_portofolio = $request->file('file_portofolio');
            if($file_portofolio){
                $nama_file_portofolio = auth()->user()->id.'_'.time() . '.' . $file_portofolio->getClientOriginalExtension();
                
                $tujuan_upload = 'public/images/portofolio';
                $file_portofolio->move($tujuan_upload,$nama_file_portofolio);

                $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
                $cif = AgencyInfo::findOrFail($cif_d->id);
                $cif->portofolio = $nama_file_portofolio;
                $cif->update(); 

            }             

            $file_doc = $request->file('file_doc');
            if($file_doc){
                $nama_file_doc = auth()->user()->id.'_'.time() . '.' . $file_doc->getClientOriginalExtension();
                
                $tujuan_upload = 'public/images/document';
                $file_doc->move($tujuan_upload,$nama_file_doc);

                $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
                $cif = AgencyInfo::findOrFail($cif_d->id);
                $cif->document = $nama_file_doc;
                $cif->update(); 

            }    

            $file_siup = $request->file('file_siup');
            if($file_siup){
                $nama_file_siup = auth()->user()->id.'_'.time() . '.' . $file_siup->getClientOriginalExtension();
                
                $tujuan_upload = 'public/images/siup';
                $file_siup->move($tujuan_upload,$nama_file_siup);

                $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
                $cif = AgencyInfo::findOrFail($cif_d->id);
                $cif->siup = $nama_file_siup;
                $cif->update(); 

            } 

            $file_siuppak = $request->file('file_siuppak');
            if($file_siuppak){
                $nama_file_siuppak = auth()->user()->id.'_'.time() . '.' . $file_siuppak->getClientOriginalExtension();
                
                $tujuan_upload = 'public/images/siuppak';
                $file_siuppak->move($tujuan_upload,$nama_file_siuppak);

                $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
                $cif = AgencyInfo::findOrFail($cif_d->id);
                $cif->siuppak = $nama_file_siuppak;
                $cif->update(); 

            }    


            Session::flash('success', 'Portofolio and Document was updated');
            
        }else if($form_t==='sosmed'){
            $fb = $request->fb;
            $ig = $request->ig;
            $tele = $request->tele;
            $tw = $request->tw;
            $wa = $request->wa;

            $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
            $cif = AgencyInfo::findOrFail($cif_d->id);
            $cif->fb = $fb;
            $cif->ig = $ig;
            $cif->tg = $tele;
            $cif->tw = $tw;
            $cif->wa = $wa;
            
            $cif->update(); 
            Session::flash('success', 'Sosmed was updated');

        }else if($form_t==='info'){

            $type_agency = $request->type_agency;
            $pic = $request->pic;
            $pic_phone = $request->pic_phone;
            $pic_phone_code = $request->pic_phone_code;

            $cif_d = AgencyInfo::select('id')->where('users_id', auth()->user()->id)->first();
            $cif = AgencyInfo::findOrFail($cif_d->id);
            $cif->type_agency = $type_agency;
            $cif->pic = $pic;
            $cif->pic_phone = $pic_phone;
            $cif->pic_phone_code = $pic_phone_code;

            
            $cif->update(); 
            Session::flash('success', 'Agency Status was updated');

        }else if($form_t==='joblist'){

            $jl = $request->jl;

            foreach ($jl as $kee) {

                if(!empty($kee['jl_po'])){
                    $cd = new AgencyJobs();
                    $cd->users_id = auth()->user()->id;
                    $cd->position = $kee['jl_po'];
                    $cd->types = $kee['jl_ty'];
                    $cd->engine = $kee['jl_en'];
                    $cd->requirement = $kee['jl_re'];
                    $cd->status = 'open';
                    $cd->date_open = date('Y-m-d',strtotime($kee['jl_open']));
                    $cd->date_close = date('Y-m-d',strtotime($kee['jl_close']));
                    $cd->save();  
                }


            } 
 

            Session::flash('success', 'Jobs was updated');

        }
        


        return redirect()->back();

    }

    public function updateAgencyByid(Request $request){
        $id = $request->id;
        $jlx_po = $request->jlx_po;
        $jlx_ty = $request->jlx_ty;
        $jlx_en = $request->jlx_en;
        $jlx_re = $request->jlx_re;
        $jlx_open = $request->jlx_open;
        $jlx_close = $request->jlx_close;


        //foreach ($jlx as $kee) {
            
        $cif = AgencyJobs::findOrFail($id);
        $cif->users_id = auth()->user()->id;
        $cif->position = $jlx_po;
        $cif->types = $jlx_ty;
        $cif->engine = $jlx_en;
        $cif->requirement = $jlx_re;
        $cif->status = 'open';
        $cif->date_open = date('Y-m-d',strtotime($jlx_open));
        $cif->date_close = date('Y-m-d',strtotime($jlx_close));   
        $cif->update();

        //} 


        Session::flash('success', 'Jobs was updated');
        return redirect()->back();
    }

    public function deleteJobsAgency($id){

        AgencyJobs::where('id', $id)->delete();
        Session::flash('success', 'Jobs was deleted');

        return redirect()->back();
    }

    public function myJob()
    {

        $jb = AgencyJobs::select('agency_jobs.id as id','position','types','engine','requirement','date_open','date_close',DB::raw('(select count(id) as jml from crew_apply cpa where cpa.agency_jobs_id = agency_jobs.id ) as jmlcount'))->where('agency_jobs.users_id',auth()->user()->id)->get();
        return view('agency.myjob',compact('jb'));

    }

    public function myjobCreate(Request $request){
        $cd = new AgencyJobs();
        $cd->users_id = auth()->user()->id;
        $cd->position = $request->jl_po;
        $cd->types = $request->jl_ty;
        $cd->engine = $request->jl_en;
        $cd->requirement = $request->jl_re;
        $cd->status = 'open';
        $cd->date_open = date('Y-m-d',strtotime($request->jl_open));
        $cd->date_close = date('Y-m-d',strtotime($request->jl_close));
        $cd->save();

        Session::flash('success', 'Jobs was Add');
        return redirect()->back(); 
    }

    public function myJobApply(Request $request)
    {
        //

        $id = $request->id;
       

        $codena = CrewInfo::select('crew_apply.users_id as users_id','photo','name','fullname','type_crew','status_account')->leftjoin('users','users.id','crew_info.users_id')->leftjoin('crew_apply','crew_apply.users_id','crew_info.users_id')->where('crew_apply.agency_jobs_id',$id)->where('crew_apply.apply_status','APPROVAL')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('name', function($codena) {

                return [$codena->users_id,$codena->name];                

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('status_account', function($codena) {

                return $codena->status_account;                

            })->addColumn('photo', function($codena) {

                return $codena->photo;                

            })->addColumn('users_id', function($codena) {

                return [$codena->users_id,$codena->fullname,$codena->type_crew];                

            })->toJson();

        
    }

    public function myInvitation()
    {

        $crew = CrewInfo::select('agency_invite.id as id','crew_info.users_id as users_id','photo','name','fullname','type_crew','status_account','status_activity')->leftjoin('users','users.id','crew_info.users_id')->leftjoin('agency_invite','agency_invite.crew_id','crew_info.users_id')->where('agency_invite.status_invitation','APPROVE')->where('agency_invite.users_id',auth()->user()->id)->get();

        return view('agency.myinvitation',compact('crew'));
    }

     public function myInvitationDestroy($id){

        $clt = AgencyInvite::findOrFail($id);
        $clt->status_invitation = 'DELETE';
        $clt->update();

        //AgencyInvite::where('id', $id)->delete();
        Session::flash('success', 'Invite was deleted');

        return redirect()->back();
    }
}