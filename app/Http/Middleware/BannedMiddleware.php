<?php
namespace App\Http\Middleware;

use Closure;

use App;
use App\Http\Controllers\MustBanned;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class BannedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        if(auth()->check() && (auth()->user()->is_ban == 1)){

            return redirect()->route('banned');

        }

        return $next($request);
    }
}