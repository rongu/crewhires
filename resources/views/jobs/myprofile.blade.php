

@extends('layouts.frontend')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }
        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>My Profile | Account
                <small>user account page</small>
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>crew</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="{{ asset('../../public/images/profile') }}/{{Auth::user()->photo}}" class="img-responsive zoomimg" alt="" style="height: 150px;" title="{{$usr->name}}" >  </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> {{$usr->name}} </div>
                                <div class="profile-usertitle-job"> {{$inf->status_activity}} </div>
                                <img src="{{$bnd}}" style="width: 70px;border: 1px solid #cdcdcd;">
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <button type="button" class="btn {{$inf->type_crew=='MARINE'?'blue-ebonyclay':'grey-cararra'}}  btn-sm">MARINE</button>
                                <button type="button" class="btn  {{$inf->type_crew=='CRUISE'?'blue-ebonyclay':'grey-cararra'}} btn-sm">CRUISE</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                <ul class="nav">
        
                                    <li class="active">
                                        <a href="{{route('crew.cv',Auth::user()->id)}}">
                                            <i class="fa fa-file-text-o"></i> Curriculum Vitae </a>
                                    </li>

                                </ul>
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                        <!-- PORTLET MAIN -->
                        <div class="portlet light ">
                            <!-- STAT -->
                            <div class="row list-separated profile-stat">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 1 </div>
                                    <div class="uppercase profile-stat-text"> Apply </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 100 </div>
                                    <div class="uppercase profile-stat-text"> Agent </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 12500 </div>
                                    <div class="uppercase profile-stat-text"> Crew </div>
                                </div>
                            </div>
                            <!-- END STAT -->
                            <div>
                                <h3 class="profile-desc-title">Social Media</h3>
                                
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.facebook.com/">{{$inf->fb}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-instagram fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.instagram.com/">{{$inf->ig}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-send-o fa-sm font-blue-ebonyclay"></i>
                                    <a href="#">{{$inf->tg}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.twitter.com/">{{$inf->tw}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-whatsapp fa-sm font-blue-ebonyclay"></i>
                                    <a href="#">{{$inf->wa}}</a>
                                </div>                                
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">

                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_6" data-toggle="tab">Information</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_4" data-toggle="tab">Account</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_3" data-toggle="tab">Document</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_7" data-toggle="tab">Portofolio</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_5" data-toggle="tab">Social Media</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane active" id="tab_1_1">
                                                <form id="submit_form_profile" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="profile">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="fullname" type="text" class="form-control" id="fullname_id" placeholder="Enter your Fullname" value="{{$inf->fullname}}">
                                                            <label for="fullname_id">Fullname</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
       
                                                        <div class="form-group form-md-line-input has-success">
                                                            <div class="input-group">
                                                                <input name="email" type="text" class="form-control" id="email_id" placeholder="Enter your Email Address" value="{{$usr->email}}">
                                                                <label for="email_id">Email</label>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <label for="phone_id">Phone Number</label>
                                                            <div class="input-group">
                                                                <span class="input-group-btn btn-left">
                                                                    <select name="phone_code" id="phone_code_id" class="form-control" data-placeholder="" width="150px">
                                                                        <option></option>
                                                                        @foreach(json_decode($calling, true) as $key => $value)
                                                                        <option value="{{$value['calling_code']}}"  {{$value['calling_code']==$usr->phone_code?'SELECTED':''}}>+{{$value['calling_code']}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </span>
                                                                <div class="input-group-control input-group">
                                                                    <input name="phone" type="text" class="form-control" id="phone_id" placeholder="Enter your Phone Number" value="{{$usr->phone}}">
                                                                    <!-- <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span> -->
                                                                </div>
                                                                
                                                            </div>
                                                        </div>


                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="nationality" id="nationality_list" class="form-control" data-placeholder="">
                                                                <option value=""></option>
                                                                @foreach(json_decode($cty, true) as $key => $value)
                                                                <option value="{{$value['country']}}" {{$value['country']==$inf->nationality?'selected':''}}>{{$value['country']}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="nationality_list">Nationality</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="place" type="text" class="form-control" id="place_id" placeholder="Enter your place" value="{{$inf->place_birth}}">
                                                            <label for="place_id">Place</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="birth" type="text" class="form-control date-picker" id="form_control_1" value="{{Carbon::parse($inf->birth)->format('m/d/Y')}}">
                                                            <label for="form_control_1">Date of Birth</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="religion" id="religion_list" class="form-control" data-placeholder="">
                                                                <option></option>
                                                                @foreach($reli as $value)
                                                                <option value="{{$value->name}}"  {{$value->name==$inf->religion?'selected':''}}>{{$value->name}}</option>
                                                                @endforeach                                                                
                                                            </select>
                                                            <label for="religion_list">Religion</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-radios has-success">
                                                            <label for="form_control_1">Gender</label>
                                                            <div class="md-radio-list">
                                                                <div class="md-radio">
                                                                    <input type="radio" id="male_id" name="gender" value="m" class="md-radiobtn" {{$inf->gender=='m'?'checked':''}} >
                                                                    <label for="male_id">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Male </label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="female_id" name="gender" value="f" class="md-radiobtn" {{$inf->gender=='f'?'checked':''}}>
                                                                    <label for="female_id">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Female </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="marital" id="marital_list" class="form-control" data-placeholder="">
                                                                <option value="Single" {{$inf->marital=='Single'?'selected':''}}>Single</option>
                                                                <option value="Married" {{$inf->marital=='Married'?'selected':''}}>Married</option>
                                                                <option value="Divorced" {{$inf->marital=='Divorced'?'selected':''}}>Divorced</option>
                                                                <option value="Separated" {{$inf->marital=='Separated'?'selected':''}}>Separated</option>
                                                                <option value="Widowed" {{$inf->marital=='Widowed'?'selected':''}}>Widowed</option>
                                                                
                                                            </select>
                                                            <label for="marital_list">Marital Status</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <textarea class="form-control" name="address" rows="3">{{$inf->address}}</textarea>
                                                            <label for="form_control_1">Address</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="city" type="text" class="form-control" id="city_id" placeholder="Enter your City" value="{{$inf->city}}">
                                                            <label for="city_id">City/Town</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="country" id="country_list" class="form-control" data-placeholder="">
                                                                <option value=""></option>
                                                                @foreach(json_decode($cty, true) as $key => $value)
                                                                <option value="{{$value['country']}}" {{$value['country']==$inf->nationality?'selected':''}}>{{$value['country']}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="country_list">Country</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="currency_id" id="currency_id" class="form-control">
                                                                <option value="IDR" {{$inf->currency=='IDR'?'selected':''}}>IDR</option>
                                                                <option value="USD" {{$inf->currency=='USD'?'selected':''}}>USD</option>
                                                            </select>
                                                            <label for="name_id">Salary Currency</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="salary_id" type="text" class="form-control" id="salary_id" placeholder="Enter your Salary" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="{{$inf->salary}}"/>
                                                            <label for="currency_id">Salary Value</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                            <div class="tab-pane" id="tab_1_6">
                                            
                                                <form id="submit_form_account" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="info">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="type_crew" id="type_crew_id" class="form-control">
                                                                <option value=""></option>
                                                                <option value="MARINE" {{$inf->type_crew=='MARINE'?'selected':''}}>MARINE</option>
                                                                <option value="CRUISE" {{$inf->type_crew=='CRUISE'?'selected':''}}>CRUISE</option>
                                                            </select>
                                                            <label for="name_id">Type Crew</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="job_id" id="job_id" class="form-control" data-placeholder="">
                                                                <option value=""></option>
                                                                @foreach($job_desc as $value)
                                                                <option value="{{$value->job_description}}"  {{$value->job_description==$inf->job_desc?'selected':''}}>{{$value->job_description}}</option>
                                                                @endforeach                                                                
                                                            </select>
                                                            <label for="name_id">Job Experience</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="profile_seaferer_id" class="form-body {{$inf->type_crew=='CRUISE'?'hidden':''}}">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="seaferer_id" placeholder="Enter your seaferer" name="seaferer" value="{{$inf->seaferer}}">
                                                            <label for="seaferer_id">Seaferer</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="status_activity" id="status_activity_id" class="form-control">
                                                                <option value=""></option>
                                                                <option value="STANDBY" {{$inf->status_activity=='STANDBY'?'selected':''}}>STANDBY</option>
                                                                <option value="AVAILABLE" {{$inf->status_activity=='AVAILABLE'?'selected':''}}>AVAILABLE</option>
                                                                <option value="ON BOARD" {{$inf->status_activity=='ON BOARD'?'selected':''}}>ON BOARD</option>
                                                            </select>
                                                            <label for="status_activity_id">Status Activity</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>




                                            </div>
                                            <!-- END PERSONAL INFO TAB -->
                                            <!-- CHANGE AVATAR TAB -->
                                            <div class="tab-pane" id="tab_1_4">
                                                <form id="submit_form_account" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="account">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" name="name" class="form-control" id="name_id" placeholder="Enter your name" value="{{$usr->name}}">
                                                            <label for="name_id">Username</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="password" class="form-control" id="password_id" placeholder="Enter your password" name="password">
                                                            <label for="password_id">New Password</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="password" class="form-control" id="repassword_id" placeholder="Enter your password" name="repassword">
                                                            <label for="repassword_id">Re-type New Password</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="tab_1_2">
                                                
                                                <form id="submit_form_account" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="photo">
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                
                                                                @if($usr->photo)
                                                                <img src="{{asset('../../public/images/profile')}}/{{$usr->photo}}" alt="" class="zoomimg" title="Photo Profile {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END CHANGE AVATAR TAB -->
                                            <!-- CHANGE PASSWORD TAB -->
                                            <div class="tab-pane" id="tab_1_3">
                                                <form id="submit_form_document" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="document">
                                                    <div class="form-body" style="background-color: #f7f7f7; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Experienced

                                                            </label>
                                                                @foreach($doc as $vlk)
                                                                
                                                                @if($vlk->types=='exp')
                                                                    <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                        <div data-repeater-list="exp">
                                                                            <div data-repeater-item class="mt-repeater-item">
                                                                                <div class="row mt-repeater-row">
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label">Name Of Vessel</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->name}}"/> </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label">Type Vessel</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->t_vessel}}"/> </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label">Rank</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->rank}}"/> </div>
                                                                                    <div class="col-md-8">
                                                                                        <label class="control-label">Company</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->company}}"/> </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Period</label>
                                                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{Carbon::parse($vlk->p_from)->format('m/d/Y')}}">
                                                                                        <span class="input-group-addon"> to </span>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{Carbon::parse($vlk->p_to)->format('m/d/Y')}}"></div>
                                                                                    </div>

                                                                                    <div class="col-md-1">
                                                                                        <a href="{{route('profile.delete.doc.crew',$vlk->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                            <i class="fa fa-times-circle fa-sm"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                                
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="exp">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Name Of Vessel</label>
                                                                                    <input name="exp_nov" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Type Vessel</label>
                                                                                    <input name="exp_tv" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Rank</label>
                                                                                    <input name="exp_rank" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-8">
                                                                                    <label class="control-label">Company</label>
                                                                                    <input name="exp_com" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Period</label>
                                                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                    <input name="exp_from" type="text" class="form-control">
                                                                                    <span class="input-group-addon"> to </span>
                                                                                    <input name="exp_to" type="text" class="form-control"></div>
                                                                                </div>

                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>


                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-body" style="background-color: #FFFFFF; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Certificate of Proficiency
                                                                
                                                            </label>
                                                        
                                                                
                                                                @foreach($doc as $vlk)
                                                                
                                                                @if($vlk->types=='cop')
                                                                    <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                        <div data-repeater-list="cop">
                                                                            <div data-repeater-item class="mt-repeater-item">
                                                                                <div class="row mt-repeater-row">
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Name Of Certificate</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->name}}" disabled="disabled"/></div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Certificate Number</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->cer_doc_number}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Place,</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->place}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Date of Issued</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->doi)->format('m/d/Y')}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Expired Date</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->ed)->format('m/d/Y')}}" disabled="disabled"/></div>
                                                                                    

                                                                                    <div class="col-md-1">
                                                                                        <a href="{{route('profile.delete.doc.crew',$vlk->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                            <i class="fa fa-times-circle fa-sm"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                                
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="cop">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Name Of Certificate</label>
                                                                                    <select name="cop_noc" class="form-control cop_list" data-placeholder="">
                                                                                        @foreach($mastercop as $cop)
                                                                                            <option value="{{$cop->name}}">{{$cop->name}} : {{$cop->codes}}</option>
                                                                                        @endforeach                                                                                     
                                                                                    </select></div>
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Certificate Number</label>
                                                                                    <input name="cop_cn" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Place,</label>
                                                                                    <input name="cop_pl" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Date of Issued</label>
                                                                                    <input name="cop_doi" class="form-control date-picker" size="16" type="text" value=""> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Expired Date</label>
                                                                                    <input name="cop_ed" class="form-control date-picker" size="16" type="text" value=""></div>
                                                                                

                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>


                                                   
                                                        </div>
                                                    </div>

                                                    <div class="form-body" style="background-color: #f7f7f7; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Certificate of Competency

                                                            </label>
                                                      
                                                                @foreach($doc as $vlk)
                                                                
                                                                @if($vlk->types=='coc')

                                                                    <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                        <div data-repeater-list="coc">
                                                                            <div data-repeater-item class="mt-repeater-item">
                                                                                <div class="row mt-repeater-row">
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Name Of Certificate</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->name}}" /> </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Certificate Number</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->cer_doc_number}}"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Place,</label>
                                                                                        <input type="text" class="form-control" disabled="disabled" value="{{$vlk->place}}"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Date of Issued</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" disabled="disabled" value="{{Carbon::parse($vlk->doi)->format('m/d/Y')}}"> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Expired Date</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" disabled="disabled" value="{{Carbon::parse($vlk->ed)->format('m/d/Y')}}"></div>
                                                                                    

                                                                                    <div class="col-md-1">
                                                                                        <a href="{{route('profile.delete.doc.crew',$vlk->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                            <i class="fa fa-times-circle fa-sm"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                @endif
                                                                @endforeach

                                                                
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="coc">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Name Of Certificate</label>
                                                                                    <select name="coc_noc" class="form-control coc_list" data-placeholder="">
                                                                                        @foreach($mastercoc as $coc)
                                                                                            <option value="{{$coc->name}}">{{$coc->name}}</option>
                                                                                        @endforeach                                                                                     
                                                                                    </select></div>
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Certificate Number</label>
                                                                                    <input name="coc_cn" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Place,</label>
                                                                                    <input name="coc_pl" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Date of Issued</label>
                                                                                    <input name="coc_doi" class="form-control date-picker" size="16" type="text" value=""> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Expired Date</label>
                                                                                    <input name="coc_ed" class="form-control date-picker" size="16" type="text" value=""></div>
                                                                                

                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>


                                                     
                                                        </div>
                                                    </div>

                                                    <div class="form-body" style="background-color: #FFFFFF; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Flagstate Document

                                                            </label>

                                                                @foreach($doc as $vlk)
                                                                
                                                                @if($vlk->types=='flg')
                                                                    <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                        <div data-repeater-list="flg">
                                                                            <div data-repeater-item class="mt-repeater-item">
                                                                                <div class="row mt-repeater-row">
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Name Of Document</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->name}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Certificate Number</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->cer_doc_number}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Place,</label>
                                                                                        <input type="text" class="form-control" value="{{$vlk->place}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Date of Issued</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->doi)->format('m/d/Y')}}" disabled="disabled"/> </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label">Expired Date</label>
                                                                                        <input class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->ed)->format('m/d/Y')}}" disabled="disabled"/></div>

                                                                                    <div class="col-md-1">
                                                                                        <a href="{{route('profile.delete.doc.crew',$vlk->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                            <i class="fa fa-times-circle fa-sm"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                        
                                                                
                                                                
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="flg">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Name Of Document</label>
                                                                                    <input name="flg_nod" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Certificate Number</label>
                                                                                    <input name="flg_cn" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Place,</label>
                                                                                    <input name="flg_pl" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Date of Issued</label>
                                                                                    <input name="flg_doi" class="form-control date-picker" size="16" type="text" value=""> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Expired Date</label>
                                                                                    <input name="flg_ed" class="form-control date-picker" size="16" type="text" value=""></div>

                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>


                                                    
                                                        </div>
                                                    </div>

                                                    <div class="form-body" style="background-color: #f7f7f7; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Standard STCW  2010 + Flagstate Medical Form
                                                                
                                                            </label>
                                                                
                                                                @foreach($doc as $vlk)
                                                                
                                                                @if($vlk->types=='stc')
                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="stc">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Name Of Document</label>
                                                                                    <input  type="text" class="form-control" disabled="disabled" value="{{$vlk->name}}" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Document Number</label>
                                                                                    <input  type="text" class="form-control" disabled="disabled" value="{{$vlk->cer_doc_number}}"/> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Place,</label>
                                                                                    <input  type="text" class="form-control" disabled="disabled" value="{{$vlk->place}}"/> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Date of Issued</label>
                                                                                    <input  class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->doi)->format('m/d/Y')}}" disabled="disabled"> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Expired Date</label>
                                                                                    <input  class="form-control date-picker" size="16" type="text" value="{{Carbon::parse($vlk->ed)->format('m/d/Y')}}" disabled="disabled"></div>

                                                                                <div class="col-md-1">
                                                                                    <a href="{{route('profile.delete.doc.crew',$vlk->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                        <i class="fa fa-times-circle fa-sm"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                @endif

                                                                @endforeach

                                                                <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="stc">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Name Of Document</label>
                                                                                    <input name="stc_nod" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Document Number</label>
                                                                                    <input name="stc_dn" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Place,</label>
                                                                                    <input name="stc_pl" type="text" class="form-control" /> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Date of Issued</label>
                                                                                    <input name="stc_doi" class="form-control date-picker" size="16" type="text" value=""> </div>
                                                                                <div class="col-md-2">
                                                                                    <label class="control-label">Expired Date</label>
                                                                                    <input name="stc_ed" class="form-control date-picker" size="16" type="text" value=""></div>

                                                                                <div class="col-md-1">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                        <i class="fa fa-close"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="tab-pane" id="tab_1_7">
                                                <form id="submit_form_portofolio" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="portofolio">
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                                @if($inf->portofolio)
                                                                <img src="{{asset('../../public/images/portofolio')}}/{{$inf->portofolio}}" alt="" class="zoomimg" title="Portofolio {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif

                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file_portofolio"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="tab-pane" id="tab_1_5">
                                                <form id="submit_form_sosmed" role="form" action="{{route('profile.update.crew')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="sosmed">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="fb_id" placeholder="Facebook" name="fb" value="{{$inf->fb}}">
                                                            <label for="fb_id">Facebook</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="ig_id" placeholder="Instagram" name="ig" value="{{$inf->ig}}">
                                                            <label for="ig_id">Instagram</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="tele_id" placeholder="Telegram" name="tele" value="{{$inf->tg}}">
                                                            <label for="tele_id">Telegram</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="tw_id" placeholder="Twitter" name="tw" value="{{$inf->tw}}">
                                                            <label for="tw_id">Twitter</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="wa_id" placeholder="WhatsApp"  name="wa" value="{{$inf->wa}}">
                                                            <label for="wa_id">WhatsApp</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    $('#type_crew_id').on('change', function() {
        var type_crew = (this).value;
        getJobDesc(type_crew);
    });

    function getJobDesc(type_crew){

        $.ajax({
            url: "{{ url('get-job_desc') }}/"+type_crew,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                var vh = '<option value=""></option>';
                for (var i = result.length - 1; i >= 0; i--) {
                    var row = result[i];
                    vh += '<option value="'+row.id+'">'+row.job_description+'</option>';
                }

                $("#job_id").html(vh);

            }
        })

    }

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }


    $("#country_list").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });    

    $("#phone_code_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: '110px', 
        
        escapeMarkup: function (m) {
            return m;
        }
    }); 

    $("#marital_list").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });     

    $("#nationality_list").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });    

    $("#religion_list").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });

    $("#job_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });



    var form_p = $('#submit_form_profile');
    var error = $('.alert-danger', form_p);
    var success = $('.alert-success', form_p);

    form_p.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            fullname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            phone_code: {
                required: true
            },
            place: {
                required: true
            },
            gender: {
                required: true
            },
            address: {
                required: true
            },
            city: {
                required: true
            },
            country: {
                required: true
            },

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_p[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });


    var form_a = $('#submit_form_account');
    var error = $('.alert-danger', form_a);
    var success = $('.alert-success', form_a);

    form_a.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            name: {
                required: true
            },
            repassword: {
                minlength: 5,
                equalTo: "#password_id"
            },

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_a[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    var form_s = $('#submit_form_sosmed');
    var error = $('.alert-danger', form_s);
    var success = $('.alert-success', form_s);

    form_s.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_s[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    var form_dc = $('#submit_form_document');
    var error = $('.alert-danger', form_dc);
    var success = $('.alert-success', form_dc);

    form_dc.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_dc[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    $('.mt-repeater').each(function(){
        $(this).repeater({
            show: function () {

                // $(this).slideDown();  
                $(this).find('.select2-container').remove();
                // $(this).find('.select2-container').remove();
                $(this).slideDown(function(){
                    $(this).find('.cop_list').select2({

                        placeholder: "Select",
                        allowClear: true,
                        width: 'auto', 

                    });

                    $(this).find('.coc_list').select2({

                        placeholder: "Select",
                        allowClear: true,
                        width: 'auto',


                    });
                });

                $(".date-picker").datepicker();

                

            },

            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },

            ready: function (setIndexes) {
                $(".cop_list").select2({
                    placeholder: "Select",
                    allowClear: true,
                    width: 'auto'
                });

                $(".coc_list").select2({
                    placeholder: "Select",
                    allowClear: true,
                    width: 'auto'
                });   
            }

        });
    });


    $('.cop_list', form_dc).change(function () {
        form_dc.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });

    $('.coc_list', form_dc).change(function () {
        form_dc.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    
    $('#phone_code_id', form_p).change(function () {
        form_p.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });

    ezoom.onInit($('.zoomimg'), {
        hideControlBtn: true,
        onClose: function (result) {
            console.log(result);
        },
        onRotate: function (result) {
            console.log(result);
        },
    });

    $("#type_crew_id").on('change',function(){
        var vl = this.value;
        console.log(vl);
        if(vl=='MARINE'){
            $("#profile_seaferer_id").removeClass('hidden');
        }else{
            $("#profile_seaferer_id").addClass('hidden');
        }
    });

});
</script>
@endsection    