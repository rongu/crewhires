<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use \App\Models\AgencyInfo;
use \App\Models\AgencyInvite;
use \App\Models\AgencyJobs;
use \App\Models\User;
use \App\Models\CrewInfo;
use \App\Models\Service;
use \App\Models\Membership;
use \App\Models\HomeSlide;
use \App\Models\CrewApply;
use \App\Models\CrewDoc;
use \App\Models\LoggerActivity;
use \App\Models\VStatistikNegara;
use Illuminate\Support\Facades\Hash;
use Alert,DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPass;
use App\Mail\BannedAccess;

class BackendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $logs = LoggerActivity::select('laravel_logger_activity.description','laravel_logger_activity.details','laravel_logger_activity.created_at','users.name')->leftjoin('users','users.id','userId')->orderBy('laravel_logger_activity.created_at','desc')->get();


        $jml_crew = User::where('role_id','=',2)->leftjoin('role_user','role_user.user_id','users.id')->count('users.id');
        $jml_agency = User::where('role_id','=',4)->where('type_agency','=','AGENCY')->leftjoin('role_user','role_user.user_id','users.id')->leftjoin('agency_info','agency_info.users_id','users.id')->count('users.id');
        $jml_so = User::where('role_id','=',4)->where('type_agency','=','SHIP OWNER')->leftjoin('role_user','role_user.user_id','users.id')->leftjoin('agency_info','agency_info.users_id','users.id')->count('users.id');
        return view('backend.index',compact('jml_crew','jml_agency','jml_so','logs'));
    }    

    public function crewPage()
    {
        return view('backend.crew-page');
    }    

    public function agencyPage()
    {
        return view('backend.agency-page');
    }

    public function serviceList()
    {
        $codena = Service::select('service.users_id','fullname','companyname','types','code_service','name','date_add','status','phone')->leftjoin('users','users.id','service.users_id')->leftjoin('crew_info','crew_info.users_id','service.users_id')->leftjoin('agency_info','agency_info.users_id','service.users_id')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('name', function($codena) {

                if($codena->fullname!=null){
                    $nn = $codena->fullname;
                }else{
                    $nn = $codena->companyname;
                }
                return $codena->name." ( ".$nn." )";

            })->addColumn('types', function($codena) {

                if($codena->types=='visa'){
                    $nn = 'Visa';
                }else if($codena->types=='passport'){
                    $nn = 'Passport';
                }else if($codena->types=='seamanbook'){
                    $nn = "Seaman's Book";
                }else if($codena->types=='certificaterevalidation'){
                    $nn = 'Certificate Revalidation';
                }else if($codena->types=='trainingcenter'){
                    $nn = 'Training Center';
                }else if($codena->types=='accommodation'){
                    $nn = 'Accommodation';
                }else if($codena->types=='payroll'){
                    $nn = 'Payroll';
                }else if($codena->types=='bunker'){
                    $nn = 'Bunker';
                }

                return $nn;

            })->addColumn('code', function($codena) {

                return $codena->code_service;

            })->addColumn('phone', function($codena) {

                return $codena->phone;

            })->addColumn('status', function($codena) {

                return $codena->status;

            })->addColumn('date_add', function($codena) {

                return Carbon::parse($codena->date_add)->format('d M Y');                

            })->addColumn('users_id', function($codena) {

                return [$codena->users_id];                

            })->toJson();
    }

    public function membershipList()
    {
        $codena = Membership::select('membership.id as id','membership.users_id','fullname','companyname','code_member','name','phone','status','date_add','date_paid','photo_receipt','bank_account','bau_name','account_number')->leftjoin('users','users.id','membership.users_id')->leftjoin('crew_info','crew_info.users_id','membership.users_id')->leftjoin('agency_info','agency_info.users_id','membership.users_id')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('code_member', function($codena) {

                return $codena->code_member;

            })->addColumn('name', function($codena) {

                if($codena->fullname!=null){
                    $nn = $codena->fullname;
                }else{
                    $nn = $codena->companyname;
                }
                return $codena->name." ( ".$nn." )";

            })->addColumn('phone', function($codena) {

                return $codena->phone;

            })->addColumn('status', function($codena) {

                return $codena->status;

            })->addColumn('date_add', function($codena) {

                return Carbon::parse($codena->date_add)->format('d M Y');

            })->addColumn('date_paid', function($codena) {

                return Carbon::parse($codena->date_paid)->format('d M Y');

            })->addColumn('photo_receipt', function($codena) {

                return $codena->photo_receipt;                

            })->addColumn('bank_account', function($codena) {

                return $codena->bank_account;                

            })->addColumn('memberpay', function($codena) {

                return $codena->account_number.' [ '.$codena->bau_name.' ]';                

            })->addColumn('users_id', function($codena) {

                return [$codena->id,$codena->users_id,$codena->code_member];                

            })->toJson();
    }

    public function agencyList(Request $request)
    {
        $typ = $request->typ;
        if($typ=='all'){
            $codena = AgencyInfo::select('users_id','photo','name','companyname','type_agency','status_account','siup','siuppak','is_ban')->leftjoin('users','users.id','agency_info.users_id')->get();

        }else if($typ=='agency'){
            $codena = AgencyInfo::select('users_id','photo','name','companyname','type_agency','status_account','siup','siuppak','is_ban')->where('type_agency','=','AGENCY')->leftjoin('users','users.id','agency_info.users_id')->get();

        }else if($typ=='shipowner'){
            $codena = AgencyInfo::select('users_id','photo','name','companyname','type_agency','status_account','siup','siuppak','is_ban')->where('type_agency','=','SHIP OWNER')->leftjoin('users','users.id','agency_info.users_id')->get();

        }

        
        return datatables()->of($codena)->addIndexColumn()->addColumn('companyname', function($codena) {

                return $codena->companyname;

            })->addColumn('name', function($codena) {

                return $codena->name;                

            })->addColumn('type_agency', function($codena) {

                return $codena->type_agency;                

            })->addColumn('status_account', function($codena) {

                return $codena->status_account;                

            })->addColumn('photo', function($codena) {

                return $codena->photo;                

            })->addColumn('siup', function($codena) {

                return $codena->siup==''?'no_image.png':$codena->siup;                

            })->addColumn('siuppak', function($codena) {

                return $codena->siuppak==''?'no_image.png':$codena->siuppak;            

            })->addColumn('users_id', function($codena) {

                return [$codena->users_id,$codena->companyname,$codena->type_agency,$codena->is_ban];                

            })->toJson();
    }

     public function crewVacancyPage()
    {
        return view('backend.crew-vacancy-page');
    }

    public function crewVacancyList()
    {
        $codena = AgencyJobs::select('agency_jobs.id','agency_jobs.users_id','agency_info.companyname','users.status_account','agency_jobs.position','agency_jobs.types','agency_jobs.engine','agency_jobs.requirement','agency_jobs.date_open','agency_jobs.date_close','ca.jumlah')->leftjoin('agency_info','agency_info.users_id','agency_jobs.users_id')->leftjoin('users','users.id','agency_jobs.users_id')->leftjoin(DB::raw('(select agency_jobs_id,count(id)  as jumlah from crew_apply where apply_status = "APPROVE" group by agency_jobs_id) AS ca') ,'ca.agency_jobs_id','agency_jobs.id')->orderBy('agency_jobs.date_open')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('companyname', function($codena) {

                return $codena->companyname;

            })->addColumn('status_account', function($codena) {

                return $codena->status_account;                

            })->addColumn('position', function($codena) {

                return $codena->position;                

            })->addColumn('types', function($codena) {

                return $codena->types; 

            })->addColumn('engine', function($codena) {

                return $codena->engine; 

            })->addColumn('date_open', function($codena) {

                return $codena->date_open; 

            })->addColumn('date_close', function($codena) {

                return $codena->date_close; 

            })->addColumn('jumlah', function($codena) {

                return $codena->jumlah;  

            })->addColumn('id', function($codena) {

                return [$codena->id,$codena->companyname,$codena->position,$codena->types,$codena->engine,$codena->date_open,$codena->date_close,$codena->requirement,$codena->status_account];                

            })->toJson();
    }

    public function crewVacancyApplyList(Request $request)
    {
        $codena = CrewApply::select('crew_info.fullname','crew_info.type_crew','crew_info.gender','crew_info.birth','crew_info.status_activity','users.status_account')->leftjoin('crew_info','crew_info.users_id','crew_apply.users_id')->leftjoin('users','users.id','crew_apply.users_id')->where('crew_apply.agency_jobs_id',$request->id)->where('crew_apply.apply_status', 'APPROVE')->orderBy('crew_info.fullname')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('gender', function($codena) {

                return $codena->gender; 

            })->addColumn('birth', function($codena) {

                return $codena->birth; 

            })->addColumn('status_activity', function($codena) {

                return $codena->status_activity; 

            })->addColumn('status_account', function($codena) {

                return $codena->status_account; 

            })->toJson();
    }

    public function crewList()
    {
        $codena = CrewInfo::select('users_id','photo','name','fullname','type_crew','status_account','is_ban')->leftjoin('users','users.id','crew_info.users_id')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('name', function($codena) {

                return $codena->name;                

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('status_account', function($codena) {

                return $codena->status_account;                

            })->addColumn('photo', function($codena) {

                return $codena->photo;                

            })->addColumn('users_id', function($codena) {

                return [$codena->users_id,$codena->fullname,$codena->type_crew,$codena->is_ban];                

            })->toJson();
    }

    public function servicePage()
    {
        return view('backend.service-page');
    }

    public function membershipPage()
    {
        return view('backend.membership-page');
    }

    public function login()
    {
        return view('auth.loginbackup');
    }

    public function membershipPaymentActivate(Request $request){

        $id = $request->id;
        $users_id = $request->users_id;
        $status = $request->status;

        $pts = User::findOrFail($users_id);
        $pts->status_account = 'premium';
        $pts->update();

        $py = Membership::findOrFail($id);
        $py->status = 'done';
        $py->update();

        Alert::success('success', 'Thank you, Membership was premium');

        return redirect()->back();  
    }


    public function frontendPage()
    {
        return view('backend.frontend-page');
    }

    public function frontendListSlider()
    {
        $codena = HomeSlide::select('id','title','images')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('title', function($codena) {

                return $codena->title;

            })->addColumn('images', function($codena) {

                return $codena->images;                

            })->addColumn('images_id', function($codena) {

                return [$codena->id,$codena->title,$codena->images];                

            })->toJson();
    }

    public function frontendCreateSlider(Request $request){
        
        $id = $request->id;
        $title = $request->title;
        $file = $request->file('file');

        if($file){
            $nama_file = auth()->user()->id.'_'.time() . '.' . $file->getClientOriginalExtension();
            
            $tujuan_upload = 'public/images/slide';
            $file->move($tujuan_upload,$nama_file);
        }  

        if($id==0){
            $usr = new HomeSlide();
            $usr->title = $title;
            if($file){
            $usr->images = $nama_file;
            }
            $usr->save();

            Alert::success('success', 'Thank you, Add new slider image');
        }else{

            $clt = HomeSlide::findOrFail($id);
            $clt->title = $title;
            if($file){
                $clt->images = $nama_file;
            }
            $clt->update();
            Alert::success('success', 'Thank you, update slider image');
        }



        return redirect()->back();  
    }

    public function frontendDestroySlider($id)
    {
        //

        HomeSlide::where('id', $id)->delete();
        $data = true;

        return response()->json($data);
    }

    public function invitationPage()
    {
        return view('backend.invitation-page');
    }

    public function invitationList()
    {
        $codena = AgencyInvite::select('agency_invite.id as id','crew_info.fullname','crew_info.type_crew','users.status_account','users.photo','crew_info.status_activity','agency_info.companyname as invited_by','comp.status_account as companystatus','agency_invite.status_invitation as invited_status')->leftjoin('crew_info','crew_info.users_id','agency_invite.crew_id')->leftjoin('agency_info','agency_info.users_id','agency_invite.users_id')->leftjoin('users','users.id','agency_invite.crew_id')->leftjoin('users as comp','comp.id','agency_invite.users_id')->orderBy('agency_invite.status_invitation')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('status_account', function($codena) {

                return $codena->status_account; 

            })->addColumn('status_activity', function($codena) {

                return $codena->status_activity; 

            })->addColumn('invited_by', function($codena) {

                return $codena->invited_by;  

            })->addColumn('companystatus', function($codena) {

                return $codena->companystatus;  

            })->addColumn('invited_status', function($codena) {

                return $codena->invited_status;                

            })->addColumn('photo', function($codena) {

                return $codena->photo;                

            })->addColumn('id', function($codena) {

                return [$codena->id,$codena->fullname,$codena->type_crew,$codena->status_account,$codena->status_activity,$codena->invited_by,$codena->invited_status,$codena->companystatus];                

            })->toJson();
    }

    public function invitationPermission(Request $request){

        $id = $request->id;
        $fullname = $request->fullname;
        $typecrew = $request->typecrew;
        $accountstatus = $request->accountstatus;
        $statuscrew = $request->statuscrew;
        $invitedby = $request->invitedby;
        $status = $request->status;

        $pts = AgencyInvite::findOrFail($id);
        $pts->status_invitation = $status;
        $pts->update();

        Alert::success('success', 'Invitation Status has Updated');

        return redirect()->back();  
    }

    public function crewApplyPage()
    {
        return view('backend.crew-apply-page');
    }

    public function crewApplyList()
    {
        $codena = CrewApply::select('crew_apply.id as id','crew_info.fullname','crew_info.type_crew','agency_jobs.position','agency_jobs.types','agency_jobs.engine','agency_info.companyname','agency_info.type_agency','users.status_account','crew_apply.apply_status')->leftjoin('crew_info','crew_info.users_id','crew_apply.users_id')->leftjoin('agency_jobs','agency_jobs.id','crew_apply.agency_jobs_id')->leftjoin('agency_info','agency_info.users_id','agency_jobs.users_id')->leftjoin('users','users.id','agency_info.users_id')->where('crew_apply.apply_by','USER')->orderBy('crew_apply.apply_status')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('position', function($codena) {

                return $codena->position; 

            })->addColumn('types', function($codena) {

                return $codena->types; 

            })->addColumn('engine', function($codena) {

                return $codena->engine;  

            })->addColumn('companyname', function($codena) {

                return $codena->companyname;                

            })->addColumn('type_agency', function($codena) {

                return $codena->type_agency;                

            })->addColumn('status_account', function($codena) {

                return $codena->status_account;                

            })->addColumn('apply_status', function($codena) {

                return $codena->apply_status;                

            })->addColumn('id', function($codena) {

                return [$codena->id,$codena->fullname,$codena->type_crew,$codena->position,$codena->types,$codena->engine,$codena->companyname,$codena->type_agency,$codena->status_account,$codena->apply_status];                

            })->toJson();
    }

    public function crewApplyApproval(Request $request){

        $id = $request->id;
        $fullname_id = $request->fullname_id;
        $type_crew_id = $request->type_crew_id;
        $position_id = $request->position_id;
        $types_id = $request->types_id;
        $engine_id = $request->engine_id;
        $companyname_id = $request->companyname_id;
        $type_agency_id = $request->type_agency_id;
        $status_account_id = $request->status_account_id;
        $status = $request->status;

        $pts = CrewApply::findOrFail($id);
        $pts->apply_status = $status;
        $pts->update();

        Alert::success('success', 'Crew Apply Status has Updated');

        return redirect()->back();  
    }

    public function crewRecommendationPage()
    {
        $client = AgencyInfo::get();

        return view('backend.crew-recommendation-page',compact('client'));
    }

    public function crewRecommendationList()
    {
        $codena = CrewApply::select('crew_apply.id as id','crew_info.fullname','crew_info.type_crew','akun.status_account as status_user','agency_jobs.position','agency_jobs.types','agency_jobs.engine','agency_info.companyname','agency_info.type_agency','comp.status_account as status_company','crew_apply.apply_status')->leftjoin('crew_info','crew_info.users_id','crew_apply.users_id')->leftjoin('agency_jobs','agency_jobs.id','crew_apply.agency_jobs_id')->leftjoin('agency_info','agency_info.users_id','agency_jobs.users_id')->leftjoin('users as comp','comp.id','agency_info.users_id')->leftjoin('users as akun','akun.id','crew_info.users_id')->where('crew_apply.apply_by','ADMIN')->orderBy('crew_apply.apply_status')->get();

        return datatables()->of($codena)->addIndexColumn()->addColumn('fullname', function($codena) {

                return $codena->fullname;

            })->addColumn('type_crew', function($codena) {

                return $codena->type_crew;                

            })->addColumn('status_user', function($codena) {

                return $codena->status_user;                

            })->addColumn('position', function($codena) {

                return $codena->position; 

            })->addColumn('types', function($codena) {

                return $codena->types; 

            })->addColumn('engine', function($codena) {

                return $codena->engine;  

            })->addColumn('companyname', function($codena) {

                return $codena->companyname;                

            })->addColumn('type_agency', function($codena) {

                return $codena->type_agency;                

            })->addColumn('status_company', function($codena) {

                return $codena->status_company;                

            })->addColumn('id', function($codena) {

                return [$codena->id,$codena->fullname,$codena->type_crew,$codena->status_user,$codena->position,$codena->types,$codena->engine,$codena->companyname,$codena->type_agency,$codena->status_company];                

            })->toJson();
    }   

    public function crewRecommendationCreate(Request $request){

        foreach($request->crewrecommendation as $key => $value) {
            $cd = new CrewApply();
            $cd->users_id = $value;
            $cd->agency_jobs_id = $request->position_id;
            $cd->date_add = now();
            $cd->apply_status = 'APPROVE';
            $cd->apply_by = 'ADMIN';
            $cd->save();
        }

        Alert::success('success', 'Crew Recommendation was saved');
        return redirect()->back(); 
    }

    public function crewRecommendationDestroy($id){

        CrewApply::where('id', $id)->delete();
        //Alert::success('success', 'Crew Recommendation was deleted');
        $data = true;
        return response()->json($data);
    }

    public function getposition(Request $request)
    {
        $agency_id = $request->agency_id;
        $position = AgencyJobs::where('users_id', '=', $agency_id)->orderBy('position')->get();
        return response()->json($position);
        
    }

    public function getcrew(Request $request)
    {
        $agency_job_id = $request->agency_job;

        $crew = CrewInfo::select('crew_info.users_id','crew_info.fullname','crew_info.type_crew','users.status_account')->leftjoin('users','users.id','crew_info.users_id')->whereRaw('crew_info.users_id NOT IN (select users_id from crew_apply where crew_apply.agency_jobs_id = '.$agency_job_id.' and crew_apply.apply_status = "APPROVE")')->orderBy('crew_info.fullname')->get();
        return response()->json($crew);
        
    }

    public function resetPassword($id){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        // Output: 54esmdr0qf
        //echo substr(str_shuffle($permitted_chars), 0, 6);
        $passbaru = substr(str_shuffle($permitted_chars), 0, 6);
        $pts = User::findOrFail($id);
        $pts->password = Hash::make($passbaru);
        $pts->update();
        
        $usr = User::select('name','email')->where('id', $id)->first();

        $data = ['message' => $passbaru, 'name' => $usr->name];

        //Mail::to('ron.gusya@gmail.com')->send(new ResetPass($data));
        Mail::to($usr->email)->send(new ResetPass($data));

        $data['status'] = true;
        $data['pass'] = $passbaru;
        return response()->json($data);   
    }

    public function banAccess(Request $request){
        $id = $request->id;
        $usr = User::select('is_ban','name','email')->where('id', $id)->first();

       

        if($usr->is_ban==1){
            $pts = User::findOrFail($id);
            $pts->is_ban = '0';
            $pts->update(); 
            $data = ['message' => 'Your Access was Active', 'name' => $usr->name];
            $data['msg'] = 'Active';
        }else{
            $pts = User::findOrFail($id);
            $pts->is_ban = '1';
            $pts->update(); 
            $data = ['message' => 'Your Access was banned', 'name' => $usr->name];
            $data['msg'] = 'Banned';
        }
       
        Mail::to($usr->email)->send(new BannedAccess($data));

        $data['status'] = true;
        return response()->json($data);   
    }

    public function getAccountInfo($id,$typ){
        if($typ=='crew'){
            $usr = User::where('id', $id)->first();
            $info = CrewInfo::where('users_id', $id)->first();
            $document = CrewDoc::select('name','cer_doc_number','place','rank','t_vessel','company','types',DB::raw('DATE_FORMAT(p_from, "%d/%m/%Y") as p_from'),DB::raw('DATE_FORMAT(p_to, "%d/%m/%Y") as p_to'),DB::raw('DATE_FORMAT(doi, "%d/%m/%Y") as doi'),DB::raw('DATE_FORMAT(ed, "%d/%m/%Y") as ed'))->where('users_id', $id)->get();
            
            $cty = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");
            $getBendera = (new static)->getCountry($info->nationality);
            $bnd = $getBendera['flag_base64'];

            $data['account'] = $usr;    
            $data['info'] = $info;    
            $data['bnd'] = $bnd;    
            $data['document'] = $document;    
            $data['birth'] = Carbon::parse($info->birth)->format('m/d/Y');    
            $data['age'] = Carbon::parse($info->birth)->diff(\Carbon\Carbon::now())->format('%y years');    
        }else if($typ=='agency'){
             $usr = User::where('id', $id)->first();
             $info = AgencyInfo::where('users_id', $id)->first();

             $cty = file_get_contents("vendor/samayo/country-json/src/country-by-capital-city.json");
             $getBendera = (new static)->getCountry($info->country);
             $bnd = $getBendera['flag_base64'];

             $data['account'] = $usr;    
             $data['info'] = $info;
             $data['bnd'] = $bnd;
        }
        
        return response()->json($data);
    }

    static public function getCountry($keys){

        $file = file_get_contents("vendor/samayo/country-json/src/country-by-flag.json");
        foreach (json_decode($file, true) as $key => $value) {
            if($value['country']===$keys){
                return $value;
            }
        }

    }

    static public function getCountryCoordinat($keys){

        $file_co = file_get_contents("vendor/samayo/country-json/src/country-by-geo-coordinates.json");
        foreach (json_decode($file_co, true) as $key => $value_co) {
            if($value_co['country']===$keys){
                return $value_co;
            }
        }

    }


    public function vStatistikNegaraSum(Request $request)
    {

        $jenis_aset = $request->jenis_aset;

        $data  = VStatistikNegara::select('negara');
        $data = $data->groupBy('negara');
        $data = $data->get();

        //$data = ViewPetaAset::where('jenis_aset', $jenis_aset)->get();

        $arr = '';

        $resultdata = array();

        foreach ($data as $keylue) {
            $cc_a  = VStatistikNegara::where('negara','=',$keylue->negara)->where('typ','=','AGENCY')->sum('jml');
            $cc_s  = VStatistikNegara::where('negara','=',$keylue->negara)->where('typ','=','SHIP OWNER')->sum('jml');
            $cc_c  = VStatistikNegara::where('negara','=',$keylue->negara)->where('typ','=','CREW')->sum('jml');
            //$ordinat = explode(",", $keylue->ordinat);

            $getBendera = (new static)->getCountry($keylue->negara);
            $getCoordinat = (new static)->getCountryCoordinat($keylue->negara);
            $bnd = $getBendera['flag_base64'];
            $coo = explode(",",$getCoordinat['coordinat']);

            $arr = [" C:".$cc_c." A:".$cc_a." S:".$cc_s."  ",$coo[0],$coo[1],$bnd];

            $resultdata[] = $arr;
        }

        return response()->json(json_encode(array_values($resultdata)));
    }

    public function banned(){
        return view('auth.banned');
    }


}
