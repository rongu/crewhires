@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Crew Invitation</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Crew Invitation
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Fullname</th>
                            <th>Photo</th>
                            <th>Type</th>
                            <th>Status Account</th>
                            <th>Status Crew</th>
                            <th>Invited By</th>
                            <th>Company Status</th>
                            <th>Invited Status</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>


<div id="modalPermmision" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form role="form" action="{{route('backend.invitation.permission')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="id" type="hidden" class="form-control" id="id_id" readonly="readonly">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Permission Invitation</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="fullname" type="text" class="form-control" id="fullname_id" readonly="readonly">
                        <label for="fullname_id">Full Name</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="typecrew" type="text" class="form-control" id="typecrew_id" readonly="readonly">
                        <label for="typecrew_id">Type Crew</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="accountstatus" type="text" class="form-control" id="accountstatus_id" readonly="readonly">
                        <label for="accountstatus_id">Account Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="statuscrew" type="text" class="form-control" id="statuscrew_id" readonly="readonly">
                        <label for="statuscrew_id">Status Crew</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="invitedby" type="text" class="form-control" id="invitedby_id" readonly="readonly">
                        <label for="invitedby_id">Invited BY</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="companystatus" type="text" class="form-control" id="companystatus_id" readonly="readonly">
                        <label for="companystatus_id">Company Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <select id="status_id" name="status" class="form-control select2">
                            <option></option>
                           
                            <option value="APPROVE">Approve</option>
                            <option value="REJECT">Reject</option>

                            
                        </select>
                        <label for="status_id">Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.invitation.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [9] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                    if (data[6] == 'ADD'){
                        return '<a href="javascript:;" data-id="'+data[0]+'" data-fullname="'+data[1]+'" data-type_crew="'+data[2]+'" data-status_account="'+data[3]+'" data-status_activity="'+data[4]+'" data-invited_by="'+data[5]+'" data-companystatus="'+data[6]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>';
                    }else{
                        return '';
                    }
                }, targets: [9]

            },
            { render: function(data, type, row, meta) {
                    if (data == 'premium'){
                        return '<a href="javascript:;" class="btn yellow disabled uppercase">'+data+'</a>';
                    }else{
                        return '<a href="javascript:;" class="btn white disabled uppercase">'+data+'</a>';
                    }
                }, targets: [7]
            },
            { render: function(data, type, row, meta) {
                return '<a href="javascript:;" class="btn green disabled uppercase">'+data+'</a>'; }, targets: [5]
            },
            { render: function(data, type, row, meta) {
                if (data == 'premium'){
                    return '<a href="javascript:;" class="btn blue disabled uppercase">'+data+'</a>';
                }else{
                    return '<a href="javascript:;" class="btn white disabled uppercase">'+data+'</a>';
                }

                }, targets: [4]
            },
            { render: function(data, type, row, meta) {
                return '<img src="{{url('public/images/profile')}}/'+data+'" height="40px" class="zoomimg">'; }, targets: [2]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'fullname', name: 'fullname' },
            { data: 'photo', name: 'photo' },
            { data: 'type_crew', name: 'type_crew' },           
            { data: 'status_account', name: 'status_account' },           
            { data: 'status_activity', name: 'status_activity' },           
            { data: 'invited_by', name: 'invited_by' },           
            { data: 'companystatus', name: 'companystatus' },           
            { data: 'invited_status', name: 'invited_status' },           
            { data: 'id', name: 'id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });    

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var fullname = e.currentTarget.dataset.fullname;
        var type_crew = e.currentTarget.dataset.type_crew;
        var status_account = e.currentTarget.dataset.status_account;
        var status_activity = e.currentTarget.dataset.status_activity;
        var invited_by = e.currentTarget.dataset.invited_by;
        var companystatus = e.currentTarget.dataset.companystatus;

        $("#id_id").val(id);
        $("#fullname_id").val(fullname);
        $("#typecrew_id").val(type_crew);
        $("#accountstatus_id").val(status_account);
        $("#statuscrew_id").val(status_activity);
        $("#invitedby_id").val(invited_by);
        $("#companystatus_id").val(companystatus);
        $("#modalPermmision").modal('show');
        
    });

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
    
});
</script>
@endsection    