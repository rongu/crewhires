

@extends('layouts.frontend')

@section('title','Frequently Asked Questions')
@section('desc','Halaman ini menyatakan Frequently Asked Questions')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')


    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Frequently Asked Questions
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">

        <div class="page-content-inner">

            <div class="faq-page faq-content-1">

                <div class="faq-content-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">General</h2>
                                <div class="panel-group accordion faq-content" id="accordion1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1"> How do I vote or respond to a poll?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2"> Do you accept purchase orders?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3"> How many responses per poll (which plan) do I need?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4"> What if my audience does not have a phone or a web-enabled device with internet access?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_6"> How fast do responses show up?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_6" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">Technical</h2>
                                <div class="panel-group accordion faq-content" id="accordion3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> How do I vote or respond to a poll?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"> Do you accept purchase orders?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3"> How many responses per poll (which plan) do I need?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4"> What if my audience does not have a phone or a web-enabled device with internet access?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5"> How can I share my poll with remote participants?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_5" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6"> How fast do responses show up?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_6" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">Pricing</h2>
                                <div class="panel-group accordion faq-content" id="accordion2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1"> How much does it cost?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2"> Do you accept purchase orders?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3"> What is the K-12 classroom size promise?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_4"> What if my audience does not have a phone or a web-enabled device with internet access?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_5"> How can I share my poll with remote participants?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_5" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_6"> How fast do responses show up?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_6" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">Admin Management</h2>
                                <div class="panel-group accordion faq-content" id="accordion4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_1"> How do I vote or respond to a poll?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_2"> Do you accept purchase orders?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_3"> How many responses per poll (which plan) do I need?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_4"> What if my audience does not have a phone or a web-enabled device with internet access?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_5"> How can I share my poll with remote participants?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_4_5" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                    nesciunt laborum eiusmod. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    