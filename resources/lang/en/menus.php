<?php 
 
return [
    'home' => 'home',
    'categories' => 'categories',
    'membership' => 'membership',
    'service' => 'service',
    'social media' => 'social media',
	'login' => 'login',
	'register' => 'register',
	'dashboard' => 'dashboard',
];