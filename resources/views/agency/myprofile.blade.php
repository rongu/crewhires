

@extends('layouts.frontend')
@section('title','Profile '.$usr->name)

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
  
    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }


        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
        }
        .mt-repeaters .mt-repeater-item {
            border-bottom: 3px dashed #4c87b9;
            padding-bottom: 15px; 
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>My Profile | Account
                <small>user account page</small>
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>Agency</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="{{ asset('../../public/images/profile') }}/{{$usr->photo}}" class="img-responsive zoomimg" alt="" title="{{$usr->name}}" style="height: 150px;">  </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> {{$usr->name}} </div>
                                
                                <img src="{{$bnd}}" style="width: 70px;border: 1px solid #cdcdcd;">
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <!-- <button type="button" class="btn {{$inf->type_agency=='AGENCY'?'blue-ebonyclay':'grey-cararra'}}  btn-sm">AGENCY</button> -->
                                <!-- <button type="button" class="btn  {{$inf->type_agency=='SHIP OWNER'?'blue-ebonyclay':'grey-cararra'}} btn-sm">SHIP OWNER</button> -->
                                <button type="button" class="btn blue-ebonyclay btn-sm">{{$inf->type_agency}}</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
<!--                                 <ul class="nav">
        
                                    <li class="active">
                                        <a href="page_user_profile_1_account.html">
                                            <i class="fa fa-file-text-o"></i> Curriculume Vitae </a>
                                    </li>

                                </ul> -->
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                        <!-- PORTLET MAIN -->
                        <div class="portlet light ">
                            <!-- STAT -->
                            <div class="row list-separated profile-stat">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 1 </div>
                                    <div class="uppercase profile-stat-text"> JOB PUBLISH </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 100 </div>
                                    <div class="uppercase profile-stat-text"> Agent </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title"> 12500 </div>
                                    <div class="uppercase profile-stat-text"> Crew </div>
                                </div>
                            </div>
                            <!-- END STAT -->
                            <div>
                                <h3 class="profile-desc-title">Social Media</h3>
                                
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.facebook.com/">{{$inf->fb}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-instagram fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.instagram.com/">{{$inf->ig}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-send-o fa-sm font-blue-ebonyclay"></i>
                                    <a href="#">{{$inf->tg}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter fa-sm font-blue-ebonyclay"></i>
                                    <a href="http://www.twitter.com/">{{$inf->tw}}</a>
                                </div>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-whatsapp fa-sm font-blue-ebonyclay"></i>
                                    <a href="#">{{$inf->wa}}</a>
                                </div>                                
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">

                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_6" data-toggle="tab">Information</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_4" data-toggle="tab">Account</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_3" data-toggle="tab">Jobs Form</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_7" data-toggle="tab">Portofolio & DOC</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_5" data-toggle="tab">Social Media</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane active" id="tab_1_1">
                                                <form id="submit_form_profile" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="profile">

                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="companyname" type="text" class="form-control" id="companyname_id" placeholder="Enter your company name" value="{{$inf->companyname}}">
                                                            <label for="companyname_id">Company Name</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
       
                                                        <div class="form-group form-md-line-input has-success">
                                                            <div class="input-group">
                                                                <input name="email" type="text" class="form-control" id="email_id" placeholder="Enter your Email Address" value="{{$usr->email}}">
                                                                <label for="email_id">Email</label>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                            </div>
                                                        </div>


                                                        <div class="form-group form-md-line-input has-success">
                                                            <label for="phone_id">Phone Number</label>
                                                            <div class="input-group">
                                                                <span class="input-group-btn btn-left">
                                                                    <select name="phone_code" id="phone_code_id" class="form-control" data-placeholder="" width="150px">
                                                                        <option></option>
                                                                        @foreach(json_decode($calling, true) as $key => $value)
                                                                        <option value="{{$value['calling_code']}}"  {{$value['calling_code']==$usr->phone_code?'SELECTED':''}}>+{{$value['calling_code']}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </span>
                                                                <div class="input-group-control input-group">
                                                                    <input name="phone" type="text" class="form-control" id="phone_id" placeholder="Enter your Phone Number" value="{{$usr->phone}}">
                                                                    <!-- <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span> -->
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
 
                                                        <div class="form-group form-md-line-input has-success">
                                                            <textarea class="form-control" name="address" rows="3">{{$inf->address}}</textarea>
                                                            <label for="form_control_1">Address</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <input name="city" type="text" class="form-control" id="city_id" placeholder="Enter your City" value="{{$inf->city}}">
                                                            <label for="city_id">City/Town</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success">
                                                            <select name="country" id="country_list" class="form-control" data-placeholder="">
                                                                <option value=""></option>
                                                                @foreach(json_decode($cty, true) as $key => $value)
                                                                    <option value="{{$value['country']}}" {{$value['country']==$inf->country?'SELECTED':''}}>{{$value['country']}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="country_list">Country</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                            <div class="tab-pane" id="tab_1_6">
                                            
                                                <form id="submit_form_info" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="info">
                                                    <input type="hidden" name="type_agency" value="{{$inf->type_agency}}">

                                                    <!-- <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <select id="type_agency_id" class="form-control">
                                                                <option value=""></option>
                                                                <option value="AGENCY" {{$inf->type_agency=='AGENCY'?'selected':''}}>AGENCY</option>
                                                                <option value="SHIP OWNER" {{$inf->type_agency=='SHIP OWNER'?'selected':''}}>SHIP OWNER</option>
                                                            </select>
                                                            <label for="name_id">Type Agency</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div> -->

                                                    <div class="form-group form-md-line-input has-success">
                                                        <input name="pic" type="text" class="form-control" id="pic_id" placeholder="Enter your pic" value="{{$inf->pic}}">
                                                        <label for="pic_id">PIC</label>
                                                        <span class="help-block">Some help goes here...</span>
                                                    </div>

<!--                                                     <div class="form-group form-md-line-input has-success">
                                                        <input name="pic_phone" type="text" class="form-control" id="pic_phone_id" placeholder="Enter your pic phone" value="{{$inf->pic_phone}}">
                                                        <label for="city_id">PIC Phone</label>
                                                        <span class="help-block">Some help goes here...</span>
                                                    </div> -->
                                                    
                                                    <div class="form-group form-md-line-input has-success">
                                                        <label for="phone_id">PIC Phone</label>
                                                        <div class="input-group">
                                                            <span class="input-group-btn btn-left">
                                                                <select name="pic_phone_code" id="pic_phone_code_id" class="form-control" data-placeholder="" width="150px">
                                                                    <option></option>
                                                                    @foreach(json_decode($calling, true) as $key => $value)
                                                                    <option value="{{$value['calling_code']}}"  {{$value['calling_code']==$inf->pic_phone_code?'SELECTED':''}}>+{{$value['calling_code']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </span>
                                                            <div class="input-group-control input-group">
                                                                <input name="pic_phone" type="text" class="form-control" id="pic_phone_id" placeholder="Enter your pic phone" value="{{$inf->pic_phone}}">
                                                                <!-- <span class="input-group-addon">
                                                                    <i class="fa fa-phone"></i>
                                                                </span> -->
                                                            </div>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>




                                            </div>
                                            <!-- END PERSONAL INFO TAB -->
                                            <!-- CHANGE AVATAR TAB -->
                                            <div class="tab-pane" id="tab_1_4">
                                                <form id="submit_form_account" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="account">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" name="name" class="form-control" id="name_id" placeholder="Enter your name" value="{{$usr->name}}">
                                                            <label for="name_id">Username</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="password" class="form-control" id="password_id" placeholder="Enter your password" name="password">
                                                            <label for="password_id">New Password</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="password" class="form-control" id="repassword_id" placeholder="Enter your password" name="repassword">
                                                            <label for="repassword_id">Re-type New Password</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="tab_1_2">
                                                
                                                <form id="submit_form_photo" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="photo">
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                                                @if($usr->photo)
                                                                <img src="{{asset('../../public/images/profile')}}/{{$usr->photo}}" alt="" class="zoomimg" title="Photo Profile {{$usr->name}}"  /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>   
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                         
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END CHANGE AVATAR TAB -->
                                            <!-- CHANGE PASSWORD TAB -->
                                            <div class="tab-pane" id="tab_1_3">

                                                    <div class="form-body" style="background-color: #ffffff; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <label class="control-label font-blue-madison bold uppercase" style="text-align: left;">Jobs Form

                                                            </label>
                                                                @foreach($job as $kee)
                                                                
                                                                <form role="form" action="{{route('profile.update.agency.byid')}}" method="post" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{$kee->id}}">
                                                                <div class="form-group mt-repeaters" style="margin-left:0px;margin-right:0px;">
                                                                    <div data-repeater-list="jlx">
                                                                        <div data-repeater-item class="mt-repeater-item">
                                                                            <div class="row mt-repeater-row">
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Position</label>
                                                                                    <input name="jlx_po" type="text" class="form-control" value="{{$kee->position}}" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Type Vessel</label>
                                                                                    <input name="jlx_ty" type="text" class="form-control" value="{{$kee->types}}" /> </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Engine</label>
                                                                                    <input name="jlx_en" type="text" class="form-control" value="{{$kee->engine}}" /> </div>
                                                                                <div class="col-md-12">
                                                                                    <label class="control-label">Requirement</label>
                                                                                    <!-- <input name="jlx_re" type="text" class="form-control" />  -->
                                                                                    <textarea class="wysihtml5x form-control" rows="6" name="jlx_re">{{$kee->requirement}}</textarea>
                                                                                </div>              
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label">Duration Recruit</label>
                                                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                    <input name="jlx_open" type="text" class="form-control" value="{{Carbon::parse($kee->date_open)->format('m/d/Y')}}">
                                                                                    <span class="input-group-addon"> to </span>
                                                                                    <input name="jlx_close" type="text" class="form-control" value="{{Carbon::parse($kee->date_close)->format('m/d/Y')}}"></div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <a href="{{route('profile.delete.job.agency',$kee->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-icon-only default">
                                                                                        <i class="fa fa-times-circle fa-sm"></i>
                                                                                    </a>
                                                                                    <button type="submit" class="btn btn-icon-only default"><i class="fa fa-floppy-o fa-sm"></i></button>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                </form>
                                                                @endforeach
                                                        </div>
                                                    </div>

                                                    <form id="submit_form_job" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="joblist">

                                                    <div class="form-body" style="background-color: #ffffff; padding-left: 5px;">
                                                        <div class="form-group form-md-line-input has-success">
                                                                
                                                            <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                                                <div data-repeater-list="jl">
                                                                    <div data-repeater-item class="mt-repeater-item">
                                                                        <div class="row mt-repeater-row">
                                                                            <div class="col-md-4">
                                                                                <label class="control-label">Position</label>
                                                                                <input name="jl_po" type="text" class="form-control" /> </div>
                                                                            <div class="col-md-4">
                                                                                <label class="control-label">Type Vessel</label>
                                                                                <input name="jl_ty" type="text" class="form-control" /> </div>
                                                                            <div class="col-md-4">
                                                                                <label class="control-label">Engine</label>
                                                                                <input name="jl_en" type="text" class="form-control" /> </div>
                                                                            <div class="col-md-12">
                                                                                <label class="control-label">Requirement</label>
                                                                                <!-- <input name="jl_re" type="text" class="form-control" />  -->
                                                                                <textarea class="wysihtml5 form-control" rows="6" name="jl_re"></textarea>
                                                                            </div>              
                                                                            <div class="col-md-4">
                                                                                <label class="control-label">Duration Recruit</label>
                                                                                <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                <input name="jl_open" type="text" class="form-control">
                                                                                <span class="input-group-addon"> to </span>
                                                                                <input name="jl_close" type="text" class="form-control"></div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                    <i class="fa fa-close"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                                    <i class="fa fa-plus"></i> Add </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="tab-pane" id="tab_1_7">
                                                <form id="submit_form_portofolio" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="portofolio">
                                                    
       
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                                                @if($inf->portofolio)
                                                                <img src="{{asset('../../public/images/portofolio')}}/{{$inf->portofolio}}" alt="" class="zoomimg" title="Portofolio {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>  
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                          
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file_portofolio"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                                                @if($inf->document)
                                                                <img src="{{asset('../../public/images/document')}}/{{$inf->document}}" alt="" class="zoomimg" title="Document {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>        
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                    
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file_doc"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                                                @if($inf->siup)
                                                                <img src="{{asset('../../public/images/siup')}}/{{$inf->siup}}" alt="" class="zoomimg" title="SIUP {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>        
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                    
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file_siup"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="formsiuppak_id" class="form-group {{$inf->type_agency=='SHIP OWNER'?'hidden':''}}">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                                                @if($inf->siuppak)
                                                                <img src="{{asset('../../public/images/siuppak')}}/{{$inf->siuppak}}" alt="" class="zoomimg" title="SIUPPAK {{$usr->name}}" /> 
                                                                @else
                                                                <img src="{{asset('../../public/images/no_image.png')}}" alt="" class="zoomimg" />
                                                                @endif
                                                            </div>        
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                    
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="file_siuppak"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="tab-pane" id="tab_1_5">
                                                <form id="submit_form_sosmed" role="form" action="{{route('profile.update.agency')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form_t" value="sosmed">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="fb_id" placeholder="Facebook" name="fb" value="{{$inf->fb}}">
                                                            <label for="fb_id">Facebook</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="ig_id" placeholder="Instagram" name="ig" value="{{$inf->ig}}">
                                                            <label for="ig_id">Instagram</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="tele_id" placeholder="Telegram" name="tele" value="{{$inf->tg}}">
                                                            <label for="tele_id">Telegram</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="tw_id" placeholder="Twitter" name="tw" value="{{$inf->tw}}">
                                                            <label for="tw_id">Twitter</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <input type="text" class="form-control" id="wa_id" placeholder="WhatsApp"  name="wa" value="{{$inf->wa}}">
                                                            <label for="wa_id">WhatsApp</label>
                                                            <span class="help-block">Some help goes here...</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn green">Submit</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }

    $('.wysihtml5x').summernote({height: 300});

    $("#country_list").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });

    $("#phone_code_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: '110px', 
        
        escapeMarkup: function (m) {
            return m;
        }
    });    

    $("#pic_phone_code_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: '110px', 
        
        escapeMarkup: function (m) {
            return m;
        }
    });



    var form_p = $('#submit_form_profile');
    var error = $('.alert-danger', form_p);
    var success = $('.alert-success', form_p);

    form_p.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            companyname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            
            address: {
                required: true
            },
            city: {
                required: true
            },
            country: {
                required: true
            },

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_p[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });


    var form_a = $('#submit_form_account');
    var error = $('.alert-danger', form_a);
    var success = $('.alert-success', form_a);

    form_a.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            name: {
                required: true
            },
            repassword: {
                minlength: 5,
                equalTo: "#password_id"
            },

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_a[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    var form_in = $('#submit_form_info');
    var error = $('.alert-danger', form_in);
    var success = $('.alert-success', form_in);

    form_in.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            name: {
                required: true
            },
            repassword: {
                minlength: 5,
                equalTo: "#password_id"
            },

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_in[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    var form_s = $('#submit_form_sosmed');
    var error = $('.alert-danger', form_s);
    var success = $('.alert-success', form_s);

    form_s.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_s[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    var form_dc = $('#submit_form_document');
    var error = $('.alert-danger', form_dc);
    var success = $('.alert-success', form_dc);

    form_dc.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_dc[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    $('.mt-repeater').each(function(){
        $(this).repeater({
            show: function () {

                $(this).slideDown();  
                $(".date-picker").datepicker();

                $('.wysihtml5').summernote({height: 300});
                

            },

            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },

            ready: function (setIndexes) {
                $('.wysihtml5').summernote({height: 300});
            }

        });
    });

    $('#phone_code_id', form_p).change(function () {
        form_p.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });    

    $('#pic_phone_code_id', form_in).change(function () {
        form_in.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });


    ezoom.onInit($('.zoomimg'), {
        hideControlBtn: true,
        onClose: function (result) {
            console.log(result);
        },
        onRotate: function (result) {
            console.log(result);
        },
    });

});
</script>
@endsection    