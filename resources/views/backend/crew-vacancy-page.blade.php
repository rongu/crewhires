@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Crew Vacancy</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Crew Vacancy
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Agency/Ship Owner</th>
                            <th>Status Company</th>
                            <th>Position</th>
                            <th>Types Vacancy</th>
                            <th>Engine Vacancy</th>
                            <th>Date Open</th>
                            <th>Date Close</th>
                            <th>Number of Applicants</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>


<div id="modalDetail" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form role="form" action="{{route('backend.invitation.permission')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="id" type="hidden" class="form-control" id="id_id" readonly="readonly">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Crew Vacancy</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="companyname" type="text" class="form-control" id="companyname_id" readonly="readonly">
                        <label for="companyname_id">Agency / Ship Owners</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="companystatus" type="text" class="form-control" id="companystatus_id" readonly="readonly">
                        <label for="companystatus_id">Agency / Ship Owners Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="position" type="text" class="form-control" id="position_id" readonly="readonly">
                        <label for="position_id">Position Vacancy</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="types" type="text" class="form-control" id="types_id" readonly="readonly">
                        <label for="types_id">Types Vacacny</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="engine" type="text" class="form-control" id="engine_id" readonly="readonly">
                        <label for="engine_id">Engine Vacancy</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="date_open" type="text" class="form-control" id="date_open_id" readonly="readonly">
                        <label for="date_open_id">Date Open</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <input name="date_close" type="text" class="form-control" id="date_close_id" readonly="readonly">
                        <label for="date_close_id">Date Close</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                
                    <div class="form-group form-md-line-input has-success">
                        <!-- <input name="deskription" type="text" class="form-control" id="requirement_id" readonly="readonly"> -->
                        <label for="deskription_id">Deskription Vacancy</label>
                        <span id="requirement_id"></span>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

            </div>
            <hr>

            <div class="portlet light " style="box-shadow:none;">

                <div class="portlet-body">
                    <table class="table table-striped  dt-responsive table-hover table-light" id="sample_2">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Crew Name</th>
                            <th>Type Crew</th>
                            <th>Gender</th>
                            <th>Birth</th>
                            <th>Status Crew</th>
                            <th>Status Akun</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.crew.vacancy.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [9] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                return '<a href="javascript:;" data-id="'+data[0]+'" data-companyname="'+data[1]+'" data-position="'+data[2]+'" data-types="'+data[3]+'" data-engine="'+data[4]+'" data-date_open="'+data[5]+'" data-date_close="'+data[6]+'" data-requirement="'+data[7]+'" data-status_account="'+data[8]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-search"></i></a>'; }, targets: [9]

            },
            { render: function(data, type, row, meta) {
                    if (data == 'premium'){
                        return '<a href="javascript:;" class="btn yellow disabled uppercase">'+data+'</a>';
                    }else{
                        return '<a href="javascript:;" class="btn white disabled uppercase">'+data+'</a>';
                    }
                }, targets: [2]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'companyname', name: 'companyname' },
            { data: 'status_account', name: 'status_account' },
            { data: 'position', name: 'position' },
            { data: 'types', name: 'types' },           
            { data: 'engine', name: 'engine' },           
            { data: 'date_open', name: 'date_open' },           
            { data: 'date_close', name: 'date_close' },           
            { data: 'jumlah', name: 'jumlah' },           
            { data: 'id', name: 'id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });    

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var companyname = e.currentTarget.dataset.companyname;
        var position = e.currentTarget.dataset.position;
        var types = e.currentTarget.dataset.types;
        var engine = e.currentTarget.dataset.engine;
        var date_open = e.currentTarget.dataset.date_open;
        var date_close = e.currentTarget.dataset.date_close;
        var requirement = e.currentTarget.dataset.requirement;
        var status_account = e.currentTarget.dataset.status_account;

        $("#id_id").val(id);
        $("#companyname_id").val(companyname);
        $("#position_id").val(position);
        $("#types_id").val(types);
        $("#engine_id").val(engine);
        $("#date_open_id").val(date_open);
        $("#date_close_id").val(date_close);
        $("#requirement_id").html(requirement);
        $("#companystatus_id").val(status_account);
        $("#modalDetail").modal('show');


        var table2 = $('#sample_2');

        table2.DataTable().clear().destroy();

        var oTable2 = table2.dataTable({
            
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        serverSide: true,
        ajax: {
            url: "{{route('backend.crew.vacancy.apply.list')}}",
            type: "POST",
            data: {id:id,"_token": "{{ csrf_token() }}"},                
        },

        //scrollY:        400,
        deferRender:    true,
        //scroller:       true,
        deferRender:    true,
        //scrollX:        true,
        // scrollCollapse: true,        
        responsive:     false,  
        stateSave:      false,
        retrieve : true,
        columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'fullname', name: 'fullname' },
                { data: 'type_crew', name: 'type_crew' },
                { data: 'gender', name: 'gender' },           
                { data: 'birth', name: 'birth' },
                { data: 'status_activity', name: 'status_activity' },
                { data: 'status_account', name: 'status_account' }
            ],
        // "order": [
        //     [0, 'asc']
        // ],

        lengthMenu: [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        pageLength: 10,

        dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        });   

        
        
    }); 

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
    
});
</script>
@endsection    