

@extends('layouts.frontend')

@section('title','Term And Condition')
@section('desc','Halaman ini menyatakan syarat penggunaan ("Ketentuan") di mana Anda ("Anda") dapat menggunakan Situs Crewhires.com, dan hubungan Anda dengan CrewhiresEnn Indonesia, PT ("crewhires.com", "kami") Harap baca dengan cermat karena hal itu memengaruhi hak dan kewajiban Anda di bawah hukum. Jika Anda tidak menyetujui Ketentuan ini, mohon untuk tidak mendaftar atau menggunakan Situs Crewhires.com. Ketentuan ini berlaku mulai…..-…..-2021. Jika Anda memiliki pertanyaan tentang Ketentuan, silakan hubungi kami.')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')



    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>
                {{ __('tnc.title')}}
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->

        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="portlet light portlet-fit ">

                <div class="portlet-body">
                    
                    <div class="search-page search-content-2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-container ">
                                    <ul class="search-container">
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <p class="search-desc">{{ __('tnc.header')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">A. {{ __('tnc.termA')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailA')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">B. {{ __('tnc.termB')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailB')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">C. {{ __('tnc.termC')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailC')}}<br>- {{ __('tnc.detailC1')}}<br>- {{ __('tnc.detailC2')}}<br>- {{ __('tnc.detailC3')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">D. {{ __('tnc.termD')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailD')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">E. {{ __('tnc.termE')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailE')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">F. {{ __('tnc.termF')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailF')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">G. {{ __('tnc.termG')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailG')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">H. {{ __('tnc.termH')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailH')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">I. {{ __('tnc.termI')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailI')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">J. {{ __('tnc.termJ')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailJ')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">K. {{ __('tnc.termK')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailK')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">L. {{ __('tnc.termL')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailL')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">M. {{ __('tnc.termM')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailM')}}</p>
                                            </div>
                                        </li>
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">N. {{ __('tnc.termN')}}</a>
                                                </h2>
                                                <p class="search-desc">{{ __('tnc.detailN')}}</p>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    