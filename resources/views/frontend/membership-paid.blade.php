

@extends('layouts.frontend')

@section('title','Membership')
@section('desc','Halaman ini menyatakan membership')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Membership Verification
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->

        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="portlet light portlet-fit ">

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="membership_payment" role="form" action="{{route('membership.payment')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                
                                @if($act=='add')

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <select id="inv_mem_id" name="inv_mem" class="form-control select2">
                                            <option></option>
                                            @foreach($mm as $vv)
                                            <option value="{{$vv->id}}">{{$vv->code_member}}</option>
                                            @endforeach
                                        </select>
                                        <label for="inv_mem_id">{{ __('payment.Invoice Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <select id="rek_to_id" name="rek_to" class="form-control select2">
                                            <option></option>
                                            
                                            <option value="BNI - xxxxxxxxxxxx">BNI - xxxxxxxxxxxx</option>
                                            <option value="BCA - xxxxxxxxxxxx">BCA - xxxxxxxxxx</option>
                                           
                                        </select>
                                        <label for="rek_to_id">{{ __('payment.To Account Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group form-md-line-input has-success">
                                        <label for="rek_to_id">{{ __('payment.Proof of Payment')}}</label>
                                    </div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">

                                        <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                            <!-- <img src="{{url('public/images/inv_mem')}}/" alt="" class="zoomimg" />  -->
                                            
                                            <img src="{{url('public/images/no_image.png')}}" alt="" class="zoomimg" />
                                            
                                        </div>   
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                         
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="file"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>

                                    </div>

                                </div>

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <input name="account_number" type="text" class="form-control" id="account_number_id" >
                                        <label for="account_number_id">{{ __('payment.Account Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <input name="bau_name" type="text" class="form-control" id="bau_name_id" >
                                        <label for="bau_name_id">{{ __('payment.Bank Account Under the Name')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                @else

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <select id="inv_mem_id" name="inv_mem" class="form-control select2">
                                            <option></option>                                            
                                            <option value="{{$mm->id}}" selected="selected">{{$mm->code_member}}</option>                                           
                                        </select>
                                        <label for="inv_mem_id">{{ __('payment.Invoice Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <select id="rek_to_id" name="rek_to" class="form-control select2">
                                            <option></option>
                                           
                                            <option value="BNI - xxxxxxxxxxxx" {{$mm->bank_account=='BNI - xxxxxxxxxxxx'?'selected':''}}>BNI - xxxxxxxxxxxx</option>
                                            <option value="BCA - xxxxxxxxxxxx" {{$mm->bank_account=='BCA - xxxxxxxxxxxx'?'selected':''}}>BCA - xxxxxxxxxx</option>
                                            
                                        </select>
                                        <label for="rek_to_id">{{ __('payment.To Account Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group form-md-line-input has-success">
                                        <label for="rek_to_id">{{ __('payment.Proof of Payment')}}</label>
                                    </div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">

                                        <div class="fileinput-new thumbnail"  style="width: 200px; height: 150px;">
                                            <!-- <img src="{{url('public/images/inv_mem')}}/" alt="" class="zoomimg" />  -->
                                            @if($mm->photo_receipt)
                                                <img src="{{url('public/images/inv_member')}}/{{$mm->photo_receipt}}" alt="" class="zoomimg" />
                                            @else
                                                
                                                <img src="{{url('public/images/no_image.png')}}" alt="" class="zoomimg" />
                                            @endif
                                            
                                            
                                        </div>   
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>                                                         
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="file"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>

                                    </div>

                                </div>

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <input name="account_number" type="text" class="form-control" id="account_number_id"  value="{{$mm->account_number}}">
                                        <label for="account_number_id">{{ __('payment.Account Number')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group form-md-line-input has-success">
                                        <input name="bau_name" type="text" class="form-control" id="bau_name_id" value="{{$mm->bau_name}}">
                                        <label for="bau_name_id">{{ __('payment.Bank Account Under the Name')}}</label>
                                        <span class="help-block">Some help goes here...</span>
                                    </div>
                                </div>

                                @endif

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn green">Submit</button> 
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    var form_p = $('#membership_payment');
    var error = $('.alert-danger', form_p);
    var success = $('.alert-success', form_p);

    form_p.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            inv_mem: {
                required: true
            },
            rek_to: {
                required: true
            },
            account_number: {
                required: true
            },
            bau_name: {
                required: true
            }

            
        },


        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else if (element.hasClass("select2-hidden-accessible")) {
               error.insertAfter($("#select2-" + element.attr("id") + "-container").parent());
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            form_p[0].submit();
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }

    });

    $("#inv_mem_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });

    $("#rek_to_id").select2({
        placeholder: "Select",
        allowClear: true,

        width: 'auto', 
        escapeMarkup: function (m) {
            return m;
        }
    });

    ezoom.onInit($('.zoomimg'), {
        hideControlBtn: true,
        onClose: function (result) {
            console.log(result);
        },
        onRotate: function (result) {
            console.log(result);
        },
    });
});
</script>
@endsection    