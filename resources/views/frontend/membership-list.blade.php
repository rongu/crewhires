

@extends('layouts.frontend')

@section('title','Membership')
@section('desc','Halaman ini menyatakan membership')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inherit;
            box-shadow: inset inherit;
            background-color: inherit;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            color: #555;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            outline: 0;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .mt-repeater .mt-repeater-item {
            border-bottom: 0px solid #ddd;
            padding-bottom: 15px;
            margin-bottom: 15px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Payment List
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->

        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="portlet light portlet-fit ">

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">

                            @php $i=1; @endphp
                            @foreach($mm as $vk)

                            <div class="form-body" style="padding-left: 5px;">
                                <div class="form-group form-md-line-input has-success">
                                        
                                    <div class="form-group mt-repeater" style="margin-left:0px;margin-right:0px;">
                                        <div data-repeater-list="jl">
                                            <div data-repeater-item class="mt-repeater-item">
                                                <div class="row mt-repeater-row">
                                                    <div class="col-md-1">
                                                        <label class="control-label">{{$i}}</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label class="control-label">{{ __('payment.Invoice Number')}}</label>
                                                        <input type="text" class="form-control" value="{{$vk->code_member}}" readonly="readonly" /> </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">Date</label>
                                                        <input type="text" class="form-control" value="{{Carbon::parse($vk->date_add)->format('m/d/Y')}}" readonly="readonly"/> </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">&nbsp;</label>
                                                        <a href="javascript:;"class="btn btn-success btn-block uppercase">
                                                            {{$vk->status}}
                                                        </a>
                                                    </div><div class="col-md-1">
                                                        <label class="control-label">&nbsp;</label>
                                                        @if($vk->photo_receipt)
                                                            <img src="{{url('public/images/inv_member')}}/{{$vk->photo_receipt}}" width="50px" class="user-pic zoomimg" style="display: block;" />
                                                        @else
                                                            <img src="{{url('public/images/no_image.png')}}" width="50px" class="user-pic zoomimg" style="display: block;" />
                                                        @endif
                                                        

                                                    </div>
                                                    <div class="col-md-1">
                                                        <label class="control-label">&nbsp;</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">{{ __('payment.Payment Verification Date')}}</label>
                                                        <input type="text" class="form-control" value="{{$vk->date_paid==null?'-':Carbon::parse($vk->date_paid)->format('m/d/Y')}}" readonly="readonly"/> </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">{{ __('payment.To Account Number')}}</label>
                                                        <input type="text" class="form-control" value="{{$vk->bank_account}}" readonly="readonly"/> </div>

                                                    <div class="col-md-3">
                                                        <label class="control-label">{{ __('payment.Bank Account Under the Name')}}</label>
                                                        <input type="text" class="form-control" value="{{$vk->bau_name}}" readonly="readonly"/> </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">{{ __('payment.Account Number')}}</label>
                                                        <input type="text" class="form-control" value="{{$vk->account_number}}" readonly="readonly"/> </div>

                                                    
                                                    @if($vk->status=='verification')
                                                         <div class="col-md-1">
                                                            <div class="btn-group btn-group-sm btn-group-solid mt-repeater-delete">
                                                                 <a href="{{route('payment.delete',$vk->id)}}" class="btn btn-sm btn-danger">
                                                                    <i class="fa fa-close fa-sm"></i>
                                                                </a>
                                                                <a href="{{route('payment.edit',$vk->id)}}" class="btn btn-sm btn-warning">
                                                                    <i class="fa fa-pencil fa-sm"></i>
                                                                </a>
                                                            </div>
                                                           
                                                        </div>
                                                    @elseif($vk->status=='done')
                                                        <div class="col-md-1">
                                                            <a href="javascript:;" class="btn btn-success mt-repeater-delete">
                                                                <i class="fa fa-check"></i>
                                                            </a>
                                                        </div>
                                                    @elseif($vk->status=='cancel')
                                                        <div class="col-md-1">
                                                            <a href="{{route('payment.delete',$vk->id)}}" class="btn btn-danger mt-repeater-delete">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                    @elseif($vk->status=='pay')
                                                        <div class="col-md-1">
                                                            <a href="{{route('payment.edit',$vk->id)}}" class="btn btn-warning mt-repeater-delete">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </div>
                                                    @endif

                                                    
                                                   


                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            @php $i++; @endphp
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    ezoom.onInit($('.zoomimg'), {
        hideControlBtn: true,
        onClose: function (result) {
            console.log(result);
        },
        onRotate: function (result) {
            console.log(result);
        },
    });
    
});
</script>
@endsection    