<?php 
 
return [
    'p1' => 'Welcome to www.crewhires.com. As a center for providing information on special job vacancies for seafarers and hotel crews in a professional manner based on a website, we act as facilitators of matching and communication of job opportunities between marine job seekers and shipping companies / ship owners / ship crew agencies both those that will operate in Indonesian waters and in the sea. outside Indonesia. We also act as media services for managing seafarers documents needed by the online system.',
    'p2' => 'Starting from working professionals in several leading shipping companies for many years, www.crewhires.com was founded in 2020 by a team of professionals who are qualified and competent in their fields to ensure that all members who join this portal get the maximum benefit.',
];