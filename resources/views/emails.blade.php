@component('mail::message')

Hello **{{$name}}**,<br>
Attention! Your new password and confirmation password do not match. Please confirm and try again.<br>

New Password : {{$psd}}<br>

@lang('Regards'),<br>
{{ config('app.name') }}