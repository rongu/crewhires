

@extends('layouts.frontend')

@section('title','Membership')
@section('desc','Halaman ini menyatakan membership')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')
    

    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Membership
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->

        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-speech"></i>
                                                            <span class="caption-subject bold uppercase"> Crew Membership</span>
                                                        </div>
                                                        
                                                    </div>
                <div class="portlet-body">
                    <div class="pricing-content-1">
                        <div class="row">
                            <div class="col-md-3"> &nbsp;
                            </div>
                            <div class="col-md-3">
                                <div class="price-column-container border-active">
                                    <div class="price-table-head bg-blue">
                                        <h2 class="no-margin">FREE</h2>
                                    </div>
                                    <div class="arrow-down border-top-blue"></div>
                                    <div class="price-table-pricing">
                                        <h3>
                                            <sup class="price-sign">$</sup>0</h3>
                                        <p>per month</p>
                                    </div>
                                    <div class="price-table-content">
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-list"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">10 LIST SUGGESTION JOB</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">NOT GET REQUIRMENT JOB</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-dot-circle-o"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">1 DAY 2 APPLY JOB</div>
                                        </div>
                                        

                                    </div>
                                    <div class="arrow-down arrow-grey"></div>
                                    <div class="price-table-footer">
                                        <button type="button" class="btn grey-salsa btn-outline sbold uppercase price-button">FREE</button>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-3">
                                <div class="price-column-container border-active">
                                    <div class="price-table-head bg-purple">
                                        <h2 class="no-margin">Premium</h2>
                                    </div>
                                    <div class="arrow-down border-top-purple"></div>
                                    <div class="price-table-pricing">
                                        <h3>
                                            <sup class="price-sign">$</sup>2</h3>
                                        <p>per month</p>
                                    </div>
                                    <div class="price-table-content">
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-list"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">UNLIMITED LIST SUGGESTION JOB</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">GET REQUIRMENT JOB</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-dot-circle-o"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">UNLIMITED APPLY JOB</div>
                                        </div>
                                        
                                    </div>
                                    <div class="arrow-down arrow-grey"></div>
                                    <div class="price-table-footer">
                                        <a type="button" href="https://site.crewhires.com/membership-activation" class="btn grey-salsa btn-outline price-button sbold uppercase">Get It NOW!</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3"> &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

<div class="page-content-inner">

            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-speech"></i>
                                                            <span class="caption-subject bold uppercase"> Ship Owner/Agency Membership</span>
                                                        </div>
                                                        
                                                    </div>
                <div class="portlet-body">
                    <div class="pricing-content-1">
                        <div class="row">
                            <div class="col-md-3"> &nbsp;
                            </div>
                            <div class="col-md-3">
                                <div class="price-column-container border-active">
                                    <div class="price-table-head bg-blue">
                                        <h2 class="no-margin">FREE</h2>
                                    </div>
                                    <div class="arrow-down border-top-blue"></div>
                                    <div class="price-table-pricing">
                                        <h3>
                                            <sup class="price-sign">$</sup>0</h3>
                                        <p>per month</p>
                                    </div>
                                    <div class="price-table-content">
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-list"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">VIEW LIST CREW ONLY</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">CANNOT INVITE CREW</div>
                                        </div>
                                        

                                    </div>
                                    <div class="arrow-down arrow-grey"></div>
                                    <div class="price-table-footer">
                                        <button type="button" class="btn grey-salsa btn-outline sbold uppercase price-button">FREE</button>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-3">
                                <div class="price-column-container border-active">
                                    <div class="price-table-head bg-purple">
                                        <h2 class="no-margin">Premium</h2>
                                    </div>
                                    <div class="arrow-down border-top-purple"></div>
                                    <div class="price-table-pricing">
                                        <h3>
                                            <sup class="price-sign">$</sup>40</h3>
                                        <p>per month</p>
                                    </div>
                                    <div class="price-table-content">
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-list"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">VIEW DETAIL CREW</div>
                                        </div>
                                        <div class="row mobile-padding">
                                            <div class="col-xs-3 text-right mobile-padding">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="col-xs-9 text-left mobile-padding">CAN INVITE CREW</div>
                                        </div>
                                        
                                    </div>
                                    <div class="arrow-down arrow-grey"></div>
                                    <div class="price-table-footer">
                                        <a type="button" href="https://site.crewhires.com/membership-activation" class="btn grey-salsa btn-outline price-button sbold uppercase">Get It NOW!</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3"> &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    