

@extends('layouts.frontend')

@section('title','Service')
@section('desc','Halaman ini menyatakan Service')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')



    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }
        .mt-widget-4 {
            color: #fff;
            margin-bottom: 25px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Service
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">

        <div class="page-content-inner">

            <div class="row">
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/visa.jpg') }}" /> </div>
                        <div class="mt-container bg-purple-opacity">
                            <div class="mt-head-title"> Visa </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','visa')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/passport.jpg') }}" /> </div>
                        <div class="mt-container bg-green-opacity">
                            <div class="mt-head-title"> Passport </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','passport')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/seamanbook.jpg') }}" /> </div>
                        <div class="mt-container bg-red-opacity">
                            <div class="mt-head-title"> Seaman Book </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','seamanbook')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ///////////////////////////////////////////////////// -->
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/certificaterevalidation.jpg') }}" /> </div>
                        <div class="mt-container bg-yellow-soft-opacity">
                            <div class="mt-head-title"> Certificate Revalidation </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','certificaterevalidation')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/trainingcenter.jpg') }}" /> </div>
                        <div class="mt-container bg-purple-soft-opacity">
                            <div class="mt-head-title"> Training Center </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','trainingcenter')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/accomodation.jpg') }}" /> </div>
                        <div class="mt-container bg-green-soft-opacity">
                            <div class="mt-head-title"> Accommodation </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','accommodation')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ...................................... -->
                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/payroll.jpg') }}" /> </div>
                        <div class="mt-container bg-blue-soft-opacity">
                            <div class="mt-head-title"> Payroll </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','payroll')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mt-widget-4">
                        <div class="mt-img-container">
                            <img src="{{ url('public/images/service/bunker.jpg') }}" /> </div>
                        <div class="mt-container bg-green-jungle-opacity">
                            <div class="mt-head-title"> Bunker </div>
                            <div class="mt-body-icons">
                                &nbsp;
                            </div>
                            <div class="mt-footer-button">
                                <a href="{{route('service.req','bunker')}}" class="btn btn-circle bg-blue-hoki btn-sm dark"> Request </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    