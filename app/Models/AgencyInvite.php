<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyInvite extends Model
{
    use HasFactory;
    protected $table = 'agency_invite';
}
