@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{ session('success') }}
        </div>
        @endif
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Vacancy</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Crew Recommendation</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Crew Recommendation
</h1>
<div class="page-toolbar">
    <!-- BEGIN THEME PANEL -->
    <div class="btn-group btn-theme-panel">
        <a class="btn btn-lg bold" data-toggle="modal" href="#basic">
            <i class="fa fa-plus-circle" style="color: #32c5d2"></i> Add
        </a>                
    </div>
    <!-- END THEME PANEL -->
</div>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Fullname</th>
                            <th>Type</th>
                            <th>Account Status</th>
                            <th>Position Vacancy</th>
                            <th>Type Vacancy</th>
                            <th>Engine Vacancy</th>
                            <th>Comapany Name</th>
                            <th>Comapany Type</th>
                            <th>Comapany Status</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <form id="submit_form_job" role="form" action="{{route('crew.recommendation.create')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}  
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New Recommendation Crew</h4>
            </div>
            <div class="modal-body">
               
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Company Name</label>
                        <select id="client_id" name="client_id" class="form-control btn btn-default" data-width="100%" data-filter="true" data-action-onchange="true">
                            <option value="0">-</option>
                            @foreach($client as $key)
                                <option value="{{$key->users_id}}">{{$key->companyname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Position Vacancy</label>
                        <select name="position_id" id="position_id" class="form-control select2">    
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input has-success">
                                <label class="control-label">Type Vessel</label>
                                <input id="jl_ty_id" name="jl_ty" type="text" class="form-control" readonly="readonly" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group  form-md-line-input has-success">
                                <label class="control-label">Engine</label>
                                <input id="jl_en_id" name="jl_en" type="text" class="form-control" readonly="readonly"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Requirement</label>
                                            <!-- <input name="jl_re" type="text" class="form-control" />  -->
                        <textarea id="jl_re_id" class="wysihtml5 form-control" rows="6" name="jl_re"></textarea>
                    </div>

                    <div class="form-group form-md-line-input has-success">
                        <label class="control-label">Duration Recruit</label>
                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                        <input id="jl_open_id" name="jl_open" type="text" class="form-control" readonly="readonly">
                        <span class="input-group-addon"> to </span>
                        <input id="jl_close_id" name="jl_close" type="text" class="form-control" readonly="readonly"></div>
                    </div>                    
                </div>
                <hr>
                
                <div class="form-group form-md-line-input has-success">
                    <label class="control-label">Crew Recommendation</label>
                    <select id="crewrecommendation" class="crew form-control" data-width="100%" name="crewrecommendation[]" multiple>
                        
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </form>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

    $('.crew').select2();
   
///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.crew.recommendation.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [10] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                    return '<a href="javascript:;" data-id="'+data[0]+'" class="delete_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-times"></i></a>'; 
                }, targets: [10]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'fullname', name: 'fullname' },
            { data: 'type_crew', name: 'type_crew' },    
            { data: 'status_user', name: 'status_user' }, 
            { data: 'position', name: 'position' },
            { data: 'types', name: 'types' },           
            { data: 'engine', name: 'engine' },           
            { data: 'companyname', name: 'companyname' },           
            { data: 'type_agency', name: 'type_agency' },           
            { data: 'status_company', name: 'status_company' },           
            { data: 'id', name: 'id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });    

    $("#sample_3").on('click','.delete_btn',function(e){
        var id = e.currentTarget.dataset.id;
        swal({
          title: "Whether to delete This Crew Recommendation data ?",
          text: "Crew Recommendation information will disappear from the web, are you sure you will do this process ... ",
          type: "info",
          allowOutsideClick: false,
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          cancelButtonClass: "btn-warning",
          closeOnConfirm: false,
          closeOnCancel: false,
          confirmButtonText: "Yes, I'm agree",
          cancelButtonText: "No, Cancle this Proses",
        },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "{{ url('/backend/crew-recommendation-destroy/') }}/"+id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data) {
                            if(data){
                                swal("Thank you", "You have deleted data", "success");
                            }else{
                                swal("Sorry!!!", "User Do not Have Delete access ", "error");
                            }
                            location.reload(true);
                        }
                    });
                    
                } else {
                    swal("Cancel", "You cancel the process of removing Crew Recommendations ... ", "error");
                }
        });
    });

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);

    $("#client_id").on("change",function(e){
        setvacancyclient();
    });
    
});
    var branch = [];
    var crew = [];
    function setvacancyclient(){
        var agency_id =  $("#client_id").val();
        branch = getvacancy(agency_id);
        var html ='<option value="0"> - </option>';
        for (var i = branch.length - 1; i >= 0; i--) {
            html += "<option value='"+branch[i].id+"'>";
            html += branch[i].position;
            html += "</option>";
        }
        $("#position_id").html(html);
    }

    function setcrewaavilable(){
        var agency_job =  $("#position_id").val();
        crew = getcrew(agency_job);
        var html ='';
        for (var i = crew.length - 1; i >= 0; i--) {
            if (crew[i].status_account == 'premium'){
                html += "<option value='"+crew[i].users_id+"'>";
                html += crew[i].fullname+" - ["+crew[i].type_crew+"]&reg;";
                html += "</option>";
            }else{
                html += "<option value='"+crew[i].users_id+"'>";
                html += crew[i].fullname+" - ["+crew[i].type_crew+"]";
                html += "</option>";
            }
        }
        //var html ='<option value="0"> ada ininh </option>';
        $("#crewrecommendation").html(html);
    }

    $("#position_id").on("change",function(e){
        
        const index = branch.findIndex(item => item.id == this.value);

        //var index = branch.map(function (br) { return br.value; }).indexOf(vl);

        $("#jl_ty_id").val(branch[index].types);
        $("#jl_en_id").val(branch[index].engine);
        $("#jl_re_id").val(branch[index].requirement);
        $("#jl_open_id").val(branch[index].date_open);
        $("#jl_close_id").val(branch[index].date_close);
        $('.wysihtml5').summernote('disable');

        setcrewaavilable();
    });
    
    function getvacancy(agency_id){
        var rst=''; 
        var agency_id =  agency_id;
        $.ajax({
            url: "{{ route('crew.recommendation.get.position') }}",
            type : "POST",  
            data : {agency_id:agency_id,"_token": "{{ csrf_token() }}"},
            async:false,    
            success : function(position){
                //console.log(position);
                rst = position            
            }
        });
        return rst;
    }

    function getcrew(agency_job){
        var rst=''; 
        var agency_job =  agency_job;
        $.ajax({
            url: "{{ route('crew.recommendation.get.crew') }}",
            type : "POST",  
            data : {agency_job:agency_job,"_token": "{{ csrf_token() }}"},
            async:false,    
            success : function(crew){
                
                rst = crew            
            }
        });
        return rst;
    }
</script>
@endsection    