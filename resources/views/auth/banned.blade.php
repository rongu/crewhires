@extends('layouts.frontend')

@section('title','Frequently Asked Questions')
@section('desc','Halaman ini menyatakan Frequently Asked Questions')

@php use Illuminate\Support\Carbon; @endphp

@section('assets-top')


    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }

        .alert-success {
            background-color: #90c9e7;
            border-color: #90c9e7;
            color: #ffffff;
        }

        .invoice-content-2 .invoice-title {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 1px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .company-address {
            text-align: left;
            font-size: 14px;
            color: #000000;
        }

        .invoice-content-2 .invoice-head .invoice-logo>img {
            float: right;
            margin-right: 45px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 0px solid #e7ecf1;
        }

        .pricing-content-1 .price-table-content {
            background-color: #f7f9fb;
            color: #5c6d7e;
            font-weight: 600;
            font-size: 14px;
        }
    </style>

@endsection
@section('content')
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Banned
                
            </h1>
        </div>

    </div>
</div>

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">

    <div class="container">

        <div class="page-content-inner">

            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="portlet light">
                        
                        <div class="portlet-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                            <p>
                                {{ __('Your Account Banned.') }}
                               
                            </p>
                        </div>
                    </div>

                    
                </div>
            </div>
            
        </div>
        
    </div>
</div>
@endsection

@section('assets-bottom')


<script>
$(document).ready(function() {

    

});
</script>
@endsection    