@extends('layouts.frontend')

@section('title','Home')
@section('desc','Halaman ini menyatakan Home')
@php 
                                                
$locale = session()->get('locale'); 

@endphp

@section('assets-top')
    <link href="{{ asset('assets/pages/css/about.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .page-header .page-header-menu {
            background: #4596bb;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li>a>i {
            color: #ffffff;
        }

        .page-prefooter {
            background: #4596bb;
            color: #ffffff;
        }
        .page-prefooter a, .page-prefooter h2 {
            color: #f5f8fd;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #59595b;
        }

        .page-footer {
            background: #59595b;
            color: #dde4ec;
        }

        .page-prefooter .social-icons li {
            opacity: 1;
            /* color: #ffffff; */
            filter: alpha(opacity=1);
        }
        .page-prefooter .subscribe-form .form-control {
            background: #59595b;
            border-color: #59595b;
            color: #ffffff;
        }

        .page-header .page-header-menu .search-form, .page-header .page-header-menu .search-form .input-group {
            background: #59595b;
        }

        .page-header .page-header-menu .search-form .input-group .form-control {
            color: #ffffff;
            background: #59595b;
        }
        .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-inbox>.dropdown-toggle>.corner {
            border-color: transparent transparent transparent #44b6ae;
        }

        .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.active>a:hover, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a, .page-header .page-header-menu .hor-menu.hor-menu-light .navbar-nav>li.current>a:hover {
            color: #ffffff;
            background: #90c9e7;
        }
    </style>

@endsection
@section('content')

<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Welcome
                <small>home us page</small>
            </h1>
        </div>
        
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <div class="page-content-inner">
            
<!--             <div class="row margin-bottom-40 about-header">
                <div class="col-md-12">
                    <h1>About Us</h1>
                    <h2>Life is either a great adventure or nothing</h2>
                    <button type="button" class="btn btn-danger">JOIN US TODAY</button>
                </div>
            </div> -->

            <div class="slider">
                @foreach($imgslide as $al)
                    <img src="{{url('public/images/slide')}}/{{$al->images}}">
                @endforeach
              
              
            </div>

            <div class="row margin-bottom-20">
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light" style="min-height:400px;">
                        <div class="card-icon">
                            <i class="icon-user-follow font-red-sunglo theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> {{ __('home_conten.h1')}} </span>
                        </div>
                        <div class="card-desc">
                            <span> {{ __('home_conten.d1')}} </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light" style="min-height:400px;">
                        <div class="card-icon">
                            <i class="icon-trophy font-green-haze theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> {{ __('home_conten.h2')}} </span>
                        </div>
                        <div class="card-desc">
                            <span> {{ __('home_conten.d2')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light" style="min-height:400px;">
                        <div class="card-icon">
                            <i class="icon-basket font-purple-wisteria theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> {{ __('home_conten.h3')}} </span>
                        </div>
                        <div class="card-desc">
                            <span> {{ __('home_conten.d3')}} </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light" style="min-height:400px;">
                        <div class="card-icon">
                            <i class="icon-layers font-blue theme-font"></i>
                        </div>
                        <div class="card-title">
                            <span> {{ __('home_conten.h4')}} </span>
                        </div>
                        <div class="card-desc">
                            <span> {{ __('home_conten.d4')}} </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CARDS -->
            <!-- BEGIN TEXT & VIDEO -->
            <div class="row margin-bottom-40">
                <div class="col-lg-6">
                    <div class="portlet light about-text">
                        <h4>
                            <i class="fa fa-check icon-info"></i> About</h4>
                        <p class="margin-top-20" style="text-align: justify;">{{ __('about.p1')}}</p>
                        <p class="margin-top-20" style="text-align: justify;">{{ __('about.p2')}}</p>
<!--                         <div class="row">
                            <div class="col-xs-6">
                                <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                    <li>
                                        <i class="fa fa-check"></i> Nam liber tempor cum soluta </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Lorem ipsum dolor sit amet </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                    <li>
                                        <i class="fa fa-check"></i> Nam liber tempor cum soluta </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Lorem ipsum dolor sit amet </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                    <li>
                                        <i class="fa fa-check"></i> Mirum est notare quam </li>
                                </ul>
                            </div>
                        </div>
                        <div class="about-quote">
                            <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</h3>
                            <p class="about-author">Tom Hardy, 2015</p>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <iframe src="http://player.vimeo.com/video/22439234" style="width:100%; height:500px;border:0" allowfullscreen> </iframe>
                </div>
            </div>
            <!-- END TEXT & VIDEO -->
            <!-- BEGIN MEMBERS SUCCESS STORIES -->
            <div class="row margin-bottom-40 stories-header" data-auto-height="true">
                <div class="col-md-12">
                    @if($locale=='id')
                        <h1>Pelayanan Unggulan Kami</h1>
                        <h2>Berlayar tanpa risau kelengkapan dokumen</h2> 
                    @else
                        <h1>Our Excellent Service </h1>
                        <h2>Sail without worrying about shipping documents </h2>
                    @endif
                </div>
            </div>
            <div class="row margin-bottom-20 stories-cont">
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ url('public/images/service/visa.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Visa </span>
                        </div>
                        <!-- <div class="desc">
                            <span> Lorem Ipsum is simply dummy text </span>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ url('public/images/service/passport.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Passport </span>
                        </div>
                        <!-- <div class="desc">
                            <span> Lorem Ipsum is simply dummy text </span>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ url('public/images/service/seamanbook.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Seaman Book </span>
                        </div>
                        <!-- <div class="desc">
                            <span> Lorem Ipsum is simply dummy text. </span>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="photo">
                            <img src="{{ url('public/images/service/certificaterevalidation.jpg') }}" alt="" class="img-responsive" /> </div>
                        <div class="title">
                            <span> Certificate Revalidation </span>
                        </div>
                        <!-- <div class="desc">
                            <span> Lorem Ipsum is simply dummy text
                                </span> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-40 stories-footer">
                <div class="col-md-12">
                    @if($locale=='id')
                        <a href="{{route('service')}}"<button type="button" class="btn btn-danger">LIHAT SEMUA PELAYANAN</button></a>
                    @else
                        <a href="{{route('service')}}"<button type="button" class="btn btn-danger">SEE MORE SERVICE</button></a>
                    @endif
                </div>
            </div>
            <!-- END MEMBERS SUCCESS STORIES -->
            <!-- BEGIN LINKS BLOCK -->
            <div class="row about-links-cont" data-auto-height="true">
                <div class="col-md-6 about-links">
                    <div class="row">
                        <div class="col-sm-12 about-links-item">
                            <table class="table table-striped table-hover" id="sample_3" width="100%">
                                <thead>
                                    <tr>
                                        <th>Crew Vacancy</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                               </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="about-image" style="background: url({{ asset('images/dashoboard_vanacy.jpg') }}) center no-repeat;"></div>
                </div>
            </div>
            <!-- END LINKS BLOCK -->
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('categories.crew.vacancy.list')}}",                  
    },

    scrollY: 400,
    deferRender: true,
    scroller: {
        loadingIndicator: true
    },
    scrollX:        true,
    // scrollCollapse: true,        
    responsive:     true,  
    stateSave:      false,
    searching: false,
    columnDefs: [
            { orderable: false, targets: [0] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                // return '<a href="javascript:;" data-id="'+data[0]+'" data-position="'+data[1]+'" data-date_open="'+data[2]+'" data-types="'+data[3]+'" data-engine="'+data[4]+'" data-requirement="'+data[5]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; }, targets: [0]
                return '<a href="{{route("categories")}}" data-id="'+data[0]+'" data-position="'+data[1]+'" data-date_open="'+data[2]+'" data-types="'+data[3]+'" data-engine="'+data[4]+'" data-requirement="'+data[5]+'" data-date_close="'+data[6]+'" data-photo="'+data[7]+'" data-companyname="'+data[8]+'" data-address="'+data[9]+'" class="edit_btn"><p><h3>'+data[1]+'</h3><br><b>Spesialisasi Pekerjaan</b><br>'+data[3]+' - '+data[4]+'<br><br>Buka Pendaftaran<br>'+data[2]+' - '+data[6]+'</p></a>'; }, targets: [0]

            },
    ],
    columns: [
            { data: 'id', name: 'id' },
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });

    $('.slider').bxSlider({
        preloadImages: 'all',
        auto: true,
    });
    
});
</script>
@endsection    