<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterCoc extends Model
{
    use HasFactory;
    protected $table = 'coc';
}
