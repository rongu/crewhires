@extends('layouts.backend')




@section('assets-top')

<style type="text/css">
    .dashboard-stat2 {
    /* border: 1px solid #cdd6e1; */
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background: #fff;
        padding: 15px 15px 30px;
        box-shadow: 4px 4px 8px 4px #cbd4e0;
        height: 105px;
    }

    .portlet.light {
        box-shadow: 4px 4px 8px 4px #cbd4e0
    }
</style>

@endsection
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.home')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Crew Apply Vacancy</a>
            
        </li>
        
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Crew Apply Vacancy
</h1>

<div class="portlet light ">
    <!-- PROJECT HEAD -->
    <!-- end PROJECT HEAD -->
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                @if(session('update'))
                <div class="alert alert-success display-show">
                <button class="close" data-close="alert"></button> {{ session('update') }} </div>
                @endif

                <table class="table table-striped  dt-responsive table table-hover table-light order-column" width="100%" id="sample_3">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Fullname</th>
                            <th>Type</th>
                            <th>Position Vacancy</th>
                            <th>Type Vacancy</th>
                            <th>Engine Vacancy</th>
                            <th>Comapany Name</th>
                            <th>Comapany Type</th>
                            <th>Comapany Status</th>
                            <th>Apply Status</th>
                            <th>&nbsp;</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        
                   </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>


<div id="modalApproval" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form role="form" action="{{route('backend.crew.apply.approval')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="id" type="hidden" class="form-control" id="id_id" readonly="readonly">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Approval Apply Vacancy</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="fullname" type="text" class="form-control" id="fullname_id" readonly="readonly">
                        <label for="fullname_id">Full Name</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="type_crew" type="text" class="form-control" id="type_crew_id" readonly="readonly">
                        <label for="type_crew_id">Type Crew</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="position" type="text" class="form-control" id="position_id" readonly="readonly">
                        <label for="position_id">Position Vacancy</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="types" type="text" class="form-control" id="types_id" readonly="readonly">
                        <label for="types_id">Type Vacancy</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="engine" type="text" class="form-control" id="engine_id" readonly="readonly">
                        <label for="engine_id">Engine Vacancy</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="companyname" type="text" class="form-control" id="companyname_id" readonly="readonly">
                        <label for="companyname_id">Company Name</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="type_agency" type="text" class="form-control" id="type_agency_id" readonly="readonly">
                        <label for="type_agency_id">Company Type</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <input name="status_account" type="text" class="form-control" id="status_account_id" readonly="readonly">
                        <label for="status_accounts_id">Company Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group form-md-line-input has-success">
                        <select id="status_id" name="status" class="form-control select2">
                            <option></option>
                           
                            <option value="APPROVE">Approve</option>
                            <option value="REJECT">Reject</option>

                            
                        </select>
                        <label for="status_id">Status</label>
                        <span class="help-block">Some help goes here...</span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('assets-bottom')
<script>
$(document).ready(function() {

///////////////////////////////////////////////////////////////////////////////////////////////////////
    var table = $('#sample_3');

    var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    serverSide: true,
    ajax: {
        url: "{{route('backend.crew.apply.list')}}",                  
    },

    scrollY:        400,
    deferRender:    true,
    scroller:       true,
    deferRender:    true,
    scrollX:        true,
    scrollCollapse: true,        
    responsive:     true,  
    stateSave:      true,
    columnDefs: [
            { orderable: false, targets: [10] },
            { searchable : false},                
            { render: function(data, type, row, meta) {
                if (data[9] == 'ADD'){
                    return '<a href="javascript:;" data-id="'+data[0]+'" data-fullname="'+data[1]+'" data-type_crew="'+data[2]+'" data-position="'+data[3]+'" data-types="'+data[4]+'" data-engine="'+data[5]+'"  data-companyname="'+data[6]+'"  data-type_agency="'+data[7]+'"  data-status_account="'+data[8]+'" class="edit_btn btn btn-circle btn-icon-only grey-mint"><i class="fa fa-edit"></i></a>'; 
                }else{
                    return '';
                }
                }, targets: [10]
            },
            { render: function(data, type, row, meta) {
                    if (data == 'premium'){
                        return '<a href="javascript:;" class="btn yellow disabled uppercase">'+data+'</a>';
                    }else{
                        return '<a href="javascript:;" class="btn white disabled uppercase">'+data+'</a>';
                    }
                }, targets: [8]
            },
    ],
    columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'fullname', name: 'fullname' },
            { data: 'type_crew', name: 'type_crew' },
            { data: 'position', name: 'position' },
            { data: 'types', name: 'types' },           
            { data: 'engine', name: 'engine' },           
            { data: 'companyname', name: 'companyname' },           
            { data: 'type_agency', name: 'type_agency' },           
            { data: 'status_account', name: 'status_account' },           
            { data: 'apply_status', name: 'apply_status' },           
            { data: 'id', name: 'id' } 
        ],
    // "order": [
    //     [0, 'asc']
    // ],

    lengthMenu: [
        [10, 15, 20, -1],
        [10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    pageLength: 10,

    dom: "<'row' ><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    });    

    $("#sample_3").on('click','.edit_btn',function(e){
        // id, name, email, phones, position, nik, address, about
        var id = e.currentTarget.dataset.id;
        var fullname = e.currentTarget.dataset.fullname;
        var type_crew = e.currentTarget.dataset.type_crew;
        var position = e.currentTarget.dataset.position;
        var types = e.currentTarget.dataset.types;
        var engine = e.currentTarget.dataset.engine;
        var companyname = e.currentTarget.dataset.companyname;
        var type_agency = e.currentTarget.dataset.type_agency;
        var status_account = e.currentTarget.dataset.status_account;

        $("#id_id").val(id);
        $("#fullname_id").val(fullname);
        $("#type_crew_id").val(type_crew);
        $("#position_id").val(position);
        $("#types_id").val(types);
        $("#engine_id").val(engine);
        $("#companyname_id").val(companyname);
        $("#type_agency_id").val(type_agency);
        $("#status_account_id").val(status_account);
        $("#modalApproval").modal('show');
        
    });

    setTimeout(function(){ 

            ezoom.onInit($('.zoomimg'), {
                hideControlBtn: true,
                onClose: function (result) {
                    console.log(result);
                },
                onRotate: function (result) {
                    console.log(result);
                },
            });

    }, 1000);
    
});
</script>
@endsection    